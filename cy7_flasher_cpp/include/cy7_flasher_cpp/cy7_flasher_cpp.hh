/**
 * @file cy7_flasher_cpp.hh
 *
 * @brief Interfaces for C++ wrapper lib for cy7_flasher lib
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_FLASHER_CPP_H
#define _CY7_FLASHER_CPP_H

#include <memory>
#include <stdexcept>
#include <vector>

namespace Cy7FlasherLib
{

class Cy7FlasherRuntimeException : public std::runtime_error
{
public:
    explicit Cy7FlasherRuntimeException(const std::string& what_arg)
        : std::runtime_error(what_arg){}

    explicit Cy7FlasherRuntimeException(const char *what_arg)
        : std::runtime_error(what_arg){}
};

using Cy7ConfigBlob = std::vector<uint8_t>;

struct ICy7DeviceSpec
{
public:
    virtual ~ICy7DeviceSpec() = default;

    virtual const void *rawSpec() const = 0;
    virtual uint16_t vendorId() const = 0;
    virtual uint16_t productId() const = 0;
    virtual uint16_t busNum() const = 0;
    virtual uint16_t portNum() const = 0;
    virtual uint16_t deviceAddress() const = 0;
    virtual bool isRecover() const = 0;

    using SPtr = std::shared_ptr<ICy7DeviceSpec>;
};

class ICy7Device
{
public:
    virtual ~ICy7Device() = default;
    virtual Cy7ConfigBlob ReadConfig() const = 0;
    virtual void WriteConfig(const Cy7ConfigBlob& config, bool force) = 0;
    using UPtr = std::unique_ptr<ICy7Device>;
};

class ICy7Flasher
{
public:
    virtual ~ICy7Flasher() = default;

    virtual std::vector<ICy7DeviceSpec::SPtr> ScanDevices() const = 0;

    virtual ICy7Device::UPtr OpenDevice(const ICy7DeviceSpec& spec) const = 0;

    using UPtr = std::unique_ptr<ICy7Flasher>;
};

class Cy7FlasherFactory
{
public:
    static ICy7Flasher::UPtr create();
};

}

#endif /* _CY7_FLASHER_CPP_H */
