/**
 * @file cy7_flasher.h
 *
 * @brief usb-debug-whistle userspace library for
 *        reading/writing configuration to/from device.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_FLASHER_LIB_H
#define _CY7_FLASHER_LIB_H
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define CY7_CONFIG_BLOB_LEN 512

typedef struct cy7_device cy7_device_t;

int cy7_lib_init(void);
void cy7_lib_deinit(void);

size_t cy7_get_config_size(void);

/*
 * CY7 USB device short form identifier.
 */
typedef struct cy7_usb_dev_spec {
	uint16_t vendor_id;
	uint16_t product_id;
	uint32_t bus_num;
	uint32_t port_num;
	uint32_t device_addr;
	bool recover;
} cy7_usb_dev_spec_t;

/*
 * cy7_scan_devices - scan the system for all USB devices and
 * return a list of devices that match by a set of rules to usb-debug-whistle.
 * @devices - caller provided array, the function will fill out this
 *            array with found devices.
 * @max_devices - number of possible elements in a caller provided array.
 * Returns -1 on error and number of found elements on success.
 */
int cy7_scan_devices(cy7_usb_dev_spec_t *devices, int max_devices);

/*
 * cy7_device_open - open device, based on a provided device spec argument.
 * @spec - device spec, that uniquely describes the device that's required to be
 *         opened. A valid device spec can be retreived by first calling cy7_scan_devices.
 * Returns NULL on error and a pointer to cy7_device_t on success.
 * The returned device should later be released by a call to cy7_device_close_and_free.
 */
cy7_device_t *cy7_device_open(const cy7_usb_dev_spec_t *spec);

/*
 * cy7_device_close_and_free - closes USB device resource and frees the structure from
 * memory.
 * @d - device instance, returned from cy7_device_open.
 */
void cy7_device_close_and_free(cy7_device_t *d);

/*
 * Read configuration blob from device
 * @dev - device to be read from.
 * @dst - destination buffer to read to.
 * @dst_len - size of the destination buffer, this is checked to be big enough to hold the config
 *            blob.
 * Returns 0 on success and other values on error.
 */
int cy7_device_read_config(cy7_device_t *dev, void *dst, size_t dst_len);

/*
 * Write configuration blob to device
 * @dev - device to write config to.
 * @src - source buffer holding a config.
 * @src_len - size of the source buffer, this should exactly match the size of the configuration blob.
 * @force - skip validation.
 * Returns 0 on success and other values on error.
 */
int cy7_device_write_config(cy7_device_t *dev, const void *src, size_t src_len, bool force);

#endif /* _CY7_FLASHER_LIB_H */
