/**
 * @file cy7_flasher.h
 *
 * @brief usb-debug-whistle userspace library for
 *        reading/writing configuration to/from device.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#include <cy7_flasher/cy7_flasher.h>
#include <errno.h>
#include <libusb.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define CY7_VENDOR_ID 0x04b4
#define CY7_PRODUCT_ID_RECOVER 0x00fa
#define CY7_INTERFACE_RECOVER_SUBCLASS 0xff

#define USB_CDC_SUBCLASS_ACM 2

/* CY7C65215 management endpoint commands API */
#define CY7_GET_VERSION_CMD       0xb0
#define CY7_READ_CONFIG_CMD       0xb5
#define CY7_WRITE_CONFIG_CMD      0xb6
#define CY7_GET_SIGNATURE_CMD     0xbd
#define CY7_UART_GET_CONFIG_CMD   0xc0
#define CY7_UART_SET_CONFIG_CMD   0xc1
#define CY7_SPI_GET_CONFIG_CMD    0xc2
#define CY7_SPI_SET_CONFIG_CMD    0xc3
#define CY7_I2C_GET_CONFIG_CMD    0xc4
#define CY7_I2C_SET_CONFIG_CMD    0xc5
#define CY7_I2C_WRITE_CMD         0xc6
#define CY7_I2C_READ_CMD          0xc7
#define CY7_I2C_GET_STATUS_CMD    0xc8
#define CY7_I2C_RESET_CMD         0xc9

#define CY7_SPI_READ_WRITE_CMD    0xca
#define CY7_SPI_RESET_CMD         0xcb
#define CY7_SPI_GET_STATUS_CMD    0xcc

#define CY7_JTAG_ENABLE_CMD       0xd0
#define CY7_JTAG_DISABLE_CMD      0xd1
#define CY7_JTAG_READ_CMD         0xd2
#define CY7_JTAG_WRITE_CMD        0xd3

#define CY7_GPIO_GET_CONFIG_CMD   0xd8
#define CY7_GPIO_SET_CONFIG_CMD   0xd9
#define CY7_GPIO_GET_VALUE_CMD    0xda
#define CY7_GPIO_SET_VALUE_CMD    0xdb

#define CY7_PROG_USER_FLASH_CMD   0xe0
#define CY7_READ_USER_FLASH_CMD   0xe1

#define CY7_MAINT_MODE_CMD        0xe2
#define CY7_DEVICE_RESET_CMD      0xe3

#define CY7_MAINT_MODE_VALUE 0xa6bc

#define CY7_ENTER_MAINT_MODE_INDEX 0xb1b0
#define CY7_EXIT_MAINT_MODE_INDEX  0xb9b0

/* MAGIC (or signature) is "CYUS" */
#define CY7_MAGIC_DWORD 0x53555943

/*
 * Byte offset in configuration blob where vendor id, product id dword is
 * located.
 */
#define CY7_CONFIG_VIDPID_OFFSET 0x94


/*
 * Return types for function cy7_what_is_device
 * CY7_DEVICE / NON_CY7_DEVICE - are self-explanatory.
 * CY7_RECOVER_DEVICE - has vid pid of 0x04b4:0x00fa and is created
 * when blob checksum does not match. It is only possible to use it
 * to write correct configuration blob.
 */
#define NON_CY7_DEVICE 0
#define CY7_DEVICE 1
#define CY7_RECOVER_DEVICE 2

/*
 * Subclass values for USB interface with class vendor(0xff).
 */
#define CY7_SUBCLASS_UART 1
#define CY7_SUBCLASS_SPI  2
#define CY7_SUBCLASS_I2C  3
#define CY7_SUBCLASS_JTAG 4
#define CY7_SUBCLASS_MFG  5

/*
 * vendor id value that is allowed to be used in configuration blob. Other
 * vendor id values can be set only with a force flag. NOTE, that by forcing
 * the blob to be written with different vendor id, scan device function
 * will not be able to find this device. usb-debug-whistle kernel module will
 * also fail to detect this device.
 */
struct field_vid_pid {
	union {
		struct {
			uint16_t vendor_id;
			uint16_t product_id;
		};
		uint32_t value;
	} u;
};

typedef struct cy7_device {
	libusb_device_handle *h;
	libusb_device *dev;
	struct libusb_device_descriptor d;
	struct libusb_config_descriptor *c;
	const struct libusb_interface *i;
	unsigned char out_ep, in_ep;
} cy7_device_t;

static libusb_context *ctx = NULL;

static int cy7_device_connect(libusb_device_handle *h)
{
	int ret;

	if (!h) {
		return -1;
	}

	ret = libusb_control_transfer(h,
		LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN,
		CY7_MAINT_MODE_CMD,
		CY7_MAINT_MODE_VALUE,
		CY7_ENTER_MAINT_MODE_INDEX,
		NULL,
		0,
		1000);

	return (ret == LIBUSB_SUCCESS) ? 0 : -1;
}

static int cy7_device_disconnect(libusb_device_handle *h)
{
	int ret;

	if (!h) {
		return -EINVAL;
	}

	ret = libusb_control_transfer(h,
		LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN,
		CY7_MAINT_MODE_CMD,
		CY7_MAINT_MODE_VALUE,
		CY7_EXIT_MAINT_MODE_INDEX,
		NULL,
		0,
		1000);

	return (ret == LIBUSB_SUCCESS) ? 0 : -1;
}

int cy7_lib_init(void)
{
	/* Return if already initialized */
	if (ctx) {
		return 0;
	}

	if (libusb_init(&ctx)) {
		return -1;
	}

	return 0;
}

void cy7_lib_deinit()
{
	if (ctx) {
		libusb_exit(ctx);
		ctx = NULL;
	}
}

size_t cy7_get_config_size(void)
{
	return CY7_CONFIG_BLOB_LEN;
}

static inline bool cy7_is_recover_interface(const struct libusb_interface *i)
{
	const struct libusb_interface_descriptor *alt;

	alt = i->altsetting;

	return alt->bInterfaceClass == LIBUSB_CLASS_VENDOR_SPEC &&
		alt->bInterfaceSubClass == CY7_INTERFACE_RECOVER_SUBCLASS;
}

static inline bool cy7_is_cdc_acm_iface(const struct libusb_interface *i)
{
	const struct libusb_interface_descriptor *alt = i->altsetting;
	return alt->bInterfaceClass == LIBUSB_CLASS_COMM &&
		alt->bInterfaceSubClass == USB_CDC_SUBCLASS_ACM;
}

static inline bool cy7_is_cdc_data_iface(const struct libusb_interface *i)
{
	const struct libusb_interface_descriptor *alt = i->altsetting;
	return alt->bInterfaceClass == LIBUSB_CLASS_DATA &&
		alt->bInterfaceSubClass == 0;
}

static inline bool cy7_is_vendor_iface(const struct libusb_interface *i)
{
	const struct libusb_interface_descriptor *alt;

	alt = i->altsetting;

	if (alt->bInterfaceClass == LIBUSB_CLASS_VENDOR_SPEC) {
		return alt->bInterfaceSubClass >= CY7_SUBCLASS_UART
			&& alt->bInterfaceSubClass <= CY7_SUBCLASS_JTAG;
	}
	return false;
}

static inline bool cy7_is_management_interface(const struct libusb_interface *i)
{
	const struct libusb_interface_descriptor *alt;

	alt = i->altsetting;

	return alt->bInterfaceClass == LIBUSB_CLASS_VENDOR_SPEC &&
		alt->bInterfaceSubClass == CY7_SUBCLASS_MFG;
}

static bool cy7_has_valid_interface_set(struct libusb_config_descriptor *dev_conf)
{
	/*
	 * CY7 device maintains special ordering of it's USB interfaces.
	 * - SCB0 interfaces if present
	 * - SCB1 interfaces if present
	 * - Management interface
	 * In that order.
	 *
	 * If any of 2 SCBs is configured as a CDC UART it would be presented with 2 interfaces
	 * - CDC ACM interface (class 0x2, subclass 0x2)
	 * - CDC DATA interface (class 0xa, subclass 0x00)
	 * In that order.
	 *
	 * To validate set of interfaces, we walk all interfaces to first get SCBs, then
	 * management.
	 */
	const struct libusb_interface *i, *i_end;

	if (!dev_conf) {
		return false;
	}

	/* No interfaces */
	if (!dev_conf->bNumInterfaces) {
		return false;
	}

	i = dev_conf->interface;
	i_end = i + dev_conf->bNumInterfaces;

	/* Enumerate SCB interfaces leaving out one management iface for last check */
	for(; i < (i_end - 1); ++i) {
		if (cy7_is_vendor_iface(i)) {
			continue;
		}
		/*
		 * CDC ACM interface has class 2, subclass 2
		 * It is followed by CDC DATA interface which is
		 * class 0xa subclass 0.
		 * This is a consistency check.
		 */
		if (!cy7_is_cdc_acm_iface(i) && !cy7_is_cdc_data_iface(i + 1)) {
			return false;
		}

		++i;
	}

	/* Last interface after the loop should be control interface */
	return cy7_is_management_interface(i);
}

static int cy7_what_is_device(libusb_device *d)
{
	int ret;
	struct libusb_device_descriptor dev_desc;
	struct libusb_config_descriptor *dev_conf;
	int num_interfaces;

	if (!d)
		return -1;

	ret = libusb_get_device_descriptor(d, &dev_desc);
	if (ret != LIBUSB_SUCCESS) {
		return -1;
	}

	if (dev_desc.idVendor != CY7_VENDOR_ID) {
		return NON_CY7_DEVICE;
	}

	ret = libusb_get_config_descriptor(d, 0, &dev_conf);
	if (ret != LIBUSB_SUCCESS) {
		return -1;
	}

	/*
	 * Corrupted config has been written to device. At startup
	 * it is shown as:
	 *   04b4:00fa Cypress Semiconductor Corp. USBSC MFG
	 *
	 * It has only one interface class 0xff, subclass 0xff
	 */
	if (dev_desc.idProduct == CY7_PRODUCT_ID_RECOVER) {
		if (cy7_is_recover_interface(&dev_conf->interface[0])) {
			ret = CY7_RECOVER_DEVICE;
			goto out;
		}
		ret = NON_CY7_DEVICE;
		goto out;
	}

	if (!cy7_has_valid_interface_set(dev_conf)) {
		ret = NON_CY7_DEVICE;
	} else {
		ret = CY7_DEVICE;
	}

out:
	libusb_free_config_descriptor(dev_conf);
	return ret;
}

static void cy7_dev_spec_init(cy7_usb_dev_spec_t *spec, libusb_device *d,
			      struct libusb_device_descriptor *dev_desc, bool is_recover)
{
	spec->vendor_id = dev_desc->idVendor;
	spec->product_id = dev_desc->idProduct;
	spec->bus_num = libusb_get_bus_number(d);
	spec->port_num = libusb_get_port_number(d);
	spec->device_addr = libusb_get_device_address(d);
	spec->recover = is_recover;

}

int cy7_scan_devices(cy7_usb_dev_spec_t *dev_list, int max_devices)
{
	int ret;
	int i, di, cnt;
	libusb_device **list;
	libusb_device *d;
	struct libusb_device_descriptor dev_desc;
	bool is_recover_device;

	if (!dev_list) {
		return -1;
	}

	ret = libusb_get_device_list(ctx, &list);
	if (ret < 0) {
		return -1;
	}

	cnt = ret;

	for (i = 0, di = 0; i < cnt && di < max_devices; ++i) {
		d = list[i];

		ret = cy7_what_is_device(d);
		if (ret < 0) {
			continue;
		}

		if (ret == NON_CY7_DEVICE) {
			continue;
		}

		is_recover_device = (ret == CY7_RECOVER_DEVICE);

		ret = libusb_get_device_descriptor(d, &dev_desc);
		if (ret != LIBUSB_SUCCESS) {
			continue;
		}

		cy7_dev_spec_init(&dev_list[di], d, &dev_desc, is_recover_device);
		di++;
	}

	libusb_free_device_list(list, 1 /* unref each dev */);
	return di;
}

static bool cy7_usb_device_matches_spec(struct libusb_device *d, const cy7_usb_dev_spec_t *spec)
{
	int ret;
	struct libusb_device_descriptor desc;

	if (!d || !spec)
		return false;

	ret = libusb_get_device_descriptor(d, &desc);
	if (ret != LIBUSB_SUCCESS) {
		return false;
	}

	if (spec->vendor_id != desc.idVendor || spec->product_id != desc.idProduct) {
		return false;
	}

	if (spec->bus_num != libusb_get_bus_number(d)) {
		return false;
	}

	if (spec->port_num != libusb_get_port_number(d)) {
		return false;
	}

	if (spec->device_addr != libusb_get_device_address(d)) {
		return false;
	}
	return true;
}

libusb_device_handle *cy7_device_open_by_spec(const cy7_usb_dev_spec_t *spec)
{
	int ret;
	int i;
	int cnt;
	libusb_device **list;
	libusb_device *d;
	libusb_device_handle *h = NULL;

	if (!spec) {
		return NULL;
	}

	ret = libusb_get_device_list(ctx, &list);
	if (ret < 0) {
		return NULL;
	}

	cnt = ret;

	for (i = 0; i < cnt; ++i) {
		d = list[i];
		if (cy7_usb_device_matches_spec(d, spec)) {
			break;
		}
	}

	/* If device matching spec is found, open it */
	if (i < cnt) {
		ret = libusb_open(d, &h);
		if (ret != LIBUSB_SUCCESS) {
			h = NULL;
		}
	}

	libusb_free_device_list(list, 1 /* unref all */);
	return h;
}

cy7_device_t *cy7_device_open(const cy7_usb_dev_spec_t *spec)
{
	int ret;

	libusb_device_handle *h;
	cy7_device_t *d = NULL;

	if (!spec) {
		return NULL;
	}

	h = cy7_device_open_by_spec(spec);
	if (!h) {
		return NULL;
	}

	d = calloc(sizeof(*d), 1);
	if (!d) {
		goto err;
	}

	d->h = h;

	d->dev = libusb_get_device(d->h);
	if (!d->dev) {
		goto err;
	}

	ret = libusb_get_descriptor(d->h, LIBUSB_DT_DEVICE, 0,
		(unsigned char *)&d->d, sizeof(d->d));

	/* Standard USB device descriptor should be returned */
	if (ret != sizeof(d->d)) {
		goto err;
	}

	ret = libusb_get_config_descriptor(d->dev, 0, &d->c);
	if (ret != LIBUSB_SUCCESS) {
		goto err;
	}

	return d;

err:
	free(d);
	libusb_close(h);
	return NULL;
}

void cy7_device_close_and_free(cy7_device_t *d)
{
	if (!d) {
		return;
	}

	libusb_free_config_descriptor(d->c);
	libusb_close(d->h);
	free(d);
}

int cy7_device_read_config(cy7_device_t *d, void *dst, size_t dst_len)
{
	int ret;
	int ret2;

	if (!d || !dst || dst_len != CY7_CONFIG_BLOB_LEN) {
		return -1;
	}

	ret = cy7_device_connect(d->h);
	if (ret) {
		return ret;
	}

	ret = libusb_control_transfer(d->h,
		LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_IN,
		CY7_READ_CONFIG_CMD,
		0,
		0,
		(unsigned char *)dst,
		CY7_CONFIG_BLOB_LEN,
		2000);

	ret = (ret == CY7_CONFIG_BLOB_LEN) ? 0: -1;

	ret2 = cy7_device_disconnect(d->h);
	if (ret2) {
		printf("Disconnect failed with err %d\n", ret2);
	}

	return ret;
}

/*
 * cy7_validate_config_blob - do minimal validation of config blob, that is
 * checksum, magic and vendor_id / product_id pair.
 */
static bool cy7_validate_config_blob(const void *blob, size_t sz)
{
	const uint32_t *ptr = blob;
	uint32_t sum_in_blob;
	uint32_t sum_now = 0;
	uint32_t vidpid_offset = CY7_CONFIG_VIDPID_OFFSET / sizeof(*ptr);
	int i;
	const struct field_vid_pid *vidpid;

	if (!blob || sz != CY7_CONFIG_BLOB_LEN) {
		return false;
	}

	if (ptr[0] != CY7_MAGIC_DWORD) {
		return false;
	}

	vidpid = (const struct field_vid_pid *)&ptr[vidpid_offset];
	if (vidpid->u.vendor_id != CY7_VENDOR_ID) {
		return false;
	}

	sum_in_blob = ptr[2];
	for (i = 3; i < CY7_CONFIG_BLOB_LEN / sizeof(*ptr); ++i) {
		sum_now += ptr[i];
	}

	return sum_now == sum_in_blob;
}

int cy7_device_write_config(cy7_device_t *d, const void *src, size_t src_len, bool force)
{
	int ret;
	int ret2;

	if (!d || !src || src_len != CY7_CONFIG_BLOB_LEN) {
		return -1;
	}

	if (!cy7_validate_config_blob(src, src_len)) {
		if (!force) {
			printf("Validation failed\n");
			return -1;
		} else {
			printf("Validation failed, but forcing\n");
		}
	}

	ret = cy7_device_connect(d->h);
	if (ret) {
		return ret;
	}

	ret = libusb_control_transfer(d->h,
		LIBUSB_REQUEST_TYPE_VENDOR | LIBUSB_RECIPIENT_DEVICE | LIBUSB_ENDPOINT_OUT,
		CY7_WRITE_CONFIG_CMD,
		0,
		0,
		(unsigned char *)src,
		CY7_CONFIG_BLOB_LEN,
		2000);

	ret = (ret == CY7_CONFIG_BLOB_LEN) ? 0: -1;

	ret2 = cy7_device_disconnect(d->h);
	if (ret2) {
		printf("Disconnect failed with err %d\n", ret2);
	}

	return ret;
}
