project(cy7_flasher LANGUAGES C)
cmake_minimum_required(VERSION 3.1)

find_package(libusb CONFIG REQUIRED)

add_library(cy7_flasher STATIC src/cy7_flasher.c)

target_include_directories(cy7_flasher
        PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/include
        PRIVATE
        ${LIBUSB_INCLUDE_DIRS}
        )

target_link_libraries(cy7_flasher
        PRIVATE
        ${LIBUSB_LIBRARIES}
        )

