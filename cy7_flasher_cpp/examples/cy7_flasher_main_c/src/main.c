/**
 * @file cy7_flasher_cpp.hh
 *
 * @brief Example usage of cy7_flasher lib in C
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */

#include <cy7_flasher/cy7_flasher.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/unistd.h>
#include <stdio.h>

char buf[512];

int main()
{
	int ret = 0;
	uint16_t *p;
	cy7_usb_dev_spec_t devs[32];

	cy7_device_t *d;

	cy7_lib_init();

	ret = cy7_scan_devices(devs, sizeof(devs)/sizeof(devs[0]));
	if (!ret) {
		ret = -1;
		goto out;
	}

	d = cy7_device_open(&devs[0]);
	if (!d) {
		ret -1;
		goto out;
	}

	ret = cy7_device_read_config(d, buf, sizeof(buf));
	if (ret) {
		ret = -1;
		goto out;
	}
	p = (uint16_t *)buf;

	ret = cy7_device_write_config(d, buf, sizeof(buf), false);

	cy7_device_close_and_free(d);
out:
	cy7_lib_deinit();
	return ret;
}
