/**
 * @file main.cc
 *
 * @brief example usage of cy7_flasher_cpp lib
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#include <cy7_flasher_cpp/cy7_flasher_cpp.hh>

using namespace Cy7FlasherLib;

int main()
{
    ICy7Flasher::UPtr flasher = Cy7FlasherFactory::create();

    try
    {
        for (const auto& deviceSpec : flasher->ScanDevices())
        {
            ICy7Device::UPtr device = flasher->OpenDevice(*deviceSpec);
            Cy7ConfigBlob config = device->ReadConfig();
            device->WriteConfig(config, false);
        }
    }
    catch (const Cy7FlasherRuntimeException& e)
    {
    }

    return 0;
}
