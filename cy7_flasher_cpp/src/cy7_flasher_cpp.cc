/**
 * @file cy7_flasher_cpp.cc
 *
 * @brief Implementation for C++ wrapper for cy7_flasher lib
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#include <cy7_flasher_cpp/cy7_flasher_cpp.hh>
#include <mutex>

extern "C" {
#include <cy7_flasher/cy7_flasher.h>
}

namespace
{

int numLibRefs = 0;
std::mutex numLibRefsMutex;

class Cy7DeviceSpec final : public Cy7FlasherLib::ICy7DeviceSpec
{
    cy7_usb_dev_spec_t _spec;

public:
    Cy7DeviceSpec(const cy7_usb_dev_spec_t &spec) : _spec { spec } {}

    const void *rawSpec() const override
    {
        return &_spec;
    }

    uint16_t vendorId() const override
    {
        return _spec.vendor_id;
    }

    uint16_t productId() const override
    {
        return _spec.product_id;
    }

    uint16_t busNum() const override
    {
        return _spec.bus_num;
    }

    uint16_t portNum() const override
    {
        return _spec.port_num;
    }

    uint16_t deviceAddress() const override
    {
        return _spec.device_addr;
    }

    bool isRecover() const override
    {
        return _spec.recover;
    }
};


class Cy7Device final : public Cy7FlasherLib::ICy7Device
{
    cy7_device_t *_device;

public:
    Cy7Device(cy7_device_t *device) : _device { device }
    {
        if (!_device)
        {
            throw Cy7FlasherLib::Cy7FlasherRuntimeException("Construct Cy7Device with nullptr");
        }
    }

    ~Cy7Device()
    {
	cy7_device_close_and_free(_device);
    }

    Cy7FlasherLib::Cy7ConfigBlob ReadConfig() const override
    {
        int ret;
        Cy7FlasherLib::Cy7ConfigBlob result(CY7_CONFIG_BLOB_LEN);
        ret = cy7_device_read_config(_device, result.data(), result.size());
        if (ret)
        {
            throw Cy7FlasherLib::Cy7FlasherRuntimeException("Failed to read config");
        }

        return result;
    }

    void WriteConfig(const Cy7FlasherLib::Cy7ConfigBlob& config, bool force) override
    {
        int ret;

        if (config.size() != CY7_CONFIG_BLOB_LEN)
        {
            throw Cy7FlasherLib::Cy7FlasherRuntimeException("Incorrect config size at write");
        }

        ret = cy7_device_write_config(_device, config.data(), config.size(), force);
        if (ret)
        {
            throw Cy7FlasherLib::Cy7FlasherRuntimeException("Failed to write config");
        }
    }
};


class Cy7Flasher final : public Cy7FlasherLib::ICy7Flasher
{
public:
    Cy7Flasher()
    {
        std::lock_guard<std::mutex> guard(numLibRefsMutex);

        if (!numLibRefs && cy7_lib_init() != 0)
        {
            throw Cy7FlasherLib::Cy7FlasherRuntimeException("Failed to initialize cy7_flasher lib");
        }

        numLibRefs++;
    }

    ~Cy7Flasher()
    {
        std::lock_guard<std::mutex> guard(numLibRefsMutex);

        numLibRefs--;
        if (!numLibRefs)
        {
            cy7_lib_deinit();
        }
    }

    std::vector<Cy7FlasherLib::ICy7DeviceSpec::SPtr> ScanDevices() const
    {
        int numReturned;
        std::vector<Cy7FlasherLib::ICy7DeviceSpec::SPtr> result;
        /*
         * Memory footprint is limited by number of possible attached USB devices,
         * so leave the vector size code simple.
         */
        std::vector<cy7_usb_dev_spec_t> specs(16);

        while(true)
        {
            numReturned = cy7_scan_devices(specs.data(), specs.size());
            if (numReturned == -1)
            {
                throw Cy7FlasherLib::Cy7FlasherRuntimeException("Scan devices failed");
            }

            if (numReturned > specs.size())
            {
                throw Cy7FlasherLib::Cy7FlasherRuntimeException("Return value too big");
            }

            if (numReturned < specs.size())
            {
                specs.resize(numReturned);
                break;
            }

            if (specs.size() == 128)
            {
                break;
            }

            specs.resize(specs.size() * 2);
        }

        for (const auto& spec : specs)
        {
            result.push_back(Cy7FlasherLib::ICy7DeviceSpec::SPtr(new Cy7DeviceSpec(spec)));
        }

        return result;
    }

    Cy7FlasherLib::ICy7Device::UPtr OpenDevice(const Cy7FlasherLib::ICy7DeviceSpec& spec) const
    {
        const auto& specImpl = dynamic_cast<const Cy7DeviceSpec&>(spec);
        const auto *cSpec = reinterpret_cast<const cy7_usb_dev_spec_t *>(specImpl.rawSpec());

        cy7_device_t *device = cy7_device_open(cSpec);

        if (!device)
        {
            throw Cy7FlasherLib::Cy7FlasherRuntimeException("Failed to open device");
        }

        return Cy7FlasherLib::ICy7Device::UPtr(new Cy7Device(device));
    }
};

}

namespace Cy7FlasherLib
{
ICy7Flasher::UPtr Cy7FlasherFactory::create()
{
    return std::unique_ptr<ICy7Flasher>(new Cy7Flasher);
}
}
