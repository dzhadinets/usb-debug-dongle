/*++

Module Name:

    public.h

Abstract:

    This module contains the common declarations shared by driver
    and user applications.

Environment:

    user and kernel

--*/

//
// Define an Interface Guid so that app can find the device and talk to it.
//

DEFINE_GUID(GUID_DEVINTERFACE_sqcyusb, 0x9211d8e3, 0x849c, 0x48f8, 0xb9, 0x5e, 0x9a, 0x02, 0x36, 0xd3, 0x7e, 0xe7);
// {9211d8e3-849c-48f8-b95e-9a0236d37ee7}

#define IOCTL_CY_READ_CONFIG CTL_CODE(FILE_DEVICE_UNKNOWN, 1, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_CY_WRITE_CONFIG CTL_CODE(FILE_DEVICE_UNKNOWN, 2, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_CY_I2C_READ CTL_CODE(FILE_DEVICE_UNKNOWN, 3, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_CY_I2C_WRITE CTL_CODE(FILE_DEVICE_UNKNOWN, 4, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_CY_GPIO_READ CTL_CODE(FILE_DEVICE_UNKNOWN, 5, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_CY_GPIO_WRITE CTL_CODE(FILE_DEVICE_UNKNOWN, 6, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_CY_SPI_READWRITE CTL_CODE(FILE_DEVICE_UNKNOWN, 7, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_CY_GET_SCB CTL_CODE(FILE_DEVICE_UNKNOWN, 8, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define CY_USB_DEVICE_NAME_PREFIX L"\\Device\\CyUsbDevice"
#define CY_USB_DEVICE_NAME_SCB0 (CY_USB_DEVICE_NAME_PREFIX L"0")
#define CY_USB_DEVICE_NAME_SCB1 (CY_USB_DEVICE_NAME_PREFIX L"1")

#define DECLARE_CONST_CY_USB_DEVNAME(var, scb)                                                                         \
    const UNICODE_STRING var = {                                                                                       \
        sizeof(CY_USB_DEVICE_NAME_SCB0) - sizeof(WCHAR),                                                               \
        sizeof(CY_USB_DEVICE_NAME_SCB0),                                                                               \
        scb == 0 ? CY_USB_DEVICE_NAME_SCB0 : CY_USB_DEVICE_NAME_SCB1,                                                  \
    }

#pragma pack(push, 1)

// I2C

typedef struct _TransferInfo
{
    USHORT address;
    BOOLEAN stop;
    BOOLEAN nak;
} TransferInfo;

typedef struct _WriteTransferData
{
    TransferInfo info;
    USHORT bufferLength;
    BYTE buffer[1];
} WriteTransferData;

// GPIO

typedef struct _WritePinData
{
    BYTE pinNumber;
    USHORT pinValue;
} WritePinData;

#pragma pack(pop)
