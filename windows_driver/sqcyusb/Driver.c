/*++

Module Name:

    driver.c

Abstract:

    This file contains the driver entry points and callbacks.

Environment:

    Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "driver.tmh"

#ifdef ALLOC_PRAGMA
#pragma alloc_text(INIT, DriverEntry)
#pragma alloc_text(PAGE, SqcyusbEvtDeviceAdd)
#pragma alloc_text(PAGE, SqcyusbEvtDriverContextCleanup)
#endif

/*!
    \brief DriverEntry initializes the driver and is the first routine called by the
        system after the driver is loaded. DriverEntry specifies the other entry
        points in the function driver, such as EvtDevice and DriverUnload.
    \param  driverObject represents the instance of the function driver that is loaded
        into memory. DriverEntry must initialize members of DriverObject before it
        returns to the caller. DriverObject is allocated by the system before the
        driver is loaded, and it is released by the system after the system unloads
        the function driver from memory.
    \param registryPath represents the driver specific path in the Registry.
        The function driver can use the path to store driver related data between
        reboots. The path does not store hardware instance specific data.
    \return STATUS_SUCCESS if successful,
        STATUS_UNSUCCESSFUL otherwise.
*/
NTSTATUS
DriverEntry(_In_ PDRIVER_OBJECT driverObject, _In_ PUNICODE_STRING registryPath)
{
    WDF_DRIVER_CONFIG config;
    NTSTATUS status;
    WDF_OBJECT_ATTRIBUTES attributes;

    //
    // Initialize WPP Tracing
    //
    WPP_INIT_TRACING(driverObject, registryPath);

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "%!FUNC! Entry");

    //
    // Register a cleanup callback so that we can call WPP_CLEANUP when
    // the framework driver object is deleted during driver unload.
    //
    WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
    attributes.EvtCleanupCallback = SqcyusbEvtDriverContextCleanup;

    WDF_DRIVER_CONFIG_INIT(&config, SqcyusbEvtDeviceAdd);

    status = WdfDriverCreate(driverObject, registryPath, &attributes, &config, WDF_NO_HANDLE);

    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfDriverCreate failed %!STATUS!", status);
        WPP_CLEANUP(driverObject);
        return status;
    }

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "%!FUNC! Exit");

    return status;
}

/*!
    \brief EvtDeviceAdd is called by the framework in response to AddDevice
        call from the PnP manager. We create and initialize a device object to
        represent a new instance of the device.
    \param driver Handle to a framework driver object created in DriverEntry
    \param deviceInit Pointer to a framework-allocated WDFDEVICE_INIT structure.
    \return NTSTATUS
*/
NTSTATUS
SqcyusbEvtDeviceAdd(_In_ WDFDRIVER driver, _Inout_ PWDFDEVICE_INIT deviceInit)
{
    NTSTATUS status;

    UNREFERENCED_PARAMETER(driver);

    PAGED_CODE();

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "%!FUNC! Entry");

    status = SqcyusbCreateDevice(deviceInit);

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "%!FUNC! Exit");

    return status;
}

/*!
    \brief Free all the resources allocated in DriverEntry.
    \param driverObject handle to a WDF Driver object
*/
VOID SqcyusbEvtDriverContextCleanup(_In_ WDFOBJECT driverObject)
{
    UNREFERENCED_PARAMETER(driverObject);

    PAGED_CODE();

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "%!FUNC! Entry");

    //
    // Stop WPP Tracing
    //
    WPP_CLEANUP(WdfDriverWdmGetDriverObject((WDFDRIVER)driverObject));
}
