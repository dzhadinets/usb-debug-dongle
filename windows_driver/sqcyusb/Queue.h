/*++

Module Name:

    queue.h

Abstract:

    This file contains the queue definitions.

Environment:

    Kernel-mode Driver Framework

--*/

EXTERN_C_START

NTSTATUS
SqcyusbQueueInitialize(
    _In_ WDFDEVICE device
    );

//
// Events from the IoQueue object
//
EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL SqcyusbEvtIoDeviceControl;
EVT_WDF_IO_QUEUE_IO_STOP SqcyusbEvtIoStop;

EXTERN_C_END
