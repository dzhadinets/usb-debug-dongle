/*++

Module Name:

    device.c - Device handling events for example driver.

Abstract:

   This file contains the device entry points and callbacks.

Environment:

    Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "device.tmh"

#ifdef ALLOC_PRAGMA
#pragma alloc_text(PAGE, SqcyusbCreateDevice)
#pragma alloc_text(PAGE, SqcyusbEvtDevicePrepareHardware)
#endif

NTSTATUS parseInteger(WCHAR *str, USHORT length, USHORT base, ULONG *result)
{
    const UNICODE_STRING ustr = {length, length, str};
    return RtlUnicodeStringToInteger(&ustr, base, result);
}

NTSTATUS parseHardwareId(WCHAR *hardwareId, USHORT *pid, USHORT *vid, USHORT *mi)
{
#define PART_VID L"VID_"
#define PART_PID L"PID_"
#define PART_MI L"MI_"

#define VID_EXAMPLE L"04B4"
#define PID_EXAMPLE L"000A"
#define MI_EXAMPLE L"00"

    NTSTATUS status;

    WCHAR *vidStr = wcsstr(hardwareId, PART_VID);
    if (vidStr == NULL)
    {
        return STATUS_INVALID_PARAMETER;
    }
    vidStr += sizeof(PART_VID) / sizeof(WCHAR) - 1;

    WCHAR *pidStr = wcsstr(vidStr, PART_PID);
    if (pidStr == NULL)
    {
        return STATUS_INVALID_PARAMETER;
    }
    pidStr += sizeof(PART_PID) / sizeof(WCHAR) - 1;

    WCHAR *miStr = wcsstr(pidStr, PART_MI);
    if (miStr == NULL)
    {
        return STATUS_INVALID_PARAMETER;
    }
    miStr += sizeof(PART_MI) / sizeof(WCHAR) - 1;

    ULONG parsedVid;
    ULONG parsedPid;
    ULONG parsedMi;

    status = parseInteger(vidStr, sizeof(VID_EXAMPLE) - sizeof(WCHAR), 16, &parsedVid);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "Failed to parse VID, %!STATUS!", status);
        return status;
    }

    status = parseInteger(pidStr, sizeof(PID_EXAMPLE) - sizeof(WCHAR), 16, &parsedPid);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "Failed to parse PID, %!STATUS!", status);
        return status;
    }

    status = parseInteger(miStr, sizeof(MI_EXAMPLE) - sizeof(WCHAR), 16, &parsedMi);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "Failed to parse MI, %!STATUS!", status);
        return status;
    }

    *vid = (USHORT)parsedVid;
    *pid = (USHORT)parsedPid;
    *mi = (USHORT)parsedMi;

    return status;
}

NTSTATUS getChannelScb(PWDFDEVICE_INIT deviceInit, BYTE *scb)
{
    NTSTATUS status = STATUS_SUCCESS;
    WDFMEMORY memory;
    USHORT pid;
    USHORT vid;
    USHORT mi;

    status = WdfFdoInitAllocAndQueryProperty(deviceInit, DevicePropertyHardwareID, NonPagedPoolNx,
                                             WDF_NO_OBJECT_ATTRIBUTES, &memory);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfFdoInitAllocAndQueryProperty failed 0x%x", status);
        return status;
    }

    WCHAR *hwId = WdfMemoryGetBuffer(memory, NULL);

    status = parseHardwareId(hwId, &pid, &vid, &mi);
    if (!NT_SUCCESS(status))
    {
        goto exit;
    }

    if (vid == 0x04b4 && pid == 0x000A && mi == 0)
    {
        *scb = 0;
    }
    else if (vid == 0x04b4 && pid == 0x000A && mi == 1)
    {
        *scb = 1;
    }
    else
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "Invaid hardware id parameters: pid=0x%x, vid=0x%x, mi=0x%x", pid,
                    vid, mi);
        status = STATUS_INVALID_PARAMETER;
    }

exit:
    WdfObjectDelete(memory);
    return status;
}

/*!
    \brief  Worker routine called to create a device and its software resources.
    \param deviceInit Pointer to an opaque init structure. Memory for this
                    structure will be freed by the framework when the WdfDeviceCreate
                    succeeds. So don't access the structure after that point
    \return NTSTATUS
*/
NTSTATUS
SqcyusbCreateDevice(_Inout_ PWDFDEVICE_INIT deviceInit)
{
    WDF_PNPPOWER_EVENT_CALLBACKS pnpPowerCallbacks;
    WDF_OBJECT_ATTRIBUTES deviceAttributes;
    PDEVICE_CONTEXT deviceContext;
    WDFDEVICE device;
    NTSTATUS status;
    BYTE scb;

    PAGED_CODE();

    WDF_PNPPOWER_EVENT_CALLBACKS_INIT(&pnpPowerCallbacks);
    pnpPowerCallbacks.EvtDevicePrepareHardware = SqcyusbEvtDevicePrepareHardware;
    WdfDeviceInitSetPnpPowerEventCallbacks(deviceInit, &pnpPowerCallbacks);

    status = getChannelScb(deviceInit, &scb);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "Failed to get SCB from Hardware ID, %!STATUS!", status);
        return status;
    }

    DECLARE_CONST_CY_USB_DEVNAME(deviceName, scb);
    status = WdfDeviceInitAssignName(deviceInit, &deviceName);
    if (!NT_SUCCESS(status))
    {
        return status;
    }

    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&deviceAttributes, DEVICE_CONTEXT);

    status = WdfDeviceCreate(&deviceInit, &deviceAttributes, &device);

    if (NT_SUCCESS(status))
    {
        //
        // Get a pointer to the device context structure that we just associated
        // with the device object. We define this structure in the device.h
        // header file. DeviceGetContext is an inline function generated by
        // using the WDF_DECLARE_CONTEXT_TYPE_WITH_NAME macro in device.h.
        // This function will do the type checking and return the device context.
        // If you pass a wrong object handle it will return NULL and assert if
        // run under framework verifier mode.
        //
        deviceContext = DeviceGetContext(device);

        //
        // Initialize the context.
        //
        deviceContext->UsbDevice = NULL;
        deviceContext->BulkReadPipe = NULL;
        deviceContext->BulkWritePipe = NULL;
        deviceContext->InterruptPipe = NULL;
        deviceContext->Scb = scb;

        //
        // Create a device interface so that applications can find and talk
        // to us.
        //
        status = WdfDeviceCreateDeviceInterface(device, &GUID_DEVINTERFACE_sqcyusb,
                                                NULL // ReferenceString
        );

        if (NT_SUCCESS(status))
        {
            //
            // Initialize the I/O Package and any Queues
            //
            status = SqcyusbQueueInitialize(device);
        }
    }

    return status;
}

/*!
    \brief In this callback, the driver does whatever is necessary to make the
        hardware ready to use.  In the case of a USB device, this involves
        reading and selecting descriptors.
*/
NTSTATUS
SqcyusbEvtDevicePrepareHardware(_In_ WDFDEVICE device, _In_ WDFCMRESLIST resourceList,
                                _In_ WDFCMRESLIST resourceListTranslated)
{
    NTSTATUS status;
    PDEVICE_CONTEXT deviceContext;
    WDF_USB_DEVICE_CREATE_CONFIG createParams;
    WDF_USB_DEVICE_SELECT_CONFIG_PARAMS configParams;
    WDFUSBINTERFACE usbInterface;
    UCHAR numberConfiguredPipes;
    WDFUSBPIPE pipe;
    WDF_USB_PIPE_INFORMATION pipeInfo;
    UCHAR index;

    UNREFERENCED_PARAMETER(resourceList);
    UNREFERENCED_PARAMETER(resourceListTranslated);

    PAGED_CODE();

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "%!FUNC! Entry");

    status = STATUS_SUCCESS;
    deviceContext = DeviceGetContext(device);

    //
    // Create a USB device handle so that we can communicate with the
    // underlying USB stack. The WDFUSBDEVICE handle is used to query,
    // configure, and manage all aspects of the USB device.
    // These aspects include device properties, bus properties,
    // and I/O creation and synchronization. We only create the device the first time
    // PrepareHardware is called. If the device is restarted by pnp manager
    // for resource rebalance, we will use the same device handle but then select
    // the interfaces again because the USB stack could reconfigure the device on
    // restart.
    //
    if (deviceContext->UsbDevice == NULL)
    {
        //
        // Specifying a client contract version of 602 enables us to query for
        // and use the new capabilities of the USB driver stack for Windows 8.
        // It also implies that we conform to rules mentioned in MSDN
        // documentation for WdfUsbTargetDeviceCreateWithParameters.
        //
        WDF_USB_DEVICE_CREATE_CONFIG_INIT(&createParams, USBD_CLIENT_CONTRACT_VERSION_602);

        status = WdfUsbTargetDeviceCreateWithParameters(device, &createParams, WDF_NO_OBJECT_ATTRIBUTES,
                                                        &deviceContext->UsbDevice);

        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfUsbTargetDeviceCreateWithParameters failed 0x%x", status);
            return status;
        }
    }

    //
    // Select the first configuration of the device, using the first alternate
    // setting of each interface
    //
    WDF_USB_DEVICE_SELECT_CONFIG_PARAMS_INIT_SINGLE_INTERFACE(&configParams);
    status = WdfUsbTargetDeviceSelectConfig(deviceContext->UsbDevice, WDF_NO_OBJECT_ATTRIBUTES, &configParams);

    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DEVICE, "WdfUsbTargetDeviceSelectConfig failed 0x%x", status);
        return status;
    }

    usbInterface = configParams.Types.SingleInterface.ConfiguredUsbInterface;

    numberConfiguredPipes = configParams.Types.SingleInterface.NumberConfiguredPipes;

    //
    // Get pipe handles
    //
    for (index = 0; index < numberConfiguredPipes; index++)
    {
        WDF_USB_PIPE_INFORMATION_INIT(&pipeInfo);

        pipe = WdfUsbInterfaceGetConfiguredPipe(usbInterface,
                                                index, // PipeIndex,
                                                &pipeInfo);
        //
        // Tell the framework that it's okay to read less than
        // MaximumPacketSize
        //
        WdfUsbTargetPipeSetNoMaximumPacketSizeCheck(pipe);

        if (WdfUsbPipeTypeInterrupt == pipeInfo.PipeType)
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "Interrupt Pipe is 0x%p\n", pipe);
            deviceContext->InterruptPipe = pipe;
        }

        if (WdfUsbPipeTypeBulk == pipeInfo.PipeType && WdfUsbTargetPipeIsInEndpoint(pipe))
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "BulkInput Pipe is 0x%p\n", pipe);
            deviceContext->BulkReadPipe = pipe;
        }

        if (WdfUsbPipeTypeBulk == pipeInfo.PipeType && WdfUsbTargetPipeIsOutEndpoint(pipe))
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DEVICE, "BulkOutput Pipe is 0x%p\n", pipe);
            deviceContext->BulkWritePipe = pipe;
        }
    }

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "%!FUNC! Exit");

    return status;
}
