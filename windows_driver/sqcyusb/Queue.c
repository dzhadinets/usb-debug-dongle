/*++

Module Name:

    queue.c

Abstract:

    This file contains the queue entry points and callbacks.

Environment:

    Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "queue.tmh"

#include <cy7_structs.h>
#include <cypressdefs.h>

#ifdef ALLOC_PRAGMA
#pragma alloc_text(PAGE, SqcyusbQueueInitialize)
#endif

/*!
    \brief The I/O dispatch callbacks for the frameworks device object
        are configured in this function.
    \param device Handle to a framework device object.
    \return NTSTATUS
*/
NTSTATUS
SqcyusbQueueInitialize(_In_ WDFDEVICE device)
{
    WDFQUEUE queue;
    NTSTATUS status;
    WDF_IO_QUEUE_CONFIG queueConfig;

    PAGED_CODE();

    //
    // Configure a default queue so that requests that are not
    // configure-fowarded using WdfDeviceConfigureRequestDispatching to goto
    // other queues get dispatched here.
    //
    WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&queueConfig, WdfIoQueueDispatchParallel);

    queueConfig.EvtIoDeviceControl = SqcyusbEvtIoDeviceControl;
    queueConfig.EvtIoStop = SqcyusbEvtIoStop;

    status = WdfIoQueueCreate(device, &queueConfig, WDF_NO_OBJECT_ATTRIBUTES, &queue);

    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfIoQueueCreate failed %!STATUS!", status);
        return status;
    }

    return status;
}

#pragma region Cypress general

#define CY_READ_CONFIG_CMD 0xb5
#define CY_WRITE_CONFIG_CMD 0xB6
#define CY_MAINTAINANCE_MODE_CMD 0xE2

#define CY_MAINTAINANCE_MODE_WVALUE 0xa6bc
#define CY_MAINTAINANCE_MODE_SET_WINDEX 0xb1b0
#define CY_MAINTAINANCE_MODE_UNSET_WINDEX 0xb9b0

// Linux / Windows mappings:
// BmRequestDeviceToHost -> USB_DIR_IN
// BmRequestToDevice -> USB_RECIP_DEVICE

/*!
    \brief Send a control message to the USB device.
    \param usbDevice USB device
    \param direction direction
    \param recipient recipient
    \param request USB request
    \param value URB value
    \param index URB index
    \param bytesTransferred pointer to ULONG which will contain the number of bytes actually transferred
    \param memDesc memory desciptor of the data to send
    \return NTSTATUS
*/
NTSTATUS usbControlMsg(WDFUSBDEVICE usbDevice, WDF_USB_BMREQUEST_DIRECTION direction,
                       WDF_USB_BMREQUEST_RECIPIENT recipient, BYTE request, USHORT value, USHORT index,
                       PULONG bytesTransferred, WDF_MEMORY_DESCRIPTOR *memDesc)
{
    WDF_USB_CONTROL_SETUP_PACKET controlSetupPacket;
    WDF_REQUEST_SEND_OPTIONS sendOptions;

    WDF_USB_CONTROL_SETUP_PACKET_INIT_VENDOR(&controlSetupPacket, direction, recipient, request, value, index);

    //
    // Send the I/O with a timeout to avoid hanging the calling
    // thread indefinitely.
    //
    WDF_REQUEST_SEND_OPTIONS_INIT(&sendOptions, WDF_REQUEST_SEND_OPTION_TIMEOUT);

    WDF_REQUEST_SEND_OPTIONS_SET_TIMEOUT(&sendOptions, WDF_REL_TIMEOUT_IN_MS(100));

    return WdfUsbTargetDeviceSendControlTransferSynchronously(usbDevice,
                                                              NULL,         // Optional WDFREQUEST
                                                              &sendOptions, // PWDF_REQUEST_SEND_OPTIONS
                                                              &controlSetupPacket, memDesc, bytesTransferred);
}

/*!
    \brief the structure that contains Cypress status. See relevant documentation for details.
*/
typedef struct _CyStatus
{
    UCHAR Value[3];
} CyStatus;

#define CY7_GEN_STOP (1 << 0)
#define CY7_GEN_NACK (1 << 1)

/*!
    \brief A helper function which sends a control command to the device.
    \param usbDevice USB whistle device
    \param scb SCB of the I2C device on the whistle
    \param address slave address of the device
    \param stop stop bit
    \param nak NACK bit (1 for NACK, 0 for ACK)
    \param dataLength length of data to transfer
    \param command command from CY_VENDOR_CMDS enum
    \return NTSTATUS
*/
static NTSTATUS sendRequestToUsb(WDFUSBDEVICE usbDevice, UCHAR scb, USHORT address, BOOLEAN stop, BOOLEAN nak,
                                 USHORT dataLength, enum CY_VENDOR_CMDS command)
{
    return usbControlMsg(usbDevice, BmRequestHostToDevice, BmRequestToDevice, command,
                         (scb << 15) | (address << 8) | (stop ? CY7_GEN_STOP : 0) | (nak ? CY7_GEN_NACK : 0),
                         dataLength, NULL, NULL);
}

#pragma endregion Cypress general

#pragma region Cypress config

#define CY_CONFIG_SIZE sizeof(struct cy7_device_config)

/*!
    \brief Calculate checksum for cy7_device_config
    \param cfg config buffer
    \return checksum
*/
uint32_t CyConfigGetChecksum(struct cy7_device_config *cfg)
{
    uint32_t *ptr = (uint32_t *)cfg;
    uint32_t *end = ptr + CY_CONFIG_SIZE / sizeof(*ptr);
    uint32_t sum = 0;

    /* skip first 12 bytes */
    ptr += 3;

    while (ptr < end)
    {
        sum += *ptr;
        ptr++;
    }

    return sum;
}

/*!
    \brief Check if memory with config buffer is valid. Size and checksum are checked.
    \param config memory object with config buffer
    \return TRUE if the config is valid, FALSE otherwise
*/
BOOLEAN isConfigValid(WDFMEMORY config)
{
    size_t bufferSize = 0;
    struct cy7_device_config *cfg = (struct cy7_device_config *)WdfMemoryGetBuffer(config, &bufferSize);
    return bufferSize == CY_CONFIG_SIZE && CyConfigGetChecksum(cfg) == cfg->checksum;
}

/*!
    \brief Reads whistle config data.
    \param usbDevice USB whistle device
    \param config memory object to read the data to
    \param bytesTransferred pointer to ULONG which will contain the number of bytes actually transferred
    \return NTSTATUS
*/
NTSTATUS readConfig(WDFUSBDEVICE usbDevice, WDFMEMORY config, PULONG bytesTransferred)
{
    WDF_MEMORY_DESCRIPTOR memDesc;
    NTSTATUS status, statusUnset;

    WDF_MEMORY_DESCRIPTOR_INIT_HANDLE(&memDesc, config, NULL);

    status = usbControlMsg(usbDevice, BmRequestHostToDevice, BmRequestToDevice, CY_MAINTAINANCE_MODE_CMD,
                           CY_MAINTAINANCE_MODE_WVALUE, CY_MAINTAINANCE_MODE_SET_WINDEX, NULL, NULL);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "usbControlMsg CY_MAINTAINANCE_MODE_CMD (on) failed %!STATUS!",
                    status);
        return status;
    }

    status = usbControlMsg(usbDevice, BmRequestDeviceToHost, BmRequestToDevice, CY_READ_CONFIG_CMD, 0, 0,
                           bytesTransferred, &memDesc);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "usbControlMsg CY7_READ_CONFIG_CMD failed %!STATUS!", status);
        // continue anyway, we need to disable maintanence mode
    }

    statusUnset = usbControlMsg(usbDevice, BmRequestHostToDevice, BmRequestToDevice, CY_MAINTAINANCE_MODE_CMD,
                                CY_MAINTAINANCE_MODE_WVALUE, CY_MAINTAINANCE_MODE_UNSET_WINDEX, NULL, NULL);
    if (!NT_SUCCESS(statusUnset))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "usbControlMsg CY_MAINTAINANCE_MODE_CMD (off) failed %!STATUS!",
                    statusUnset);
        if (NT_SUCCESS(status))
        {
            status = statusUnset;
        }
    }

    return status;
}

/*!
    \brief Writes whistle config data.
    \param usbDevice USB whistle device
    \param config memory object with the config buffer
    \param bytesTransferred pointer to ULONG which will contain the number of bytes actually transferred
    \return NTSTATUS
*/
NTSTATUS writeConfig(WDFUSBDEVICE usbDevice, WDFMEMORY config, PULONG bytesTransferred)
{
    WDF_MEMORY_DESCRIPTOR memDesc;
    NTSTATUS status, statusUnset;

    if (!isConfigValid(config))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "invalid config");
        return STATUS_INVALID_PARAMETER;
    }

    WDF_MEMORY_DESCRIPTOR_INIT_HANDLE(&memDesc, config, NULL);

    status = usbControlMsg(usbDevice, BmRequestHostToDevice, BmRequestToDevice, CY_MAINTAINANCE_MODE_CMD,
                           CY_MAINTAINANCE_MODE_WVALUE, CY_MAINTAINANCE_MODE_SET_WINDEX, NULL, NULL);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "usbControlMsg CY_MAINTAINANCE_MODE_CMD (on) failed %!STATUS!",
                    status);
        return status;
    }

    status = usbControlMsg(usbDevice, BmRequestHostToDevice, BmRequestToDevice, CY_WRITE_CONFIG_CMD, 0, 0,
                           bytesTransferred, &memDesc);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "usbControlMsg CY_WRITE_CONFIG_CMD failed %!STATUS!", status);
        // continue anyway, we need to disable maintanence mode
    }

    statusUnset = usbControlMsg(usbDevice, BmRequestHostToDevice, BmRequestToDevice, CY_MAINTAINANCE_MODE_CMD,
                                CY_MAINTAINANCE_MODE_WVALUE, CY_MAINTAINANCE_MODE_UNSET_WINDEX, NULL, NULL);
    if (!NT_SUCCESS(statusUnset))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "usbControlMsg CY_MAINTAINANCE_MODE_CMD (off) failed %!STATUS!",
                    statusUnset);
        if (NT_SUCCESS(status))
        {
            status = statusUnset;
        }
    }

    return status;
}

#pragma endregion

#pragma region Cypress I2C

/*!
    \brief Send write request to the USB whistle device. It should be sent before an attempt to write
    \param usbDevice USB whistle device
    \param scb SCB of the I2C device on the whistle
    \param address slave address of the device
    \param stop stop bit
    \param nak NACK bit (1 for NACK, 0 for ACK)
    \param dataLength length of data to transfer
    \return NTSTATUS
*/
static NTSTATUS sendWriteRequest(WDFUSBDEVICE usbDevice, UCHAR scb, USHORT address, BOOLEAN stop, BOOLEAN nak,
                                 USHORT dataLength)
{
    return sendRequestToUsb(usbDevice, scb, address, stop, nak, dataLength, CY_I2C_WRITE_CMD);
}

/*!
    \brief Send read request to the USB whistle device. It should be sent before an attempt to read
    \param usbDevice USB whistle device
    \param scb SCB of the I2C device on the whistle
    \param address slave address of the device
    \param stop stop bit
    \param nak NACK bit (1 for NACK, 0 for ACK)
    \param dataLength length of data to transfer
    \return NTSTATUS
*/
static NTSTATUS sendReadRequest(WDFUSBDEVICE usbDevice, UCHAR scb, USHORT address, BOOLEAN stop, BOOLEAN nak,
                                USHORT dataLength)
{
    return sendRequestToUsb(usbDevice, scb, address, stop, nak, dataLength, CY_I2C_READ_CMD);
}

/*!
    \brief Read a status of the whistle
    \param usbDevice USB whistle device
    \param scb SCB of the I2C device on the whistle
    \param rw either CY_I2C_MODE_WRITE or CY_I2C_MODE_READ
    \param cystatus pointer to write the status to
    \return NTSTATUS
*/
NTSTATUS readStatus(WDFUSBDEVICE usbDevice, UCHAR scb, UCHAR rw, CyStatus *cystatus)
{
    WDF_MEMORY_DESCRIPTOR memDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memDesc, &cystatus->Value, sizeof(cystatus->Value));
    return usbControlMsg(usbDevice, BmRequestDeviceToHost, BmRequestToDevice, CY_I2C_GET_STATUS_CMD, rw | (scb << 15),
                         0, NULL, &memDesc);
}

/*!
    \brief Waits for a status of the whistle using interrupt pipe
    \param deviceContext USB whistle device context
    \param cystatus pointer to write the status to
    \return NTSTATUS
*/
NTSTATUS waitStatus(PDEVICE_CONTEXT deviceContext, CyStatus *cystatus)
{
    WDF_REQUEST_SEND_OPTIONS sendOptions;
    WDF_MEMORY_DESCRIPTOR memDesc;

    WDF_REQUEST_SEND_OPTIONS_INIT(&sendOptions, WDF_REQUEST_SEND_OPTION_TIMEOUT);
    WDF_REQUEST_SEND_OPTIONS_SET_TIMEOUT(&sendOptions, WDF_REL_TIMEOUT_IN_MS(2000));

    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memDesc, &cystatus->Value, sizeof(cystatus->Value));

    return WdfUsbTargetPipeReadSynchronously(deviceContext->InterruptPipe, NULL, &sendOptions, &memDesc, NULL);
}

/*!
    \brief Reset the device
    \param usbDevice USB whistle device
    \param scb SCB of the I2C device on the whistle
    \param rw either CY_I2C_MODE_WRITE or CY_I2C_MODE_READ
    \return NTSTATUS
*/
NTSTATUS resetDevice(WDFUSBDEVICE usbDevice, UCHAR scb, UCHAR rw)
{
    return usbControlMsg(usbDevice, BmRequestHostToDevice, BmRequestToDevice, CY_I2C_RESET_CMD, (scb << 15) | rw, 0,
                         NULL, NULL);
}

/*!
    \brief Attemp to recover the device
    \param usbDevice USB whistle device
    \param scb SCB of the I2C device on the whistle
    \param rw either CY_I2C_MODE_WRITE or CY_I2C_MODE_READ
    \return NTSTATUS
*/
NTSTATUS recoverDevice(WDFUSBDEVICE usbDevice, UCHAR scb, UCHAR rw)
{
    CyStatus cystatus = {0};
    NTSTATUS status = readStatus(usbDevice, scb, rw, &cystatus);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "failed to read status (1), %!STATUS!", status);
        return status;
    }

    if (cystatus.Value[0] & 0x7F)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE,
                    "adapter reported an error, trying to recover, status = [0x%02x, 0x%02x, 0x%02x]",
                    cystatus.Value[0], cystatus.Value[1], cystatus.Value[2]);

        resetDevice(usbDevice, scb, rw); // maybe check the status

        status = readStatus(usbDevice, scb, rw, &cystatus);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "failed to read status (2), %!STATUS!", status);
            return status;
        }

        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "Recover %s",
                    ((cystatus.Value[0] & 0x7F) == 0) ? "succeeded" : "failed");
    }

    return status;
}

/*!
    \brief Write data to the device
    \param deviceContext USB whistle device context
    \param info pointer to TransferInfo structure with the transfer data info
    \param buffer data buffer to write
    \param bufferLength length of the data buffer
    \param bytesTransferred number of bytes transferred (out param)
    \return NTSTATUS
*/
NTSTATUS writeData(PDEVICE_CONTEXT deviceContext, TransferInfo *info, PVOID buffer, USHORT bufferLength,
                   PULONG bytesTransferred)
{
    NTSTATUS status = STATUS_SUCCESS;
    WDF_REQUEST_SEND_OPTIONS sendOptions;
    WDF_MEMORY_DESCRIPTOR memDesc;
    CyStatus cystatus = {0};

    status = sendWriteRequest(deviceContext->UsbDevice, deviceContext->Scb, info->address, info->stop, info->nak,
                              bufferLength);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "sendWriteRequest failed %!STATUS!", status);
        return status;
    }

    WDF_REQUEST_SEND_OPTIONS_INIT(&sendOptions, WDF_REQUEST_SEND_OPTION_TIMEOUT);
    WDF_REQUEST_SEND_OPTIONS_SET_TIMEOUT(&sendOptions, WDF_REL_TIMEOUT_IN_MS(2000));

    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memDesc, buffer, bufferLength);

    status = WdfUsbTargetPipeWriteSynchronously(deviceContext->BulkWritePipe, NULL, &sendOptions, &memDesc,
                                                bytesTransferred);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfUsbTargetPipeWriteSynchronously failed %!STATUS!", status);

        if (NT_SUCCESS(readStatus(deviceContext->UsbDevice, deviceContext->Scb, CY_I2C_MODE_WRITE, &cystatus)))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "Reported status: %x", cystatus.Value[0]);
        }

        return status;
    }

    status = waitStatus(deviceContext, &cystatus);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "waitStatus failed %!STATUS!", status);
    }

    if (!NT_SUCCESS(status) || (cystatus.Value[0] & CY_I2C_ERROR_BIT))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "write failed with status %x", cystatus.Value[0]);

        recoverDevice(deviceContext->UsbDevice, deviceContext->Scb, CY_I2C_MODE_WRITE);

        status = STATUS_UNSUCCESSFUL;
    }

    return status;
}

/*!
    \brief Read data from the device
    \param deviceContext USB whistle device context
    \param info pointer to TransferInfo structure with the transfer data info
    \param buffer data buffer to write
    \param bufferLength length of the data buffer
    \param bytesTransferred number of bytes transferred (out param)
    \return NTSTATUS
*/
NTSTATUS readData(PDEVICE_CONTEXT deviceContext, TransferInfo *info, PVOID buffer, USHORT bufferLength,
                  PULONG bytesTransferred)
{
    NTSTATUS status = STATUS_SUCCESS;
    WDF_REQUEST_SEND_OPTIONS sendOptions;
    WDF_MEMORY_DESCRIPTOR memDesc;
    CyStatus cystatus = {0};

    status = sendReadRequest(deviceContext->UsbDevice, deviceContext->Scb, info->address, info->stop, info->nak,
                             bufferLength);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "sendWriteRequest failed %!STATUS!", status);
        return status;
    }

    WDF_REQUEST_SEND_OPTIONS_INIT(&sendOptions, WDF_REQUEST_SEND_OPTION_TIMEOUT);
    WDF_REQUEST_SEND_OPTIONS_SET_TIMEOUT(&sendOptions, WDF_REL_TIMEOUT_IN_MS(2000));

    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memDesc, buffer, bufferLength);

    status =
        WdfUsbTargetPipeReadSynchronously(deviceContext->BulkReadPipe, NULL, &sendOptions, &memDesc, bytesTransferred);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfUsbTargetPipeReadSynchronously failed %!STATUS!", status);

        if (NT_SUCCESS(readStatus(deviceContext->UsbDevice, deviceContext->Scb, CY_I2C_MODE_READ, &cystatus)))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "Reported status: %x", cystatus.Value[0]);
        }

        return status;
    }

    status = waitStatus(deviceContext, &cystatus);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "waitStatus failed %!STATUS!", status);
        return status;
    }

    if (!NT_SUCCESS(status) || (cystatus.Value[0] & CY_I2C_ERROR_BIT))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "read failed with status %x", cystatus.Value[0]);

        recoverDevice(deviceContext->UsbDevice, deviceContext->Scb, CY_I2C_MODE_READ);

        status = STATUS_UNSUCCESSFUL;
    }

    return status;
}

#pragma endregion

#pragma region Cypress GPIO

NTSTATUS gpioRead(PDEVICE_CONTEXT deviceContext, BYTE gpioNum, USHORT *gpioValue, ULONG *bytesTransferred)
{
    WDF_MEMORY_DESCRIPTOR memDesc;

    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memDesc, gpioValue, sizeof(*gpioValue));

    return usbControlMsg(deviceContext->UsbDevice, BmRequestDeviceToHost, BmRequestToDevice, CY_GPIO_GET_VALUE_CMD,
                         gpioNum, 0, bytesTransferred, &memDesc);
}

NTSTATUS gpioWrite(PDEVICE_CONTEXT deviceContext, BYTE gpioNum, USHORT gpioValue, ULONG *bytesTransferred)
{
    return usbControlMsg(deviceContext->UsbDevice, BmRequestHostToDevice, BmRequestToDevice, CY_GPIO_SET_VALUE_CMD,
                         gpioNum, gpioValue, bytesTransferred, NULL);
}

#pragma endregion

#pragma region Cypress SPI

NTSTATUS spiReset(WDFUSBDEVICE usbDevice, BYTE scb)
{
    return usbControlMsg(usbDevice, BmRequestHostToDevice, BmRequestToDevice, CY_SPI_RESET_CMD, (scb << 15), 0, NULL,
                         NULL);
}

NTSTATUS spiGetStatus(WDFUSBDEVICE usbDevice, BYTE scb, ULONG *status)
{
    WDF_MEMORY_DESCRIPTOR memDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memDesc, status, sizeof(*status));

    return usbControlMsg(usbDevice, BmRequestDeviceToHost, BmRequestToDevice, CY_SPI_GET_STATUS_CMD, (scb << 15), 0,
                         NULL, &memDesc);
}

NTSTATUS spiReadWrite(PDEVICE_CONTEXT deviceContext, BYTE *readBuffer, BYTE *writeBuffer, USHORT length, BYTE scb,
                      PULONG bytesTransferred)
{
    NTSTATUS status = STATUS_SUCCESS;
    WDF_REQUEST_SEND_OPTIONS sendOptions;
    WDF_MEMORY_DESCRIPTOR memDesc;
    USHORT value = (scb << 15);

    if (readBuffer)
    {
        value |= CY_SPI_READ_BIT;
    }

    if (writeBuffer)
    {
        value |= CY_SPI_WRITE_BIT;
    }

    status = usbControlMsg(deviceContext->UsbDevice, BmRequestHostToDevice, BmRequestToDevice, CY_SPI_READ_WRITE_CMD,
                           value, length, NULL, NULL);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "usbControlMsg failed %!STATUS!", status);
        return status;
    }

    WDF_REQUEST_SEND_OPTIONS_INIT(&sendOptions, WDF_REQUEST_SEND_OPTION_TIMEOUT);
    WDF_REQUEST_SEND_OPTIONS_SET_TIMEOUT(&sendOptions, WDF_REL_TIMEOUT_IN_MS(2000));

    if (writeBuffer)
    {
        WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memDesc, writeBuffer, length);

        status = WdfUsbTargetPipeWriteSynchronously(deviceContext->BulkWritePipe, NULL, &sendOptions, &memDesc,
                                                    bytesTransferred);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfUsbTargetPipeWriteSynchronously failed %!STATUS!", status);
        }
    }
    if (NT_SUCCESS(status) && readBuffer)
    {
        ULONG bytesRead = 0;
        ULONG readTotal = 0;
        while (readTotal < length)
        {
            WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memDesc, readBuffer + readTotal, length - readTotal);
            status = WdfUsbTargetPipeReadSynchronously(deviceContext->BulkReadPipe, NULL, &sendOptions, &memDesc,
                                                       &bytesRead);
            if (!NT_SUCCESS(status))
            {
                TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfUsbTargetPipeReadSynchronously failed %!STATUS!",
                            status);
                break;
            }
            readTotal += bytesRead;
        }
        *bytesTransferred = readTotal;
    }
    if (!NT_SUCCESS(status))
    {
        spiReset(deviceContext->UsbDevice, scb);

        // Maybe we should get status first and then reset the device?
        ULONG spiStatus;
        if (NT_SUCCESS(spiGetStatus(deviceContext->UsbDevice, scb, &spiStatus)))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "Reported status: %x", spiStatus);
        }
    }

    return status;
}

#pragma endregion

/*!
    \brief This event is invoked when the framework receives IRP_MJ_DEVICE_CONTROL request.
    \param Queue Handle to the framework queue object that is associated with the
             I/O request.
    \param Request Handle to a framework request object.
    \param OutputBufferLength Size of the output buffer in bytes
    \param InputBufferLength Size of the input buffer in bytes
    \param IoControlCode I/O control code.
*/
VOID SqcyusbEvtIoDeviceControl(_In_ WDFQUEUE queue, _In_ WDFREQUEST request, _In_ size_t outputBufferLength,
                               _In_ size_t inputBufferLength, _In_ ULONG ioControlCode)
{
    ULONG bytesTransferred = 0;
    WDFDEVICE device;
    PDEVICE_CONTEXT pDevContext;
    NTSTATUS status;
    WDFMEMORY outputMemory, inputMemory;
    WriteTransferData *transferData;
    TransferInfo *transferInfo;
    PVOID buffer;
    size_t bufferLength;

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                "%!FUNC! Queue 0x%p, Request 0x%p OutputBufferLength %d InputBufferLength %d IoControlCode %d", queue,
                request, (int)outputBufferLength, (int)inputBufferLength, ioControlCode);

    device = WdfIoQueueGetDevice(queue);
    pDevContext = DeviceGetContext(device);

    switch (ioControlCode)
    {
    case IOCTL_CY_GET_SCB:
    {
        BYTE *outputScb = NULL;
        status = WdfRequestRetrieveOutputBuffer(request, sizeof(pDevContext->Scb), &outputScb, NULL);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveOutputBuffer failed %!STATUS!", status);
            break;
        }

        *outputScb = pDevContext->Scb;
        bytesTransferred = sizeof(pDevContext->Scb);
        status = STATUS_SUCCESS;

        break;
    }

    case IOCTL_CY_READ_CONFIG:

        if (outputBufferLength != CY_CONFIG_SIZE)
        {
            status = STATUS_INFO_LENGTH_MISMATCH;
            break;
        }

        status = WdfRequestRetrieveOutputMemory(request, &outputMemory);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveMemory failed %!STATUS!", status);
            break;
        }

        status = readConfig(pDevContext->UsbDevice, outputMemory, &bytesTransferred);

        break;

    case IOCTL_CY_WRITE_CONFIG:

        if (inputBufferLength != CY_CONFIG_SIZE)
        {
            status = STATUS_INFO_LENGTH_MISMATCH;
            break;
        }

        status = WdfRequestRetrieveInputMemory(request, &inputMemory);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveMemory failed %!STATUS!", status);
            break;
        }

        status = writeConfig(pDevContext->UsbDevice, inputMemory, &bytesTransferred);

        break;

    case IOCTL_CY_I2C_READ:

        if (inputBufferLength < sizeof(TransferInfo))
        {
            status = STATUS_INFO_LENGTH_MISMATCH;
            break;
        }

        status = WdfRequestRetrieveInputMemory(request, &inputMemory);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveInputMemory failed %!STATUS!", status);
            break;
        }

        transferInfo = WdfMemoryGetBuffer(inputMemory, NULL);

        status = WdfRequestRetrieveOutputMemory(request, &outputMemory);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveOutputMemory failed %!STATUS!", status);
            break;
        }

        buffer = WdfMemoryGetBuffer(outputMemory, &bufferLength);

        status = readData(pDevContext, transferInfo, buffer, (USHORT)bufferLength, &bytesTransferred);

        break;

    case IOCTL_CY_I2C_WRITE:

        if (inputBufferLength < sizeof(WriteTransferData))
        {
            status = STATUS_INFO_LENGTH_MISMATCH;
            break;
        }

        status = WdfRequestRetrieveInputMemory(request, &outputMemory);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveMemory failed %!STATUS!", status);
            break;
        }

        transferData = (WriteTransferData *)WdfMemoryGetBuffer(outputMemory, NULL);

        // the last memeber of TransferData's declared size is 1, but a user can append any amount of data as
        // long as it is specified correctly in transferData->bufferLength
        if (inputBufferLength < transferData->bufferLength + sizeof(WriteTransferData) - sizeof(transferData->buffer))
        {
            status = STATUS_INVALID_PARAMETER;
            break;
        }

        status = writeData(pDevContext, &transferData->info, transferData->buffer, transferData->bufferLength,
                           &bytesTransferred);

        break;

    case IOCTL_CY_GPIO_READ:
    {
        BYTE *gpioNum;
        USHORT *gpioValue;
        status = WdfRequestRetrieveInputBuffer(request, sizeof(BYTE), &gpioNum, NULL);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveInputBuffer failed %!STATUS!", status);
            break;
        }

        status = WdfRequestRetrieveOutputBuffer(request, sizeof(USHORT), &gpioValue, NULL);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveOutputBuffer failed %!STATUS!", status);
            break;
        }

        status = gpioRead(pDevContext, *gpioNum, gpioValue, &bytesTransferred);
        if (NT_SUCCESS(status))
        {
            // reverse byte order
            *gpioValue = (*gpioValue & 0xFF) | (*gpioValue >> 8);
        }

        break;
    }

    case IOCTL_CY_GPIO_WRITE:
    {
        WritePinData *writePinData;
        status = WdfRequestRetrieveInputBuffer(request, sizeof(*writePinData), &writePinData, NULL);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveInputBuffer failed %!STATUS!", status);
            break;
        }

        status = gpioWrite(pDevContext, writePinData->pinNumber, writePinData->pinValue, &bytesTransferred);

        break;
    }

    case IOCTL_CY_SPI_READWRITE:
    {
        if (inputBufferLength > 0 && outputBufferLength > 0 && inputBufferLength != outputBufferLength)
        {
            // Input and output buffers must be equal if both are specified
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "Input and output buffers must be equal failed");
            status = STATUS_INVALID_PARAMETER;
            break;
        }

        BYTE *inBuffer = NULL;
        BYTE *outBuffer = NULL;

        if (inputBufferLength > 0)
        {
            status = WdfRequestRetrieveInputBuffer(request, 1, &inBuffer, NULL);
            if (!NT_SUCCESS(status))
            {
                TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveInputBuffer failed %!STATUS!", status);
                break;
            }
        }

        if (outputBufferLength > 0)
        {
            status = WdfRequestRetrieveOutputBuffer(request, 1, &outBuffer, NULL);
            if (!NT_SUCCESS(status))
            {
                TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveOutputBuffer failed %!STATUS!", status);
                break;
            }
        }

        status = spiReadWrite(pDevContext, outBuffer, inBuffer, (USHORT)inputBufferLength, pDevContext->Scb,
                              &bytesTransferred);

        break;
    }

    default:
        status = STATUS_INVALID_DEVICE_REQUEST;
        break;
    }

    WdfRequestCompleteWithInformation(request, status, bytesTransferred);
}

/*!
    \brief This event is invoked for a power-managed queue before the device leaves the working state (D0).
    \param Queue Handle to the framework queue object that is associated with the
             I/O request.
    \param Request Handle to a framework request object.
    \param ActionFlags A bitwise OR of one or more WDF_REQUEST_STOP_ACTION_FLAGS-typed flags
                  that identify the reason that the callback function is being called
                  and whether the request is cancelable.
*/
VOID SqcyusbEvtIoStop(_In_ WDFQUEUE queue, _In_ WDFREQUEST request, _In_ ULONG actionFlags)
{
    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE, "%!FUNC! Queue 0x%p, Request 0x%p ActionFlags %d", queue, request,
                actionFlags);

    //
    // In most cases, the EvtIoStop callback function completes, cancels, or postpones
    // further processing of the I/O request.
    //
    // Typically, the driver uses the following rules:
    //
    // - If the driver owns the I/O request, it either postpones further processing
    //   of the request and calls WdfRequestStopAcknowledge, or it calls WdfRequestComplete
    //   with a completion status value of STATUS_SUCCESS or STATUS_CANCELLED.
    //
    //   The driver must call WdfRequestComplete only once, to either complete or cancel
    //   the request. To ensure that another thread does not call WdfRequestComplete
    //   for the same request, the EvtIoStop callback must synchronize with the driver's
    //   other event callback functions, for instance by using interlocked operations.
    //
    // - If the driver has forwarded the I/O request to an I/O target, it either calls
    //   WdfRequestCancelSentRequest to attempt to cancel the request, or it postpones
    //   further processing of the request and calls WdfRequestStopAcknowledge.
    //
    // A driver might choose to take no action in EvtIoStop for requests that are
    // guaranteed to complete in a small amount of time. For example, the driver might
    // take no action for requests that are completed in one of the driver�s request handlers.
    //

    return;
}
