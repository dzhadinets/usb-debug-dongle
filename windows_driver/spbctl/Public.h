/*++

Module Name:

    public.h

Abstract:

    This module contains the common declarations shared by driver
    and user applications.

Environment:

    user and kernel

--*/

//
// Define an Interface Guid so that apps can find the device and talk to it.
//

DEFINE_GUID (GUID_DEVINTERFACE_SPBCTL,
    0x59eea68b,0x321c,0x4a62,0xbf,0x89,0x7a,0xec,0xf2,0x96,0x39,0xfc);
// {59eea68b-321c-4a62-bf89-7aecf29639fc}
