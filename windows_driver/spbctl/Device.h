/*++

Module Name:

    device.h

Abstract:

    This file contains the device definitions.

Environment:

    Kernel-mode Driver Framework

--*/

#include "public.h"
#include <wdf.h>
#include <spbcx.h>
#include <reshub.h>

EXTERN_C_START

typedef struct _I2C_TARGET_CONTEXT
{
    // ULONG ConnectionSpeed; // not used, but might be in future
    UCHAR SerialBusType;
    USHORT Address; // TODO: rename to i2cAddress
    WDFIOTARGET UsbIoTarget;
} I2C_TARGET_CONTEXT, *PI2C_TARGET_CONTEXT;
WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(I2C_TARGET_CONTEXT, TargetGetContext);

//
// Function to initialize the device and its callbacks
//
NTSTATUS
SpbctlCreateDevice(
    _Inout_ PWDFDEVICE_INIT deviceInit
    );

//
// I2C Serial Bus ACPI Descriptor
// See ACPI 5.0 spec table 6-192
//
#include "pshpack1.h"

typedef struct _PNP_I2C_SERIAL_BUS_DESCRIPTOR
{
    PNP_SERIAL_BUS_DESCRIPTOR SerialBusDescriptor;
    ULONG ConnectionSpeed;
    USHORT Address;
} PNP_I2C_SERIAL_BUS_DESCRIPTOR, *PPNP_I2C_SERIAL_BUS_DESCRIPTOR;

typedef struct _PNP_SPI_SERIAL_BUS_DESCRIPTOR
{
    PNP_SERIAL_BUS_DESCRIPTOR SerialBusDescriptor;
    // the SPI descriptor, see ACPI 5.0 spec table 6-192
    ULONG ConnectionSpeed;
    UCHAR DataBitLength;
    UCHAR Phase;
    UCHAR Polarity;
    USHORT DeviceSelection;
    // follwed by optional Vendor Data
    // followed by PNP_IO_DESCRIPTOR_RESOURCE_NAME
} PNP_SPI_SERIAL_BUS_DESCRIPTOR, *PPNP_SPI_SERIAL_BUS_DESCRIPTOR;

#include "poppack.h"

//
// See section 6.4.3.8.2 of the ACPI 5.0 specification
//
enum PNP_SERIAL_BUS_TYPE
{
    PNP_SERIAL_BUS_TYPE_I2C = 0x1,
    PNP_SERIAL_BUS_TYPE_SPI = 0x2,
    PNP_SERIAL_BUS_TYPE_UART = 0x3,
};

EXTERN_C_END
