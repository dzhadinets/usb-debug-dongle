/*++

Module Name:

    device.c - Device handling events for example driver.

Abstract:

   This file contains the device entry points and callbacks.

Environment:

    Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "device.tmh"

#include <Acpiioct.h> // required for GetScbFromAcpi

#include "..\sqcyusb\Public.h"

#ifdef ALLOC_PRAGMA
#pragma alloc_text(PAGE, SpbctlCreateDevice)
#endif

EVT_SPB_TARGET_CONNECT OnTargetConnect;
EVT_SPB_TARGET_DISCONNECT OnTargetDisconnect;
EVT_SPB_CONTROLLER_READ OnRead;
EVT_SPB_CONTROLLER_WRITE OnWrite;
EVT_SPB_CONTROLLER_SEQUENCE OnSequence;

EVT_WDF_IO_IN_CALLER_CONTEXT OnOtherInCallerContext;
EVT_SPB_CONTROLLER_OTHER OnOther;

/*!
    \brief Convert SPB_TRANSFER_DIRECTION to a string (const char *)
    \param direction SPB_TRANSFER_DIRECTION
    \return string representation
*/
const char *DirectionToString(SPB_TRANSFER_DIRECTION direction)
{
    switch (direction)
    {
    case SpbTransferDirectionNone:
        return "None";
    case SpbTransferDirectionFromDevice:
        return "FromDevice";
    case SpbTransferDirectionToDevice:
        return "ToDevice";
    case SpbTransferDirectionMax: // this value is probably wrong too
        return "Max";
    default:
        return "(wrong value)";
    }
}

/*!
    \brief Gets ULONG driver parameter from the registry (HKLM\SYSTEM\CurrentControlSet\Services\spbctl\Parameters).
    \param driver driver object
    \param valueName the name of the parameter
    \value pointer to ULONG to read the data to
    \return NTSTATUS
*/
NTSTATUS GetDriverUlongParameter(WDFDRIVER driver, PCWSTR valueName, ULONG *Value)
{
    WDFKEY key;
    NTSTATUS status;
    UNICODE_STRING unicodeValueName;

    PAGED_CODE();

    status = WdfDriverOpenParametersRegistryKey(driver, GENERIC_READ, WDF_NO_OBJECT_ATTRIBUTES, &key);
    if (NT_SUCCESS(status))
    {
        RtlInitUnicodeString(&unicodeValueName, valueName);
        status = WdfRegistryQueryULong(key, &unicodeValueName, Value);
        WdfRegistryClose(key);
    }

    return status;
}

_IRQL_requires_max_(PASSIVE_LEVEL) NTSTATUS GetScbFromAcpi(WDFDEVICE device, ULONG *scb)
{
    NTSTATUS status;
    WDFMEMORY inputMemory;
    WDF_MEMORY_DESCRIPTOR inputMemDesc;
    PACPI_EVAL_INPUT_BUFFER_COMPLEX inputBuffer;
    WDF_MEMORY_DESCRIPTOR outputMemDesc;
    PACPI_EVAL_OUTPUT_BUFFER outputBuffer = NULL;
    size_t outputBufferSize;
    size_t outputArgumentBufferSize;
    WDF_OBJECT_ATTRIBUTES attributes;
    WDF_REQUEST_SEND_OPTIONS sendOptions;

    PAGED_CODE();

    WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
    attributes.ParentObject = device;

    status = WdfMemoryCreate(&attributes, NonPagedPoolNx, 0, sizeof(ACPI_EVAL_INPUT_BUFFER_COMPLEX), &inputMemory,
                             (PVOID *)&inputBuffer);

    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Device=0x%p, WdfMemoryCreate failed (%Iu bytes) - %!STATUS!",
                    device, sizeof(ACPI_EVAL_INPUT_BUFFER_COMPLEX), status);
        goto Exit;
    }

    RtlZeroMemory(inputBuffer, sizeof(ACPI_EVAL_INPUT_BUFFER_COMPLEX));

    inputBuffer->Signature = ACPI_EVAL_INPUT_BUFFER_COMPLEX_SIGNATURE;
    inputBuffer->Size = (ULONG)sizeof(ACPI_EVAL_INPUT_BUFFER_COMPLEX);
    inputBuffer->ArgumentCount = 0;
    inputBuffer->MethodNameAsUlong = (ULONG)'_BCS';

    outputArgumentBufferSize = ACPI_METHOD_ARGUMENT_LENGTH(sizeof(ULONG));
    outputBufferSize = FIELD_OFFSET(ACPI_EVAL_OUTPUT_BUFFER, Argument) + outputArgumentBufferSize;

    outputBuffer = (PACPI_EVAL_OUTPUT_BUFFER)ExAllocatePool2(POOL_FLAG_NON_PAGED, outputBufferSize, 'SCBc');

    if (outputBuffer == NULL)
    {
        status = STATUS_INSUFFICIENT_RESOURCES;
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Device=0x%p ExAllocatePoolWithTag failed (%Iu bytes)", device,
                    outputBufferSize);
        goto Exit;
    }

    WDF_MEMORY_DESCRIPTOR_INIT_HANDLE(&inputMemDesc, inputMemory, NULL);
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&outputMemDesc, outputBuffer, (ULONG)outputBufferSize);

    WDF_REQUEST_SEND_OPTIONS_INIT(&sendOptions, WDF_REQUEST_SEND_OPTION_SYNCHRONOUS);
    WDF_REQUEST_SEND_OPTIONS_SET_TIMEOUT(&sendOptions, WDF_REL_TIMEOUT_IN_MS(3000));

    status = WdfIoTargetSendInternalIoctlSynchronously(WdfDeviceGetIoTarget(device), NULL, IOCTL_ACPI_EVAL_METHOD,
                                                       &inputMemDesc, &outputMemDesc, &sendOptions, NULL);

    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Device=0x%p IOCTL_ACPI_EVAL_METHOD failed - %!STATUS!", device,
                    status);
        goto Exit;
    }

    if (outputBuffer->Signature != ACPI_EVAL_OUTPUT_BUFFER_SIGNATURE)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Device=0x%p ACPI_EVAL_OUTPUT_BUFFER signature is incorrect",
                    device);
        status = STATUS_ACPI_INVALID_DATA;
        goto Exit;
    }

Exit:

    if (NT_SUCCESS(status) && scb != NULL && outputBuffer->Count == 1 && outputBuffer->Argument->DataLength > 0)
    {
        *scb = outputBuffer->Argument[0].Data[0];
    }

    if (inputMemory != WDF_NO_HANDLE)
    {
        WdfObjectDelete(inputMemory);
    }

    if (outputBuffer)
    {
        ExFreePoolWithTag(outputBuffer, 'SCBc');
    }

    return status;
}

/*!
    \brief A callback which is called when a connection to the target is initiated
    \param device WDF device object
    \param spbTarget SPB framework extension target object
    \return NTSTATUS
*/
_Use_decl_annotations_ NTSTATUS OnTargetConnect(WDFDEVICE device, SPBTARGET spbTarget)
{
    PAGED_CODE();

    NTSTATUS status = STATUS_SUCCESS;
    ULONG scb;
    USHORT i2cAddress = 0;

    // Get parameters (slave address, connection speed)

    SPB_CONNECTION_PARAMETERS params;
    SPB_CONNECTION_PARAMETERS_INIT(&params);

    SpbTargetGetConnectionParameters(spbTarget, &params);

    RH_QUERY_CONNECTION_PROPERTIES_OUTPUT_BUFFER *rhBuffer =
        (RH_QUERY_CONNECTION_PROPERTIES_OUTPUT_BUFFER *)params.ConnectionParameters;

    PNP_SERIAL_BUS_DESCRIPTOR *sbDescriptor = (PNP_SERIAL_BUS_DESCRIPTOR *)rhBuffer->ConnectionProperties;

    switch (sbDescriptor->SerialBusType)
    {
    case PNP_SERIAL_BUS_TYPE_I2C:
    {
        PNP_I2C_SERIAL_BUS_DESCRIPTOR *i2cDescriptor;
        if (rhBuffer->PropertiesLength < sizeof(*i2cDescriptor))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                        "Connection properties is too small for I2C: propertiesLength = %lu < %lu",
                        rhBuffer->PropertiesLength, sizeof(*i2cDescriptor));
            return STATUS_INVALID_PARAMETER;
        }

        i2cDescriptor = (PNP_I2C_SERIAL_BUS_DESCRIPTOR *)rhBuffer->ConnectionProperties;
        i2cAddress = i2cDescriptor->Address;
        break;
    }
    case PNP_SERIAL_BUS_TYPE_SPI:
    {
        PNP_SPI_SERIAL_BUS_DESCRIPTOR *spiDescriptor;
        if (rhBuffer->PropertiesLength < sizeof(*spiDescriptor))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                        "Connection properties is too small for SPI: propertiesLength = %lu < %lu",
                        rhBuffer->PropertiesLength, sizeof(*spiDescriptor));
            return STATUS_INVALID_PARAMETER;
        }

        // we do not currently need any info from it
        // spiDescriptor = (PNP_SPI_SERIAL_BUS_DESCRIPTOR *)rhBuffer->ConnectionProperties;
        break;
    }
    default:
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                    "ACPI Connnection descriptor is neither I2C nor SPI (SerialBusType = 0x%lx)",
                    sbDescriptor->SerialBusType);
        return STATUS_INVALID_PARAMETER;
    }

    // Get SCB value

#define LOAD_SCB_FROM_ACPI
#if defined(LOAD_SCB_FROM_REGISTRY)
    // Read settings from the registry
    // this #if is here for educational purposes and shows how to get SCB from the registry instead of ACPI
    status = GetDriverUlongParameter(WdfDeviceGetDriver(device), L"SCB", &scb);
#elif defined(LOAD_SCB_FROM_ACPI)
    status = GetScbFromAcpi(device, &scb);
#else
    // get SCB from RawDataBuffer
    scb = i2cDescriptor->Scb;
#endif
    if (!NT_SUCCESS(status) || scb > 1)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed to get SCB status=%!STATUS!, scb=%lu", status, scb);
        if (status == STATUS_OBJECT_NAME_NOT_FOUND)
        {
            // if we open a device, STATUS_OBJECT_NAME_NOT_FOUND might be misleading
            status = STATUS_INVALID_PARAMETER;
        }
        return status;
    }

    // Open USB IO target

    WDF_IO_TARGET_OPEN_PARAMS openParams;
    WDFIOTARGET ioTarget;

    DECLARE_CONST_CY_USB_DEVNAME(deviceName, scb);
    status = WdfIoTargetCreate(device, WDF_NO_OBJECT_ATTRIBUTES, &ioTarget);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed to create IO target %!STATUS!", status);
        return status;
    }

    WDF_IO_TARGET_OPEN_PARAMS_INIT_OPEN_BY_NAME(&openParams, &deviceName, STANDARD_RIGHTS_ALL);

    status = WdfIoTargetOpen(ioTarget, &openParams);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                    "Failed to open USB IO target (probably no device attached) %!STATUS!", status);
        return status;
    }

    // Fill in target context

    I2C_TARGET_CONTEXT *targetContext = TargetGetContext(spbTarget);
    targetContext->SerialBusType = sbDescriptor->SerialBusType;
    targetContext->Address = i2cAddress;
    targetContext->UsbIoTarget = ioTarget;

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "Connected to target (%p), address = 0x%lx, type id = 0x%lx",
                spbTarget, targetContext->Address, targetContext->SerialBusType);

    return STATUS_SUCCESS;
}

/*!
    \brief A callback which is called when a connection to the target is terminated
    \param device WDF device object
    \param spbTarget SPB framework extension target object
*/
_Use_decl_annotations_ VOID OnTargetDisconnect(WDFDEVICE device, SPBTARGET spbTarget)
{
    UNREFERENCED_PARAMETER(device);

    I2C_TARGET_CONTEXT *targetContext = TargetGetContext(spbTarget);
    WdfIoTargetClose(targetContext->UsbIoTarget);
}

// Helper functions

/*!
    \brief Calculates MDL total size
    \param mdl MDL
    \return total sizes of MDL
*/
size_t GetMdlTotalSize(PMDL mdl)
{
    size_t mdlByteCount = 0;
    for (PMDL curMdl = mdl; curMdl != NULL; curMdl = curMdl->Next)
    {
        mdlByteCount += MmGetMdlByteCount(curMdl);
    }
    return mdlByteCount;
}

/*!
    \brief Copies MDL data to WDFMEMORY object
    \param mdl MDL to copy data from
    \param memory WDFMEMORY object which contains enough space to hold MDL data
    \param offset offset in memory object to start copying the data
    \return NTSTATUS
*/
NTSTATUS CopyMdlToWdfMemory(PMDL mdl, WDFMEMORY memory, size_t offset)
{
    size_t memByteOffset = offset;
    NTSTATUS status = STATUS_SUCCESS;
    for (PMDL curMdl = mdl; curMdl != NULL; curMdl = curMdl->Next)
    {
        BYTE *mdlBuffer = MmGetSystemAddressForMdlSafe(curMdl, NormalPagePriority | MdlMappingNoExecute);
        size_t mdlByteCount = MmGetMdlByteCount(curMdl);

        status = WdfMemoryCopyFromBuffer(memory, memByteOffset, mdlBuffer, mdlByteCount);
        if (!NT_SUCCESS(status))
        {
            break;
        }

        memByteOffset += mdlByteCount;
    }

    return status;
}

// I2C read/write

/*!
    \brief Writes MDL to the device using bulk write pipe
    \param ioTarget io target object
    \param address I2C slave address
    \param scb of the I2C device on the whistle
    \param mdl MDL to write
    \param stop stop flag
    \param nak NACK flag
    \return NTSTATUS
*/
NTSTATUS WriteI2CMdlToDevice(WDFIOTARGET ioTarget, USHORT address, PMDL mdl, BOOLEAN stop, BOOLEAN nak,
                             ULONG_PTR *bytesTransferred)
{
    NTSTATUS status = STATUS_SUCCESS;
    WDFMEMORY memory = WDF_NO_HANDLE;
    size_t mdlTotalSize = GetMdlTotalSize(mdl);
    WriteTransferData transferData = {{address, stop, nak}, (USHORT)mdlTotalSize, {0}};
    const size_t payloadOffset = (size_t)&transferData.buffer - (size_t)&transferData;
    const size_t memorySize = payloadOffset + mdlTotalSize;

    status = WdfMemoryCreate(WDF_NO_OBJECT_ATTRIBUTES, NonPagedPool, 'i2cM', memorySize, &memory, NULL);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Allocate %llu bytes memory %!STATUS!", memorySize, status);
        return status;
    }

    status = WdfMemoryCopyFromBuffer(memory, 0, &transferData, payloadOffset);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed to copy transfer data at %llu: %!STATUS!", payloadOffset,
                    status);
        goto exit;
    }

    status = CopyMdlToWdfMemory(mdl, memory, payloadOffset);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed to copy MDL at %llu: %!STATUS!", payloadOffset, status);
        goto exit;
    }

    WDF_MEMORY_DESCRIPTOR memDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_HANDLE(&memDesc, memory, NULL);

    status =
        WdfIoTargetSendIoctlSynchronously(ioTarget, NULL, IOCTL_CY_I2C_WRITE, &memDesc, NULL, NULL, bytesTransferred);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed send IOCTL_CY_I2C_WRITE: %!STATUS!", status);
    }

exit:
    WdfObjectDelete(memory);
    return status;
}

/*!
    \brief Reads data from bulk read pipe to MDL
    \param ioTarget io target object
    \param address I2C slave address
    \param scb of the I2C device on the whistle
    \param mdl MDL to store data to
    \param stop stop flag
    \param nak NACK flag
    \return NTSTATUS
*/
NTSTATUS ReadI2CToMdlFromDevice(WDFIOTARGET ioTarget, USHORT address, PMDL mdl, BOOLEAN stop, BOOLEAN nak,
                                ULONG_PTR *bytesTransferred)
{
    NTSTATUS status = STATUS_SUCCESS;
    size_t mdlTotalSize = GetMdlTotalSize(mdl);
    TransferInfo transferInfo = {address, stop, nak};

    WDF_MEMORY_DESCRIPTOR infoMemDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&infoMemDesc, &transferInfo, sizeof(transferInfo));

    WDF_MEMORY_DESCRIPTOR bufferMemDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_MDL(&bufferMemDesc, mdl, (ULONG)mdlTotalSize);

    status = WdfIoTargetSendIoctlSynchronously(ioTarget, NULL, IOCTL_CY_I2C_READ, &infoMemDesc, &bufferMemDesc, NULL,
                                               bytesTransferred);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed send IOCTL_CY_I2C_READ: %!STATUS!", status);
    }
    return status;
}

#define CY_STOP TRUE
#define CY_NO_STOP FALSE

#define CY_ACK FALSE
#define CY_NACK TRUE

// SPI read/write

/*!
    \brief Reads and writes MDL to the device using bulk write pipe
    \param ioTarget io target object
    \param readMdl MDL to read to
    \param writeMdl MDL to write
    \return NTSTATUS
*/
NTSTATUS ReadWriteSPIMdlToDevice(WDFIOTARGET ioTarget, PMDL readMdl, PMDL writeMdl, ULONG_PTR *bytesTransferred)
{
    NTSTATUS status = STATUS_SUCCESS;
    size_t mdlTotalSize = GetMdlTotalSize(readMdl); // must be equal to GetMdlTotalSize(writeMdl)

    WDF_MEMORY_DESCRIPTOR readMemDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_MDL(&readMemDesc, readMdl, (ULONG)mdlTotalSize);

    WDF_MEMORY_DESCRIPTOR writeMemDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_MDL(&writeMemDesc, writeMdl, (ULONG)mdlTotalSize);

    status = WdfIoTargetSendIoctlSynchronously(ioTarget, NULL, IOCTL_CY_SPI_READWRITE, &writeMemDesc, &readMemDesc,
                                               NULL, bytesTransferred);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed send IOCTL_CY_SPI_READWRITE: %!STATUS!", status);
    }

    return status;
}

/*!
    \brief Process a sequence request by logging its content (for debug/logging purposes only)
    \param spbTarget SPB target object
    \param spbRequest SPB request object
    \param transferCount the number of transfers in the sequence
    \return NTSTATUS
*/
NTSTATUS ProcessSequence(SPBTARGET spbTarget, SPBREQUEST spbRequest, ULONG transferCount)
{
    I2C_TARGET_CONTEXT *targetContext = TargetGetContext(spbTarget);
    NTSTATUS status = STATUS_SUCCESS;
    ULONG_PTR totalBytesTransferred = 0;

    for (ULONG descriptorIndex = 0; descriptorIndex < transferCount; ++descriptorIndex)
    {
        PMDL mdl;
        ULONG_PTR bytesTransferred = 0;
        SPB_TRANSFER_DESCRIPTOR descriptor;
        SPB_TRANSFER_DESCRIPTOR_INIT(&descriptor);

        SpbRequestGetTransferParameters(spbRequest, descriptorIndex, &descriptor, &mdl);

        switch (descriptor.Direction)
        {
        case SpbTransferDirectionFromDevice:
            status = ReadI2CToMdlFromDevice(targetContext->UsbIoTarget, targetContext->Address, mdl, CY_STOP, CY_NACK,
                                            &bytesTransferred);
            break;
        case SpbTransferDirectionToDevice:
            status = WriteI2CMdlToDevice(targetContext->UsbIoTarget, targetContext->Address, mdl, CY_STOP, CY_ACK,
                                         &bytesTransferred);
            break;
        default:
            break;
        }
        totalBytesTransferred += bytesTransferred;
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed %s MDL to the device: %!STATUS!",
                        DirectionToString(descriptor.Direction), status);
            break;
        }
    }

    WdfRequestSetInformation((WDFREQUEST)spbRequest, totalBytesTransferred);
    return status;
}

/*!
    \brief A callback which is called when a write request is sent to the driver
    \param device WDF device object
    \param spbTarget SPB framework extension target object
    \param spbRequest SPB framework extension request object
    \param length the number of bytes in the buffer to write
*/
_Use_decl_annotations_ VOID OnWrite(WDFDEVICE device, SPBTARGET spbTarget, SPBREQUEST spbRequest, size_t length)
{
    UNREFERENCED_PARAMETER(device);
    UNREFERENCED_PARAMETER(length);

    NTSTATUS status = ProcessSequence(spbTarget, spbRequest, 1);

    SpbRequestComplete(spbRequest, status);
}

/*!
    \brief A callback which is called when a read request is sent to the driver
    \param device WDF device object
    \param spbTarget SPB framework extension target object
    \param spbRequest SPB framework extension request object
    \param length the number of bytes in the buffer for read
*/
_Use_decl_annotations_ VOID OnRead(WDFDEVICE device, SPBTARGET spbTarget, SPBREQUEST spbRequest, size_t length)
{
    UNREFERENCED_PARAMETER(device);
    UNREFERENCED_PARAMETER(spbTarget);
    UNREFERENCED_PARAMETER(length);

    NTSTATUS status = ProcessSequence(spbTarget, spbRequest, 1);

    SpbRequestComplete(spbRequest, status);
}

/*!
    \brief A callback which is called when a write request is sent to the driver
    \param device WDF device object
    \param spbTarget SPB framework extension target object
    \param spbRequest SPB framework extension request object
    \param transferCount the number of transfers
*/
_Use_decl_annotations_ VOID OnSequence(WDFDEVICE device, SPBTARGET spbTarget, SPBREQUEST spbRequest,
                                       ULONG transferCount)
{
    UNREFERENCED_PARAMETER(device);

    NTSTATUS status = ProcessSequence(spbTarget, spbRequest, transferCount);

    SpbRequestComplete(spbRequest, status);
}

VOID OnOtherInCallerContext(_In_ WDFDEVICE SpbController, _In_ WDFREQUEST FxRequest)
/*++

  Routine Description:

        This routine preprocesses custom IO requests before the framework
        places them in an IO queue. For requests using the SPB transfer list
        format, it calls SpbRequestCaptureIoOtherTransferList to capture the
        client's buffers.

  Arguments:

        SpbController - a handle to the framework device object
                representing an SPB controller
        SpbRequest - a handle to the SPBREQUEST object

  Return Value:

        None.  The request is either completed or enqueued asynchronously.

--*/
{
    NTSTATUS status = STATUS_NOT_SUPPORTED;

    //
    // Check for custom IOCTLs that this driver handles. If
    // unrecognized mark as STATUS_NOT_SUPPORTED and complete.
    //

    WDF_REQUEST_PARAMETERS fxParams;
    WDF_REQUEST_PARAMETERS_INIT(&fxParams);

    WdfRequestGetParameters(FxRequest, &fxParams);

    if ((fxParams.Type != WdfRequestTypeDeviceControl) && (fxParams.Type != WdfRequestTypeDeviceControlInternal))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Unsupported request type");
        goto exit;
    }

    if (fxParams.Parameters.DeviceIoControl.IoControlCode != IOCTL_SPB_FULL_DUPLEX)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Not supported IOCTL");
        goto exit;
    }

    //
    // For custom IOCTLs that use the SPB transfer list format
    // (i.e. sequence formatting), call SpbRequestCaptureIoOtherTransferList
    // so that the driver can leverage other SPB DDIs for this request.
    //

    status = SpbRequestCaptureIoOtherTransferList((SPBREQUEST)FxRequest);

    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "SpbRequestCaptureIoOtherTransferList failed, %!STATUS!", status);
        goto exit;
    }

    //
    // Preprocessing has succeeded, enqueue the request.
    //

    status = WdfDeviceEnqueueRequest(SpbController, FxRequest);

    if (!NT_SUCCESS(status))
    {
        goto exit;
    }

exit:

    if (!NT_SUCCESS(status))
    {
        WdfRequestComplete(FxRequest, status);
    }
}

VOID OnOther(_In_ WDFDEVICE spbController, _In_ SPBTARGET spbTarget, _In_ SPBREQUEST spbRequest,
             _In_ size_t outputBufferLength, _In_ size_t inputBufferLength, _In_ ULONG ioControlCode)
/*++

  Routine Description:

        This routine processes custom IO requests that are not natively
        supported by the SPB framework extension. For requests using the
        SPB transfer list format, SpbRequestCaptureIoOtherTransferList
        must have been called in the driver's OnOtherInCallerContext routine.

  Arguments:

        SpbController - a handle to the framework device object
                representing an SPB controller
        SpbTarget - a handle to the SPBTARGET object
        SpbRequest - a handle to the SPBREQUEST object
        OutputBufferLength - the request's output buffer length
        InputBufferLength - the requests input buffer length
        IoControlCode - the device IO control code

  Return Value:

        None.  The request is completed asynchronously.

--*/
{
    NTSTATUS status = STATUS_INVALID_DEVICE_REQUEST;

    UNREFERENCED_PARAMETER(spbController);
    UNREFERENCED_PARAMETER(inputBufferLength);
    UNREFERENCED_PARAMETER(outputBufferLength);
    UNREFERENCED_PARAMETER(ioControlCode);

    //
    // Tthe driver should take the following steps
    //
    //    1. Verify this specific DeviceIoContol code is supported,
    //       otherwise mark as STATUS_NOT_SUPPORTED and complete.
    //
    //    2. If this IOCTL uses SPB_TRANSFER_LIST and the driver has
    //       called SpbRequestCaptureIoOtherTransferList previously,
    //       validate the request format. The driver can make use of
    //       SpbRequestGetTransferParameters to retrieve each transfer
    //       descriptor.
    //
    //       If this IOCTL uses some proprietary buffer formating
    //       instead of SPB_TRANSFER_LIST, validate appropriately.
    //
    //    3. Setup the device, target, and request contexts as necessary,
    //       and program the hardware for the transfer.
    //

    I2C_TARGET_CONTEXT *targetContext = TargetGetContext(spbTarget);
    ULONG_PTR bytesTransferred = 0;

    // we process only one IOCTL
    if (targetContext->SerialBusType != PNP_SERIAL_BUS_TYPE_SPI)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Controller must be SPI for IOCTL_SPB_FULL_DUPLEX");
        goto exit;
    }

    status = STATUS_INVALID_PARAMETER; // for future gotos

    enum DuplexIndex
    {
        DUPLEX_INDEX_WRITE, // first must be write (SpbTransferDirectionToDevice)
        DUPLEX_INDEX_READ,  // second must be read (SpbTransferDirectionFromDevice)
        DUPLEX_TRANSFER_COUNT
    };

    SPB_REQUEST_PARAMETERS parameters;
    SPB_REQUEST_PARAMETERS_INIT(&parameters);
    SpbRequestGetParameters(spbRequest, &parameters);
    if (parameters.SequenceTransferCount != DUPLEX_TRANSFER_COUNT)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "IOCTL_SPB_FULL_DUPLEX: %d transfers expected (%d given)",
                    DUPLEX_TRANSFER_COUNT, parameters.SequenceTransferCount);
        goto exit;
    }

    PMDL writeMdl;
    SPB_TRANSFER_DESCRIPTOR writeDescriptor;
    SPB_TRANSFER_DESCRIPTOR_INIT(&writeDescriptor);
    SpbRequestGetTransferParameters(spbRequest, DUPLEX_INDEX_WRITE, &writeDescriptor, &writeMdl);

    if (writeDescriptor.Direction != SpbTransferDirectionToDevice)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                    "Direction of the first transfer must be SpbTransferDirectionToDevice");
        goto exit;
    }

    PMDL readMdl;
    SPB_TRANSFER_DESCRIPTOR readDescriptor;
    SPB_TRANSFER_DESCRIPTOR_INIT(&readDescriptor);
    SpbRequestGetTransferParameters(spbRequest, DUPLEX_INDEX_READ, &readDescriptor, &readMdl);

    if (readDescriptor.Direction != SpbTransferDirectionFromDevice)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                    "Direction of the second transfer must be SpbTransferDirectionFromDevice");
        goto exit;
    }

    status = ReadWriteSPIMdlToDevice(targetContext->UsbIoTarget, readMdl, writeMdl, &bytesTransferred);

exit:
    WdfRequestSetInformation((WDFREQUEST)spbRequest, bytesTransferred);
    SpbRequestComplete(spbRequest, status);
}

/*!
    \brief Worker routine called to create a device and its software resources
    \param deviceInit Pointer to an opaque init structure. Memory for this
                    structure will be freed by the framework when the WdfDeviceCreate
                    succeeds. So don't access the structure after that point
    \return NTSTATUS
*/
NTSTATUS
SpbctlCreateDevice(_Inout_ PWDFDEVICE_INIT deviceInit)
{
    WDFDEVICE device;
    NTSTATUS status;

    PAGED_CODE();

    // Init SPB framework extension

    status = SpbDeviceInitConfig(deviceInit);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "SpbDeviceInitConfig failed with status %!STATUS!", status);
        return status;
    }

    // Create device object

    status = WdfDeviceCreate(&deviceInit, WDF_NO_OBJECT_ATTRIBUTES, &device);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfDeviceCreate failed with status %!STATUS!", status);
        return status;
    }

    // Allow the device to be disabled

    {
        WDF_DEVICE_STATE deviceState;
        WDF_DEVICE_STATE_INIT(&deviceState);

        deviceState.NotDisableable = WdfFalse;
        WdfDeviceSetDeviceState(device, &deviceState);
    }

    {
        SPB_CONTROLLER_CONFIG spbConfig;
        SPB_CONTROLLER_CONFIG_INIT(&spbConfig);

        //
        // Register for target connect callback.  The driver
        // does not need to respond to target disconnect.
        //

        spbConfig.EvtSpbTargetConnect = OnTargetConnect;
        spbConfig.EvtSpbTargetDisconnect = OnTargetDisconnect;

        //
        // Register for IO callbacks.
        //

        spbConfig.ControllerDispatchType = WdfIoQueueDispatchSequential;
        spbConfig.EvtSpbIoRead = OnRead;
        spbConfig.EvtSpbIoWrite = OnWrite;
        spbConfig.EvtSpbIoSequence = OnSequence;

        status = SpbDeviceInitialize(device, &spbConfig);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                        "SpbDeviceInitialize failed. (wdfDevice = %p, status = %!STATUS!)", device, status);
            return status;
        }

        SpbControllerSetIoOtherCallback(device, OnOther, OnOtherInCallerContext);
    }

    //
    // Set target object attributes.
    //
    {
        WDF_OBJECT_ATTRIBUTES targetAttributes;
        WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&targetAttributes, I2C_TARGET_CONTEXT);

        SpbControllerSetTargetAttributes(device, &targetAttributes);
    }

    return status;
}
