#pragma once

#include <ntddk.h>
#include <wdf.h>
#define RESHUB_USE_HELPER_ROUTINES
#include <reshub.h>

#include "Trace.h"

NTSTATUS
ConnectionOpen(WDFDEVICE device, LARGE_INTEGER connectionId, WDFIOTARGET *target);

VOID ConnectionClose(WDFIOTARGET target);
