#include "rhhelpers.h"
#include "rhhelpers.tmh"

/*!
    \brief This routine opens a handle to a resource hub connection.
    \param device WDF device object
    \param connectionId connection ID for resource hub
    \param target opened io target object
    \return NTSTATUS
*/
NTSTATUS ConnectionOpen(WDFDEVICE device, LARGE_INTEGER connectionId, WDFIOTARGET *target)
{
    PAGED_CODE();

    NTSTATUS status;
    WDF_IO_TARGET_OPEN_PARAMS openParams;

    // Create the device path using the connection ID.
    DECLARE_UNICODE_STRING_SIZE(devicePath, RESOURCE_HUB_PATH_SIZE);

    RESOURCE_HUB_CREATE_PATH_FROM_ID(&devicePath, connectionId.LowPart, connectionId.HighPart);

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "Opening handle to connection target via %wZ", &devicePath);

    status = WdfIoTargetCreate(device, WDF_NO_OBJECT_ATTRIBUTES, target);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfIoTargetCreate failed - %!STATUS!", status);
        goto Exit;
    }

    // Open a handle to the I2C controller.
    WDF_IO_TARGET_OPEN_PARAMS_INIT_OPEN_BY_NAME(&openParams, &devicePath, (GENERIC_READ | GENERIC_WRITE));

    openParams.ShareAccess = 0;
    openParams.CreateDisposition = FILE_OPEN;
    openParams.FileAttributes = FILE_ATTRIBUTE_NORMAL;

    status = WdfIoTargetOpen(*target, &openParams);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed to open connection I/O target - %!STATUS!", status);
        goto Exit;
    }

Exit:
    return status;
}

/*!
    \brief This routine closes a handle to a resource hub connection.
    \param target opened io target object
*/
void ConnectionClose(WDFIOTARGET target)
{
    PAGED_CODE();

    if (target != WDF_NO_HANDLE)
    {
        TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "Closing handle to a target");
        WdfIoTargetClose(target);
    }
}
