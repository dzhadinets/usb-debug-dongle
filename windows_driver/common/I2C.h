#pragma once

#include <ntddk.h>
#include <wdf.h>
#include <spb.h>

#include "Trace.h"

#define REGISTER_ADDR_SIZE (sizeof(UINT8))
#define I2C_READ_TRANSFER_COUNT 2

#define I2C_SYNCHRONOUS_TIMEOUT 300

#define SOME_WRITE_REGISTER 0x11
#define SOME_READ_REGISTER 0x12

#define I2C_BUFFER_SIZE 50

NTSTATUS
I2CWriteAsynchronously(WDFIOTARGET i2cTarget, UINT8 registerAddress, _In_reads_bytes_(length) PVOID data,
                       _In_ size_t length, _In_opt_ PFN_WDF_REQUEST_COMPLETION_ROUTINE completionRoutine,
                       _In_opt_ __drv_aliasesMem WDFCONTEXT cmpletionContext);

NTSTATUS
I2CWriteSynchronously(WDFIOTARGET i2cTarget, UINT8 registerAddress, _In_reads_bytes_(length) PVOID data,
                      _In_ size_t length);

NTSTATUS
I2CReadAsynchronously(WDFIOTARGET i2cTarget, UINT8 registerAddress, _In_reads_bytes_(length) PVOID data,
                      _In_ size_t length, _In_opt_ PFN_WDF_REQUEST_COMPLETION_ROUTINE completionRoutine,
                      _In_opt_ __drv_aliasesMem WDFCONTEXT completionContext);

NTSTATUS
I2CReadSynchronously(WDFIOTARGET i2cTarget, UINT8 registerAddress, _In_reads_bytes_(length) PVOID data,
                     _In_ size_t length);
