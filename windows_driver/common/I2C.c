#include "I2C.h"
#include "I2C.tmh"

/*!
    \brief Sends data to the I2C controller to write to the specified register on the port controller hardware.
    \param i2cTarget io target object
    \param registerAddress register that should be written
    \param data data to write
    \param length length of the data
    \param completionRoutine completion routine that will be executed when the request is completed
    \param completionContext context parameter to completionRoutine
    \return NTSTATUS
*/
NTSTATUS
I2CWriteAsynchronously(WDFIOTARGET i2cTarget, UINT8 registerAddress, _In_reads_bytes_(length) PVOID data,
                       _In_ size_t length, _In_opt_ PFN_WDF_REQUEST_COMPLETION_ROUTINE completionRoutine,
                       _In_opt_ __drv_aliasesMem WDFCONTEXT completionContext)
{
    NTSTATUS status;
    WDFREQUEST request;
    WDF_OBJECT_ATTRIBUTES attributes;
    WDFMEMORY memory;
    BYTE *buffer = NULL;

    if (length == 0)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Parameter 'Length' cannot be 0.");
        status = STATUS_INVALID_PARAMETER;
        goto Exit;
    }

    status = WdfRequestCreate(WDF_NO_OBJECT_ATTRIBUTES, i2cTarget, &request);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfRequestCreate failed with status %!STATUS!", status);
        goto Exit;
    }

    WdfRequestSetCompletionRoutine(request, completionRoutine, completionContext);

    WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
    attributes.ParentObject = request;

    status = WdfMemoryCreate(&attributes, NonPagedPoolNx, 'I2Cm', REGISTER_ADDR_SIZE + length, &memory, &buffer);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfMemoryCreate failed with status %!STATUS!", status);
        WdfObjectDelete(request);
        goto Exit;
    }

    buffer[0] = registerAddress;
    RtlCopyMemory(&buffer[REGISTER_ADDR_SIZE], data, length);

    status = WdfIoTargetFormatRequestForWrite(i2cTarget, request, memory, NULL, NULL);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfIoTargetFormatRequestForWrite failed with status %!STATUS!",
                    status);
        WdfObjectDelete(request);
        goto Exit;
    }

    // Send the request to the I2C I/O Target.
    if (WdfRequestSend(request, i2cTarget, NULL) == FALSE)
    {
        status = WdfRequestGetStatus(request);
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                    "[WDFREQUEST: 0x%p] WdfRequestSend for I2C write failed with status %!STATUS!", request, status);
        WdfObjectDelete(request);
        goto Exit;
    }

Exit:
    return status;
}

/*!
    \brief Sends data to the I2C controller to write to the specified register on the port controller hardware
    and waits for result.
    \param i2cTarget io target object
    \param registerAddress register that should be written
    \param data data to write
    \param length length of the data
    \return NTSTATUS
*/
NTSTATUS
I2CWriteSynchronously(WDFIOTARGET i2cTarget, UINT8 registerAddress, _In_reads_bytes_(length) PVOID data,
                      _In_ size_t length)
{
    NTSTATUS status;
    WDFREQUEST request = WDF_NO_HANDLE;
    WDF_MEMORY_DESCRIPTOR memoryDescriptor;
    ULONG_PTR bytesTransferred = 0;
    WDF_REQUEST_SEND_OPTIONS requestOptions;
    BYTE buffer[I2C_BUFFER_SIZE] = {0};

    if (length == 0)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Parameter 'Length' cannot be 0.");
        status = STATUS_INVALID_PARAMETER;
        goto Exit;
    }

    if (length + REGISTER_ADDR_SIZE > sizeof(buffer))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Unexpected value of length. Length: %llu. Size of buffer: %llu",
                    length, sizeof(buffer));
        status = STATUS_INVALID_PARAMETER;
        goto Exit;
    }

    status = WdfRequestCreate(WDF_NO_OBJECT_ATTRIBUTES, i2cTarget, &request);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfRequestCreate failed with status %!STATUS!", status);
        goto Exit;
    }

    buffer[0] = registerAddress;
    RtlCopyMemory(&buffer[REGISTER_ADDR_SIZE], data, length);

    // Initialize the memory descriptor with the transfer list.
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memoryDescriptor, &buffer, (ULONG)(REGISTER_ADDR_SIZE + length));

    WDF_REQUEST_SEND_OPTIONS_INIT(&requestOptions, WDF_REQUEST_SEND_OPTION_TIMEOUT);
    requestOptions.Timeout = WDF_REL_TIMEOUT_IN_MS(I2C_SYNCHRONOUS_TIMEOUT);

    status = WdfIoTargetSendWriteSynchronously(i2cTarget, request, &memoryDescriptor, NULL, &requestOptions,
                                               &bytesTransferred);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfIoTargetSendWriteSynchronously failed with status %!STATUS!",
                    status);
        WdfObjectDelete(request);
        goto Exit;
    }

Exit:
    return status;
}

/*!
    \brief Asynchronously reads data from the port controller's registers over the I2C controller.
    \param i2cTarget io target object
    \param registerAddress register address that should be read
    \param data data buffer to read to
    \param length length of the data buffer
    \param completionRoutine completion routine that will be executed when the request is completed
    \param completionContext context parameter to completionRoutine
    \return NTSTATUS
*/
NTSTATUS
I2CReadAsynchronously(WDFIOTARGET i2cTarget, UINT8 registerAddress, _In_reads_bytes_(length) PVOID data,
                      _In_ size_t length, _In_opt_ PFN_WDF_REQUEST_COMPLETION_ROUTINE completionRoutine,
                      _In_opt_ __drv_aliasesMem WDFCONTEXT completionContext)
{
    NTSTATUS status;
    WDFREQUEST request;
    WDF_OBJECT_ATTRIBUTES attributes;
    WDFMEMORY memory;
    SPB_TRANSFER_LIST_AND_ENTRIES(I2C_READ_TRANSFER_COUNT) transferList;

    // Static analysis cannot figure out the SPB_TRANSFER_LIST_ENTRY
    // size but using an index variable quiets the warning.
    ULONG index = 0;

    if (length == 0)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Parameter 'Length' cannot be 0.");
        status = STATUS_INVALID_PARAMETER;
        goto Exit;
    }

    status = WdfRequestCreate(WDF_NO_OBJECT_ATTRIBUTES, i2cTarget, &request);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfRequestCreate failed with status %!STATUS!", status);
        goto Exit;
    }

    WdfRequestSetCompletionRoutine(request, completionRoutine, completionContext);

    SPB_TRANSFER_LIST_INIT(&(transferList.List), I2C_READ_TRANSFER_COUNT);

    // Static analysis can't figure out the relationship between the transfer array size
    // and the transfer count.
    _Analysis_assume_(ARRAYSIZE(transferList.List.Transfers) == I2C_READ_TRANSFER_COUNT);

    transferList.List.Transfers[index] =
        SPB_TRANSFER_LIST_ENTRY_INIT_SIMPLE(SpbTransferDirectionToDevice, 0, &registerAddress, REGISTER_ADDR_SIZE);

    transferList.List.Transfers[index + 1] =
        SPB_TRANSFER_LIST_ENTRY_INIT_SIMPLE(SpbTransferDirectionFromDevice, 0, data, (ULONG)length);

    WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
    attributes.ParentObject = request;
    status = WdfMemoryCreate(&attributes, NonPagedPoolNx, 'i2cC', sizeof(transferList), &memory, NULL);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfMemoryCreate failed with status %!STATUS!", status);
        WdfObjectDelete(request);
        goto Exit;
    }

    status = WdfMemoryAssignBuffer(memory, (PVOID)&transferList, sizeof(transferList));
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfMemoryAssignBuffer failed with status %!STATUS!", status);
        WdfObjectDelete(request);
        goto Exit;
    }

    status = WdfIoTargetFormatRequestForIoctl(i2cTarget, request, IOCTL_SPB_EXECUTE_SEQUENCE, memory, NULL, NULL, NULL);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfIoTargetFormatRequestForIoctl failed with status %!STATUS!",
                    status);
        WdfObjectDelete(request);
        goto Exit;
    }

    // Send the request to the I2C I/O Target.
    if (WdfRequestSend(request, i2cTarget, NULL) == FALSE)
    {
        status = WdfRequestGetStatus(request);
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                    "[WDFREQUEST: 0x%p] WdfRequestSend for I2C read failed with status %!STATUS!", request, status);
        WdfObjectDelete(request);
        goto Exit;
    }

Exit:
    return status;
}

/*!
    \brief Synchronously reads data from the port controller's registers over the I2C controller
        for a request originating from the client driver.
    \param i2cTarget io target object
    \param registerAddress register address that should be read
    \param data data buffer to read to
    \param length length of the data buffer
    \return NTSTATUS
*/
NTSTATUS
I2CReadSynchronously(WDFIOTARGET i2cTarget, UINT8 registerAddress, _In_reads_bytes_(length) PVOID data,
                     _In_ size_t length)
{
    NTSTATUS status;
    WDFREQUEST request;
    WDF_REQUEST_SEND_OPTIONS requestOptions;
    ULONG_PTR bytesTransferred = 0;
    WDF_MEMORY_DESCRIPTOR memoryDescriptor;
    SPB_TRANSFER_LIST_AND_ENTRIES(I2C_READ_TRANSFER_COUNT) transferList;

    // Static analysis cannot figure out the SPB_TRANSFER_LIST_ENTRY
    // size but using an index variable quiets the warning.
    ULONG index = 0;

    if (length == 0)
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Parameter 'Length' cannot be 0.");
        status = STATUS_INVALID_PARAMETER;
        goto Exit;
    }

    status = WdfRequestCreate(WDF_NO_OBJECT_ATTRIBUTES, i2cTarget, &request);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "WdfRequestCreate failed with status %!STATUS!", status);
        goto Exit;
    }

    SPB_TRANSFER_LIST_INIT(&(transferList.List), I2C_READ_TRANSFER_COUNT);

    // Static analysis can't figure out the relationship between the transfer array size
    // and the transfer count.
    _Analysis_assume_(ARRAYSIZE(transferList.List.Transfers) == I2C_READ_TRANSFER_COUNT);

    transferList.List.Transfers[index] =
        SPB_TRANSFER_LIST_ENTRY_INIT_SIMPLE(SpbTransferDirectionToDevice, 0, &registerAddress, REGISTER_ADDR_SIZE);

    transferList.List.Transfers[index + 1] =
        SPB_TRANSFER_LIST_ENTRY_INIT_SIMPLE(SpbTransferDirectionFromDevice, 0, data, (ULONG)length);

    // Initialize the memory descriptor with the transfer list.
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memoryDescriptor, &transferList, sizeof(transferList));

    WDF_REQUEST_SEND_OPTIONS_INIT(&requestOptions, WDF_REQUEST_SEND_OPTION_TIMEOUT);
    requestOptions.Timeout = WDF_REL_TIMEOUT_IN_MS(I2C_SYNCHRONOUS_TIMEOUT);

    // Send the request to the I2C I/O Target.
    status = WdfIoTargetSendIoctlSynchronously(i2cTarget, request, IOCTL_SPB_EXECUTE_SEQUENCE, &memoryDescriptor, NULL,
                                               &requestOptions, &bytesTransferred);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                    "[WDFREQUEST: 0x%p] WdfIoTargetSendIoctlSynchronously failed with status %!STATUS!", request,
                    status);
        goto Exit;
    }

Exit:
    return status;
}
