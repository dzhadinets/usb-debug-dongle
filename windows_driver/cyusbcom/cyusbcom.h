#pragma once

#include <vector>

typedef const char *CyUsbDeviceDescriptor;

// This workaround is implemented because if we include windows.h, UI related modules will start popping a lot of
// compile errors. It should be removed when I figure out how to avoid that.
#ifndef HANDLE
typedef void *HANDLE;
#endif

class CyDevice
{
    HANDLE _device;

public:
    using ConfigBlob = std::vector<uint8_t>;
    using Buffer = std::vector<uint8_t>;

    /*!
        \brief Creates a CyDevice not related to any device
    */
    CyDevice();

    /*!
        \brief Creates a CyDevice corresponding to a descriptor
        \param descriptor of a device to open
    */
    CyDevice(CyUsbDeviceDescriptor descriptor);

    /*!
        \brief Move assignment operator
    */
    CyDevice &operator=(CyDevice &&other) noexcept;

    /*!
        \brief Check if device is opened
        \return true if device is valid and can be used
    */
    operator bool() const;

    ~CyDevice();

    /*!
        \brief Requests SCB number of the device (0 or 1)
        \return SCB number
    */
    unsigned char getScb() const;

    /*!
        \brief Reads configuration from a device
        \param descriptor A device descriptor from devices() vector.
        \return Config blob
    */
    ConfigBlob readConfig() const;

    /*!
        \brief Programs a device with a config. The config should be valid,
        otherwise an error will be returned from the driver.
        \param descriptor A device descriptor from devices() vector.
        \param config config blob
    */
    void writeConfig(ConfigBlob &config) const;

    /*!
        \brief Write one byte to I2C device
        \param addess address of I2C device
        \param value byte to write
        \param stop stop bit
        \param nak nak bit
    */
    void i2cWriteByte(uint16_t address, uint8_t value, uint8_t stop = 1, uint8_t nak = 0) const;

    /*!
        \brief Read one byte to I2C device
        \param addess address of I2C device
        \param stop stop bit
        \param nak nak bit
        \return a byte read from the device
    */
    uint8_t i2cReadByte(uint16_t address, uint8_t stop = 1, uint8_t nak = 1) const;

    /*!
        \brief Read GPOI value
        \param number number of GPIO
        \return the value of GPIO
    */
    uint16_t gpioRead(uint8_t number) const;
};

/*!
    \brief Class which responsibe for communication with Cy devices
*/
class CyDeviceManager
{
    // null-separated string containig all driver interfaces
    std::vector<char> _devIfList;

    // array of device interface descriptors (essentially strings)
    std::vector<CyUsbDeviceDescriptor> _devices;

    /*!
        \brief Enumerates Cy devices and stores them in internal fields
    */
    void scanDevices();

public:
    /*!
        \brief Constructs an object. During construction, it enumerates devices.
    */
    CyDeviceManager();

    /*!
        \brief Returns a vector of device descriptors which can be accessed using
        functions below.
    */
    const std::vector<CyUsbDeviceDescriptor> &devices() const;

    /*!
        \brief Returns a unique name of a device that can be displayes (e.g. in a list)
        \param descriptor device descriptor which can be aquired using devices() call
        \return string representation
    */
    const char *friendlyName(CyUsbDeviceDescriptor descriptor) const;

    /*!
        \brief Creates a CyDevice corresponding to a descriptor
        \param descriptor
        \return CyDevice object
    */
    CyDevice open(CyUsbDeviceDescriptor descriptor) const;
};
