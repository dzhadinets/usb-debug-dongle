#include <cyusbcom.h>

#include <wrl.h>

#include <string>
#include <stdexcept>

#include <Windows.h>
#include <cfgmgr32.h> // CM_* functions

#include <initguid.h> // if we do not include it, link will fail to find GUID_DEVINTERFACE_spbclient
#include <winioctl.h>
#include "..\sqcyusb\Public.h"

#include <cy7_structs.h> // struct cy7_device_config

using Microsoft::WRL::Wrappers::FileHandle;

#define CY_CONFIG_SIZE (sizeof(struct cy7_device_config))

namespace
{
HANDLE openDevice(CyUsbDeviceDescriptor descriptor)
{
    HANDLE dev = CreateFile(descriptor, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                            nullptr,       // no SECURITY_ATTRIBUTES structure
                            OPEN_EXISTING, // No special create flags
                            0,             // No special attributes
                            nullptr);      // No template file
    if (dev == INVALID_HANDLE_VALUE)
    {
        auto err = GetLastError();
        throw std::runtime_error("failed open device, error " + std::to_string(err));
    }
    return dev;
}
} // namespace

CyDevice::CyDevice()
    : _device(INVALID_HANDLE_VALUE)
{
}

CyDevice::CyDevice(CyUsbDeviceDescriptor descriptor)
    : _device(openDevice(descriptor))
{
}

CyDevice &CyDevice::operator=(CyDevice &&other) noexcept
{
    _device = other._device;
    other._device = INVALID_HANDLE_VALUE;
    return *this;
}

CyDevice::operator bool() const
{
    return _device != INVALID_HANDLE_VALUE;
}

CyDevice::~CyDevice()
{
    // the handle must be valid, otherwise the object won't be created
    CloseHandle(_device);
}

unsigned char CyDevice::getScb() const
{
    BYTE scb;
    DWORD bytes;
    if (!DeviceIoControl(_device, IOCTL_CY_GET_SCB, nullptr, 0, &scb, sizeof(scb), &bytes, nullptr))
    {
        auto err = GetLastError();
        throw std::runtime_error("failed to IOCTL_CY_GET_SCB, error " + std::to_string(err));
    }
    return scb;
}

CyDevice::ConfigBlob CyDevice::readConfig() const
{
    ConfigBlob config(CY_CONFIG_SIZE, 0);
    DWORD bytes;
    if (!DeviceIoControl(_device, IOCTL_CY_READ_CONFIG, nullptr, 0, config.data(), CY_CONFIG_SIZE, &bytes, nullptr))
    {
        auto err = GetLastError();
        throw std::runtime_error("failed to IOCTL_CY_READ_CONFIG, error " + std::to_string(err));
    }
    return config;
}

void CyDevice::writeConfig(CyDevice::ConfigBlob &config) const
{
    DWORD bytes;
    if (!DeviceIoControl(_device, IOCTL_CY_WRITE_CONFIG, config.data(), CY_CONFIG_SIZE, nullptr, 0, &bytes, nullptr))
    {
        auto err = GetLastError();
        throw std::runtime_error("failed to IOCTL_CY_WRITE_CONFIG, error " + std::to_string(err));
    }
}

void CyDevice::i2cWriteByte(uint16_t address, uint8_t value, uint8_t stop, uint8_t nak) const
{
    DWORD bytes = 0;
    WriteTransferData transferData = {
        {address, stop, nak}, // transfer info
        1,                    // buffer length
        {value}               // buffer
    };
    if (!DeviceIoControl(_device, IOCTL_CY_I2C_WRITE, &transferData, sizeof(transferData), nullptr, 0, &bytes, nullptr))
    {
        auto err = GetLastError();
        throw std::runtime_error("failed to IOCTL_CY_I2C_WRITE, error " + std::to_string(err));
    }
}

uint8_t CyDevice::i2cReadByte(uint16_t address, uint8_t stop, uint8_t nak) const
{
    DWORD bytes = 0;
    TransferInfo transferInfo = {address, stop, nak};
    BYTE buffer[1];
    if (!DeviceIoControl(_device, IOCTL_CY_I2C_READ, &transferInfo, sizeof(transferInfo), buffer, sizeof(buffer),
                         &bytes, nullptr))
    {
        auto err = GetLastError();
        throw std::runtime_error("failed to IOCTL_CY_I2C_READ, error " + std::to_string(err));
    }

    return buffer[0];
}

uint16_t CyDevice::gpioRead(uint8_t number) const
{
    DWORD bytes = 0;
    uint16_t buffer[1];
    if (!DeviceIoControl(_device, IOCTL_CY_GPIO_READ, &number, sizeof(number), buffer, sizeof(buffer), &bytes, nullptr))
    {
        auto err = GetLastError();
        throw std::runtime_error("failed to IOCTL_CY_GPIO_READ, error " + std::to_string(err));
    }

    return buffer[0];
}

void CyDeviceManager::scanDevices()
{
    CONFIGRET cr = CR_SUCCESS;
    ULONG deviceInterfaceListLength = 0;
    GUID interfaceGuid = GUID_DEVINTERFACE_sqcyusb;

    // Get the length of null-separted list of all interfaces
    cr = CM_Get_Device_Interface_List_Size(&deviceInterfaceListLength, &interfaceGuid, NULL,
                                           CM_GET_DEVICE_INTERFACE_LIST_PRESENT);
    if (cr != CR_SUCCESS)
    {
        throw std::runtime_error("failed to get interfaces list size, error " + std::to_string(cr));
    }

    // Get null-separted list of all interfaces. In our case there should be only one of them.
    std::vector<char> deviceInterfaceList(deviceInterfaceListLength, 0);
    cr = CM_Get_Device_Interface_List(&interfaceGuid, NULL, deviceInterfaceList.data(), deviceInterfaceListLength,
                                      CM_GET_DEVICE_INTERFACE_LIST_PRESENT);
    if (cr != CR_SUCCESS)
    {
        throw std::runtime_error("failed to list interfaces, error " + std::to_string(cr));
    }

    std::vector<const char *> devices;
    for (const char *iface = deviceInterfaceList.data(); *iface != '\0'; iface += std::strlen(iface) + 1)
    {
        devices.push_back(iface);
    }

    _devices = std::move(devices);
    _devIfList = std::move(deviceInterfaceList);
}

CyDeviceManager::CyDeviceManager()
{
    scanDevices();
}

const std::vector<CyUsbDeviceDescriptor> &CyDeviceManager::devices() const
{
    return _devices;
}

const char *CyDeviceManager::friendlyName(CyUsbDeviceDescriptor descriptor) const
{
    return descriptor;
}

CyDevice CyDeviceManager::open(CyUsbDeviceDescriptor descriptor) const
{
    return CyDevice(descriptor);
}
