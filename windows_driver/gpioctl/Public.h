/*++

Module Name:

    public.h

Abstract:

    This module contains the common declarations shared by driver
    and user applications.

Environment:

    user and kernel

--*/

//
// Define an Interface Guid so that apps can find the device and talk to it.
// (we should probably remove it since GpioClx takes care of UM interfaces)

DEFINE_GUID (GUID_DEVINTERFACE_gpioctl,
    0x170f7e23,0x6884,0x4b19,0x91,0x95,0x07,0x9c,0x0a,0x8b,0x33,0x1d);
// {170f7e23-6884-4b19-9195-079c0a8b331d}
