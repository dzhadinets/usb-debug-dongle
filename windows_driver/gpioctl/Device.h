/*++

Module Name:

    device.h

Abstract:

    This file contains the device definitions.

Environment:

    Kernel-mode Driver Framework

--*/

#include "public.h"
#include <gpioclx.h>

EXTERN_C_START

#define PINS_PER_BANK 18
#define TOTAL_PINS 18

//
// The device context performs the same job as
// a WDM device extension in the driver frameworks
//
typedef struct _DEVICE_CONTEXT
{
    WDFDEVICE Device;
    WDFIOTARGET UsbTarget;
    PIN_NUMBER Pins[TOTAL_PINS];
    USHORT PinCount;
} DEVICE_CONTEXT, *PDEVICE_CONTEXT;

//
// This macro will generate an inline function called DeviceGetContext
// which will be used to get a pointer to the device context memory
// in a type safe manner.
//
//WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(DEVICE_CONTEXT, DeviceGetContext)

GPIO_CLIENT_PREPARE_CONTROLLER prepareController;
GPIO_CLIENT_RELEASE_CONTROLLER releaseController;
GPIO_CLIENT_QUERY_CONTROLLER_BASIC_INFORMATION queryControllerBasicInformation;

GPIO_CLIENT_START_CONTROLLER startController;
GPIO_CLIENT_STOP_CONTROLLER stopController;

GPIO_CLIENT_CONNECT_IO_PINS connectIoPins;
GPIO_CLIENT_DISCONNECT_IO_PINS disconnectIoPins;
GPIO_CLIENT_READ_PINS_MASK readGpioPins;
GPIO_CLIENT_WRITE_PINS_MASK writeGpioPins;

EXTERN_C_END
