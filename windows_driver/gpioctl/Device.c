/*++

Module Name:

    device.c - Device handling events for example driver.

Abstract:

   This file contains the device entry points and callbacks.

Environment:

    Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "device.tmh"

#include "..\sqcyusb\Public.h"

#ifdef ALLOC_PRAGMA
// #pragma alloc_text(PAGE, gpioctlCreateDevice)
#endif

_Must_inspect_result_ _IRQL_requires_(PASSIVE_LEVEL) NTSTATUS
    prepareController(_In_ WDFDEVICE Device, _In_ PVOID Context, _In_ WDFCMRESLIST ResourcesRaw,
                      _In_ WDFCMRESLIST ResourcesTranslated)

/*++

Routine Description:

    This routine is called by the GPIO class extension to prepare the
    simulated GPIO controller for use.

    N.B. This function is not marked pageable because this function is in
         the device power up path.

Arguments:

    Context - Supplies a pointer to the GPIO client driver's device extension.

    ResourcesRaw - Supplies a handle to a collection of framework resource
        objects. This collection identifies the raw (bus-relative) hardware
        resources that have been assigned to the device.

    ResourcesTranslated - Supplies a handle to a collection of framework
        resource objects. This collection identifies the translated
        (system-physical) hardware resources that have been assigned to the
        device. The resources appear from the CPU's point of view.

Return Value:

    NTSTATUS code.

--*/

{
    UNREFERENCED_PARAMETER(ResourcesRaw);
    UNREFERENCED_PARAMETER(ResourcesTranslated);

    DEVICE_CONTEXT *devContext = Context;

    devContext->Device = Device;
    devContext->UsbTarget = WDF_NO_HANDLE;
    devContext->PinCount = 0;

    // Here we need to go over resources, make sure we have everything we need and set them up. But since we do
    // not have any resources, just return STATUS_SUCCESS.

    return STATUS_SUCCESS;
}

_Must_inspect_result_ _IRQL_requires_(PASSIVE_LEVEL) NTSTATUS
    queryControllerBasicInformation(_In_ PVOID Context,
                                    _Out_ PCLIENT_CONTROLLER_BASIC_INFORMATION ControllerInformation)

/*++

Routine Description:

    This routine returns the GPIO controller's attributes to the class extension.

    N.B. This function is not marked pageable because this function is in
         the device power up path.

Arguments:

    Context - Supplies a pointer to the GPIO client driver's device extension.

    ControllerInformation - Supplies a pointer to a buffer that receives
        controller's information.

Return Value:

    NTSTATUS code.

--*/

{
    UNREFERENCED_PARAMETER(Context);
    // PSIM_GPIO_CONTEXT GpioContext;

    ControllerInformation->Version = GPIO_CONTROLLER_BASIC_INFORMATION_VERSION;
    ControllerInformation->Size = sizeof(CLIENT_CONTROLLER_BASIC_INFORMATION);

    //
    // Specify the number of pins on the SimGPIO controller.
    //

    // GpioContext = (PSIM_GPIO_CONTEXT)Context;
    ControllerInformation->TotalPins = TOTAL_PINS;
    ControllerInformation->NumberOfPinsPerBank = PINS_PER_BANK;

    // Indicate that the GPIO controller is off-SoC (i.e. behind some I2C bus)
    // and thus can only be serviced at PASSIVE_LEVEL IRQL.

    ControllerInformation->Flags.MemoryMappedController = FALSE;

    // Indicate that status register must be cleared explicitly.

    ControllerInformation->Flags.ActiveInterruptsAutoClearOnRead = FALSE;

    // Indicate that the client driver would like to receive IO requests as a
    // set of bitmasks as that maps directly to the register operations.

    ControllerInformation->Flags.FormatIoRequestsAsMasks = TRUE;

    // Indicate that the GPIO controller does not support controller-level
    // D-state power management or F-state power management.
    //
    // N.B. F-state management is only supported for on-SoC GPIO controllers.

    ControllerInformation->Flags.DeviceIdlePowerMgmtSupported = FALSE;
    ControllerInformation->Flags.BankIdlePowerMgmtSupported = FALSE;

    // Note the IdleTimeout parameter does not need to be initialized if
    // D-state power management is not supported.
    //
    // ControllerInformation->IdleTimeout = IdleTimeoutDefaultValue;

    // Indicate that the client driver prefers GPIO class extension ActiveBoth
    // emulation.

    ControllerInformation->Flags.EmulateActiveBoth = TRUE;

    return STATUS_SUCCESS;
}

_Must_inspect_result_ _IRQL_requires_(PASSIVE_LEVEL) NTSTATUS
    startController(_In_ PVOID Context, _In_ BOOLEAN RestoreContext, _In_ WDF_POWER_DEVICE_STATE PreviousPowerState)

/*++

Routine Description:

    This routine starts the simulated GPIO controller. This routine is
    responsible for configuring all the pins to their default modes.

    N.B. This function is not marked pageable because this function is in
         the device power up path. It is called at PASSIVE_IRQL though.

Arguments:

    Context - Supplies a pointer to the GPIO client driver's device extension.

    RestoreContext - Supplies a flag that indicates whether the client driver
        should restore the GPIO controller state to a previously saved state
        or not.

    PreviousPowerState - Supplies the device power state that the device was in
        before this transition to D0.

Return Value:

    NTSTATUS code.

--*/

{
    UNREFERENCED_PARAMETER(Context);
    UNREFERENCED_PARAMETER(RestoreContext);
    UNREFERENCED_PARAMETER(PreviousPowerState);

    //
    // Perform all the steps necessary to start the device.
    //

    return STATUS_SUCCESS;
}

_IRQL_requires_(PASSIVE_LEVEL) NTSTATUS
    stopController(_In_ PVOID Context, _In_ BOOLEAN SaveContext, _In_ WDF_POWER_DEVICE_STATE TargetState)

/*++

Routine Description:

    This routine stops the GPIO controller. This routine is responsible for
    resetting all the pins to their default modes.

    N.B. This function is not marked pageable because this function is in
         the device power down path.

Arguments:

    Context - Supplies a pointer to the GPIO client driver's device extension.

    SaveContext - Supplies a flag that indicates whether the client driver
        should save the GPIO controller state or not. The state may need
        to be restored when the controller is restarted.

    TargetState - Supplies the device power state which the device will be put
        in once the callback is complete.

Return Value:

    NTSTATUS code.

--*/

{
    UNREFERENCED_PARAMETER(Context);
    UNREFERENCED_PARAMETER(SaveContext);
    UNREFERENCED_PARAMETER(TargetState);

    return STATUS_SUCCESS;
}

_Must_inspect_result_ _IRQL_requires_(PASSIVE_LEVEL) NTSTATUS
    releaseController(_In_ WDFDEVICE Device, _In_ PVOID Context)

/*++

Routine Description:

    This routine is called by the GPIO class extension to uninitialize the GPIO
    controller.

    N.B. This function is not marked pageable because this function is in
         the device power down path.

Arguments:

    Context - Supplies a pointer to the GPIO client driver's device extension.

Return Value:

    NTSTATUS code.

--*/

{
    UNREFERENCED_PARAMETER(Device);
    UNREFERENCED_PARAMETER(Context);

    //
    // Release the mappings established in the initialize callback.
    //
    // N.B. Disconnecting of the interrupt is handled by the GPIO class
    //      extension.
    //

    return STATUS_SUCCESS;
}

NTSTATUS openUsbDevice(DEVICE_CONTEXT *devContext)
{
    WDF_IO_TARGET_OPEN_PARAMS openParams;
    WDFIOTARGET ioTarget;
    NTSTATUS status;

    DECLARE_CONST_UNICODE_STRING(deviceName, CY_USB_DEVICE_NAME);

    status = WdfIoTargetCreate(devContext->Device, WDF_NO_OBJECT_ATTRIBUTES, &ioTarget);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed to create IO target %!STATUS!", status);
        return status;
    }

    WDF_IO_TARGET_OPEN_PARAMS_INIT_OPEN_BY_NAME(&openParams, &deviceName, STANDARD_RIGHTS_ALL);

    status = WdfIoTargetOpen(ioTarget, &openParams);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER,
                    "Failed to open USB IO target (probably no device attached) %!STATUS!", status);
    }
    else
    {
        devContext->UsbTarget = ioTarget;
    }

    return status;
}

void closeUsbDevice(DEVICE_CONTEXT *devContext)
{
    if (devContext->UsbTarget != WDF_NO_HANDLE)
    {
        WdfIoTargetClose(devContext->UsbTarget);
        devContext->UsbTarget = WDF_NO_HANDLE;
    }
}

_Must_inspect_result_ _IRQL_requires_(PASSIVE_LEVEL) NTSTATUS
    connectIoPins(_In_ PVOID Context, _In_ PGPIO_CONNECT_IO_PINS_PARAMETERS ConnectParameters)

/*++

Routine Description:

    This routine invokes connects the specified pins for IO. The pins can
    be read from if connected for input, or written to if connected for
    output.

    N.B. This routine is called at PASSIVE_LEVEL but is not marked as
         PAGED_CODE as it could be executed late in the hibernate or
         early in resume sequence (or the deep-idle sequence).

Arguments:

    Context - Supplies a pointer to the GPIO client driver's device extension.

    ConnectParameters - Supplies a pointer to a structure supplying the
        parameters for connecting the IO pins. Fields description:

        BankId - Supplies the ID for the GPIO bank.

        PinNumberTable - Supplies an array of pins to be connected for IO. The
            pin numbers are 0-based and relative to the GPIO bank.

        PinCount - Supplies the number of pins in the pin number table.

        ConnectMode - Supplies the mode in which the pins should be configured
            (viz. input or output).

        ConnectFlags - Supplies the flags controlling the IO setup. Currently
            no flags are defined.

        PullConfiguration - Supplies the pin pull-up/pull-down configuration.

        DebouceTimeout - Supplies the debounce timeout to be applied. The
            field is in 100th of milli-seconds (i.e., 5.84ms will be supplied
            as 584). Default value is zero, which implies, no debounce.

        DriveStrength - Supplies the drive strength to be applied. The value
            is in 100th of mA (i.e., 1.21mA will be supplied as 121mA).

        VendorData - Supplies an optional pointer to a buffer containing the
            vendor data supplied in the GPIO descriptor. This field will be
            NULL if no vendor data was supplied. This buffer is read-only.

        VendorDataLength - Supplies the length of the vendor data buffer.

        ConnectFlags - Supplies the flag to be used for connect operation.
            Currently no flags are defined.

Return Value:

    NT status code.

Environment:

    Entry IRQL: PASSIVE_LEVEL.

    Synchronization: The GPIO class extension will synchronize this call
        against other passive-level interrupt callbacks (e.g. enable/disable/
        unmask) and IO callbacks.

--*/

{
    DEVICE_CONTEXT *devContext = Context;

    devContext->PinCount = ConnectParameters->PinCount;
    memcpy(devContext->Pins, ConnectParameters->PinNumberTable,
           ConnectParameters->PinCount * sizeof(ConnectParameters->PinNumberTable[0]));

    return STATUS_SUCCESS;
}

_Must_inspect_result_ _IRQL_requires_(PASSIVE_LEVEL) NTSTATUS
    disconnectIoPins(_In_ PVOID Context, _In_ PGPIO_DISCONNECT_IO_PINS_PARAMETERS DisconnectParameters)

/*++

Routine Description:

    This routine invokes disconnects the specified IO pins. The pins are
    put back in their original mode.

    N.B. This routine is called at PASSIVE_LEVEL but is not marked as
         PAGED_CODE as it could be executed late in the hibernate or
         early in resume sequence (or the deep-idle sequence).

Arguments:

    Context - Supplies a pointer to the GPIO client driver's device extension.

    DisconnectParameters - Supplies a pointer to a structure containing
        disconnect operation parameters. Fields are:

        BankId - Supplies the ID for the GPIO bank.

        PinNumberTable - Supplies an array of pins to be disconnected. The pin
            numbers are relative to the GPIO bank.

        PinCount - Supplies the number of pins in the pin number table.

        DisconnectMode - Supplies the mode in which the pins are currently
            configured (viz. input or output).

        DisconnectFlags - Supplies the flags controlling the IO setup. Currently
            no flags are defined.

Return Value:

    NTSTATUS code (STATUS_SUCCESS always for memory-mapped GPIO controllers).

Environment:

    Entry IRQL: PASSIVE_LEVEL.

    Synchronization: The GPIO class extension will synchronize this call
        against other passive-level interrupt callbacks (e.g. enable/disable/
        unmask) and IO callbacks.

--*/

{
    UNREFERENCED_PARAMETER(Context);
    UNREFERENCED_PARAMETER(DisconnectParameters);

    return STATUS_SUCCESS;
}

NTSTATUS readGpioPinValue(WDFIOTARGET usbTarget, BYTE pinNumber, USHORT *pinValue)
{
    WDF_MEMORY_DESCRIPTOR inputMemDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&inputMemDesc, &pinNumber, sizeof(pinNumber));

    WDF_MEMORY_DESCRIPTOR outputMemDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&outputMemDesc, pinValue, sizeof(*pinValue));

    NTSTATUS status =
        WdfIoTargetSendIoctlSynchronously(usbTarget, NULL, IOCTL_CY_GPIO_READ, &inputMemDesc, &outputMemDesc, NULL,
                                          NULL // bytesReturned - maybe it's worth filling
        );
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed send IOCTL_CY_GPIO_READ: %!STATUS!", status);
    }
    return status;
}

NTSTATUS writeGpioPinValue(WDFIOTARGET usbTarget, BYTE pinNumber, USHORT pinValue)
{
    WritePinData writeData = {pinNumber, pinValue};

    WDF_MEMORY_DESCRIPTOR inputMemDesc;
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&inputMemDesc, &writeData, sizeof(writeData));

    NTSTATUS status = WdfIoTargetSendIoctlSynchronously(usbTarget, NULL, IOCTL_CY_GPIO_WRITE, &inputMemDesc, NULL, NULL,
                                                        NULL // bytesReturned - maybe it's worth filling
    );
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed send IOCTL_CY_GPIO_WRITE: %!STATUS!", status);
    }
    return status;
}

_Must_inspect_result_ NTSTATUS readGpioPins(_In_ PVOID Context, _In_ PGPIO_READ_PINS_MASK_PARAMETERS ReadParameters)

/*++

Routine Description:

    This routine reads the current values for all the pins.

    As the FormatIoRequestsAsMasks bit was set inside
    SimGpioQueryControllerInformation(), all this routine needs to do is read
    the level register value and return to the GPIO class extension. It will
    return the right set of bits to the caller.

    N.B. This routine is called at DIRQL for memory-mapped GPIOs and thus not
         marked as PAGED.

Arguments:

    Context - Supplies a pointer to the GPIO client driver's device extension.

    ReadParameters - Supplies a pointer to a structure containing read
        operation parameters. Fields are:

        BankId - Supplies the ID for the GPIO bank.

        PinValues - Supplies a pointer to a variable that receives the current
            pin values.

        Flags - Supplies the flag to be used for read operation. Currently
            defined flags are:

            WriteConfiguredPins: If set, the read is being done on a set of
                pin that were configured for write. In such cases, the
                GPIO client driver is expected to read and return the
                output register value.

Return Value:

    NTSTATUS code (STATUS_SUCCESS always for memory-mapped GPIO controllers).

Environment:

    Entry IRQL: DIRQL if the GPIO controller is memory-mapped;
        PASSIVE_LEVEL if the controller is behind some serial-bus.

    Synchronization: The GPIO class extension will synchronize this call
        against other passive-level interrupt callbacks (e.g. enable/disable)
        and IO callbacks (connect/disconnect).

--*/

{
    NTSTATUS status = STATUS_SUCCESS;
    ULONG pinValues = 0;
    DEVICE_CONTEXT *devContext = Context;

    status = openUsbDevice(devContext);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed to open USB device: %!STATUS!", status);
        goto exit;
    }

    // Here we need to perform actual reading
    for (size_t i = 0; i < devContext->PinCount && NT_SUCCESS(status); ++i)
    {
        USHORT pinValue = 0;
        status = readGpioPinValue(devContext->UsbTarget, (BYTE)devContext->Pins[i], &pinValue);
        pinValues |= pinValue << devContext->Pins[i];
    }

    if (NT_SUCCESS(status))
    {
        *ReadParameters->PinValues = pinValues;
    }

exit:
    closeUsbDevice(devContext);
    return status;
}

_Must_inspect_result_ NTSTATUS writeGpioPins(_In_ PVOID Context, _In_ PGPIO_WRITE_PINS_MASK_PARAMETERS WriteParameters)

/*++

Routine Description:

    This routine sets the current values for the specified pins. This call is
    synchronized with the write and connect/disconnect IO calls.

    N.B. This routine is called at DIRQL for memory-mapped GPIOs and thus not
         marked as PAGED.

Arguments:

    Context - Supplies a pointer to the GPIO client driver's device extension.

    WriteParameters - Supplies a pointer to a structure containing write
        operation parameters. Fields are:

        BankId - Supplies the ID for the GPIO bank.

        SetMask - Supplies a mask of pins which should be set (0x1). If a pin
            should be set, then the corresponding bit is set in the mask.
            All bits that are clear in the mask should be left intact.

        ClearMask - Supplies a mask of pins which should be cleared (0x0). If
            a pin should be cleared, then the bit is set in the bitmask. All
            bits that are clear in the mask should be left intact.

        Flags - Supplies the flag controlling the write operation. Currently
            no flags are defined.

Return Value:

    NTSTATUS code (STATUS_SUCCESS always for memory-mapped GPIO controllers).

Environment:

    Entry IRQL: DIRQL if the GPIO controller is memory-mapped;
        PASSIVE_LEVEL if the controller is behind some serial-bus.

    Synchronization: The GPIO class extension will synchronize this call
        against other passive-level interrupt callbacks (e.g. enable/disable)
        and IO callbacks (connect/disconnect).

--*/

{
    UNREFERENCED_PARAMETER(Context);

    //
    // Read the current level register value.
    //

    // PinValue = READ_REGISTER_ULONG(&SimGpioRegisters->LevelRegister);

    NTSTATUS status = STATUS_SUCCESS;
    BYTE i;
    const ULONG64 one = 1;
    DEVICE_CONTEXT *devContext = Context;

    status = openUsbDevice(devContext);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed to open USB device: %!STATUS!", status);
        goto exit;
    }

    for (i = 0; i < TOTAL_PINS && NT_SUCCESS(status); ++i)
    {
        if (WriteParameters->SetMask & (one << i))
        {
            status = writeGpioPinValue(devContext->UsbTarget, i, 1);
            if (!NT_SUCCESS(status))
            {
                break;
            }
        }
    }
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed set pin %d: %!STATUS!", i, status);
        goto exit;
    }

    for (i = 0; i < TOTAL_PINS && NT_SUCCESS(status); ++i)
    {
        if (WriteParameters->ClearMask & (one << i))
        {
            status = writeGpioPinValue(devContext->UsbTarget, i, 0);
        }
    }
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed unset pin %d: %!STATUS!", i, status);
        goto exit;
    }

    //
    // Set the bits specified in the set mask and clear the ones specified
    // in the clear mask.
    //

    // PinValue |= WriteParameters->SetMask;
    // PinValue &= ~WriteParameters->ClearMask;

    //
    // Write the updated value to the register.
    //

    // WRITE_REGISTER_ULONG(&SimGpioRegisters->LevelRegister, PinValue);

exit:
    closeUsbDevice(devContext);
    return status;
}
