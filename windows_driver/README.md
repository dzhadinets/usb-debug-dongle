# SPB drivers overview

## General insformation

The idea of the driver(s) is to show how can a Windows driver may be implemented that uses I2C bus. SPBCtl is a controller driver and SPBClient is a peripheral driver which supports a device attached to I2C bus.

## Components

### Spbctl

Sptctl project is a SPB controller driver. It redirect SPB requests to the debug whistle.

### SpbClient

It is an example of an SPB client device driver. In this case it is an RTC PCF8523

### dfr1602rgb

One more example of an SPB client device driver which is RGB display.

### sqcyusb

USB driver of the debug whistle.

### clientcom

A test application which shows how to communicate with the drivers.

### cyusbcom

A library for communication with USB debug whistle directly, i.e. without SPB abstraction. It uses sqcyusb driver.

# Installation and testing

The main purpose of the driver is demonstration so the testing part will be prevalent.

## System configuration

All the test should be done on a separate test machine because it is easy to crash it.

If there is no such physical device on the machine, we will need to simulate one. In order to do it, we can load a fake ACPI table which has an I2C controller and peripheral device that is linked to this controller. In order to do it the system needs to be in test mode which can be achieved using the following command and reboot:
```
bcdedit /set testsigning on`
```

Test mode will also allow loading drivers which are not properly signed.

We suggest two options ACPI table can be edited. The first one is to edit DSDT, which is not advisable due to it's complexity, but might be useful in rare cases when you do not have SSDT on your machine, or you cannot replace SSDT for some reason. The (recommended) second option is to replace SSDT.

### Edit DSDT

In order to edit ACPI table, we will need to dump ACPI table using 
```
asl /tab=DSDT
```

`asl` and other tools can be found in Windows DDK.

(Dumping SSDT table might be a nicer idea because it is smaller, but in theory they should work the same way.)

After the command DSDT.asl file will appear in the current directory. We will need to edit it by adding the devices we need under main definition block, e.g.:

```
...
    Scope(_SB_)
    {
        // controller 
        Device(SCB1)
        {
            Name(_HID, "spbctl")
            Name(_UID, 2)      
            Method(SCB_, 0, NotSerialized)
            {
                Return(1)
            }
        }
        // client
        // Real-time clock (PCF8523)
        Device(CLIC)
        {
                Name(_HID, "spbclient")
                Name(_UID, 3)
                Method(_CRS, 0x0, NotSerialized)
                {
                    Name (RBUF, ResourceTemplate ()
                    {
                        // 0x68 is a PCF8523 slave address
                        I2CSerialBusV2(0x68, ControllerInitiated, 400000, AddressingMode7Bit, "\\_SB.SCB1",,,,,)
                    })
                    Return(RBUF)
                }
        }
    }
...
```

_HID names should have the same values as in drivers' .inf files (e.g. ACPI\spbctl for controller and ACPI\spbclient for the client). _CRS method represents resources the device will have and in this case it links resources from the first device, including slave address (0x1D), speed etc.

After the file is modified, it needs to be compiled and loaded to the system:
```
asl DSDT.asl
asl /loadtable /v DSDT.AML
```

The command might show minor errors, but it will not be a problem if you are using a test machine.

After reboot you will see devices without drivers in the Device Manager. You can install drivers for them using Device Manager or `devcon` utility (e.g. `devcon install spbctl.inf ACPI\spbctl` and `devcon install spbclient.inf ACPI\spbclient` and subsequently update them using `devcon update ...` commands).

### Replace SSDT

Create a file (say, spbtest.asl) with the description of the devices, e.g.

```
DefinitionBlock("ACPITABL.dat", "SSDT", 5, "SFTQ", "", 1)
{
    Scope(_SB_)
    {
        // controllers
        Device(SCB0)
        {
            Name(_HID, "spbctl")
            Name(_UID, 1)
            Method(SCB_, 0, NotSerialized)
            {
                Return(0)
            }
        }       
        Device(SCB1)
        {
            Name(_HID, "spbctl")
            Name(_UID, 2)      
            Method(SCB_, 0, NotSerialized)
            {
                Return(1)
            }
        }
        Device(GPIO)
        {
            Name(_HID, "sqgpioctl")
            Name(_UID, 5)
        }

        // clients
        // Real-time clock (PCF8523)
        Device(CLIC)
        {
                Name(_HID, "spbclient")
                Name(_UID, 3)
                Method(_CRS, 0x0, NotSerialized)
                {
                    Name (RBUF, ResourceTemplate ()
                    {
                        // 0x68 is a PCF8523 slave address, RawDataBuffer(){1} specifies SCB1
                        I2CSerialBusV2(0x68, ControllerInitiated, 400000, AddressingMode7Bit, "\\_SB.SCB1",,,,,RawDataBuffer(){1})
                    })
                    Return(RBUF)
                }
        }
        // LCD display
        Device(CLID)
        {
            Name(_HID, "dfr1602rgb")
            Name(_UID, 4)
            Method(_CRS, 0x0, NotSerialized)
            {
                Name (RBUF, ResourceTemplate ()
                {
                    // The resources for this display must be in this specific order (first LCD and then RGB), determined by the driver
			  
                    // 0x3e is LCD address
                    I2CSerialBusV2(0x3e, ControllerInitiated, 400000, AddressingMode7Bit, "\\_SB.SCB1",,,,,RawDataBuffer(){1})
                    // 0x60 is RGB address
                    I2CSerialBusV2(0x60, ControllerInitiated, 400000, AddressingMode7Bit, "\\_SB.SCB1",,,,,RawDataBuffer(){1})
                    // GPIO
                    GpioIo(Exclusive, PullUp, 0, 0,, "\\_SB.GPIO", 0, ResourceConsumer, , RawDataBuffer() {1}) {12}
                    GpioIo(Exclusive, PullUp, 0, 0,, "\\_SB.GPIO", 0, ResourceConsumer, , RawDataBuffer() {1}) {13}
                })
                Return(RBUF)
            }
        }
    }
}
```

Compile is using asl.exe tool (`asl.exe spbtest.asl`) and copy generated ACPITABL.dat to C:\Windows\System32 folder. After reboot the SSDT table will be replaced with the one described in ACPITABL.dat.

## Logging

Both drivers spbctl and spbclient use WPP logging. The easier way of collecting logs is using traceview GUI application. 

In order to see logs in real time, you will need to start traceview.exe application as administrator, choose File / Create New Log Session, enable PDB and locate relevant .pdb.
