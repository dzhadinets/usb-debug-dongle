
// This project is designed to demonstrate how user-mode application can use the drivers to work with the debug whistle
// device (e.g. read configuration, modifying it and writting it back to the device, communicating to I2C device on the
// bus).

#include <Windows.h>
#include <cfgmgr32.h> // CM_* functions

#include <wrl.h> // RAII for Windows objects

#include <cstdio>
#include <cctype>
#include <vector>
#include <exception>
#include <stdexcept>
#include <string>

#include <initguid.h> // if we do not include it, link will fail to find GUID_DEVINTERFACE_spbclient
#include <winioctl.h>
#include "..\spbclient\Public.h"
#include "..\sqcyusb\Public.h"
#include "..\dfr1602rgb\Public.h"

#include <cy7_structs.h>

#include <cyusbcom.h>

#pragma region Utility functions

/*!
    \brief Returns a string description of a Windows error code
    \param errorCode win32 error code
    \return string description
*/
std::string winErrorMessage(DWORD errorCode)
{
    LPSTR lpMsgBuf;
    std::string what;

    if (FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                       NULL, errorCode, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPSTR>(&lpMsgBuf),
                       0, NULL))
    {
        what.append(lpMsgBuf);
        LocalFree(lpMsgBuf);
    }
    else
    {
        what.append("Error 0x");
        char buffer[sizeof(errorCode) * 2 + 1] = {0}; // should be enough to fill hex representation
        sprintf_s(buffer, "%x", errorCode);
        what.append(buffer);
    }

    return what;
}

/*!
    \brief Prints out a hex dump of a memory.
    The main purpose is to deonstrate data to a user. Probably should be removed.
    \param buffer data to disaply
    \param size size of the data to display
*/
void hexDump(const BYTE *buffer, DWORD size)
{
    const DWORD LINE_WIDTH = 16;

    DWORD i = 0;
    char printable[LINE_WIDTH + 1] = {0};

    while (i < size)
    {
        if (i % LINE_WIDTH == 0)
        {
            std::printf("%04x: ", i);
        }

        std::printf(" %02x", buffer[i]);
        printable[i % LINE_WIDTH] = std::isgraph(buffer[i]) ? buffer[i] : '.';
        ++i;

        if (i % LINE_WIDTH == 0)
        {
            std::printf(" | %s\n", printable);
        }
    }
    if (size % LINE_WIDTH != 0)
    {
        printable[i % LINE_WIDTH] = '\0';
        for (; i < LINE_WIDTH * (size / LINE_WIDTH + 1); ++i)
        {
            std::printf("   ");
        }
        std::printf(" | %s\n", printable);
    }
}

/*!
    \brief calculates checksum and config field to that value.
    \param cfg config structure
    \return the checksum
 */
uint32_t CyConfigGetChecksum(struct cy7_device_config *cfg)
{
    uint32_t *ptr = (uint32_t *)cfg;
    uint32_t *end = ptr + sizeof(*cfg) / sizeof(*ptr);
    uint32_t sum = 0;

    /* skip first 12 bytes */
    ptr += 3;

    while (ptr < end)
    {
        sum += *ptr;
        ptr++;
    }

    return sum;
}

#pragma endregion

#pragma region SPB related functions

/*!
    \brief Opens a driver interface object by GUID.
    It picks the first instance of the driver, so it is good for demonstration purpuses only.
    \param interfaceGuid driver interface GUID
    \return handle to the device or INVALID_HANDLE_VALUE
*/
HANDLE OpenClientDevice(GUID interfaceGuid, int devNo = 0)
{
    CONFIGRET cr = CR_SUCCESS;
    ULONG deviceInterfaceListLength = 0;
    HANDLE handle = INVALID_HANDLE_VALUE;

    // Get the length of null-separted list of all interfaces
    cr = CM_Get_Device_Interface_List_Size(&deviceInterfaceListLength, &interfaceGuid, NULL,
                                           CM_GET_DEVICE_INTERFACE_LIST_PRESENT);
    if (cr != CR_SUCCESS)
    {
        std::printf("Error 0x%x retrieving device interface list size.\n", cr); // TODO: throw exception
        return handle;
    }

    // Get null-separted list of all interfaces. In our case there should be only one of them.
    std::vector<char> deviceInterfaceList(deviceInterfaceListLength, 0);
    cr = CM_Get_Device_Interface_List(&interfaceGuid, NULL, deviceInterfaceList.data(), deviceInterfaceListLength,
                                      CM_GET_DEVICE_INTERFACE_LIST_PRESENT);
    if (cr != CR_SUCCESS)
    {
        std::printf("Error 0x%x retrieving device interface list.\n", cr);
        return handle;
    }

    if (*deviceInterfaceList.data()) // if we have at least one
    {
        const char *devIf = deviceInterfaceList.data();
        for (int devId = 0; *devIf && devId < devNo; ++devId)
        {
            devIf += strlen(devIf) + 1;
        }
        handle = CreateFile(devIf, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
                            nullptr,       // no SECURITY_ATTRIBUTES structure
                            OPEN_EXISTING, // No special create flags
                            0,             // No special attributes
                            nullptr);      // No template file
    }
    else
    {
        SetLastError(ERROR_NOT_FOUND);
    }
    if (handle == INVALID_HANDLE_VALUE)
    {
        auto err = GetLastError();
        std::printf("Error 0x%x opening device interface: %s\n", err, winErrorMessage(err).c_str());
        return handle;
    }

    return handle;
}

/*!
    \brief Demonstration of how we can open whistle I2C device and read data from PCF8523
*/
DWORD testSpbClient()
{
    using Microsoft::WRL::Wrappers::FileHandle;
    DWORD ec = ERROR_SUCCESS;

    FileHandle device(OpenClientDevice(GUID_DEVINTERFACE_SPBCLIENT, 0));

    if (device.Get() != INVALID_HANDLE_VALUE)
    {
        // try to read something from the peripheral device
        BYTE data[3] = {0}; // 3 bytes = seconds, minutes and hours
        DWORD dataLength = sizeof(data);
        DWORD bytesRead = 0;
        std::printf("Receiving data from the driver... ");

        if (ReadFile(device.Get(), data, dataLength, &bytesRead, nullptr))
        {
            std::printf("ok, %d of %d bytes\n", bytesRead, dataLength);
            // the data will be printed as it is revceived from the device, i.e. with additional flags
            hexDump(data, sizeof(data));
        }
        else
        {
            ec = GetLastError();
            std::printf("error %d: %s\n", ec, winErrorMessage(ec).c_str());
        }
    }
    else
    {
        ec = ERROR_NOT_FOUND;
    }

    std::printf("Done\n");

    return ec;
}

#pragma endregion

#pragma region USB / PCF8523 related functions

enum class PCF8523_REG : UCHAR
{
    SECONDS = 0x03,
    MINUTES,
    HOURS,
    DAYS,
    WEEKDAYS,
    MONTH,
    YEARS
};

#define PCF8523_ADDRESS 0x68

/*!
    \brief Connect to the USB device directly and send some commands to PCF8523
*/
DWORD testUSB()
{
    using Microsoft::WRL::Wrappers::FileHandle;
    CyDeviceManager devmgr;

    if (devmgr.devices().empty())
    {
        std::printf("No devices found\n");
        return ERROR_FILE_NOT_FOUND;
    }

    try
    {
        // open device
        CyDevice devices[2];
        for (auto descriptor : devmgr.devices())
        {
            CyDevice device(descriptor);
            devices[device.getScb()] = std::move(device);
        }

        // read config
        // it does not matter which SCB to use for config
        auto config = devices[0].readConfig();
        std::printf("Config data:\n");
        hexDump(config.data(), static_cast<DWORD>(config.size()));

        // IMO modifying the config each test might be a bad idea, but here is how it's done
#ifdef MODIFY_CPNFIG
        // modify config
        {
            cy7_device_config *cfg = reinterpret_cast<cy7_device_config *>(config.data());
            std::printf("Device: VID_%04x&PID_%04x\n", cfg->usb.vendor_id, cfg->usb.product_id);
            cfg->usb.vendor_id = 0x04B4;
            cfg->usb.product_id = 0x7;
            cfg->checksum = CyConfigGetChecksum(cfg);
        }

        // write config
        devices[0].writeConfig(config);
#endif

        // Read current minutes from the PCF8523
        const USHORT address = PCF8523_ADDRESS;
        const UCHAR scb = 0; // NOTE: PCF8523 should be on SCB0
        CyDevice &device = devices[scb];

        device.i2cWriteByte(address, static_cast<UCHAR>(PCF8523_REG::MINUTES), scb);
        BYTE buffer[1] = {device.i2cReadByte(address, scb)};

        std::printf("Value returned:\n");
        hexDump(buffer, sizeof(buffer));

        // Read value from GPIO 5
        auto gpioValue = device.gpioRead(5);
        printf("GPIO 5 = %d\n", gpioValue);
    }
    catch (std::runtime_error &error)
    {
        std::printf("Error: %s\n", error.what());
        return ERROR_DEVICE_UNREACHABLE;
    }
    return ERROR_SUCCESS;
}

#pragma endregion

#pragma region Disaplay driver

class DisplayButtonTest
{
public:
    DisplayButtonTest();
    void run();

private:
    void showText(std::string text);
    void setColor(DisplaySetColor color);
    bool buttonPressed();
    void writePin(BYTE value);
    std::string readWriteSpi(std::string writeData);

    Microsoft::WRL::Wrappers::FileHandle _device;
};

DisplayButtonTest::DisplayButtonTest()
    : _device(OpenClientDevice(GUID_DEVINTERFACE_dfr1602rgb, 0))
{
    if (_device == INVALID_HANDLE_VALUE)
    {
        throw GetLastError();
    }
}

void DisplayButtonTest::run()
{
    const size_t testDurationMs = 9000;
    const size_t buttonPollFrequescyMs = 100;

    bool buttonWasPressed = true;

    // GPIO test
    writePin(1);

    // SPI test
    std::string reply = readWriteSpi("Hello SPI from UM");
    std::printf("SPI replied [%llu bytes]: \n", reply.size());
    hexDump((BYTE*)reply.data(), (DWORD)reply.size());

    for (size_t i = 0; i < testDurationMs / buttonPollFrequescyMs; ++i)
    {
        if (buttonPressed())
        {
            if (!buttonWasPressed)
            {
                showText("Good job!");
            }
            buttonWasPressed = true;
        }
        else
        {
            if (buttonWasPressed)
            {
                showText("Plese press\n the button");
            }
            buttonWasPressed = false;
        }
        Sleep(buttonPollFrequescyMs);
    }

    setColor({0x20, 0x00, 0x00});
    showText("Time's out\n Bye!");
    Sleep(1500);
}

void DisplayButtonTest::showText(std::string text)
{
    char *textToDisplay = text.data();
    DWORD textSize = static_cast<DWORD>(text.length());
    DWORD bytesWritten = 0;
    if (!WriteFile(_device.Get(), textToDisplay, textSize, &bytesWritten, nullptr))
    {
        DWORD ec = GetLastError();
        std::printf("Failed writting data, error %d\n", ec);
        throw ec;
    }
}

void DisplayButtonTest::setColor(DisplaySetColor color)
{
    DWORD bytesReturned = 0;
    if (!DeviceIoControl(_device.Get(), IOCTL_DISPLAY_SET_COLOR, &color, sizeof(color), nullptr, 0, &bytesReturned,
                         nullptr))
    {
        DWORD ec = GetLastError();
        std::printf("Failed setting color, error %d\n", ec);
        throw ec;
    }
}

bool DisplayButtonTest::buttonPressed()
{
    const DWORD buttonErrorValue = 0xFF;
    DWORD bytesReturned = 0;
    BYTE buttonValue = buttonErrorValue;
    if (!DeviceIoControl(_device.Get(), IOCTL_DISPLAY_READ_BUTTON, nullptr, 0, &buttonValue, sizeof(buttonValue),
                         &bytesReturned, nullptr) ||
        buttonValue == buttonErrorValue)
    {
        DWORD ec = GetLastError();
        std::printf("Failed getting buton value, error %d\n", ec);
        throw ec; // if rc == 0 then buttonValue == buttonErrorValue
    }
    return (buttonValue == 1);
}

void DisplayButtonTest::writePin(BYTE value)
{
    DWORD bytesReturned = 0;
    if (!DeviceIoControl(_device.Get(), IOCTL_DISPLAY_WRITE_PIN, &value, sizeof(value), nullptr, 0, &bytesReturned,
                         nullptr))
    {
        DWORD ec = GetLastError();
        std::printf("Failed setting pin value, error %d\n", ec);
        throw ec; // if rc == 0 then buttonValue == buttonErrorValue
    }
}

std::string DisplayButtonTest::readWriteSpi(std::string writeData)
{
    if (writeData.size() == 0)
    {
        return {};
    }

    char *dataToWrite = writeData.data();
    DWORD textSize = static_cast<DWORD>(writeData.length()) + 1;
    std::vector<char> resultData(textSize);
    DWORD bytesTrensferred = 0;
    if (!DeviceIoControl(_device.Get(), IOCTL_DISPLAY_READWRITE_SPI, dataToWrite, textSize, resultData.data(), textSize,
                         &bytesTrensferred, nullptr))
    {
        DWORD ec = GetLastError();
        std::printf("Failed writting data, error %d\n", ec);
        throw ec;
    }
    return std::string{resultData.begin() + 1, resultData.end()};
}

DWORD testDisplay()
{
    DWORD ec = ERROR_SUCCESS;

    try
    {
        DisplayButtonTest().run();
    }
    catch (DWORD error)
    {
        ec = error;
    }

    std::printf("Done\n");

    return ec;
}

#pragma endregion

// Main

#define TEST_DIRECT_USB 1
#define TEST_SPB_RTC 2
#define TEST_SPB_DISPLAY 4

#define TEST_ALL (TEST_DIRECT_USB | TEST_SPB_RTC | TEST_SPB_DISPLAY)

int main(int argc, const char **argv)
{
    DWORD testMask = TEST_ALL;
    if (argc > 1)
    {
        testMask = atoi(argv[1]);
    }

    DWORD rc = ERROR_SUCCESS;

    do
    {
        if (testMask & TEST_DIRECT_USB)
        {
            std::printf("\nTesting direct USB connection...\n\n");
            rc = testUSB();
            if (rc != ERROR_SUCCESS)
            {
                break;
            }
        }

        if (testMask & TEST_SPB_RTC)
        {
            std::printf("\nTesting SPB connection...\n\n");
            rc = testSpbClient();
            if (rc != ERROR_SUCCESS)
            {
                break;
            }
        }

        if (testMask & TEST_SPB_DISPLAY)
        {
            std::printf("\nTesting display...\n\n");
            rc = testDisplay();
            if (rc != ERROR_SUCCESS)
            {
                break;
            }
        }
    } while (false);

    if (rc != ERROR_SUCCESS)
    {
        std::printf("Finished with error %d - %s\n\n", rc, winErrorMessage(rc).c_str());
    }

    return rc;
}
