/*++

Module Name:

    device.h

Abstract:

    This file contains the device definitions.

Environment:

    Kernel-mode Driver Framework

--*/

#include "public.h"

EXTERN_C_START

enum DISPLAY_RESOURCES
{
    RES_LCD,
    RES_RGB,
    RES_SPI,
    RES_GPIO_0,
    RES_GPIO_1,
    RES_COUNT
};

//
// The device context performs the same job as
// a WDM device extension in the driver frameworks
//
typedef struct _DEVICE_CONTEXT
{
    LARGE_INTEGER ConnectionIds[RES_COUNT];
    WDFIOTARGET Targets[RES_COUNT];

    BYTE ShowControl;
    BYTE ShowMode;
} DEVICE_CONTEXT, *PDEVICE_CONTEXT;

//
// This macro will generate an inline function called DeviceGetContext
// which will be used to get a pointer to the device context memory
// in a type safe manner.
//
WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(DEVICE_CONTEXT, DeviceGetContext)

//
// Function to initialize the device and its callbacks
//
NTSTATUS dfr1602rgbCreateDevice(_Inout_ PWDFDEVICE_INIT DeviceInit);

EXTERN_C_END
