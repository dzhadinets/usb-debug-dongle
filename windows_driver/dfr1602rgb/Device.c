/*++

Module Name:

    device.c - Device handling events for example driver.

Abstract:

   This file contains the device entry points and callbacks.

Environment:

    Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "device.tmh"

#include "rhhelpers.h"
#include "I2C.h"
#include "Display.h"

#ifdef ALLOC_PRAGMA
#pragma alloc_text(PAGE, dfr1602rgbCreateDevice)
#endif

NTSTATUS
EvtPrepareHardware(WDFDEVICE device, WDFCMRESLIST resourcesRaw, WDFCMRESLIST resourcesTranslated)
{
    UNREFERENCED_PARAMETER(device);
    UNREFERENCED_PARAMETER(resourcesRaw);
    UNREFERENCED_PARAMETER(resourcesTranslated);

    PAGED_CODE();

    NTSTATUS status = STATUS_SUCCESS;

    PCM_PARTIAL_RESOURCE_DESCRIPTOR descriptors[RES_COUNT] = {NULL};
    ULONG resourceCount;
    PDEVICE_CONTEXT deviceContext = DeviceGetContext(device);

    // check if our resources are correct

    UCHAR connectionTypes[RES_COUNT] = {
        CM_RESOURCE_CONNECTION_TYPE_SERIAL_I2C, // RES_LCD
        CM_RESOURCE_CONNECTION_TYPE_SERIAL_I2C, // RES_RGB
        CM_RESOURCE_CONNECTION_TYPE_SERIAL_SPI, // RES_SPI
        CM_RESOURCE_CONNECTION_TYPE_GPIO_IO,    // RES_GPIO_0
        CM_RESOURCE_CONNECTION_TYPE_GPIO_IO     // RES_GPIO_1
    };

    resourceCount = WdfCmResourceListGetCount(resourcesTranslated);
    if (RES_COUNT != resourceCount)
    {
        TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "Not enough resources: %d, must be %d\n", resourceCount,
                    RES_COUNT);
        return STATUS_INSUFFICIENT_RESOURCES;
    }

    for (enum DISPLAY_RESOURCES i = 0; i < RES_COUNT; ++i)
    {
        descriptors[i] = WdfCmResourceListGetDescriptor(resourcesTranslated, i);
        if (descriptors[i]->Type != CmResourceTypeConnection || descriptors[i]->u.Connection.Type != connectionTypes[i])
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER,
                        "Resource %d type is incorrect: (%d,%d), must be (%d,%d)\n", i, descriptors[i]->Type,
                        descriptors[i]->u.Connection.Type, CmResourceTypeConnection, connectionTypes[i]);
            status = STATUS_RESOURCE_TYPE_NOT_FOUND;
            break;
        }
    }

    // store connection IDs in device context to open them later
    if (NT_SUCCESS(status))
    {
        for (enum DISPLAY_RESOURCES i = 0; i < RES_COUNT; ++i)
        {
            deviceContext->ConnectionIds[i].LowPart = descriptors[i]->u.Connection.IdLowPart;
            deviceContext->ConnectionIds[i].HighPart = descriptors[i]->u.Connection.IdHighPart;
        }
    }

    return status;
}

/*!
    \brief This callback is triggered when device interface is opened
*/
VOID ClientDeviceFileCreate(IN WDFDEVICE device, IN WDFREQUEST request, IN WDFFILEOBJECT fileObject)
{
    NTSTATUS status = STATUS_SUCCESS;
    PDEVICE_CONTEXT deviceContext = DeviceGetContext(device);
    WDFIOTARGET ioTargets[RES_COUNT];

    enum DISPLAY_RESOURCES i = 0;

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "ClientDeviceFileCreate %p\n", fileObject);

    // Open targets

    for (i = 0; i < RES_COUNT; ++i)
    {
        status = ConnectionOpen(device, deviceContext->ConnectionIds[i], &ioTargets[i]);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "Failed opening target #%d, res id %lld. Status: %!STATUS!", i,
                        deviceContext->ConnectionIds[i].QuadPart, status);
            goto Exit;
        }
    }

    // Save opened targets

    for (i = 0; i < RES_COUNT; ++i)
    {
        deviceContext->Targets[i] = ioTargets[i];
    }

    // Initialize devices

    status = DisplayInit(deviceContext);

Exit:
    if (!NT_SUCCESS(status))
    {
        for (enum DISPLAY_RESOURCES j = i - 1; j >= 0; --j)
        {
            ConnectionClose(ioTargets[j]);
            ioTargets[j] = WDF_NO_HANDLE;
        }
    }

    WdfRequestComplete(request, status);
}

/*!
    \brief This callback is triggered when device interface is closed
*/
VOID ClientDeviceFileCleanup(IN WDFFILEOBJECT fileObject)
{
    WDFDEVICE device = WdfFileObjectGetDevice(fileObject);
    PDEVICE_CONTEXT deviceContext = DeviceGetContext(device);

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_DRIVER, "ClientDeviceFileCleanup %p\n", fileObject);

    DisplayRemove(deviceContext);

    for (enum DISPLAY_RESOURCES j = RES_COUNT - 1; j >= 0; --j)
    {
        ConnectionClose(deviceContext->Targets[j]);
        deviceContext->Targets[j] = WDF_NO_HANDLE;
    }
}

/*!
    \brief Worker routine called to create a device and its software resources
*/
NTSTATUS
dfr1602rgbCreateDevice(_Inout_ PWDFDEVICE_INIT deviceInit)
{
    WDF_OBJECT_ATTRIBUTES deviceAttributes;
    PDEVICE_CONTEXT deviceContext;
    WDFDEVICE device;
    WDF_FILEOBJECT_CONFIG fileConfig;
    NTSTATUS status;
    WDF_PNPPOWER_EVENT_CALLBACKS pnpPowerCallbacks;

    PAGED_CODE();

    // Set file object settings

    WDF_FILEOBJECT_CONFIG_INIT(&fileConfig, ClientDeviceFileCreate, NULL, ClientDeviceFileCleanup);
    WdfDeviceInitSetFileObjectConfig(deviceInit, &fileConfig, WDF_NO_OBJECT_ATTRIBUTES);

    // Set power events

    WDF_PNPPOWER_EVENT_CALLBACKS_INIT(&pnpPowerCallbacks);
    pnpPowerCallbacks.EvtDevicePrepareHardware = EvtPrepareHardware;
    WdfDeviceInitSetPnpPowerEventCallbacks(deviceInit, &pnpPowerCallbacks);

    // Only one client can talk to this device
    WdfDeviceInitSetExclusive(deviceInit, TRUE);

    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&deviceAttributes, DEVICE_CONTEXT);

    status = WdfDeviceCreate(&deviceInit, &deviceAttributes, &device);

    if (NT_SUCCESS(status))
    {
        //
        // Get a pointer to the device context structure that we just associated
        // with the device object. We define this structure in the device.h
        // header file. DeviceGetContext is an inline function generated by
        // using the WDF_DECLARE_CONTEXT_TYPE_WITH_NAME macro in device.h.
        // This function will do the type checking and return the device context.
        // If you pass a wrong object handle it will return NULL and assert if
        // run under framework verifier mode.
        //
        deviceContext = DeviceGetContext(device);

        //
        // Initialize the context.
        //
        for (enum DISPLAY_RESOURCES i = 0; i < RES_COUNT; ++i)
        {
            deviceContext->ConnectionIds[i].QuadPart = 0;
            deviceContext->Targets[i] = WDF_NO_HANDLE;
        }

        deviceContext->ShowControl = 0;
        deviceContext->ShowMode = 0;

        //
        // Create a device interface so that applications can find and talk
        // to us.
        //
        status = WdfDeviceCreateDeviceInterface(device, &GUID_DEVINTERFACE_dfr1602rgb,
                                                NULL // ReferenceString
        );

        if (NT_SUCCESS(status))
        {
            //
            // Initialize the I/O Package and any Queues
            //
            status = dfr1602rgbQueueInitialize(device);
        }
    }

    return status;
}
