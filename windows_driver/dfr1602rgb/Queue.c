/*++

Module Name:

    queue.c

Abstract:

    This file contains the queue entry points and callbacks.

Environment:

    Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "queue.tmh"

#include <gpio.h>

#include "I2C.h"
#include "Display.h"

#ifdef ALLOC_PRAGMA
#pragma alloc_text(PAGE, dfr1602rgbQueueInitialize)
#endif

NTSTATUS
dfr1602rgbQueueInitialize(_In_ WDFDEVICE device)
/*++

Routine Description:

     The I/O dispatch callbacks for the frameworks device object
     are configured in this function.

     A single default I/O Queue is configured for parallel request
     processing, and a driver context memory allocation is created
     to hold our structure QUEUE_CONTEXT.

Arguments:

    Device - Handle to a framework device object.

Return Value:

    VOID

--*/
{
    WDFQUEUE queue;
    NTSTATUS status;
    WDF_IO_QUEUE_CONFIG queueConfig;

    PAGED_CODE();

    //
    // Configure a default queue so that requests that are not
    // configure-fowarded using WdfDeviceConfigureRequestDispatching to goto
    // other queues get dispatched here.
    //
    WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&queueConfig, WdfIoQueueDispatchParallel);

    queueConfig.EvtIoDeviceControl = dfr1602rgbEvtIoDeviceControl;
    queueConfig.EvtIoWrite = dfr1602rgbEvtIoWrite;
    queueConfig.EvtIoStop = dfr1602rgbEvtIoStop;

    status = WdfIoQueueCreate(device, &queueConfig, WDF_NO_OBJECT_ATTRIBUTES, &queue);

    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfIoQueueCreate failed %!STATUS!", status);
        return status;
    }

    return status;
}

VOID dfr1602rgbEvtIoWrite(_In_ WDFQUEUE queue, _In_ WDFREQUEST request, _In_ size_t length)
{
    NTSTATUS status = STATUS_SUCCESS;
    WDFDEVICE device = WdfIoQueueGetDevice(queue);
    PDEVICE_CONTEXT deviceContext = DeviceGetContext(device);
    BYTE *buffer = NULL;
    ULONG_PTR bytesWritten = 0;

    UNREFERENCED_PARAMETER(queue);

    status = WdfRequestRetrieveInputBuffer(request, 1, &buffer, NULL);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveInputMemory failed %!STATUS!", status);
        goto Exit;
    }

    status = DisplayPrint(deviceContext, buffer, length, &bytesWritten);

Exit:
    WdfRequestCompleteWithInformation(request, status, bytesWritten);
}

NTSTATUS spiSendReadWriteRequest(WDFIOTARGET target, BYTE *readBuffer, BYTE *writeBuffer, ULONG length,
                                 ULONG_PTR *bytesTransferred)
{
    // should always be 2 (read + write)
#define SPI_DUPLEX_TRANSFER_COUNT 2

    NTSTATUS status = STATUS_SUCCESS;
    WDF_MEMORY_DESCRIPTOR memDesc;
    WDF_REQUEST_SEND_OPTIONS requestOptions;

    SPB_TRANSFER_LIST_AND_ENTRIES(SPI_DUPLEX_TRANSFER_COUNT) transferList;

    // Static analysis cannot figure out the SPB_TRANSFER_LIST_ENTRY
    // size but using an index variable quiets the warning.
    ULONG index = 0;

    SPB_TRANSFER_LIST_INIT(&(transferList.List), SPI_DUPLEX_TRANSFER_COUNT);

    // Static analysis can't figure out the relationship between the transfer array size
    // and the transfer count.
    _Analysis_assume_(ARRAYSIZE(transferList.List.Transfers) == SPI_DUPLEX_TRANSFER_COUNT);

    transferList.List.Transfers[index] =
        SPB_TRANSFER_LIST_ENTRY_INIT_SIMPLE(SpbTransferDirectionToDevice, 0, writeBuffer, length);

    transferList.List.Transfers[index + 1] =
        SPB_TRANSFER_LIST_ENTRY_INIT_SIMPLE(SpbTransferDirectionFromDevice, 0, readBuffer, length);

    // Initialize the memory descriptor with the transfer list.
    WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memDesc, &transferList, sizeof(transferList));

    WDF_REQUEST_SEND_OPTIONS_INIT(&requestOptions, WDF_REQUEST_SEND_OPTION_TIMEOUT);
    requestOptions.Timeout = WDF_REL_TIMEOUT_IN_MS(SPI_SYNCHRONOUS_TIMEOUT);

    status =
        WdfIoTargetSendIoctlSynchronously(target, NULL, IOCTL_SPB_FULL_DUPLEX, &memDesc, NULL, NULL, bytesTransferred);
    if (!NT_SUCCESS(status) || *bytesTransferred == 0)
    {
        TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                    "%!FUNC! Failed to send read/write request to SPI target: %!STATUS!", status);
    }

    return status;
}

VOID dfr1602rgbEvtIoDeviceControl(_In_ WDFQUEUE queue, _In_ WDFREQUEST request, _In_ size_t outputBufferLength,
                                  _In_ size_t inputBufferLength, _In_ ULONG ioControlCode)
/*++

Routine Description:

    This event is invoked when the framework receives IRP_MJ_DEVICE_CONTROL request.

Arguments:

    Queue -  Handle to the framework queue object that is associated with the
             I/O request.

    Request - Handle to a framework request object.

    OutputBufferLength - Size of the output buffer in bytes

    InputBufferLength - Size of the input buffer in bytes

    IoControlCode - I/O control code.

Return Value:

    VOID

--*/
{
    NTSTATUS status;
    ULONG_PTR information = 0;

    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                "%!FUNC! Queue 0x%p, Request 0x%p OutputBufferLength %d InputBufferLength %d IoControlCode %d", queue,
                request, (int)outputBufferLength, (int)inputBufferLength, ioControlCode);

    switch (ioControlCode)
    {
    case IOCTL_DISPLAY_SET_COLOR:
    {
        DisplaySetColor *color = NULL;
        status = WdfRequestRetrieveInputBuffer(request, sizeof(*color), &color, NULL);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                        "%!FUNC! Failed to retrieve input buffer for IOCTL_DISPLAY_SET_COLOR: %!STATUS!", status);
            break;
        }

        PDEVICE_CONTEXT deviceContext = DeviceGetContext(WdfIoQueueGetDevice(queue));
        status = DisplaySetRgb(deviceContext, color->red, color->green, color->blue);

        break;
    }
    case IOCTL_DISPLAY_READ_BUTTON:
    {
        BYTE *buttonValue = NULL;
        size_t length = 0;
        status = WdfRequestRetrieveOutputBuffer(request, sizeof(*buttonValue), &buttonValue, &length);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                        "%!FUNC! Failed to retrieve input buffer for IOCTL_DISPLAY_SET_COLOR: %!STATUS!", status);
            break;
        }

        PDEVICE_CONTEXT deviceContext = DeviceGetContext(WdfIoQueueGetDevice(queue));
        WDF_MEMORY_DESCRIPTOR bufferMemDesc;
        WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&bufferMemDesc, buttonValue, (ULONG)length);

        status = WdfIoTargetSendIoctlSynchronously(deviceContext->Targets[RES_GPIO_0], NULL, IOCTL_GPIO_READ_PINS, NULL,
                                                   &bufferMemDesc, NULL, &information);
        if (!NT_SUCCESS(status) || information == 0)
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                        "%!FUNC! Failed to send IOCTL_GPIO_READ_PINS to GPIO target: %!STATUS!", status);
            break;
        }

        break;
    }

    case IOCTL_DISPLAY_WRITE_PIN:
    {
        BYTE *pinValue = NULL;
        size_t length = 0;
        status = WdfRequestRetrieveInputBuffer(request, sizeof(*pinValue), &pinValue, &length);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                        "%!FUNC! Failed to retrieve input buffer for IOCTL_DISPLAY_SET_COLOR: %!STATUS!", status);
            break;
        }

        PDEVICE_CONTEXT deviceContext = DeviceGetContext(WdfIoQueueGetDevice(queue));
        WDF_MEMORY_DESCRIPTOR bufferMemDesc;
        WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&bufferMemDesc, pinValue, (ULONG)length);

        status = WdfIoTargetSendIoctlSynchronously(deviceContext->Targets[RES_GPIO_1], NULL, IOCTL_GPIO_WRITE_PINS,
                                                   &bufferMemDesc, NULL, NULL, &information);
        if (!NT_SUCCESS(status) || information == 0)
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                        "%!FUNC! Failed to send IOCTL_GPIO_WRITE_PINS to GPIO target: %!STATUS!", status);
            break;
        }

        break;
    }
    case IOCTL_DISPLAY_READWRITE_SPI:
    {
        if (inputBufferLength > 0 && outputBufferLength > 0 && outputBufferLength != inputBufferLength)
        {
            TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                        "%!FUNC! Input and output buffers must have equal size for IOCTL_DISPLAY_READWRITE_SPI");
            status = STATUS_INVALID_PARAMETER;
            break;
        }

        BYTE *inputBuffer = NULL;
        BYTE *outputBuffer = NULL;
        size_t length = 0;

        if (inputBufferLength > 0)
        {
            status = WdfRequestRetrieveInputBuffer(request, 1, &inputBuffer, NULL);
            if (!NT_SUCCESS(status))
            {
                TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                            "%!FUNC! Failed to retrieve input buffer for IOCTL_DISPLAY_WRITE_SPI: %!STATUS!", status);
                break;
            }
            length = inputBufferLength;
        }

        if (outputBufferLength > 0)
        {
            status = WdfRequestRetrieveOutputBuffer(request, 1, &outputBuffer, NULL);
            if (!NT_SUCCESS(status))
            {
                TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE,
                            "%!FUNC! Failed to retrieve output buffer for IOCTL_DISPLAY_WRITE_SPI: %!STATUS!", status);
                break;
            }
            length = outputBufferLength;
        }

        PDEVICE_CONTEXT deviceContext = DeviceGetContext(WdfIoQueueGetDevice(queue));
        status =
            spiSendReadWriteRequest(deviceContext->Targets[RES_SPI], outputBuffer, inputBuffer, (ULONG)length, &information);

        break;
    }

    default:
        status = STATUS_INVALID_DEVICE_REQUEST;
        break;
    }

    WdfRequestCompleteWithInformation(request, status, information);
    return;
}

VOID dfr1602rgbEvtIoStop(_In_ WDFQUEUE queue, _In_ WDFREQUEST request, _In_ ULONG actionFlags)
/*++

Routine Description:

    This event is invoked for a power-managed queue before the device leaves the working state (D0).

Arguments:

    Queue -  Handle to the framework queue object that is associated with the
             I/O request.

    Request - Handle to a framework request object.

    ActionFlags - A bitwise OR of one or more WDF_REQUEST_STOP_ACTION_FLAGS-typed flags
                  that identify the reason that the callback function is being called
                  and whether the request is cancelable.

Return Value:

    VOID

--*/
{
    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE, "%!FUNC! Queue 0x%p, Request 0x%p ActionFlags %d", queue, request,
                actionFlags);

    //
    // In most cases, the EvtIoStop callback function completes, cancels, or postpones
    // further processing of the I/O request.
    //
    // Typically, the driver uses the following rules:
    //
    // - If the driver owns the I/O request, it calls WdfRequestUnmarkCancelable
    //   (if the request is cancelable) and either calls WdfRequestStopAcknowledge
    //   with a Requeue value of TRUE, or it calls WdfRequestComplete with a
    //   completion status value of STATUS_SUCCESS or STATUS_CANCELLED.
    //
    //   Before it can call these methods safely, the driver must make sure that
    //   its implementation of EvtIoStop has exclusive access to the request.
    //
    //   In order to do that, the driver must synchronize access to the request
    //   to prevent other threads from manipulating the request concurrently.
    //   The synchronization method you choose will depend on your driver's design.
    //
    //   For example, if the request is held in a shared context, the EvtIoStop callback
    //   might acquire an internal driver lock, take the request from the shared context,
    //   and then release the lock. At this point, the EvtIoStop callback owns the request
    //   and can safely complete or requeue the request.
    //
    // - If the driver has forwarded the I/O request to an I/O target, it either calls
    //   WdfRequestCancelSentRequest to attempt to cancel the request, or it postpones
    //   further processing of the request and calls WdfRequestStopAcknowledge with
    //   a Requeue value of FALSE.
    //
    // A driver might choose to take no action in EvtIoStop for requests that are
    // guaranteed to complete in a small amount of time.
    //
    // In this case, the framework waits until the specified request is complete
    // before moving the device (or system) to a lower power state or removing the device.
    // Potentially, this inaction can prevent a system from entering its hibernation state
    // or another low system power state. In extreme cases, it can cause the system
    // to crash with bugcheck code 9F.
    //

    return;
}
