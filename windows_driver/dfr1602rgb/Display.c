#include "driver.h"

#include "Display.h"
#include "Display.tmh"

#include "I2C.h"

/*!
    \brief Sends a command to LCD
    \param lcd LCD
    \param command command to send
    \return NTSTATUS
*/
static NTSTATUS LcdCommand(WDFIOTARGET lcd, BYTE command)
{
    return I2CWriteSynchronously(lcd, 0x80, &command, sizeof(command));
}

/*!
    \brief Initializes LCD
    \param devieContext device context
    \return NTSTATUS
*/
static NTSTATUS InitLcd(PDEVICE_CONTEXT deviceContext)
{
    NTSTATUS status = STATUS_SUCCESS;
    WDFIOTARGET targetLcd = deviceContext->Targets[RES_LCD];

    // try the same stuff 3 times
    const int triesCount = 3;
    for (int i = 0; i < triesCount; ++i)
    {
        status = LcdCommand(targetLcd, LCD_FUNCTIONSET | LCD_4BITMODE | LCD_1LINE | LCD_2LINE | LCD_5x8DOTS);
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "LcdCommand (attempt %d) failed. Status: %!STATUS!", i,
                        status);
        }

        if (i + 1 != triesCount)
        {
            LARGE_INTEGER delay = {.QuadPart = WDF_REL_TIMEOUT_IN_MS(50)};
            KeDelayExecutionThread(KernelMode, FALSE, &delay);
        }
    }

    deviceContext->ShowControl = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;

    // turn the display on with no cursor or blinking default
    status = LcdCommand(targetLcd, LCD_DISPLAYCONTROL | deviceContext->ShowControl);

    // clear display
    status = LcdCommand(targetLcd, LCD_CLEARDISPLAY);

    // Initialize to default text direction (for romance languages)
    // set the entry mode
    deviceContext->ShowMode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;

    status = LcdCommand(targetLcd, LCD_ENTRYMODESET | deviceContext->ShowMode);

    return status;
}

/*!
    \brief Internal function which sets RGB register
    \param rgb RBG device target
    \param reg RGB register
    \param value register value
    \return NTSTATUS
*/
static NTSTATUS RgbSetReg(WDFIOTARGET rgb, BYTE reg, BYTE value)
{
    return I2CWriteSynchronously(rgb, reg, &value, sizeof(value));
}

/*!
    \brief Initializes RGB device
    \param deviceContext device context
    \return NTSTATUS
*/
static NTSTATUS InitRgb(PDEVICE_CONTEXT deviceContext)
{
    NTSTATUS status;
    WDFIOTARGET targetRgb = deviceContext->Targets[RES_RGB];

    // backlight init
    status = RgbSetReg(targetRgb, REG_MODE1, 0);

    // set LEDs controllable by both PWM and GRPPWM registers
    status = RgbSetReg(targetRgb, REG_OUTPUT, 0xFF);

    // set MODE2 values
    // 0010 0000 -> 0x20  (DMBLNK to 1, ie blinky mode)
    status = RgbSetReg(targetRgb, REG_MODE2, 0x20);

    status = DisplaySetRgb(deviceContext, 0x80, 0x10, 0x10);

    return status;
}

/*!
    \brief Sets RGB (color) of display
    \param deviceContext device context
    \param r red component
    \param g green component
    \param b blue component
    \return NTSTATUS
*/
NTSTATUS DisplaySetRgb(PDEVICE_CONTEXT deviceContext, BYTE r, BYTE g, BYTE b)
{
    NTSTATUS status;
    WDFIOTARGET rgb = deviceContext->Targets[RES_RGB];

    if (!NT_SUCCESS(status = RgbSetReg(rgb, REG_RED, r)) || !NT_SUCCESS(status = RgbSetReg(rgb, REG_GREEN, g)) ||
        !NT_SUCCESS(status = RgbSetReg(rgb, REG_BLUE, b)))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "RgbSetReg failed. Status: %!STATUS!", status);
    }

    return status;
}

/*!
    \brief Initialize display device
    \param deviceContext device context
    \return NTSTATUS
*/
NTSTATUS DisplayInit(PDEVICE_CONTEXT deviceContext)
{
    NTSTATUS status;
    if (!NT_SUCCESS(status = InitLcd(deviceContext)) || !NT_SUCCESS(status = InitRgb(deviceContext)))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "InitDisplay failed. Status: %!STATUS!", status);
    }

    return status;
}

/*!
    \brief Clears display
    \param deviceContext device context
    \return NTSTATUS
*/
static NTSTATUS DisplayClear(PDEVICE_CONTEXT deviceContext)
{
    return LcdCommand(deviceContext->Targets[RES_LCD], LCD_CLEARDISPLAY);
}

/*!
    \brief Performs operations by enabling and disabling the display
    \param deviceContext device context
    \param enable TRUE for enable a FALSE for disable
    \return NTSTATUS
*/
static NTSTATUS DisplayEnable(PDEVICE_CONTEXT deviceContext, BOOLEAN enable)
{
    if (enable)
    {
        deviceContext->ShowControl |= LCD_DISPLAYON;
    }
    else
    {
        deviceContext->ShowControl &= ~LCD_DISPLAYON;
    }
    return LcdCommand(deviceContext->Targets[RES_LCD], LCD_DISPLAYCONTROL | deviceContext->ShowControl);
}

/*!
    \brief Removed a disaply. Needs to be called when display is no longer used
    \param deviceContext device context
*/
VOID DisplayRemove(PDEVICE_CONTEXT deviceContext)
{
    // disable LCD
    LcdCommand(deviceContext->Targets[RES_LCD], LCD_CLEARDISPLAY);
    DisplayEnable(deviceContext, FALSE);

    // remove RGB
    DisplaySetRgb(deviceContext, 0, 0, 0);
}

/*!
    \brief Set cursor position
    \param deviceContext device context
    \param col column
    \param row row (0 or 1)
    \return NTSTATUS
*/
NTSTATUS DisplaySetCursor(PDEVICE_CONTEXT deviceContext, BYTE col, BYTE row)
{
    BYTE cmd = row == 0 ? col | 0x80 : col | 0xc0;
    return LcdCommand(deviceContext->Targets[RES_LCD], cmd);
}

/*!
    \brief Finds a character in a BYTE buffer
    \param start start search position
    \param length size of the buffer
    \param byteToFind byte to find
    \return either the position of the byte found or length
*/
static size_t FindChar(BYTE *buffer, size_t start, size_t length, BYTE byteToFind)
{
    size_t pos = start;
    for (; pos < length; ++pos)
    {
        if (buffer[pos] == byteToFind)
        {
            break;
        }
    }
    return pos;
}

/*!
    \brief Displays data buffer on the display. It does not handle newlines
    \param deviceContext device context
    \param buffer bytes to show
    \param length size of the buffer
    \param bytesTransferred pointer to a size_t or NULL indicating the number of bytes transferred to the device
    \return NTSTATUS
*/
static NTSTATUS DisplayPrintBuffer(PDEVICE_CONTEXT deviceContext, BYTE *buffer, size_t length, size_t *bytesTransferred)
{
    NTSTATUS status = STATUS_SUCCESS;
    size_t bytesWritten = 0;
    for (; bytesWritten < length; ++bytesWritten)
    {
        status = I2CWriteSynchronously(deviceContext->Targets[RES_LCD], 0x40, &buffer[bytesWritten], sizeof(buffer[0]));
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_DRIVER, "I2CWriteSynchronously failed %!STATUS!", status);
            break;
        }
    }
    if (bytesTransferred != NULL)
    {
        *bytesTransferred = bytesWritten;
    }
    return status;
}

/*!
    \brief Shows text buffer on the display. \n is newline.
    Note that since the display has only two lines, it is not advisable to show more.
    \param deviceContext device context
    \param buffer bytes to show
    \param length size of the buffer
    \param bytesTransferred pointer to a size_t or NULL indicating the number of bytes transferred to the device
    \return NTSTATUS
*/
NTSTATUS DisplayPrint(PDEVICE_CONTEXT deviceContext, BYTE *buffer, size_t length, size_t *bytesTransferred)
{
    size_t start = 0;
    size_t end = 0;
    NTSTATUS status = DisplayClear(deviceContext);

    if (!NT_SUCCESS(status))
    {
        return status;
    }

    if (bytesTransferred != NULL)
    {
        *bytesTransferred = 0;
    }

    while (start < length)
    {
        BYTE row = 0;
        size_t bytesWritten = 0;

        end = FindChar(buffer, start, length, '\n');
        status = DisplayPrintBuffer(deviceContext, buffer + start, end - start, &bytesWritten);
        if (bytesTransferred != NULL)
        {
            *bytesTransferred += bytesWritten + (end != length);
        }
        if (!NT_SUCCESS(status))
        {
            break;
        }

        status = DisplaySetCursor(deviceContext, 0, ++row % 2);
        if (!NT_SUCCESS(status))
        {
            break;
        }

        start = end + 1;
    }
    return status;
}
