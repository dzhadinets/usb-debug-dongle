/*++

Module Name:

    public.h

Abstract:

    This module contains the common declarations shared by driver
    and user applications.

Environment:

    user and kernel

--*/

//
// Define an Interface Guid so that apps can find the device and talk to it.
//

#define IOCTL_DISPLAY_SET_COLOR CTL_CODE(FILE_DEVICE_UNKNOWN, 0x101, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_DISPLAY_READ_BUTTON CTL_CODE(FILE_DEVICE_UNKNOWN, 0x102, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_DISPLAY_WRITE_PIN CTL_CODE(FILE_DEVICE_UNKNOWN, 0x103, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_DISPLAY_READWRITE_SPI CTL_CODE(FILE_DEVICE_UNKNOWN, 0x104, METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _DisplaySetColor
{
    BYTE red;
    BYTE green;
    BYTE blue;
} DisplaySetColor;

DEFINE_GUID(GUID_DEVINTERFACE_dfr1602rgb, 0x22b6645d, 0x6217, 0x4ea2, 0xb7, 0x1c, 0xf7, 0x51, 0xb9, 0x9e, 0x71, 0xc4);
// {22b6645d-6217-4ea2-b71c-f751b99e71c4}
