rem Windbg locks the .pdb file which causes errors linking the driver. This file renames the .pdb to a random name.

set oldname=%1.pdb
set newname=%oldname%.%RANDOM%
rename ..\x64\Debug\%oldname% %newname%
echo rename x64\Debug\%oldname% to %newname%

