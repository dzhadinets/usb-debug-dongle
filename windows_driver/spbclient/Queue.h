/*++

Module Name:

    queue.h

Abstract:

    This file contains the queue definitions.

Environment:

    Kernel-mode Driver Framework

--*/

EXTERN_C_START

NTSTATUS
SpbclientQueueInitialize(
    _In_ WDFDEVICE device
    );

//
// Events from the IoQueue object
//
EVT_WDF_IO_QUEUE_IO_WRITE SpbclientEvtIoWrite;
EVT_WDF_IO_QUEUE_IO_READ SpbclientEvtIoRead;
EVT_WDF_IO_QUEUE_IO_STOP SpbclientEvtIoStop;

EXTERN_C_END
