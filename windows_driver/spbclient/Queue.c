/*++

Module Name:

    queue.c

Abstract:

    This file contains the queue entry points and callbacks.

Environment:

    Kernel-mode Driver Framework

--*/

#include "driver.h"
#include "I2C.h"
#include "queue.tmh"

#ifdef ALLOC_PRAGMA
#pragma alloc_text(PAGE, SpbclientQueueInitialize)
#endif

NTSTATUS
SpbclientQueueInitialize(_In_ WDFDEVICE device)
/*++

Routine Description:

     The I/O dispatch callbacks for the frameworks device object
     are configured in this function.

     A single default I/O Queue is configured for parallel request
     processing, and a driver context memory allocation is created
     to hold our structure QUEUE_CONTEXT.

Arguments:

    Device - Handle to a framework device object.

Return Value:

    VOID

--*/
{
    WDFQUEUE queue;
    NTSTATUS status;
    WDF_IO_QUEUE_CONFIG queueConfig;

    PAGED_CODE();

    //
    // Configure a default queue so that requests that are not
    // configure-fowarded using WdfDeviceConfigureRequestDispatching to goto
    // other queues get dispatched here.
    //
    WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&queueConfig, WdfIoQueueDispatchParallel);

    queueConfig.EvtIoWrite = SpbclientEvtIoWrite;
    queueConfig.EvtIoRead = SpbclientEvtIoRead;
    queueConfig.EvtIoStop = SpbclientEvtIoStop;

    status = WdfIoQueueCreate(device, &queueConfig, WDF_NO_OBJECT_ATTRIBUTES, &queue);

    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfIoQueueCreate failed %!STATUS!", status);
    }

    return status;
}

VOID WriteCompletion(_In_ WDFREQUEST request, _In_ WDFIOTARGET target, _In_ PWDF_REQUEST_COMPLETION_PARAMS params,
                     _In_ WDFCONTEXT context)
{
    UNREFERENCED_PARAMETER(request);
    UNREFERENCED_PARAMETER(target);

    WdfRequestComplete((WDFREQUEST)context, params->IoStatus.Status);
}

VOID ReadCompletion(_In_ WDFREQUEST request, _In_ WDFIOTARGET target, _In_ PWDF_REQUEST_COMPLETION_PARAMS params,
                    _In_ WDFCONTEXT context)
{
    UNREFERENCED_PARAMETER(request);
    UNREFERENCED_PARAMETER(target);

    WdfRequestComplete((WDFREQUEST)context, params->IoStatus.Status);
}

VOID SpbclientEvtIoWrite(WDFQUEUE queue, WDFREQUEST request, size_t length)
{
    UNREFERENCED_PARAMETER(queue);

    WdfRequestCompleteWithInformation(request, STATUS_SUCCESS, length); // just tell UM that all went fine

    // The RTC device does not need to write anything, but below is a demonstration of how writting can be acheived

    // WDFDEVICE device = WdfIoQueueGetDevice(queue);
    // WDFIOTARGET i2cTarget = DeviceGetContext(device)->I2CIoTarget;

    // BYTE *buffer = NULL;
    // size_t bufferLength = 0;
    // NTSTATUS status = WdfRequestRetrieveInputBuffer(request, 1, &buffer, &bufferLength);
    // if (!NT_SUCCESS(status))
    //{
    //    TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveInputBuffer failed %!STATUS!", status);
    //    WdfRequestComplete(request, status);
    //    return;
    //}

    // TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "Write (%llu bytes)", bufferLength);
    // WdfRequestSetInformation(request, length);
    // I2CWriteAsynchronously(i2cTarget, SOME_WRITE_REGISTER, buffer, bufferLength, WriteCompletion, request);
}

VOID SpbclientEvtIoRead(WDFQUEUE queue, WDFREQUEST request, size_t length)
{
    enum PCF8523_REGISTER
    {
        REG_SECONDS = 0x03,
        REG_MINUTES,
        REG_HOURS,
        REG_DAYS,
        REG_WEEKDAYS,
        REG_MONTH,
        REG_YEARS
    };

    WDFDEVICE device = WdfIoQueueGetDevice(queue);
    WDFIOTARGET i2cTarget = DeviceGetContext(device)->I2CIoTarget;

    BYTE *buffer = NULL;
    NTSTATUS status = WdfRequestRetrieveOutputBuffer(request, 1, &buffer, NULL);
    if (!NT_SUCCESS(status))
    {
        TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "WdfRequestRetrieveInputBuffer failed %!STATUS!", status);
        goto Exit;
    }

    // This is an example how asynchronous read can be done. Here we need to read several registers so synchronous way
    // would be more convenient.

    // TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "Read (%llu bytes)", length);
    // WdfRequestSetInformation(request, length);
    // I2CReadAsynchronously(i2cTarget,
    //                      0x04, // 0x04 = minutes //SOME_READ_REGISTER,
    //                      buffer, length, ReadCompletion, request);

    // Registers to read
    // Depending on the size of the buffer provided by UM client, we read seconds, minutes, hours etc.
    static const BYTE registers[] = {REG_SECONDS, REG_MINUTES, REG_HOURS, REG_DAYS, REG_WEEKDAYS, REG_MONTH, REG_YEARS};
    size_t i = 0;

    for (; i < sizeof(registers) / sizeof(registers[0]) && i < length; ++i)
    {
        status = I2CReadSynchronously(i2cTarget, registers[i], &buffer[i], sizeof(buffer[i]));
        if (!NT_SUCCESS(status))
        {
            TraceEvents(TRACE_LEVEL_ERROR, TRACE_QUEUE, "I2CReadSynchronously (reg=0x%x) failed %!STATUS!",
                        registers[i], status);
            goto Exit;
        }
    }
    WdfRequestSetInformation(request, i);

Exit:
    WdfRequestComplete(request, status);
}

VOID SpbclientEvtIoStop(_In_ WDFQUEUE queue, _In_ WDFREQUEST request, _In_ ULONG actionFlags)
/*++

Routine Description:

    This event is invoked for a power-managed queue before the device leaves the working state (D0).

Arguments:

    Queue -  Handle to the framework queue object that is associated with the
             I/O request.

    Request - Handle to a framework request object.

    ActionFlags - A bitwise OR of one or more WDF_REQUEST_STOP_ACTION_FLAGS-typed flags
                  that identify the reason that the callback function is being called
                  and whether the request is cancelable.

Return Value:

    VOID

--*/
{
    TraceEvents(TRACE_LEVEL_INFORMATION, TRACE_QUEUE, "%!FUNC! Queue 0x%p, Request 0x%p ActionFlags %d", queue, request,
                actionFlags);

    //
    // In most cases, the EvtIoStop callback function completes, cancels, or postpones
    // further processing of the I/O request.
    //
    // Typically, the driver uses the following rules:
    //
    // - If the driver owns the I/O request, it calls WdfRequestUnmarkCancelable
    //   (if the request is cancelable) and either calls WdfRequestStopAcknowledge
    //   with a Requeue value of TRUE, or it calls WdfRequestComplete with a
    //   completion status value of STATUS_SUCCESS or STATUS_CANCELLED.
    //
    //   Before it can call these methods safely, the driver must make sure that
    //   its implementation of EvtIoStop has exclusive access to the request.
    //
    //   In order to do that, the driver must synchronize access to the request
    //   to prevent other threads from manipulating the request concurrently.
    //   The synchronization method you choose will depend on your driver's design.
    //
    //   For example, if the request is held in a shared context, the EvtIoStop callback
    //   might acquire an internal driver lock, take the request from the shared context,
    //   and then release the lock. At this point, the EvtIoStop callback owns the request
    //   and can safely complete or requeue the request.
    //
    // - If the driver has forwarded the I/O request to an I/O target, it either calls
    //   WdfRequestCancelSentRequest to attempt to cancel the request, or it postpones
    //   further processing of the request and calls WdfRequestStopAcknowledge with
    //   a Requeue value of FALSE.
    //
    // A driver might choose to take no action in EvtIoStop for requests that are
    // guaranteed to complete in a small amount of time.
    //
    // In this case, the framework waits until the specified request is complete
    // before moving the device (or system) to a lower power state or removing the device.
    // Potentially, this inaction can prevent a system from entering its hibernation state
    // or another low system power state. In extreme cases, it can cause the system
    // to crash with bugcheck code 9F.
    //

    return;
}
