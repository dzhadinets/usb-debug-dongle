/*++

Module Name:

    public.h

Abstract:

    This module contains the common declarations shared by driver
    and user applications.

Environment:

    user and kernel

--*/

//
// Define an Interface Guid so that apps can find the device and talk to it.
//

DEFINE_GUID (GUID_DEVINTERFACE_SPBCLIENT,
    0x192f8f5e,0x68c4,0x4653,0x8b,0xf9,0xf7,0x25,0x8a,0xa8,0x19,0x88);
// {192f8f5e-68c4-4653-8bf9-f7258aa81988}
