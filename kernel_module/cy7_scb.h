/* SPDX-License-Identifier: GPL-2.0 */
/**
 *
 * @brief usb-debug-whistle linux kernel driver SCB config
 *        implementation.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_SCB_H
#define _CY7_SCB_H

#include <linux/types.h>
#include  "cy7_device.h"

int cy7_scb_dev_create(struct cy7_device *cy7_dev);
void cy7_scb_dev_remove(struct cy7_device *cy7_dev);

#endif /* _CY7_SCB_H */
