/* SPDX-License-Identifier: GPL-2.0 */
/**
 *
 * @brief usb-debug-whistle linux kernel driver I2C SCB
 *        implementation.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_I2C_H
#define _CY7_I2C_H

#include "cy7_device.h"

int cy7_i2c_probe(struct cy7_device *cy7_dev);

void cy7_i2c_remove(struct cy7_device *cy7_dev);
#endif /* _CY7_I2C_H */
