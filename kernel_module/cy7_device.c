// SPDX-License-Identifier: GPL-2.0
/**
 *
 * @brief usb-debug-whistle linux kernel driver management
 *        routines.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#include "cy7_device.h"
#include "cy7_config.h"
#include "cypressdefs.h"

static int cy7_set_maintainance_mode(struct cy7_device *cy7_dev)
{
	int ret = 0;

	ret = usb_control_msg(cy7_dev->usb_dev,
			      usb_sndctrlpipe(cy7_dev->usb_dev, 0),
			      CY_MAINTAINANCE_MODE_CMD,
			      USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT,
			      CY_MAINTAINANCE_MODE_WVALUE,
			      CY_MAINTAINANCE_MODE_SET_WINDEX,
			      NULL,
			      0,
			      100);

	SQ_INFO(DRIVER_TAG, "device set maintainance mode %d", ret);
	return ret;
}

static int cy7_unset_maintainance_mode(struct cy7_device *cy7_dev)
{
	int ret = 0;

	ret = usb_control_msg(cy7_dev->usb_dev,
			      usb_sndctrlpipe(cy7_dev->usb_dev, 0),
			      CY_MAINTAINANCE_MODE_CMD,
			      USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT,
			      CY_MAINTAINANCE_MODE_WVALUE,
			      CY_MAINTAINANCE_MODE_UNSET_WINDEX,
			      NULL,
			      0,
			      100);

	SQ_INFO(DRIVER_TAG, "device unset maintainance mode %d", ret);
	return ret;
}

static int cy7_device_read_config(struct cy7_device *cy7_dev, struct cy7_config_blob *blob)
{
	int ret = 0;

	ret = usb_control_msg(cy7_dev->usb_dev,
			      usb_rcvctrlpipe(cy7_dev->usb_dev, 0),
			      CY_READ_CONFIG_CMD,
			      USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_IN,
			      0,
			      0,
			      blob->buf,
			      sizeof(*blob),
			      100);

	if (ret != sizeof(*blob)) {
		SQ_ERROR(DRIVER_TAG, "Failed to read config blob: %d", ret);
		return ret;
	}

	return 0;
}

static int cy7_device_write_config(struct cy7_device *cy7_dev, struct cy7_config_blob *blob)
{
	int ret = 0;

	ret = usb_control_msg(cy7_dev->usb_dev,
			      usb_sndctrlpipe(cy7_dev->usb_dev, 0),
			      CY_WRITE_CONFIG_CMD,
			      USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT,
			      0,
			      0,
			      blob->buf,
			      sizeof(*blob),
			      100);

	if (ret != sizeof(*blob)) {
		SQ_ERROR(DRIVER_TAG, "Failed to write config blob: %d", ret);
		return ret;
	}

	return 0;
}

int cy7_read_configuration(struct cy7_device *cy7_dev)
{
	int ret = 0;
	struct cy7_config_blob *blob;
	struct cy7_device_config *cfg;

	blob = cy7_config_blob_alloc();
	if (!blob)
		return -ENOMEM;

	ret = cy7_set_maintainance_mode(cy7_dev);
	if (ret) {
		SQ_ERROR(DRIVER_TAG, "Failed to set maintainance mode: %d",
			ret);
		goto out_err;
	}

	ret = cy7_device_read_config(cy7_dev, blob);
	if (ret) {
		SQ_ERROR(DRIVER_TAG, "Failed to read device config: %d",
			ret);
		goto out_err;
	}
	cfg = (struct cy7_device_config *)blob->buf;

	SQ_INFO(DRIVER_TAG, "blob: %02x %02x %02x %02x\n",
		blob->buf[0],
		blob->buf[1],
		blob->buf[2],
		blob->buf[3]);

	SQ_INFO(DRIVER_TAG, "scb0 mode: %d, scb1 mode: %d\n",
		cfg->scb[0].mode, cfg->scb[1].mode);

	ret = cy7_unset_maintainance_mode(cy7_dev);
	if (ret) {
		SQ_ERROR(DRIVER_TAG,
			"Failed to set maintainance mode: %d", ret);
		goto out_err;
	}

	cy7_dev->config_blob = (struct cy7_device_config *)blob;
	return 0;

out_err:
	cy7_config_blob_free(blob);
	return ret;
}

static bool cy7_validate_configuration(struct cy7_device_config *cfg)
{
	uint32_t checksum;

	checksum = cy7_config_get_checksum(cfg);
	if (checksum != cfg->checksum)
		return false;

	if (cfg->usb.vendor_id != CY7_DEFAULT_VENDOR_ID)
		return false;

	return true;
}

int cy7_write_configuration(struct cy7_device *cy7_dev,
			    struct cy7_device_config *cfg, bool validate)
{
	int ret;
	struct cy7_config_blob *blob;

	if (validate && !cy7_validate_configuration(cfg))
		return -EINVAL;

	blob = cy7_config_blob_alloc();
	if (!blob)
		return -ENOMEM;

	memcpy(blob->buf, cfg, sizeof(*blob));

	ret = cy7_set_maintainance_mode(cy7_dev);
	if (ret) {
		SQ_ERROR(DRIVER_TAG, "Failed to set maintainance mode: %d",
			ret);
		goto out;
	}

	ret = cy7_device_write_config(cy7_dev, blob);
	if (ret) {
		SQ_ERROR(DRIVER_TAG, "Failed to read device config: %d",
			ret);
		goto out;
	}

	SQ_INFO(DRIVER_TAG, "scb0 mode: %d, scb1 mode: %d\n",
		cfg->scb[0].mode, cfg->scb[1].mode);

	ret = cy7_unset_maintainance_mode(cy7_dev);
	if (ret) {
		SQ_ERROR(DRIVER_TAG, "Failed to set maintainance mode: %d",
			ret);
		goto out;
	}

out:
	cy7_config_blob_free(blob);
	return ret;
}

int cy7_device_reset(struct cy7_device *cy7_dev)
{
	int ret;

	ret = usb_control_msg(cy7_dev->usb_dev,
				  usb_sndctrlpipe(cy7_dev->usb_dev, 0),
				  CY_DEVICE_RESET_CMD,
				  USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT,
				  CY_DEVICE_RESET_WVALUE,
				  CY_DEVICE_RESET_WINDEX,
				  NULL,
				  0,
				  100);
	if (ret) {
		SQ_ERROR(DRIVER_TAG,
			"reset request not sent, ret: %d", ret);
	}



	SQ_INFO(DRIVER_TAG, "reset request sent");

	/* USB hub will see port change event after any next USB request */
	ret = usb_control_msg(cy7_dev->usb_dev,
			      usb_sndctrlpipe(cy7_dev->usb_dev, 0),
			      CY_DEVICE_RESET_CMD,
			      USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT,
			      0,
			      0,
			      NULL,
			      0,
			      100);

	return 0;
}

int cy7_scb_update_config(struct cy7_device *cy7_dev)
{
	cy7_dev->config_blob->checksum =
		cy7_config_get_checksum(cy7_dev->config_blob);
	cy7_write_configuration(cy7_dev, cy7_dev->config_blob,
				true);
	return 0;
}
