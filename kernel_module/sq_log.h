#ifndef __SQ_LOG_H__
#define __SQ_LOG_H__

#include <linux/printk.h>

#define SQ_TAG          "SQ"
#define SQ_TAG_ERROR    "error"
#define SQ_TAG_INFO     "info"
#define SQ_TAG_WARNING  "warn"
#define SQ_TAG_DEBUG    "dbg"

#define KERN_TO_SQ(lvl) (\
	(strcmp(lvl, KERN_ERR) == 0) ? SQ_TAG_ERROR : \
		(strcmp(lvl, KERN_WARNING) == 0) ? SQ_TAG_WARNING : \
			(strcmp(lvl, KERN_INFO) == 0) ? SQ_TAG_INFO : \
				SQ_TAG_DEBUG)

#define SQ_LOG(lvl, driver_tag, fmt, ...) \
	printk( \
        lvl "[ %s ] [ %s ] [ %s ] " \
        fmt, SQ_TAG, driver_tag, \
		KERN_TO_SQ(lvl), ##__VA_ARGS__ \
	)

#define SQ_ERROR(driver_tag, fmt, ...) \
	SQ_LOG(KERN_ERR, driver_tag, fmt, ##__VA_ARGS__)

#define SQ_INFO(driver_tag, fmt, ...) \
	SQ_LOG(KERN_INFO, driver_tag, fmt, ##__VA_ARGS__)

#define SQ_WARNING(driver_tag, fmt,  ...) \
	SQ_LOG(KERN_WARNING, driver_tag, fmt, ##__VA_ARGS__)

#ifdef SQ_DEBUG
#define SQ_DBG(driver_tag, fmt, ...) \
	SQ_LOG(KERN_CRIT, driver_tag, fmt, ##__VA_ARGS__)
#else
#define SQ_DBG(driver_tag, fmt, ...)
#endif /* SQ_DEBUG */

#endif /* __SQ_LOG_H__ */
