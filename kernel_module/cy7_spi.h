/* SPDX-License-Identifier: GPL-2.0 */
/**
 *
 * @brief usb-debug-whistle linux kernel driver SPI SCB
 *        implementation.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_SPI_H
#define _CY7_SPI_H

#include "cy7_device.h"

int cy7_spi_probe(struct cy7_device *cy7_dev);

void cy7_spi_remove(struct cy7_device *cy7_dev);

#endif /* _CY7_SPI_H */
