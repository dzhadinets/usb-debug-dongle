// SPDX-License-Identifier: GPL-2.0
/**
 *
 * @brief usb-debug-whistle linux kernel driver SPI SCB
 *        implementation.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */

#include <linux/spi/spi.h>
#include <linux/usb.h>
#include "cypressdefs.h"
#include "cy7_spi.h"

#define CY7C65215_IF_ADDR (&(cy7_dev->usb_if->dev))
#define CY7C65215_SPI_BUS_NUM 0
#define CY7C65215_SPI_MODALIAS "spidev"
#define CY7C65215_SPI_MODE SPI_MODE_0
#define CY7C65215_SPI_MIN_FREQ 1000
#define CY7C65215_SPI_MAX_FREQ 3000000
#define CY7C65215_SPI_MIN_BITS_PER_WORD 4
#define CY7C65215_SPI_MAX_BITS_PER_WORD 16

#define cy7_spi_maser_to_dev(m)                                                \
	(*((struct cy7_device **)spi_master_get_devdata(m)))

static int cy7_spi_reset(struct cy7_device *cy7_dev)
{
	uint16_t wIndex = 0, wLength = 0;

	uint8_t bmRequestType =
		USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT;
	uint8_t bmRequest = CY_SPI_RESET_CMD;
	uint16_t wValue = cy7_dev->scb << 15;

	return usb_control_msg(cy7_dev->usb_dev,
			       usb_sndctrlpipe(cy7_dev->usb_dev, 0), bmRequest,
			       bmRequestType, wValue, wIndex, NULL, wLength,
			       100);
}

static int cy7_spi_get_status(struct cy7_device *cy7_dev, uint32_t *status)
{
	uint16_t wIndex = 0, wLength = sizeof(*status);

	uint16_t bmRequestType =
		USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_IN;
	uint16_t bmRequest = CY_SPI_GET_STATUS_CMD;
	uint16_t wValue = cy7_dev->scb << 15;
	int ret;

	ret = usb_control_msg(cy7_dev->usb_dev,
				 usb_rcvctrlpipe(cy7_dev->usb_dev, 0),
				 bmRequest, bmRequestType, wValue, wIndex,
				 status, wLength, 100);

	return (ret == wLength) ? 0 : ret;
}

static int cy7_spi_get_config(struct cy7_device *cy7_dev,
			      struct cy7_spi_config *cfg)
{
	uint16_t wIndex = 0, wLength = sizeof(*cfg);
	uint16_t wValue = cy7_dev->scb << 15;
	uint8_t bmRequestType = USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_IN;
	uint8_t bmRequest = CY_SPI_GET_CONFIG_CMD;
	int ret;

	ret = usb_control_msg(cy7_dev->usb_dev,
				 usb_rcvctrlpipe(cy7_dev->usb_dev, 0),
				 bmRequest, bmRequestType, wValue, wIndex, cfg,
				 wLength, 100);

	return (ret == wLength) ? 0 : ret;
}

static int cy7_spi_set_config(struct cy7_device *cy7_dev,
				  struct cy7_spi_config *cfg)
{
	uint16_t wIndex = 0, wLength = sizeof(*cfg);
	uint16_t wValue = cy7_dev->scb << 15;
	uint8_t bmRequestType =
		USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT;
	uint8_t bmRequest = CY_SPI_SET_CONFIG_CMD;
	int ret;

	ret = usb_control_msg(cy7_dev->usb_dev,
				 usb_sndctrlpipe(cy7_dev->usb_dev, 0),
				 bmRequest, bmRequestType, wValue, wIndex, cfg,
				 wLength, 100);

	return (ret == wLength) ? 0 : ret;
}

static int cy7_spi_do_bulk_tx(struct cy7_device *cy7_dev, int len)
{
	int ret;
	int actual;
	uint32_t *status;

	status = &cy7_dev->u.spi.status;

	ret = usb_bulk_msg(
		cy7_dev->usb_dev,
		usb_sndbulkpipe(cy7_dev->usb_dev,
				usb_endpoint_num(cy7_dev->ep_out)),
		cy7_dev->out_buf, len, &actual, 2000);

	if (ret) {
		SQ_ERROR(DRIVER_TAG, "BULK OUT failed %d", ret);

		cy7_spi_reset(cy7_dev);

		usb_clear_halt(cy7_dev->usb_dev,
			usb_sndbulkpipe(cy7_dev->usb_dev,
				usb_endpoint_num(cy7_dev->ep_out)));

		return ret;
	}

	ret = cy7_spi_get_status(cy7_dev, status);

	while (!ret && *status)
		ret = cy7_spi_get_status(cy7_dev, status);

	if (ret)
		SQ_ERROR(DRIVER_TAG, "BULK OUT failed on status %d", ret);

	return ret;
}

static int cy7_spi_do_bulk_rx(struct cy7_device *cy7_dev, uint8_t *rx, int len)
{
	int ret;
	int actual;
	int total;

	ret = usb_bulk_msg(cy7_dev->usb_dev,
			   usb_rcvbulkpipe(cy7_dev->usb_dev,
				usb_endpoint_num(cy7_dev->ep_in)),
			   cy7_dev->in_buf, len, &actual, 2000);

	if (ret) {
		SQ_ERROR(DRIVER_TAG, "BULK IN failed %d", ret);

		cy7_spi_reset(cy7_dev);

		usb_clear_halt(cy7_dev->usb_dev,
				usb_rcvbulkpipe(cy7_dev->usb_dev,
					usb_endpoint_num(cy7_dev->ep_in)));

		return ret;
	}

	total = actual;

	while (total < len) {
		ret = usb_bulk_msg(
			cy7_dev->usb_dev,
			usb_rcvbulkpipe(cy7_dev->usb_dev,
				usb_endpoint_num(cy7_dev->ep_in)),
			&cy7_dev->in_buf[total],
			len - total,
			&actual,
			2000);

		if (ret < 0) {
			SQ_ERROR(DRIVER_TAG, "BULK IN tail failed %d",
				ret);

			cy7_spi_reset(cy7_dev);

			usb_clear_halt(cy7_dev->usb_dev, usb_rcvbulkpipe(
					cy7_dev->usb_dev, usb_endpoint_num(
						cy7_dev->ep_in)));

			return ret;
		}

		total += actual;
	}

	memcpy(rx, cy7_dev->in_buf, len);

	return 0;
}

static int cy7_spi_usb_transfer(struct cy7_device *cy7_dev, const uint8_t *tx,
				uint8_t *rx, uint32_t size)
{
	uint16_t wIndex = size;
	uint16_t wValue = (cy7_dev->scb << 15);
	uint8_t bmRequestType =
		USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT;
	uint8_t bmRequest = CY_SPI_READ_WRITE_CMD;
	int ret;

	SQ_DBG(DRIVER_TAG, "tx=%p, rx=%p, size=%d", tx, rx, size);

	if ((tx && rx) || tx) {
		memcpy(cy7_dev->out_buf, tx, size);

		wValue |= CY_SPI_WRITE_BIT;
	}

	if (rx)
		wValue |= CY_SPI_READ_BIT;

	ret = usb_control_msg(cy7_dev->usb_dev,
			      usb_sndctrlpipe(cy7_dev->usb_dev, 0), bmRequest,
			      bmRequestType, wValue, wIndex, NULL, 0, 100);

	if (ret) {
		SQ_ERROR(DRIVER_TAG, "CTRL EP failed %d", ret);
		return ret;
	}

	if (tx) {
		ret = cy7_spi_do_bulk_tx(cy7_dev, (int)size);
		if (ret)
			return ret;
	}

	if (rx) {
		ret = cy7_spi_do_bulk_rx(cy7_dev, rx, (int)size);
		if (ret)
			return ret;
	}

	return 0;
}

/*
 * Checks that spi->mode fields match the cy7 device spi
 * configuration.
 */
static inline bool cy7_spi_match_cfg(struct spi_device *spi,
				     struct cy7_spi_config *cfg)
{
	uint32_t spi_cpol_cpha = spi->mode & (SPI_CPOL|SPI_CPHA);
	uint32_t cfg_cpol_cpha = cfg->cpha | (cfg->cpol << 1);
	bool spi_lsb;
	bool cfg_lsb;

	if (spi_cpol_cpha != cfg_cpol_cpha)
		return false;

	spi_lsb = !!(spi->mode & SPI_LSB_FIRST);
	cfg_lsb = !cfg->isMsbFirst;

	if (cfg_lsb != spi_lsb)
		return false;

	return true;
}

/*
 * updates config fields accodring to bitfields in spi->mode
 */
static inline void cy7_spi_update_config(struct cy7_spi_config *cfg,
					 struct spi_device *spi)
{
	cfg->cpha = ((spi->mode & SPI_CPHA) >> 0) & 1;
	cfg->cpol = ((spi->mode & SPI_CPOL) >> 1) & 1;
	cfg->isMsbFirst = !(((spi->mode & SPI_LSB_FIRST) >> 3) & 0x01);
}

int cy7_spi_sync_configs(struct cy7_device *cy7_dev, struct spi_device *spi,
			 struct cy7_spi_config *cfg)
{
	int ret;

	/* If controller mode matches cy7 spi config we are good */
	if (cy7_spi_match_cfg(spi, cfg))
		return 0;

	/*
	 * Mismatch means controller has been updated from outside,
	 * we should update the device now.
	 */
	SQ_INFO(DRIVER_TAG, "new mode 0x%08X", spi->mode);
	cy7_spi_update_config(cfg, spi);

	ret = cy7_spi_set_config(cy7_dev, cfg);

	if (ret)
		SQ_ERROR(DRIVER_TAG, "set configuration failed %d", ret);

	return ret;
}

static int cy7_spi_transfer_one(struct spi_master *master,
				struct spi_device *spi, struct spi_transfer *t)
{
	struct cy7_device *cy7_dev = cy7_spi_maser_to_dev(spi->master);
	int ret = 0;

	if (!cy7_dev)
		return -EIO;

	if (!master)
		return -EIO;

	if (!spi)
		return -EIO;

	if (!t)
		return -EIO;

	if (t->len > CY7C65215_USB_MAX_BULK_SIZE)
		return -EIO;

	mutex_lock(&cy7_dev->lock);

	ret = cy7_spi_sync_configs(cy7_dev, spi, &cy7_dev->u.spi.config);
	if (ret)
		goto out;

	ret = cy7_spi_usb_transfer(cy7_dev, t->tx_buf, t->rx_buf, t->len);

out:
	spi_finalize_current_transfer(master);

	mutex_unlock(&cy7_dev->lock);

	return ret;
}

/*
 * Latest kernels do not have spi_busnum_to_master, so we need to implement
 * this logic in this module.
 */
static int __spi_controller_match(struct device *dev, const void *data)
{
	struct spi_controller *ctlr;
	const u16 *bus_num = data;

	ctlr = container_of(dev, struct spi_controller, dev);
	return ctlr->bus_num == *bus_num;
}

static struct spi_controller *cy7_spi_busnum_to_master(struct class *cl, u16 bus_num)
{
	struct device           *dev;
	struct spi_controller   *ctlr = NULL;

	dev = class_find_device(cl, NULL, &bus_num,
						   __spi_controller_match);
	if (dev)
		ctlr = container_of(dev, struct spi_controller, dev);
	/* reference got in class_find_device */
	return ctlr;
}

int cy7_spi_probe(struct cy7_device *cy7_dev)
{
	int bus = 0;
	int ret;
	struct spi_board_info *bi;
	struct class *cl;
	struct spi_controller *ctrl;

	if (!cy7_dev)
		return -EINVAL;

	/* allocate a new SPI master with a pointer to cy7_device as device data */
	cy7_dev->u.spi.master = spi_alloc_master(CY7C65215_IF_ADDR,
						 sizeof(struct cy7_device *));
	if (!cy7_dev->u.spi.master) {
		SQ_ERROR(DRIVER_TAG, "SPI master allocation failed");
		return -ENOMEM;
	}

	cl = cy7_dev->u.spi.master->dev.class;

	/* search for next free bus number */
	while ((ctrl = cy7_spi_busnum_to_master(cl, bus))) {
		/* returns a refcounted pointer to an existing master */
		spi_master_put(ctrl);
		bus++;
	}

	bi = &cy7_dev->u.spi.board_info;

	/* save the pointer to cy7_dev in the SPI master device data field */
	cy7_spi_maser_to_dev(cy7_dev->u.spi.master) = cy7_dev;

	SQ_INFO(DRIVER_TAG, "SPI master connected to SPI bus %d", bus);

	/* set SPI master configuration */
	cy7_dev->u.spi.master->bus_num = -1;
	cy7_dev->u.spi.master->num_chipselect = CY7_SCB_BIND_SPI_DEVICE_MAX;
	cy7_dev->u.spi.master->mode_bits =
		SPI_MODE_3 | SPI_LSB_FIRST | SPI_READY;
	cy7_dev->u.spi.master->flags = SPI_MASTER_MUST_RX | SPI_MASTER_MUST_TX;
	cy7_dev->u.spi.master->bits_per_word_mask =
		SPI_BPW_RANGE_MASK(CY7C65215_SPI_MIN_BITS_PER_WORD,
				   CY7C65215_SPI_MAX_BITS_PER_WORD);
	cy7_dev->u.spi.master->transfer_one = cy7_spi_transfer_one;
	cy7_dev->u.spi.master->max_speed_hz = CY7C65215_SPI_MAX_FREQ;
	cy7_dev->u.spi.master->min_speed_hz = CY7C65215_SPI_MIN_FREQ;

	/* register the new master */
	ret = spi_register_master(cy7_dev->u.spi.master);
	if (ret) {
		SQ_ERROR(DRIVER_TAG, "could not register SPI master");
		spi_master_put(cy7_dev->u.spi.master);
		/* in case of error, reset the master to avoid crash during free */
		cy7_dev->u.spi.master = 0;
		return ret;
	}

	ret = cy7_spi_get_config(cy7_dev, &cy7_dev->u.spi.config);

	if (ret == 0) {
		SQ_INFO(DRIVER_TAG,
			 "config: %dHz,mode=%d,xfer=%d,%d,%s,%s,%s,%s,%s,%s,%s",
			 cy7_dev->u.spi.config.frequency,
			 cy7_dev->u.spi.config.dataWidth,
			 cy7_dev->u.spi.config.mode,
			 cy7_dev->u.spi.config.xferMode,
			 cy7_dev->u.spi.config.isMsbFirst ? "msb" : "lsb",
			 cy7_dev->u.spi.config.isMaster ? "master" : "slave",
			 cy7_dev->u.spi.config.isContinuous ? "cont" : "",
			 cy7_dev->u.spi.config.isSelectPrecede ? "sp" : "",
			 cy7_dev->u.spi.config.cpha ? "cpha" : "",
			 cy7_dev->u.spi.config.cpol ? "cpol" : "",
			 cy7_dev->u.spi.config.isLoopback ? "lo" : "");
	} else {
		SQ_ERROR(DRIVER_TAG, "failed to read configuration: %d",
			ret);
	}

	mutex_init(&cy7_dev->lock);

	return 0;
}

void cy7_spi_remove(struct cy7_device *cy7_dev)
{
	if (!cy7_dev)
		return;


	if (cy7_dev->u.spi.master) {
		spi_unregister_master(cy7_dev->u.spi.master);
		spi_master_put(cy7_dev->u.spi.master);
	}

	mutex_destroy(&cy7_dev->lock);
}
