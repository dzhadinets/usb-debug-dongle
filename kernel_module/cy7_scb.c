// SPDX-License-Identifier: GPL-2.0
/**
 *
 * @brief usb-debug-whistle linux kernel driver SCB config
 *        implementation.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/device.h>
#include <linux/module.h>

#include "sq_log.h"
#include "cy7_scb.h"
#include "cy7_device.h"
#include "cy7_config_default.h"

#include "cy7_gpio.h"

#include "cy7_scb_slavedev.h"

#ifndef sizeof_field
#define sizeof_field(TYPE, MEMBER) sizeof((((TYPE *)0)->MEMBER))
#endif

extern struct class *cy7_config_dev_class;

#define CY7C65215_MODE_NAME_MAX 32
#define CY7C65215_PARAM_LEN_MAX 32

struct min_max_val {
	int min;
	int max;
};

struct array_val {
	int size;
	const int *list;
};

enum cy7_attr_type {
	CY7_ATTR_TYPE_UNKNOWN = 0,
	CY7_ATTR_TYPE_ARRAY,
	CY7_ATTR_TYPE_MINMAX
};

union cy_scb_attr_val {
	struct array_val array;
	struct min_max_val minmax;
};

struct cy7_attribute {
	struct kobj_attribute kattr;

	int field_offset;
	int field_size;
	const char *(*get_string_val)(int val);
	enum cy7_attr_type type;
	union cy_scb_attr_val val;
};

#define CY7_SCB_ATTR(_name, _mode, _show, _store)                              \
	static struct cy7_attribute cy7_scb_attr_##_name = {                   \
		.kattr = __ATTR(_name, _mode, _show, _store),                  \
		.type = CY7_ATTR_TYPE_UNKNOWN                                  \
	}

#define CY7_SCB_ATTR_VAL(_name, _mode, _show, _store, _struct, _func, _type,   \
			 _val)                                                 \
	static struct cy7_attribute cy7_scb_attr_##_name = {                   \
		.kattr = __ATTR(_name, _mode, _show, _store),                  \
		.field_offset = offsetof(_struct, _name),                      \
		.field_size = sizeof_field(_struct, _name),                    \
		.get_string_val = _func,                                       \
		.type = _type,                                                 \
		.val = _val                                                    \
	}

#define CY7_SCB_ATTR_ARRAY_VAL(_name, _mode, _show, _store, _struct, _func,    \
			       _val)                                           \
	static struct cy7_attribute cy7_scb_attr_##_name = {                   \
		.kattr = __ATTR(_name, _mode, _show, _store),                  \
		.field_offset = offsetof(_struct, _name),                      \
		.field_size = sizeof_field(_struct, _name),                    \
		.get_string_val = _func,                                       \
		.type = CY7_ATTR_TYPE_ARRAY,                                   \
		.val = _val                                                    \
	}

#define CY7_SCB_ATTR_MINMAX_VAL(_name, _mode, _show, _store, _struct, _func,   \
				_val)                                          \
	static struct cy7_attribute cy7_scb_attr_##_name = {                   \
		.kattr = __ATTR(_name, _mode, _show, _store),                  \
		.field_offset = offsetof(_struct, _name),                      \
		.field_size = sizeof_field(_struct, _name),                    \
		.get_string_val = _func,                                       \
		.type = CY7_ATTR_TYPE_MINMAX,                                  \
		.val = _val                                                    \
	}

#define CY7_SCB_ATTR_FIELD(_name, _mode, _show, _store, _struct)               \
	static struct cy7_attribute cy7_scb_attr_##_name = {                   \
		.kattr = __ATTR(_name, _mode, _show, _store),                  \
		.field_offset = offsetof(_struct, _name),                      \
		.field_size = sizeof_field(_struct, _name)                     \
	}

static struct kobj_type sq_kobj_ktype = {
	.sysfs_ops = &kobj_sysfs_ops,
};

static inline struct cy7_attribute *
kattr_to_cy7attr(struct kobj_attribute *kattr)
{
	return container_of(kattr, struct cy7_attribute, kattr);
}

static ssize_t scb_mode_store(struct kobject *kobj, struct kobj_attribute *attr,
			      const char *buf, size_t len)
{
	int idx = 0;
	int found_idx = -1;
	char name[CY7C65215_MODE_NAME_MAX] = { 0 };
	struct cy7_scb_device *cy7_scb = NULL;
	struct cy7_attribute *c7_attr = NULL;
	struct cy7_scb_config *config = NULL;

	if (!kobj || !attr)
		return -ENODEV;

	cy7_scb = kobj_to_cy7_scb(kobj);
	c7_attr = kattr_to_cy7attr(attr);
	config = &cy7_scb->cy7_dev->config_blob->scb[cy7_scb->idx];

	(void)c7_attr;

	/* check string in buffer */
	if ((len == 0) || (strnlen(buf, CY7C65215_MODE_NAME_MAX) <= 0)) {
		SQ_ERROR(DRIVER_TAG, "Error string format");
		return -ENOBUFS;
	}

	strncpy(name, buf, len - 1);

	for (idx = 0; idx < CY7_SCB_MODE_NUM; idx++) {
		///TODO now we unsupported jtag mode for both SCB
		/* Only SBC1 contain JTAG */
		if (/*(cy7_scb->idx==0) && */ (
			strncmp("jtag", cy7_scb_mode_to_str(idx),
				CY7C65215_MODE_NAME_MAX) == 0))
			continue;

		if (strncmp(name, cy7_scb_mode_to_str(idx),
			    CY7C65215_MODE_NAME_MAX) == 0) {
			found_idx = idx;
			break;
		}
	};

	if (found_idx >= CY7_SCB_MODE_DISABLE &&
	    found_idx <= CY7_SCB_MODE_JTAG) {
		config->mode = found_idx;
		if (config->mode == CY7_SCB_MODE_DISABLE)
			cy7_set_defaults_disable(config);
		else if (config->mode == CY7_SCB_MODE_UART)
			cy7_set_defaults_uart(config);
		else if (config->mode == CY7_SCB_MODE_SPI)
			cy7_set_defaults_spi(config);
		else if (config->mode == CY7_SCB_MODE_I2C)
			cy7_set_defaults_i2c(config);
		else if (config->mode == CY7_SCB_MODE_JTAG)
			cy7_set_defaults_jtag(config);
	}

	if (found_idx == -1) {
		len = -EINVAL;
		SQ_ERROR(DRIVER_TAG, "scb%d: mode %s unsupported\n",
			 cy7_scb->idx, name);
	} else {
		cy7_scb_update_config(cy7_scb->cy7_dev);
		SQ_INFO(DRIVER_TAG, "scb%d: new mode %s applied\n",
			cy7_scb->idx, name);
	}

	return len;
}

static ssize_t scb_mode_show(struct kobject *kobj, struct kobj_attribute *attr,
			     char *buf)
{
	int idx = 0;
	ssize_t len = 0;
	struct cy7_scb_device *cy7_scb = NULL;
	struct cy7_attribute *c7_attr = NULL;
	struct cy7_scb_config *config = NULL;
	struct cy7_device_config *cy7_config = NULL;

	if (!kobj || !attr)
		return -ENODEV;

	cy7_scb = kobj_to_cy7_scb(kobj);
	c7_attr = kattr_to_cy7attr(attr);
	cy7_config = cy7_scb->cy7_dev->config_blob;
	config = &cy7_config->scb[cy7_scb->idx];

	(void)c7_attr;

	for (idx = 0; idx < CY7_SCB_MODE_NUM; idx++) {
		///TODO now we unsupported jtag mode for both SCB
		/* Only SBC1 contain JTAG */
		if (/*(cy7_scb->idx==0) && */ (
			strncmp("jtag", cy7_scb_mode_to_str(idx),
				CY7C65215_MODE_NAME_MAX) == 0))
			continue;

		if (idx == config->mode)
			len += scnprintf(buf + len, PAGE_SIZE - len, "[%s] ",
					 cy7_scb_mode_to_str(idx));
		else
			len += scnprintf(buf + len, PAGE_SIZE - len, "%s ",
					 cy7_scb_mode_to_str(idx));
	}

	len += scnprintf(len + buf, PAGE_SIZE - len, "\n");

	return len;
}

static ssize_t scb_config_store_kobj(struct kobject *kobj,
				     struct kobj_attribute *attr,
				     const char *buf, size_t len)
{
	int idx = 0;
	char new_val_str[CY7C65215_PARAM_LEN_MAX] = { 0 };
	bool found = false;
	struct cy7_scb_device *cy7_scb = NULL;
	struct cy7_attribute *c7_attr = NULL;
	struct cy7_scb_config *config = NULL;
	struct cy7_device_config *cy7_config = NULL;

	if (!kobj || !attr)
		return -ENODEV;

	cy7_scb = kobj_to_cy7_scb(kobj);
	c7_attr = kattr_to_cy7attr(attr);
	cy7_config = cy7_scb->cy7_dev->config_blob;
	config = &cy7_config->scb[cy7_scb->idx];

	/* check string in buffer */
	if ((len == 0) || (strnlen(buf, CY7C65215_PARAM_LEN_MAX) <= 0)) {
		SQ_ERROR(DRIVER_TAG, "Error string format\n");
		return -ENOBUFS;
	}

	strncpy(new_val_str, buf, len - 1);

	if (c7_attr->type == CY7_ATTR_TYPE_ARRAY) {
		for (idx = 0; idx < c7_attr->val.array.size; idx++) {
			if (strcmp(c7_attr->get_string_val(
					   c7_attr->val.array.list[idx]),
				   new_val_str) == 0) {
				int new_val = c7_attr->val.array.list[idx];

				memcpy((char *)&config->u +
					       c7_attr->field_offset,
				       &new_val, c7_attr->field_size);
				found = true;
				break;
			}
		}
	} else if (c7_attr->type == CY7_ATTR_TYPE_MINMAX) {
		int new_val = 0;

		if ((kstrtoint(new_val_str, 10, &new_val) == 0) &&
		    (c7_attr->val.minmax.min <= new_val) &&
		    (new_val <= c7_attr->val.minmax.max)) {
			memcpy((char *)&config->u + c7_attr->field_offset,
			       &new_val, c7_attr->field_size);
			found = true;
		}

	} else {
		SQ_ERROR(DRIVER_TAG, "attibute %s has unknown type\n",
			 c7_attr->kattr.attr.name);
	}

	if (!found) {
		len = -EINVAL;
		SQ_ERROR(DRIVER_TAG, "wrong parameter %s\n", new_val_str);
	} else {
		cy7_scb_update_config(cy7_scb->cy7_dev);
		SQ_INFO(DRIVER_TAG,
			"scb%d: new value \"%s\" for parameter %s applied",
			cy7_scb->idx, new_val_str, c7_attr->kattr.attr.name);
	}

	return len;
}

static ssize_t scb_config_show_kobj(struct kobject *kobj,
				    struct kobj_attribute *attr, char *buf)
{
	int idx = 0;
	int len = 0;
	int cur_val = 0;
	char str_val[32] = { 0 };
	struct cy7_scb_device *cy7_scb = NULL;
	struct cy7_attribute *c7_attr = NULL;
	struct cy7_scb_config *config = NULL;
	struct cy7_device_config *cy7_config = NULL;

	if (!kobj || !attr)
		return -ENODEV;

	cy7_scb = kobj_to_cy7_scb(kobj);
	c7_attr = kattr_to_cy7attr(attr);
	cy7_config = cy7_scb->cy7_dev->config_blob;
	config = &cy7_config->scb[cy7_scb->idx];

	memcpy(&cur_val, (char *)&config->u + c7_attr->field_offset,
	       c7_attr->field_size);
	snprintf(str_val, 32, "%s", c7_attr->get_string_val(cur_val));

	if (c7_attr->type == CY7_ATTR_TYPE_ARRAY) {
		for (idx = 0; idx < c7_attr->val.array.size; idx++) {
			if (strcmp(c7_attr->get_string_val(
					   c7_attr->val.array.list[idx]),
				   str_val) == 0) {
				len += scnprintf(
					buf + len, PAGE_SIZE - len, "[%s] ",
					c7_attr->get_string_val(
						c7_attr->val.array.list[idx]));
			} else {
				len += scnprintf(
					buf + len, PAGE_SIZE - len, "%s ",
					c7_attr->get_string_val(
						c7_attr->val.array.list[idx]));
			}
		}
		len += scnprintf(buf + len, PAGE_SIZE - len, "\n");
	} else if (c7_attr->type == CY7_ATTR_TYPE_MINMAX) {
		len += scnprintf(
			buf + len, PAGE_SIZE - len, "%s ",
			c7_attr->get_string_val(c7_attr->val.minmax.min));

		len += scnprintf(buf + len, PAGE_SIZE - len,
				 "=< [%s] >=", str_val);

		len += scnprintf(
			buf + len, PAGE_SIZE - len, " %s\n",
			c7_attr->get_string_val(c7_attr->val.minmax.max));

	} else {
		SQ_ERROR(DRIVER_TAG, "attibute %s has unknown type\n",
			 c7_attr->kattr.attr.name);
	}

	return len;
}

static ssize_t scb_indication_store(struct kobject *kobj,
				    struct kobj_attribute *attr,
				    const char *buf, size_t len)
{
	char new_val_str[CY7C65215_PARAM_LEN_MAX] = { 0 };
	int new_val = 0xFF;
	struct cy7_scb_device *cy7_scb = NULL;
	struct cy7_attribute *c7_attr = NULL;
	struct cy7_scb_config *config = NULL;
	struct cy7_device_config *cy7_config = NULL;

	if (!kobj || !attr)
		return -ENODEV;

	cy7_scb = kobj_to_cy7_scb(kobj);
	c7_attr = kattr_to_cy7attr(attr);
	cy7_config = cy7_scb->cy7_dev->config_blob;
	config = &cy7_config->scb[cy7_scb->idx];

	/* check string in buffer */
	if ((len == 0) || (strnlen(buf, CY7C65215_PARAM_LEN_MAX) <= 0)) {
		SQ_ERROR(DRIVER_TAG, "Error string format\n");
		return -ENOBUFS;
	}

	strncpy(new_val_str, buf, len - 1);

	if (kstrtoint(new_val_str, 10, &new_val) != 0)
		return -EINVAL;

	if ((new_val < CY7_NUM_GPIO &&
	    is_gpio_avalible_for_led(cy7_scb->cy7_dev, new_val) == true) ||
	    new_val == 255) {
		memcpy((char *)config + c7_attr->field_offset, &new_val,
		       c7_attr->field_size);
		cy7_scb_update_config(cy7_scb->cy7_dev);
		cy7_gpio_update_states(cy7_scb->cy7_dev);
	} else {
		SQ_WARNING(DRIVER_TAG, "gpio%d unavailable\n", new_val);
	}

	return len;
}

static ssize_t scb_indication_show(struct kobject *kobj,
				   struct kobj_attribute *attr, char *buf)
{
	int idx = 0;
	int len = 0;
	bool found = false;
	struct cy7_scb_device *cy7_scb = NULL;
	struct cy7_attribute *c7_attr = NULL;
	struct cy7_scb_config *config = NULL;
	struct cy7_device_config *cy7_config = NULL;
	int cur_val = 0xFF;

	if (!kobj || !attr)
		return -ENODEV;

	cy7_scb = kobj_to_cy7_scb(kobj);
	c7_attr = kattr_to_cy7attr(attr);
	cy7_config = cy7_scb->cy7_dev->config_blob;
	config = &cy7_config->scb[cy7_scb->idx];

	memcpy(&cur_val, (char *)config + c7_attr->field_offset,
	       c7_attr->field_size);

	for (idx = 0; idx < CY7_NUM_GPIO; idx++) {
		if (idx == cur_val) {
			len += scnprintf(buf + len, PAGE_SIZE - len, "[%d] ",
					 idx);
			found = true;
		} else if (is_gpio_avalible_for_led(cy7_scb->cy7_dev, idx) == true) {
			len += scnprintf(buf + len, PAGE_SIZE - len, "%d ",
					 idx);
		}
	}

	len += scnprintf(buf + len, PAGE_SIZE - len, "%s\n",
			 found ? "255" : "[255]");

	return len;
}

/* SCB mode attribute */
CY7_SCB_ATTR(mode, 0664, scb_mode_show, scb_mode_store);

/* Indication attributes */
CY7_SCB_ATTR_FIELD(tx_led, 0664, scb_indication_show, scb_indication_store,
		   struct cy7_scb_config);
CY7_SCB_ATTR_FIELD(rx_led, 0664, scb_indication_show, scb_indication_store,
		   struct cy7_scb_config);
CY7_SCB_ATTR_FIELD(rx_tx_led, 0664, scb_indication_show, scb_indication_store,
		   struct cy7_scb_config);

static struct attribute *cy7_scb_indication_attrs[] = {
	&cy7_scb_attr_tx_led.kattr.attr,
	&cy7_scb_attr_rx_led.kattr.attr,
	&cy7_scb_attr_rx_tx_led.kattr.attr,
	NULL,
};

static const struct attribute_group cy7_scb_indication_group = {
	.name = "indication",
	.attrs = cy7_scb_indication_attrs,
};

/* UART attributes */
static const union cy_scb_attr_val cy7_scb_attr_val_uart_baudrate = {
	.array.size = ARRAY_SIZE(cy7_scb_uart_speeds),
	.array.list = cy7_scb_uart_speeds,
};

static const union cy_scb_attr_val cy7_scb_attr_val_uart_type = {
	.array.size = ARRAY_SIZE(cy7_scb_uart_types),
	.array.list = cy7_scb_uart_types,
};

static const union cy_scb_attr_val cy7_scb_attr_val_uart_data_width = {
	.array.size = ARRAY_SIZE(cy7_scb_uart_data_widths),
	.array.list = cy7_scb_uart_data_widths,
};

static const union cy_scb_attr_val cy7_scb_attr_val_uart_stop_bits = {
	.array.size = ARRAY_SIZE(cy7_scb_uart_stop_bits),
	.array.list = cy7_scb_uart_stop_bits,
};

static const union cy_scb_attr_val cy7_scb_attr_val_uart_protocols = {
	.array.size = ARRAY_SIZE(cy7_scb_uart_protocols),
	.array.list = cy7_scb_uart_protocols,
};

static const union cy_scb_attr_val cy7_scb_attr_val_uart_paritys = {
	.array.size = ARRAY_SIZE(cy7_scb_uart_paritys),
	.array.list = cy7_scb_uart_paritys,
};

static const union cy_scb_attr_val cy7_scb_attr_val_uart_bit_positions = {
	.array.size = ARRAY_SIZE(cy7_scb_uart_bit_positions),
	.array.list = cy7_scb_uart_bit_positions,
};

static const union cy_scb_attr_val cy7_scb_attr_val_uart_flow_ctrls = {
	.array.size = ARRAY_SIZE(cy7_scb_uart_flow_ctrls),
	.array.list = cy7_scb_uart_flow_ctrls,
};

CY7_SCB_ATTR_ARRAY_VAL(uart_baud_rate, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_uart,
		       &cy7_scb_uart_baudrate_to_str,
		       cy7_scb_attr_val_uart_baudrate);
CY7_SCB_ATTR_ARRAY_VAL(uart_type, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_uart,
		       &cy7_scb_uart_type_to_str, cy7_scb_attr_val_uart_type);
CY7_SCB_ATTR_ARRAY_VAL(uart_data_width, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_uart,
		       &cy7_scb_uart_data_width_to_str,
		       cy7_scb_attr_val_uart_data_width);
CY7_SCB_ATTR_ARRAY_VAL(uart_stop_bits, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_uart,
		       &cy7_scb_uart_stop_bits_to_str,
		       cy7_scb_attr_val_uart_stop_bits);
CY7_SCB_ATTR_ARRAY_VAL(uart_protocol, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_uart,
		       &cy7_scb_uart_protocol_to_str,
		       cy7_scb_attr_val_uart_protocols);
CY7_SCB_ATTR_ARRAY_VAL(uart_parity, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_uart,
		       &cy7_scb_uart_parity_to_str,
		       cy7_scb_attr_val_uart_paritys);
CY7_SCB_ATTR_ARRAY_VAL(uart_bit_position, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_uart,
		       &cy7_scb_uart_bit_position_to_str,
		       cy7_scb_attr_val_uart_bit_positions);
CY7_SCB_ATTR_ARRAY_VAL(uart_flow_ctrl, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_uart,
		       &cy7_scb_uart_flow_ctrl_to_str,
		       cy7_scb_attr_val_uart_flow_ctrls);

///TODO
//uart_tx_retry_on_nack;
//uart_rx_invert_polarity;
//uart_drop_on_err;
//uart_loopback;
//uart_line_active_state;

static struct attribute *cy7_scb_uart_attrs[] = {
	&cy7_scb_attr_uart_baud_rate.kattr.attr,
	&cy7_scb_attr_uart_type.kattr.attr,
	&cy7_scb_attr_uart_data_width.kattr.attr,
	&cy7_scb_attr_uart_stop_bits.kattr.attr,
	&cy7_scb_attr_uart_protocol.kattr.attr,
	&cy7_scb_attr_uart_parity.kattr.attr,
	&cy7_scb_attr_uart_flow_ctrl.kattr.attr,
	&cy7_scb_attr_uart_bit_position.kattr.attr,
	NULL,
};

static const struct attribute_group cy7_scb_uart_group = {
	.name = "uart",
	.attrs = cy7_scb_uart_attrs,
};

/* I2C attributes */
const union cy_scb_attr_val cy7_scb_attr_val_i2c_freq = {
	.minmax = { .min = CY7_I2C_FREQ_MIN, .max = CY7_I2C_FREQ_MAX }
};

const union cy_scb_attr_val cy7_scb_attr_val_i2c_slave_addr = {
	.minmax = { .min = CY7_I2C_SLAVE_ADDRESS_MIN,
		    .max = CY7_I2C_SLAVE_ADDRESS_MAX }
};

const union cy_scb_attr_val cy7_scb_attr_val_i2c_bit_position = {
	.array.size = ARRAY_SIZE(cy7_scb_i2c_bit_positions),
	.array.list = cy7_scb_i2c_bit_positions,
};

const union cy_scb_attr_val cy7_scb_attr_val_i2c_stretch = {
	.array.size = ARRAY_SIZE(cy7_scb_i2c_stretch),
	.array.list = cy7_scb_i2c_stretch,
};

const union cy_scb_attr_val cy7_scb_attr_val_i2c_mode = {
	.array.size = ARRAY_SIZE(cy7_scb_i2c_modes),
	.array.list = cy7_scb_i2c_modes,
};

CY7_SCB_ATTR_MINMAX_VAL(i2c_freq, 0664, scb_config_show_kobj,
			scb_config_store_kobj, struct cy7_scb_config_i2c,
			&cy7_scb_i2c_freq_to_str, cy7_scb_attr_val_i2c_freq);
//CY7_SCB_ATTR_MINMAX_VAL(i2c_slave_address, 0664, scb_config_show_kobj,
//			scb_config_store_kobj, struct cy7_scb_config_i2c,
//			&cy7_scb_i2c_slave_addr_to_str, cy7_scb_attr_val_i2c_slave_addr);
//CY7_SCB_ATTR_ARRAY_VAL(i2c_is_master, 0664, scb_config_show_kobj,
//			scb_config_store_kobj, struct cy7_scb_config_i2c,
//			&cy7_scb_i2c_mode_to_str, cy7_scb_attr_val_i2c_mode);
CY7_SCB_ATTR_ARRAY_VAL(i2c_bit_position, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_i2c,
		       &cy7_scb_i2c_bit_position_to_str,
		       cy7_scb_attr_val_i2c_bit_position);
CY7_SCB_ATTR_ARRAY_VAL(i2c_clock_stretch, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_i2c,
		       &cy7_scb_i2c_stretch_to_str,
		       cy7_scb_attr_val_i2c_stretch);

///TODO
//i2c_signore;
//i2c_loopback;

static struct attribute *cy7_scb_i2c_attrs[] = {
	&cy7_scb_attr_i2c_freq.kattr.attr,
	//	&cy7_scb_attr_i2c_is_master.kattr.attr,
	//	&cy7_scb_attr_i2c_slave_address.kattr.attr,
	&cy7_scb_attr_i2c_bit_position.kattr.attr,
	&cy7_scb_attr_i2c_clock_stretch.kattr.attr,
	NULL,
};

static const struct attribute_group cy7_scb_i2c_group = {
	.name = "i2c",
	.attrs = cy7_scb_i2c_attrs,
};

/* SPI attributes */
const union cy_scb_attr_val cy7_scb_attr_val_spi_freq = {
	.minmax = { .min = CY7_SPI_FREQ_MIN, .max = CY7_SPI_FREQ_MAX }
};

const union cy_scb_attr_val cy7_scb_attr_val_spi_mode = {
	.array.size = ARRAY_SIZE(cy7_scb_spi_modes),
	.array.list = cy7_scb_spi_modes,
};

const union cy_scb_attr_val cy7_scb_attr_val_spi_protocol = {
	.array.size = ARRAY_SIZE(cy7_scb_spi_protocols),
	.array.list = cy7_scb_spi_protocols,
};

const union cy_scb_attr_val cy7_scb_attr_val_spi_ssn_toggle_mode = {
	.array.size = ARRAY_SIZE(cy7_scb_spi_ssn_toggles),
	.array.list = cy7_scb_spi_ssn_toggles,
};

const union cy_scb_attr_val cy7_scb_attr_val_spi_data_width = {
	.minmax = { .min = CY7_SPI_DATA_WIDTH_MIN,
		    .max = CY7_SPI_DATA_WIDTH_MAX }
};

const union cy_scb_attr_val cy7_scb_attr_val_spi_bit_position = {
	.array.size = ARRAY_SIZE(cy7_scb_spi_bit_positions),
	.array.list = cy7_scb_spi_bit_positions,
};

const union cy_scb_attr_val cy7_scb_attr_val_spi_xfer_mode = {
	.array.size = ARRAY_SIZE(cy7_scb_spi_xfer_mode),
	.array.list = cy7_scb_spi_xfer_mode,
};

CY7_SCB_ATTR_MINMAX_VAL(spi_freq, 0664, scb_config_show_kobj,
			scb_config_store_kobj, struct cy7_scb_config_spi,
			&cy7_scb_spi_freq_to_str, cy7_scb_attr_val_spi_freq);
//CY7_SCB_ATTR_ARRAY_VAL(spi_is_master, 0664, scb_config_show_kobj,
//			scb_config_store_kobj, struct cy7_scb_config_spi,
//			&cy7_scb_spi_mode_to_str, cy7_scb_attr_val_spi_mode);
CY7_SCB_ATTR_ARRAY_VAL(spi_protocol, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_spi,
		       &cy7_scb_spi_protocol_to_str,
		       cy7_scb_attr_val_spi_protocol);
CY7_SCB_ATTR_ARRAY_VAL(spi_ssn_toggle_mode, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_spi,
		       &cy7_scb_spi_ssn_toggles_to_str,
		       cy7_scb_attr_val_spi_ssn_toggle_mode);
CY7_SCB_ATTR_MINMAX_VAL(spi_data_width, 0664, scb_config_show_kobj,
			scb_config_store_kobj, struct cy7_scb_config_spi,
			&cy7_scb_spi_data_width_to_str,
			cy7_scb_attr_val_spi_data_width);
CY7_SCB_ATTR_ARRAY_VAL(spi_bit_position, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_spi,
		       &cy7_scb_spi_bit_position_to_str,
		       cy7_scb_attr_val_spi_bit_position);
CY7_SCB_ATTR_ARRAY_VAL(spi_transfer_mode, 0664, scb_config_show_kobj,
		       scb_config_store_kobj, struct cy7_scb_config_spi,
		       &cy7_scb_spi_xfer_mode_to_str,
		       cy7_scb_attr_val_spi_xfer_mode);

///TODO
//spi_select_precede;
//spi_cpha;
//spi_cpol;
//spi_loopback;

static struct attribute *cy7_scb_spi_attrs[] = {
	//	&cy7_scb_attr_spi_is_master.kattr.attr,
	&cy7_scb_attr_spi_freq.kattr.attr,
	&cy7_scb_attr_spi_protocol.kattr.attr,
	&cy7_scb_attr_spi_ssn_toggle_mode.kattr.attr,
	&cy7_scb_attr_spi_data_width.kattr.attr,
	&cy7_scb_attr_spi_bit_position.kattr.attr,
	&cy7_scb_attr_spi_transfer_mode.kattr.attr,
	NULL,
};

static const struct attribute_group cy7_scb_spi_group = {
	.name = "spi",
	.attrs = cy7_scb_spi_attrs,
};

/* JTAG attributes */
const union cy_scb_attr_val cy7_scb_attr_val_jtag_freq = {
	.minmax = { .min = CY7_JTAG_FREQ_MIN, .max = CY7_JTAG_FREQ_MAX }
};

CY7_SCB_ATTR_MINMAX_VAL(jtag_freq, 0664, scb_config_show_kobj,
			scb_config_store_kobj, struct cy7_scb_config_jtag,
			&cy7_scb_jtag_freq_to_str, cy7_scb_attr_val_jtag_freq);

static struct attribute *cy7_scb_jtag_attrs[] = {
	&cy7_scb_attr_jtag_freq.kattr.attr,
	NULL,
};

static const struct attribute_group cy7_scb_jtag_group = {
	.name = "jtag",
	.attrs = cy7_scb_jtag_attrs,
};

ssize_t cy7_reset_show(struct device *dev, struct device_attribute *attr,
		       char *buf)
{
	int len = 0;

	(void)dev;
	(void)attr;
	(void)buf;

	len += scnprintf(buf + len, PAGE_SIZE - len,
			 "Write any value to reset\n");

	return len;
}

ssize_t cy7_reset_store(struct device *dev, struct device_attribute *attr,
			const char *buf, size_t len)
{
	struct cy7_device *cy7_dev = NULL;
	(void)attr;
	(void)buf;
	(void)len;

	if (IS_ERR_OR_NULL(dev))
		return -ENODEV;

	cy7_dev = (struct cy7_device *)dev_get_drvdata(dev);

	cy7_device_reset(cy7_dev);

	return len;
}

DEVICE_ATTR(reset, 0664, cy7_reset_show, cy7_reset_store);

int cy7_scb_dev_create(struct cy7_device *cy7_dev)
{
	int ret = 0;
	int idx = 0;
	char name[32] = { 0 };

	if (IS_ERR_OR_NULL(cy7_dev))
		return -ENODEV;

	cy7_dev->config_dev =
		device_create(cy7_config_dev_class, NULL, MKDEV(0, 0), cy7_dev,
			      "whistle_device-%d.%d",
			      cy7_dev->usb_dev->bus->busnum,
			      cy7_dev->usb_dev->devnum);

	if (cy7_dev->config_dev == NULL)
		return PTR_ERR(cy7_dev->config_dev);

	ret = cy7_gpio_probe(cy7_dev);
	if (ret < 0) {
		device_del(cy7_dev->config_dev);
		return ret;
	}

	for (idx = 0; idx < CY7C65215_SCB_DEV_MAX; idx++) {
		struct cy7_scb_device *scb_dev = &cy7_dev->scb_dev[idx];
		struct cy7_device_config *cy7_config = cy7_dev->config_blob;
		struct cy7_scb_config *scb_config = &cy7_config->scb[idx];

		scb_dev->idx = idx;
		scb_dev->cy7_dev = cy7_dev;

		snprintf(name, 32, "scb%d", idx);

		kobject_init(&scb_dev->scb_kobj, &sq_kobj_ktype);
		ret = kobject_add(&scb_dev->scb_kobj,
				  &cy7_dev->config_dev->kobj, "%s", name);
		if (ret) {
			SQ_ERROR(DRIVER_TAG, "sq_kobject_add error: %d\n", ret);
			kobject_put(&scb_dev->scb_kobj);
			continue;
		}

		ret = sysfs_create_file(&scb_dev->scb_kobj,
					&cy7_scb_attr_mode.kattr.attr);
		if (ret != 0) {
			kobject_put(&scb_dev->scb_kobj);
			continue;
		}
		ret = sysfs_create_group(&scb_dev->scb_kobj,
					 &cy7_scb_indication_group);
		if (ret != 0) {
			sysfs_remove_file(&scb_dev->scb_kobj,
					  &cy7_scb_attr_mode.kattr.attr);
			kobject_put(&scb_dev->scb_kobj);
			continue;
		}

		if (scb_config->mode == CY7_SCB_MODE_UART) {
			ret = sysfs_create_group(&scb_dev->scb_kobj,
						 &cy7_scb_uart_group);
		} else if (scb_config->mode == CY7_SCB_MODE_SPI) {
			ret = sysfs_create_group(&scb_dev->scb_kobj,
						 &cy7_scb_spi_group);
		} else if (scb_config->mode == CY7_SCB_MODE_I2C) {
			ret = sysfs_create_group(&scb_dev->scb_kobj,
						 &cy7_scb_i2c_group);
		}
		///TODO now we unsupported jtag mode for both SCB
#if 0
		else if (scb_config->mode == CY7_SCB_MODE_JTAG) {
			ret = sysfs_create_group(&scb_dev->scb_kobj,
							&cy7_scb_jtag_group);
		}
#endif
		else if (scb_config->mode == CY7_SCB_MODE_DISABLE) {
			SQ_INFO(DRIVER_TAG, "SCB%d has disabled interface",
				idx);
		} else {
			SQ_ERROR(DRIVER_TAG, "SCB%d has unknown interface",
				 idx);
		}

		if (ret != 0) {
			sysfs_remove_group(&scb_dev->scb_kobj,
					   &cy7_scb_indication_group);
			sysfs_remove_file(&scb_dev->scb_kobj,
					  &cy7_scb_attr_mode.kattr.attr);
			kobject_put(&scb_dev->scb_kobj);
			continue;
		}

		cy7_scb_slavedev_probe(scb_dev); /* don't check. secondary functional */

		SQ_INFO(DRIVER_TAG, "SCB%d created", idx);
	}

	device_create_file(cy7_dev->config_dev, &dev_attr_reset);

	return ret;
}

void cy7_scb_dev_remove(struct cy7_device *cy7_dev)
{
	int idx = 0;

	if (IS_ERR_OR_NULL(cy7_dev))
		return;

	cy7_gpio_remove(cy7_dev);

	for (idx = 0; idx < CY7C65215_SCB_DEV_MAX; idx++) {
		struct cy7_scb_device *scb_dev = &cy7_dev->scb_dev[idx];
		struct cy7_device_config *dev_config = cy7_dev->config_blob;
		struct cy7_scb_config *scb_config =
			&dev_config->scb[scb_dev->idx];
		if (scb_config->mode == CY7_SCB_MODE_UART) {
			sysfs_remove_group(&scb_dev->scb_kobj,
					   &cy7_scb_uart_group);
		} else if (scb_config->mode == CY7_SCB_MODE_SPI) {
			sysfs_remove_group(&scb_dev->scb_kobj,
					   &cy7_scb_spi_group);
		} else if (scb_config->mode == CY7_SCB_MODE_I2C) {
			sysfs_remove_group(&scb_dev->scb_kobj,
					   &cy7_scb_i2c_group);
		}
		///TODO now we unsupported jtag mode for both SCB
#if 0
		else if (scb_config->mode == CY7_SCB_MODE_JTAG) {
			sysfs_remove_group(&scb_dev->scb_kobj,
						&cy7_scb_jtag_group);
		}
#endif
		else if (scb_config->mode == CY7_SCB_MODE_DISABLE) {
			SQ_INFO(DRIVER_TAG, "SCB%d has disabled interface",
				idx);
		} else {
			SQ_ERROR(DRIVER_TAG, "SCB%d has unknown interface",
				 idx);
		}

		cy7_scb_slavedev_remove(scb_dev);

		sysfs_remove_group(&scb_dev->scb_kobj,
				   &cy7_scb_indication_group);
		sysfs_remove_file(&scb_dev->scb_kobj,
				  &cy7_scb_attr_mode.kattr.attr);
		kobject_put(&scb_dev->scb_kobj);

		SQ_INFO(DRIVER_TAG, "SCB%d removed", idx);
	}

	device_remove_file(cy7_dev->config_dev, &dev_attr_reset);

	device_del(cy7_dev->config_dev);
}
