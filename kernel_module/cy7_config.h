/* SPDX-License-Identifier: GPL-2.0 */
/**
 *
 * @brief usb-debug-whistle linux kernel driver config blob
 *        structure and values.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_DEVICE_CONFIG_H
#define _CY7_DEVICE_CONFIG_H
#include <linux/types.h>
#include <linux/build_bug.h>

#include <cy7_structs.h>

static inline const char *cy7_scb_mode_to_str(int mode)
{
	switch (mode) {
	case CY7_SCB_MODE_DISABLE:
		return "off";
	case CY7_SCB_MODE_UART:
		return "uart";
	case CY7_SCB_MODE_SPI:
		return "spi";
	case CY7_SCB_MODE_I2C:
		return "i2c";
	case CY7_SCB_MODE_JTAG:
		return "jtag";
	default:
		return "unknown";
	}
}

/*
 * cy7_config_do_checksum - calculates checksum and config field
 * to that value.
 */
static __maybe_unused uint32_t cy7_config_get_checksum(struct cy7_device_config *cfg)
{
	uint32_t *ptr = (uint32_t *)cfg;
	uint32_t *end = ptr + sizeof(*cfg) / sizeof(*ptr);
	uint32_t sum = 0;

	/* skip first 12 bytes */
	ptr += 3;

	while (ptr < end) {
		sum += *ptr;
		ptr++;
	}

	return sum;
}

#endif /* _CY7_DEVICE_CONFIG_H */
