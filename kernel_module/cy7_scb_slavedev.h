/* SPDX-License-Identifier: GPL-2.0 */
/**
 *
 * @brief usb-debug-whistle linux kernel driver SCB config
 *        implementation.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_SCB_SLAVEDEV_H
#define _CY7_SCB_SLAVEDEV_H

#include <linux/types.h>
#include  "cy7_device.h"

int cy7_scb_slavedev_probe(struct cy7_scb_device *scb_dev);
int cy7_scb_slavedev_remove(struct cy7_scb_device *scb_dev);

#endif /* _CY7_SCB_SLAVEDEV_H */
