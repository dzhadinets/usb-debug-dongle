# CY7C65215 USB to I2C Linux kernel driver

The driver can be used with **CY7C65215** USB to UART/I2C/SPI adapter boards to connect I2C/SPI devices to a Linux host.

Additionally, CY7C65215 data pins that are not used for synchronous serial interfaces can be configured as **GPIO** pins. (NOT IMPLEMENTED YET)

The I2C interface driver was initially derived from CH341 I2C driver from Gunar Schorcht [https://github.com/gschorcht/i2c-ch341-usb.git]
The SPI interface driver was initially derived from CH341 SPI driver from Gunar Schorcht [https://github.com/gschorcht/spi-ch341-usb.git]

## I2C interface limitations

By default, the driver uses the standard I2C bus speed of **100 kbps**. I2C bus speeds up to **400 kbps** are also available. 

Currently only basic I2C read and write functions (**```I2C_FUNC_I2C```**) are supported natively. However, SMBus protocols are emulated (**```I2C_FUNC_SMBUS_EMUL```**) with the exception of SMBus Block Read (```I2C_FUNC_SMBUS_READ_BLOCK_DATA```) and SMBus Block Process Call (```I2C_FUNC_SMBUS_BLOCK_PROC_CALL```).

The CY7C65215 only supports **7 bit addressing**.

Due of the limited CY7C65215 USB endpoint buffer size of 64 byte that is used for I2C data as well as adapter in-band signaling, the driver supports only I2C messages with a **maximum data size of 64 bytes**.

## Limitations of the SPI interface

The **SPI hardware interface** implementation is limited to

- **SPI mode 0, 1, 2, 3** are supported in Motorola format only (support for TI and other can be added in the SPI driver)
- SCK frequency in range 1000Hz-3MHz in case of single interface (come os SCB disabled) or in range 1000Hz-1.5MHz in case both SCB are enabled
- low active CS signal,
- single bit transfers,
- 4-16 bits per word 
- 1 slave at maximum per SCB
- master mode only (slave support can be added in the SPI driver)

The transmission with MSB first (```SPI_MSB_FIRST```) and LSB first (```SPI_LSB_FIRST```) are supported

## Installation of the driver

#### Prerequisites

To compile the driver, you must have installed current **kernel header files**. 

Even though it is not mandatory, it is highly recommended to use **DKMS** (dynamic kernel module support) for the installation of the driver. DKMS allows to manage kernel modules whose sources reside outside the kernel source tree. Such modules are then automatically rebuilt when a new kernel version is installed.

To use DKMS, it has to be installed before, e.g., with command
```
sudo apt-get install dkms
```
on Debian based systems.

#### Installaton

The driver can be compiled with following commands:

```
make
sudo make install
```

If **DKMS** is installed (**recommended**), command ```sudo make install``` adds the driver to the DKMS tree so that the driver is recompiled automatically when a new kernel version is installed.

> **_NOTE:_**  If dkms fails build step  with ```ERROR: Kernel configuration is invalid.```, try reinstalling linux-headers ```sudo apt install --reinstall linux-headers-$(uname -r)```

In case you have not installed DKMS, command ```sudo make install``` simply copies the driver after compilation to the kernel modules directory. However, the module will not be loadable anymore and have to be recompiled explicitly when kernel version changes.

If you do not want to install the driver in the kernel directory at all because you only want to load it manually when needed, simply omit the ```sudo make install```.

#### Loading

Once the driver is installed, it should be loaded automatically when you connect a device with USB device id ```04b4:000a```. If not try to figure out, whether the USB device is detected correctly using command

```
lsusb
```
and try to load it manually with command:
```
insmod i2c-CY7C65215-usb.ko
```

#### Uninstallation

To uninstall the module simply use command
```
make uninstall
```
in the source directory.

## Usage from user space

### Using I2C slaves

Once the driver is loaded successfully, it provides an new I2C bus as device, e.g.,

```
/dev/i2c-2
```

according to the naming scheme ```/dev/i2c-<bus>``` where ```<bus>``` is the bus number selected automatically by the driver. Standard I/O functions such as ```open```, ```read```, ```write```, ```ioctl``` and ```close``` can then be used to communicate with slaves which are connected to this I2C bus.

#### Open the I2C device

To open the I2C bus device for data transfer simply use function ```open```, e.g.,
```
int i2c_bus = open ("/dev/i2c-2", O_RDWR));
```
Once the device is opened successfully, you can communicate with the slaves connected to the I2C bus.

Function ```close``` can be used to close the device anytime.

#### Data transfer with function ```ioctl```

Before data are transfered using function ```ioctl```, a data structure of type ```struct i2c_rdwr_ioctl_data``` has to be created. This can either contain only a single I2C message of type ```struct i2c_msg``` or an array of I2C messages of type ```struct i2c_msg```, all of which are transfered together as a combined transaction. In latter case each I2C message begins with start condition, but only the last ends with stop condition to indicate the end of the combined transaction.

Each I2C message consists of 

- a slave address, 
- some flags combined into a single value, e.g., read/write flag, 
- a pointer to the buffer for data bytes written to or read from the slave, and
- the length of data in bytes written to or read from the slave.

The following example shows an array of messages with two command messages written to the slave and two data messages to read the results from the slave.
```
#define I2C_SLAVE_ADDR   0x18

uint8_t i2c_id_cmd  [] = { 0x0f };  // get ID command 
uint8_t i2c_rd_cmd  [] = { 0xa8 };  // read data command

uint8_t i2c_id_data [1];            // ID is one byte
uint8_t i2c_rd_data [6];            // data are 6 bytes

struct i2c_msg i2c_messages[] = 
{
    {
        .addr  = I2C_SLAVE_ADDR,
        .flags = 0,
        .buf   = i2c_id_cmd,
        .len   = sizeof(i2c_id_cmd)        
    },
    {
        .addr  = I2C_SLAVE_ADDR,
        .flags = I2C_M_RD,
        .buf   = i2c_id_data,
        .len   = sizeof(i2c_id_data)
    },
    {
        .addr  = I2C_SLAVE_ADDR,
        .flags = 0,
        .buf   = i2c_rd_cmd,
        .len   = sizeof(i2c_rd_cmd)
    },
    {
        .addr  = I2C_SLAVE_ADDR,
        .flags = I2C_M_RD,
        .buf   = i2c_rd_data,
        .len   = sizeof(i2c_rd_data)
    }
};
```
These messages can then be transfered to the slave by filling a data structure of type ```struct i2c_rdwr_ioctl_data``` with them and calling function ```ioctl```, e.g., 
```
struct i2c_rdwr_ioctl_data ioctl_data = 
{
    .msgs  = i2c_messages,
    .nmsgs = 4
};

if (ioctl(i2c_bus, I2C_RDWR, &ioctl_data) < 0)
{
    perror("ioctl");
    return -1;
}
```

#### Using ```read``` and ```write``` for data transfer

Functions ```read``` and ```write``` can also be used for data transfer. However, since these functions do not allow to specify the slave address, it has to be set before using function ```ioctl```.

```
if (ioctl(i2c_bus, I2C_SLAVE_FORCE, I2C_SLAVE_ADDR) < 0)
{
    perror("Could not set i2c slave addr");
    return -1;
}
```

This slave address is then used for all subsequent ```read``` and ```write``` function calls until it is changed again with function ```ioctl```.

Supposing the data are preparated as in the example with ```ioctl```, the transfer of them is quite simple.

```
if (write(i2c_bus, i2c_id_cmd, sizeof(i2c_id_cmd)) != sizeof(i2c_id_cmd))
{
    perror("Could not write id command to i2c slave");
    return -1;
}

if (read (i2c_bus, i2c_id_data, sizeof(i2c_id_data)) != sizeof(i2c_id_data))
{
    perror("Could not write read id from i2c slave");
    return -1;
}

if (write(i2c_bus, i2c_rd_cmd, sizeof(i2c_rd_cmd)) != sizeof(i2c_rd_cmd))
{
    perror("Could not write read data command to i2c slave");
    return -1;
}

if (read (i2c_bus, i2c_rd_data, sizeof(i2c_rd_data)) != sizeof(i2c_rd_data))
{
    perror("Could not write read data from i2c slave");
    return -1;
}

```

### Using SPI slaves

Once the driver is loaded successfully, it provides up to three SPI slave devices on next available SPI bus, e.g.,

```
/dev/spidev0.0
/dev/spidev0.1
/dev/spidev0.2
```

according to the naming scheme ```/dev/spidev<bus>.<cs>```. ```<bus>``` is the bus number selected automatically by the driver and ```<cs>``` is the chip select signal of the according pin. Please note that independent on how many pins are configured as chip select signals, pin 15 gives always 0, pin 16 gives always 1, and pin 17 gives always 2 as chip select signal.

Standard I/O functions like ```open```, ```ioctl``` and ```close``` can be used to communicate with one of the slaves connected to the SPI.

To open an SPI device simply use:
```
int spi = open("/dev/spidev0.0", O_RDWR));
```

Once the device is opened successfully, you can modify SPI configurations and transfer data using ```ioctl``` function.

```
uint8_t mode = SPI_MODE_0;
uint8_t lsb = SPI_LSB_FIRST;
...
ioctl(spi, SPI_IOC_WR_MODE, &mode);
ioctl(spi, SPI_IOC_WR_LSB_FIRST, &lsb);
```
Function ```ioctl``` is also used to transfer data:

```
uint8_t *mosi; // output data
uint8_t *miso; // input data
...
// fill mosi with output data
...
struct spi_ioc_transfer spi_trans;
memset(&spi_trans, 0, sizeof(spi_trans));

spi_trans.tx_buf = (unsigned long) mosi;
spi_trans.rx_buf = (unsigned long) miso;
spi_trans.len = len;

int status = ioctl (spi, SPI_IOC_MESSAGE(1), &spi_trans);

// use input data in miso
```

### Fast reconfiguration device
When the kernel module is loaded, a sysfs class is created at
```
/sys/class/cy7_config/
```

When usb-debug-whistle is attached, this folder will contain a new device, something like:
```
/sys/class/cy7_config/cy7_usb_config-1.2
```
where "-1.2" suffix is unique id, generated by the combination of usb bus number and usb device number relatively.
The device folder has one attribute file that accepts configuration string.
```
Next /device_path/ read as /sys/class/cy7_config/cy7_usb_config-1.2
```

folders for each SCB
```
/device_path/scb*
```
##### SCB configuration
For all internal files:
* read (cat) - show all possible values and current value in brackets [ ] . For some options (like i2c frequency) show range range from-to
* write (echo) - set new value. Return error if this value out of range (see read)
```
Examples:
cat /sys/class/cy7_config/whistle_device-1.6/scb0/mode
off uart [spi] i2c

cat /sys/class/cy7_config/whistle_device-1.6/scb0/spi/spi_freq
1000 =< [1000000] >= 3000000
```

For reset device write "1" to reset in main device folder
```
echo 1 > /device_path/reset - write "1" to reset device
```

##### GPIO states
```
/device_path/gpio_state
```
* read - show gpios state,
* write - set gpio configuration

write format:
```
echo [pin index]:[pin config] > /device_path/gpio_state
```
Where
pin index: index from 0 to 18. Return error if wrong index or pin is busy for other functions
pin config:
"in"
"out"

##### Bind/Unbind upper level devices
If you need to debug upper level driver you could bind it with scb i2c/spi bus
For bind:
```
echo [addr]:[alias] > /device_path/scb*/slave_alias/slave_bind
```
Where
addr - i2c addres or spi chipselect
alias - alias device (Note: device driver must be prebuit in kernel and preset in /lib/modules/`uname -r`/modules.alias)

For unbind:
```
echo [addr]: > /device_path/scb*/slave_alias/slave_bind
```

Show bound devices
```
cat /sys/class/cy7_config/whistle_device-1.6/scb0/slave_alias/slave
_bind_info
addr : name
0x00   spidev
0x01   lsm330_accel
```
