// SPDX-License-Identifier: GPL-2.0
/**
 *
 * @brief usb-debug-whistle linux kernel driver gpio
 *        management routines.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#include <linux/irqdomain.h>
#include <linux/parser.h>
#include <linux/version.h>
#include "cy7_device.h"
#include "cy7_gpio.h"

#define CY7_GPIO_IRQ_POLL_TIMEOUT_MS 1000

#define TOKEN_GPIO_CONFIG 1
static const match_table_t tokens = {
	{ TOKEN_GPIO_CONFIG, "%d:%s" },
	{ 0, NULL },
};

#include <linux/version.h>
#if (LINUX_VERSION_CODE < KERNEL_VERSION(5, 5, 0))
#define GPIO_LINE_DIRECTION_IN	1
#define GPIO_LINE_DIRECTION_OUT	0
#endif

static inline uint8_t *cy7_get_pergpio_buf(struct cy7_device *cy7_dev,
					   int gpio_num)
{
	uint8_t *ptr = cy7_dev->u.gpio.gpio_buf;

	if (gpio_num >= CY7_NUM_GPIO) {
		SQ_WARNING(DRIVER_TAG,
			   "gpio number %d is out of range. Max is %d\n",
			   gpio_num, CY7_NUM_GPIO - 1);
		return ERR_PTR(-ERANGE);
	}

	return ptr + gpio_num * CY7_PER_GPIO_BUF_SIZE;
}

static int cy7_usb_gpio_set(struct cy7_device *cy7_dev, unsigned int gpio_num,
			    uint16_t value)
{
	int ret;

	ret = usb_control_msg(cy7_dev->usb_dev,
			      usb_sndctrlpipe(cy7_dev->usb_dev, 0),
			      CY_GPIO_SET_VALUE_CMD,
			      USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT,
			      gpio_num, value, NULL, 0, 100);

	return ret;
}

static int cy7_usb_gpio_get(struct cy7_device *cy7_dev, unsigned int gpio_num,
			    uint16_t *value)
{
	int ret;
	char *buf;
	const uint16_t length = sizeof(uint16_t);

	buf = cy7_get_pergpio_buf(cy7_dev, gpio_num);
	if (IS_ERR(buf)) {
		SQ_ERROR(DRIVER_TAG, "Failed to get per-gpio buffer: %ld",
			 PTR_ERR(buf));
		return PTR_ERR(buf);
	}

	ret = usb_control_msg(cy7_dev->usb_dev,
			      usb_rcvctrlpipe(cy7_dev->usb_dev, 0),
			      CY_GPIO_GET_VALUE_CMD,
			      USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_IN,
			      gpio_num, 0, buf, length, 10000);

	if (ret != length) {
		SQ_ERROR(DRIVER_TAG, "Failed to get gpio value %d\n", ret);
		return ret;
	}

	*value = buf[1] | (buf[0] << 8);

	return 0;
}

#define CY7_CONF_BLOB_GPIO_INPUT_OFFSET 0x1b4

struct cy7_config_blob_gpio {
	uint32_t drive0_mask;
	uint32_t drive1_mask;
	uint32_t input_mask;
} __packed;

static int cy7_gpio_get_direction(struct gpio_chip *gc, unsigned int offset)
{
	struct cy7_device *cy7_dev = gpiochip_get_data(gc);
	struct cy7_config_blob *blob;
	struct cy7_config_blob_gpio *blob_gpio;

	if (!cy7_dev)
		return -EFAULT;

	if (cy7_dev->devtype != CY7_TYPE_CONTROL)
		return -EINVAL;

	blob = (struct cy7_config_blob *) cy7_dev->config_blob;
	if (!blob) {
		SQ_ERROR(DRIVER_TAG, "Configuration blob not initialized");
		return -EFAULT;
	}

	blob_gpio = (struct cy7_config_blob_gpio
			     *)(&blob->buf[CY7_CONF_BLOB_GPIO_INPUT_OFFSET]);

	SQ_INFO(DRIVER_TAG,
		"%s: gpio config drive0:%08x, drive1:%08x, input:%08x\n",
		__func__, blob_gpio->drive0_mask, blob_gpio->drive1_mask,
		blob_gpio->input_mask);

	return blob_gpio->input_mask & (1 << offset) ? GPIO_LINE_DIRECTION_OUT :
						       GPIO_LINE_DIRECTION_IN;
}

static int cy7_gpio_get(struct gpio_chip *gc, unsigned int offset)
{
	int ret;
	uint16_t value;
	struct cy7_device *cy7_dev = gpiochip_get_data(gc);

	SQ_INFO(DRIVER_TAG, "%s: gpio_num: %d\n", __func__, offset);
	ret = cy7_usb_gpio_get(cy7_dev, offset, &value);
	if (ret) {
		SQ_ERROR(DRIVER_TAG, "failed to get gpio %d", ret);
		return ret;
	}

	return value;
}

static void cy7_gpio_set(struct gpio_chip *gc, unsigned int offset, int value)
{
	int ret;
	struct cy7_device *cy7_dev = gpiochip_get_data(gc);

	ret = cy7_usb_gpio_set(cy7_dev, offset, value);
	SQ_INFO(DRIVER_TAG, "%s: %p %d %d ret: %d\n", __func__, gc, offset,
		value, ret);
}

static int cy7_gpio_direction_input(struct gpio_chip *gpio_chip,
				    unsigned int pin_index)
{
	struct cy7_device *cy7_dev = gpiochip_get_data(gpio_chip);

	(void)cy7_dev;

	SQ_WARNING(
		DRIVER_TAG,
		"Set GPIO dirrection isn't supported. Use sysfs for configure\n");
	return -EPERM;
}

static int cy7_gpio_direction_output(struct gpio_chip *gpio_chip,
				     unsigned int pin_index, int value)
{
	struct cy7_device *cy7_dev = gpiochip_get_data(gpio_chip);

	(void)cy7_dev;

	SQ_WARNING(
		DRIVER_TAG,
		"Set GPIOdirrection isn't supported. Use sysfs for configure\n");
	return -EPERM;
}

#ifdef CONFIG_GPIOLIB_IRQCHIP
static void cy7_gpio_irq_poll_work_func(struct work_struct *work)
{
	int idx = 0;
	struct cy7_gpio_device *cy7_gpio =
		(struct cy7_gpio_device *)container_of(
			work, struct cy7_gpio_device, poll_thread.work);
	struct cy7_device *cy7_dev = (struct cy7_device *)container_of(
		cy7_gpio, struct cy7_device, u.gpio);

#ifdef CY7_GPIO_IRQ_DEBUG
#define CY7_GPIO_BUF_VAL_MAX 100
	char idx_str[CY7_GPIO_BUF_VAL_MAX] = "";
	char val_str[CY7_GPIO_BUF_VAL_MAX] = "";
	int len_idx = 0;
	int len_val = 0;

	if (!cy7_gpio->poll_enabled) {
		mod_delayed_work(
			system_wq, &cy7_gpio->poll_thread,
			msecs_to_jiffies(CY7_GPIO_IRQ_POLL_TIMEOUT_MS));
		return;
	}

	SQ_INFO(DRIVER_TAG, "cy7_gpio->irq_stat=0x%04X\n", cy7_gpio->irq_stat);
#endif

	for (idx = 0; idx < CY7_NUM_GPIO; idx++) {
		int val = -1;

		if ((cy7_gpio->irq_enabled & BIT(idx)) == 0)
			continue;

		if (!is_gpio_avalible_for_gpio(cy7_dev, idx))
			continue;

		val = cy7_gpio_get(&cy7_gpio->gpio, idx);
		if (val < 0)
			continue;

#ifdef CY7_GPIO_IRQ_DEBUG
		len_idx = snprintf(idx_str, CY7_GPIO_BUF_VAL_MAX, "%s%02d ",
				   idx_str, idx);
		len_val = snprintf(val_str, CY7_GPIO_BUF_VAL_MAX, "%s%02d ",
				   val_str, val);
#endif

		if (((cy7_gpio->irq_stat >> idx) & 1) == val)
			continue;

		if (val == 1 &&
		    (cy7_gpio->irq_trig_raise & BIT(idx))) { //rising
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4, 15, 0))
			generic_handle_irq(irq_find_mapping(
				cy7_gpio->gpio.irqdomain, idx));
#else
			generic_handle_irq(irq_find_mapping(
				cy7_gpio->gpio.irq.domain, idx));
#endif

#ifdef CY7_GPIO_IRQ_DEBUG
			len_idx = snprintf(idx_str, CY7_GPIO_BUF_VAL_MAX,
					   "%sr ", idx_str);
			len_val = snprintf(val_str, CY7_GPIO_BUF_VAL_MAX,
					   "%sr ", val_str);
#endif
		}
		if (!val &&
		    (cy7_gpio->irq_trig_fall & BIT(idx))) { //falling
#if (LINUX_VERSION_CODE < KERNEL_VERSION(4, 15, 0))
			generic_handle_irq(irq_find_mapping(
				cy7_gpio->gpio.irqdomain, idx));
#else
			generic_handle_irq(irq_find_mapping(
				cy7_gpio->gpio.irq.domain, idx));
#endif

#ifdef CY7_GPIO_IRQ_DEBUG
			len_idx = snprintf(idx_str, CY7_GPIO_BUF_VAL_MAX,
					   "%sf ", idx_str);
			len_val = snprintf(val_str, CY7_GPIO_BUF_VAL_MAX,
					   "%sf ", val_str);
#endif
		}

		cy7_gpio->irq_stat &= ~BIT(idx);
		cy7_gpio->irq_stat |= (val && 1) << idx;
	}

#ifdef CY7_GPIO_IRQ_DEBUG
	SQ_INFO(DRIVER_TAG, "idx: %s\n", idx_str);
	SQ_INFO(DRIVER_TAG, "val: %s\n", val_str);
#endif

	mod_delayed_work(system_wq, &cy7_gpio->poll_thread,
			 msecs_to_jiffies(CY7_GPIO_IRQ_POLL_TIMEOUT_MS));
}

static void cy7_irq_enable(struct irq_data *d)
{
	struct gpio_chip *gc = irq_data_get_irq_chip_data(d);
	struct cy7_device *cy7_dev = gpiochip_get_data(gc);
	struct cy7_gpio_device *cy7_gpio = &cy7_dev->u.gpio;

	cy7_gpio->irq_enabled |= BIT(d->hwirq);
}

static void cy7_irq_disable(struct irq_data *d)
{
	struct gpio_chip *gc = irq_data_get_irq_chip_data(d);
	struct cy7_device *cy7_dev = gpiochip_get_data(gc);
	struct cy7_gpio_device *cy7_gpio = &cy7_dev->u.gpio;

	cy7_gpio->irq_enabled &= ~BIT(d->hwirq);
}

static int cy7_irq_set_type(struct irq_data *d, unsigned int type)
{
	struct gpio_chip *gc = irq_data_get_irq_chip_data(d);
	struct cy7_device *cy7_dev = gpiochip_get_data(gc);
	struct cy7_gpio_device *cy7_gpio = &cy7_dev->u.gpio;
	uint32_t mask = BIT(d->hwirq);

	if (!is_gpio_avalible_for_gpio(cy7_dev, d->hwirq)) {
		SQ_ERROR(DRIVER_TAG, "IRQ %lu isn't avalible\n", d->hwirq);
		return -EPERM;
	}

	if (type & IRQ_TYPE_EDGE_FALLING)
		cy7_gpio->irq_trig_fall |= mask;
	else
		cy7_gpio->irq_trig_fall &= ~mask;

	if (type & IRQ_TYPE_EDGE_RISING)
		cy7_gpio->irq_trig_raise |= mask;
	else
		cy7_gpio->irq_trig_raise &= ~mask;

	return 0;
}

#ifdef CY7_GPIO_IRQ_DEBUG
ssize_t cy7_gpio_irq_enable_show(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	int len = 0;
	struct cy7_device *cy7_dev = dev_get_drvdata(dev);
	struct cy7_gpio_device *cy7_gpio = &cy7_dev->u.gpio;

	len += scnprintf(buf + len, PAGE_SIZE - len, "irq %s\n",
			 cy7_gpio->poll_enabled ? "enabled" : "disabled");

	return len;
}

ssize_t cy7_gpio_irq_enable_store(struct device *dev,
				  struct device_attribute *attr,
				  const char *buf, size_t len)
{
	struct cy7_device *cy7_dev = dev_get_drvdata(dev);
	struct cy7_gpio_device *cy7_gpio = &cy7_dev->u.gpio;
	int new_val = 0;

	if (kstrtoint(buf, 10, &new_val) != 0)
		return -EINVAL;

	//	SQ_ERROR(DRIVER_TAG, "new_val=%d\n", new_val);

	if (new_val > 0)
		cy7_gpio->poll_enabled = true;
	else
		cy7_gpio->poll_enabled = false;

	return len;
}

DEVICE_ATTR(irq_enable, 0664, cy7_gpio_irq_enable_show,
	    cy7_gpio_irq_enable_store);
#endif // CY7_GPIO_IRQ_DEBUG
#endif // CONFIG_GPIOLIB_IRQCHIP

ssize_t cy7_gpio_state_show(struct device *dev, struct device_attribute *attr,
			    char *buf)
{
	int idx = 0;
	int len = 0;
	struct cy7_device *cy7_dev = dev_get_drvdata(dev);

	for (idx = 0; idx < CY7_NUM_GPIO; idx++) {
		len += scnprintf(
			buf + len, PAGE_SIZE - len, "GPIO%02d - %s\n", idx,
			cy7_gpio_state_to_str(cy7_dev->gpios_type[idx]));
	}

	return len;
}

ssize_t cy7_gpio_state_store(struct device *dev, struct device_attribute *attr,
			     const char *buf, size_t len)
{
	int ret = -EINVAL;
	struct cy7_device *cy7_dev = dev_get_drvdata(dev);
	substring_t args[MAX_OPT_ARGS];
	int optlen = 0;
	char *options, *tmp_options = NULL;
	char *p;

	if (!buf || !len) {
		SQ_ERROR(DRIVER_TAG, "Error string format\n");
		return -ENOBUFS;
	}

	optlen = strnlen(buf, len);
	if (optlen <= 0) {
		SQ_ERROR(DRIVER_TAG, "Error string length\n");
		return -ENOBUFS;
	}

	tmp_options = kmemdup_nul(buf, optlen,
				   GFP_KERNEL);
	if (!tmp_options)
		return 0;

	options = tmp_options;

	while ((p = strsep(&options, ",")) != NULL) {
		int token;

		if (!*p)
			continue;

		token = match_token(p, tokens, args);

		switch (token) {
		case TOKEN_GPIO_CONFIG: {
			int gpio_num = -1;
			char *s;
			struct cy7_config_blob *blob =
				(struct cy7_config_blob *) cy7_dev->config_blob;
			struct cy7_config_blob_gpio *blob_gpio =
				(struct cy7_config_blob_gpio *)
						(&blob->buf[CY7_CONF_BLOB_GPIO_INPUT_OFFSET]);

			ret = match_int(&args[0], &gpio_num);
			if (ret)
				goto error;

			if (!is_gpio_avalible_for_gpio(cy7_dev, gpio_num)) {
				SQ_WARNING(
					DRIVER_TAG,
					"GPIO%02d used for other function and doesn't configure\n",
					gpio_num);
				goto error;
			}

			s = match_strdup(&args[1]);

#define CY7_GPIO_IN_STR "in\n"
#define CY7_GPIO_OUT_STR "out\n"

			if (!strcmp(s, CY7_GPIO_IN_STR)) {
				SQ_ERROR(DRIVER_TAG, "OK3 gpio%d set to IN\n",
					 gpio_num);
				blob_gpio->input_mask &= ~BIT(gpio_num);
				blob_gpio->drive0_mask &= ~BIT(gpio_num);
				blob_gpio->drive0_mask |= BIT(gpio_num);
				blob_gpio->drive1_mask &= ~BIT(gpio_num);
				cy7_scb_update_config(cy7_dev);
				ret = len;
			} else if (!strcmp(s, CY7_GPIO_OUT_STR)) {
				SQ_ERROR(DRIVER_TAG, "OK3 gpio%d set to OUT\n",
					 gpio_num);
				blob_gpio->input_mask |= BIT(gpio_num);
				blob_gpio->drive0_mask |= BIT(gpio_num);
				blob_gpio->drive1_mask &= ~BIT(gpio_num);
				cy7_scb_update_config(cy7_dev);
				ret = len;
			} else {
				blob_gpio->input_mask &= ~BIT(gpio_num);
				blob_gpio->drive0_mask &= ~BIT(gpio_num);
				blob_gpio->drive1_mask &= ~BIT(gpio_num);
				SQ_ERROR(DRIVER_TAG, "OK3 gpio%d set to Hz\n",
					 gpio_num);
				ret = -EINVAL;
			}

			kfree(s);
			break;
		}
		default:
			SQ_ERROR(DRIVER_TAG, "FAIL\n");
			break;
		}
	}

error:
	cy7_gpio_update_states(cy7_dev);
	kfree(tmp_options);
	return ret;
}

DEVICE_ATTR(gpio_state, 0664, cy7_gpio_state_show, cy7_gpio_state_store);

enum _CY7_GPIOS {
	CY7_GPIO_0 = 0,
	CY7_GPIO_1,
	CY7_GPIO_2,
	CY7_GPIO_3,
	CY7_GPIO_4,
	CY7_GPIO_5,
	CY7_GPIO_6,
	CY7_GPIO_7,
	CY7_GPIO_8,
	CY7_GPIO_9,
	CY7_GPIO_10,
	CY7_GPIO_11,
	CY7_GPIO_12,
	CY7_GPIO_13,
	CY7_GPIO_14,
	CY7_GPIO_15,
	CY7_GPIO_16,
	CY7_GPIO_17,
	CY7_GPIO_18,

	CY7_SCB0_GPIO0 = CY7_GPIO_8,
	CY7_SCB0_GPIO1 = CY7_GPIO_2,
	CY7_SCB0_GPIO2 = CY7_GPIO_3,
	CY7_SCB0_GPIO3 = CY7_GPIO_4,
	CY7_SCB0_GPIO4 = CY7_GPIO_5,
	CY7_SCB0_GPIO5 = CY7_GPIO_9,
	CY7_SCB1_GPIO0 = CY7_GPIO_10,
	CY7_SCB1_GPIO1 = CY7_GPIO_11,
	CY7_SCB1_GPIO2 = CY7_GPIO_12,
	CY7_SCB1_GPIO3 = CY7_GPIO_13,
	CY7_SCB1_GPIO4 = CY7_GPIO_14,
	CY7_SCB1_GPIO5 = CY7_GPIO_15,
};

int cy7_gpio_update_states(struct cy7_device *cy7_dev)
{
	int ret = 0;
	int idx = 0;
	struct cy7_device_config *cy7_config = cy7_dev->config_blob;
	struct cy7_scb_config *scb = NULL;
	struct cy7_config_blob *blob = (struct cy7_config_blob *) cy7_dev->config_blob;
	struct cy7_config_blob_gpio *blob_gpio =
		(struct cy7_config_blob_gpio *)
				(&blob->buf[CY7_CONF_BLOB_GPIO_INPUT_OFFSET]);

	for (idx = 0; idx < ARRAY_SIZE(cy7_dev->gpios_type); idx++)
		cy7_dev->gpios_type[idx] = CY7_GPIO_TYPE_TRISTATE;


	for (idx = 0; idx < ARRAY_SIZE(cy7_dev->gpios_type); idx++) {
		if (blob_gpio->input_mask & BIT(idx)) {
			if (blob_gpio->drive0_mask & BIT(idx))
				cy7_dev->gpios_type[idx] =
					CY7_GPIO_TYPE_DRIVE_0;
			else if (blob_gpio->drive1_mask & BIT(idx))
				cy7_dev->gpios_type[idx] =
					CY7_GPIO_TYPE_DRIVE_1;
			else
				cy7_dev->gpios_type[idx] =
					CY7_GPIO_TYPE_TRISTATE;
		} else {
			cy7_dev->gpios_type[idx] = CY7_GPIO_TYPE_INPUT;
		}
	}

	/* LEDs and RS485 gpios for SCBx */
	for (idx = 0; idx < CY7C65215_SCB_DEV_MAX; idx++) {
		scb = &cy7_config->scb[idx];
		scb->tx_led != 0xFF ?
			cy7_dev->gpios_type[scb->tx_led] =
				(CY7_GPIO_TYPE_SCB0_TxLED + 4 * idx) :
			CY7_GPIO_TYPE_TRISTATE;
		scb->rx_led != 0xFF ?
			cy7_dev->gpios_type[scb->rx_led] =
				(CY7_GPIO_TYPE_SCB0_RxLED + 4 * idx) :
			CY7_GPIO_TYPE_TRISTATE;
		scb->rx_tx_led != 0xFF ?
			cy7_dev->gpios_type[scb->rx_tx_led] =
				(CY7_GPIO_TYPE_SCB0_RxTxLED + 4 * idx) :
			CY7_GPIO_TYPE_TRISTATE;

		if (cy7_config->scb_uart_rs485[idx].rs485_enable != 0) {
			cy7_config->scb_uart_rs485[idx].rs485_gpio != 0xFF ?
				cy7_dev->gpios_type[cy7_config
							    ->scb_uart_rs485[idx]
							    .rs485_gpio] =
					(CY7_GPIO_TYPE_SCB0_UART_RS485 +
					 4 * idx) :
				CY7_GPIO_TYPE_TRISTATE;
		}
	}

	/* SCB0 */
	idx = 0;
	scb = &cy7_config->scb[idx];
	if (scb->mode == CY7_SCB_MODE_UART) {
		if (scb->u.uart.uart_type == CY7_UART_TYPE_PIN_6) {
			cy7_dev->gpios_type[CY7_SCB0_GPIO0] =
				CY7_GPIO_TYPE_SCB0;
			cy7_dev->gpios_type[CY7_SCB0_GPIO1] =
				CY7_GPIO_TYPE_SCB0;
			cy7_dev->gpios_type[CY7_SCB0_GPIO2] =
				CY7_GPIO_TYPE_SCB0;
			cy7_dev->gpios_type[CY7_SCB0_GPIO3] =
				CY7_GPIO_TYPE_SCB0;
			cy7_dev->gpios_type[CY7_SCB0_GPIO4] =
				CY7_GPIO_TYPE_SCB0;
			cy7_dev->gpios_type[CY7_SCB0_GPIO5] =
				CY7_GPIO_TYPE_SCB0;
		} else if (scb->u.uart.uart_type == CY7_UART_TYPE_PIN_4) {
			cy7_dev->gpios_type[CY7_SCB0_GPIO0] =
				CY7_GPIO_TYPE_SCB0;
			cy7_dev->gpios_type[CY7_SCB0_GPIO2] =
				CY7_GPIO_TYPE_SCB0;
			cy7_dev->gpios_type[CY7_SCB0_GPIO3] =
				CY7_GPIO_TYPE_SCB0;
			cy7_dev->gpios_type[CY7_SCB0_GPIO4] =
				CY7_GPIO_TYPE_SCB0;
		} else {/*if (scb->u.uart.uart_type == CY7_UART_TYPE_PIN_2)*/
			cy7_dev->gpios_type[CY7_SCB0_GPIO0] =
				CY7_GPIO_TYPE_SCB0;
			cy7_dev->gpios_type[CY7_SCB0_GPIO4] =
				CY7_GPIO_TYPE_SCB0;
		}
	} else if (scb->mode == CY7_SCB_MODE_I2C) {
		cy7_dev->gpios_type[CY7_SCB0_GPIO2] = CY7_GPIO_TYPE_SCB0;
		cy7_dev->gpios_type[CY7_SCB0_GPIO3] = CY7_GPIO_TYPE_SCB0;
	} else if (scb->mode == CY7_SCB_MODE_SPI) {
		cy7_dev->gpios_type[CY7_SCB0_GPIO1] = CY7_GPIO_TYPE_SCB0;
		cy7_dev->gpios_type[CY7_SCB0_GPIO2] = CY7_GPIO_TYPE_SCB0;
		cy7_dev->gpios_type[CY7_SCB0_GPIO3] = CY7_GPIO_TYPE_SCB0;
		cy7_dev->gpios_type[CY7_SCB0_GPIO4] = CY7_GPIO_TYPE_SCB0;
	} else {
	}

	/* SCB1 */
	idx = 1;
	scb = &cy7_config->scb[idx];
	if (scb->mode == CY7_SCB_MODE_UART) {
		if (scb->u.uart.uart_type == CY7_UART_TYPE_PIN_6) {
			cy7_dev->gpios_type[CY7_SCB1_GPIO0] =
				CY7_GPIO_TYPE_SCB1;
			cy7_dev->gpios_type[CY7_SCB1_GPIO1] =
				CY7_GPIO_TYPE_SCB1;
			cy7_dev->gpios_type[CY7_SCB1_GPIO2] =
				CY7_GPIO_TYPE_SCB1;
			cy7_dev->gpios_type[CY7_SCB1_GPIO3] =
				CY7_GPIO_TYPE_SCB1;
			cy7_dev->gpios_type[CY7_SCB1_GPIO4] =
				CY7_GPIO_TYPE_SCB1;
			cy7_dev->gpios_type[CY7_SCB1_GPIO5] =
				CY7_GPIO_TYPE_SCB1;
		} else if (scb->u.uart.uart_type == CY7_UART_TYPE_PIN_4) {
			cy7_dev->gpios_type[CY7_SCB1_GPIO0] =
				CY7_GPIO_TYPE_SCB1;
			cy7_dev->gpios_type[CY7_SCB1_GPIO1] =
				CY7_GPIO_TYPE_SCB1;
			cy7_dev->gpios_type[CY7_SCB1_GPIO2] =
				CY7_GPIO_TYPE_SCB1;
			cy7_dev->gpios_type[CY7_SCB1_GPIO3] =
				CY7_GPIO_TYPE_SCB1;
		} else {/*if (scb->u.uart.uart_type == CY7_UART_TYPE_PIN_2)*/
			cy7_dev->gpios_type[CY7_SCB1_GPIO0] =
				CY7_GPIO_TYPE_SCB1;
			cy7_dev->gpios_type[CY7_SCB1_GPIO1] =
				CY7_GPIO_TYPE_SCB1;
		}
	} else if (scb->mode == CY7_SCB_MODE_I2C) {
		cy7_dev->gpios_type[CY7_SCB1_GPIO0] = CY7_GPIO_TYPE_SCB1;
		cy7_dev->gpios_type[CY7_SCB1_GPIO1] = CY7_GPIO_TYPE_SCB1;
	} else if (scb->mode == CY7_SCB_MODE_SPI) {
		cy7_dev->gpios_type[CY7_SCB1_GPIO0] = CY7_GPIO_TYPE_SCB1;
		cy7_dev->gpios_type[CY7_SCB1_GPIO1] = CY7_GPIO_TYPE_SCB1;
		cy7_dev->gpios_type[CY7_SCB1_GPIO2] = CY7_GPIO_TYPE_SCB1;
		cy7_dev->gpios_type[CY7_SCB1_GPIO3] = CY7_GPIO_TYPE_SCB1;
	} else if (scb->mode == CY7_SCB_MODE_JTAG) {
		cy7_dev->gpios_type[CY7_SCB1_GPIO0] = CY7_GPIO_TYPE_SCB1;
		cy7_dev->gpios_type[CY7_SCB1_GPIO1] = CY7_GPIO_TYPE_SCB1;
		cy7_dev->gpios_type[CY7_SCB1_GPIO2] = CY7_GPIO_TYPE_SCB1;
		cy7_dev->gpios_type[CY7_SCB1_GPIO3] = CY7_GPIO_TYPE_SCB1;
		cy7_dev->gpios_type[CY7_SCB1_GPIO4] = CY7_GPIO_TYPE_SCB1;
	} else {
	}

	return ret;
}

int cy7_gpio_probe(struct cy7_device *cy7_dev)
{
	int ret;
	int idx = 0;
	struct cy7_gpio_device *cy7_gpio = &cy7_dev->u.gpio;
	struct gpio_chip *gpio_chip = &cy7_gpio->gpio;
	struct irq_chip *irq_chip = &cy7_gpio->irq_chip;
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(4, 15, 0))
	struct gpio_irq_chip *girq;
#endif

	cy7_gpio_update_states(cy7_dev);
	device_create_file(cy7_dev->config_dev, &dev_attr_gpio_state);

	gpio_chip->label = "cy7c65215/gpio";
	gpio_chip->request = gpiochip_generic_request;
	gpio_chip->free = gpiochip_generic_free;
	gpio_chip->get_direction = cy7_gpio_get_direction;
	gpio_chip->direction_input = cy7_gpio_direction_input;
	gpio_chip->direction_output = cy7_gpio_direction_output;
	gpio_chip->set = cy7_gpio_set;
	gpio_chip->get = cy7_gpio_get;
	gpio_chip->ngpio = CY7_NUM_GPIO;
	gpio_chip->owner = THIS_MODULE;
	gpio_chip->base = -1;
#if (LINUX_VERSION_CODE < KERNEL_VERSION(5, 0, 0))
	gpio_chip->parent = cy7_dev->config_dev;
#endif

	cy7_dev->u.gpio.gpio_buf =
		kzalloc(CY7_PER_GPIO_BUF_SIZE * CY7_NUM_GPIO, GFP_KERNEL);

	if (!cy7_dev->u.gpio.gpio_buf)
		goto err;

#ifdef CONFIG_GPIOLIB_IRQCHIP
	irq_chip->name = "cy7c65215/irq";
	irq_chip->irq_enable = cy7_irq_enable;
	irq_chip->irq_disable = cy7_irq_disable;
	irq_chip->irq_set_type = cy7_irq_set_type;

#if (LINUX_VERSION_CODE < KERNEL_VERSION(4, 15, 0))

	ret = gpiochip_add_data(&cy7_dev->u.gpio.gpio, cy7_dev);
	if (ret)
		goto err;
	gpio_chip->irqchip = irq_chip;
	gpio_chip->irq_default_type = IRQ_TYPE_EDGE_BOTH;

	ret = gpiochip_irqchip_add_nested(&cy7_dev->u.gpio.gpio,
								 irq_chip,
								 0, handle_level_irq,
								 IRQ_TYPE_NONE);
	if (ret) {
		gpiochip_remove(&cy7_dev->u.gpio.gpio);
		goto err;
	}
#else
	girq = &gpio_chip->irq;
	girq->chip = irq_chip;
	girq->parent_handler = NULL;
	girq->num_parents = 0;
	girq->parents = NULL;
	girq->default_type = IRQ_TYPE_NONE;
	girq->handler = handle_simple_irq;
	girq->threaded = true;
	girq->default_type = IRQ_TYPE_EDGE_BOTH;

	ret = gpiochip_add_data(&cy7_dev->u.gpio.gpio, cy7_dev);
	if (ret)
		goto err;
#endif // (LINUX_VERSION_CODE < KERNEL_VERSION(4, 15, 0))
#else
	ret = gpiochip_add_data(&cy7_dev->u.gpio.gpio, cy7_dev);
	if (ret)
		goto err;
#endif // CONFIG_GPIOLIB_IRQCHIP

	cy7_gpio->irq_stat = 0;
	for (idx = 0; idx < CY7_NUM_GPIO; idx++) {
		int val = -1;

		val = cy7_gpio_get(&cy7_gpio->gpio, idx);
		if (val < 0)
			continue;

		cy7_gpio->irq_stat |= (val && 1) << idx;
	}

#ifdef CONFIG_GPIOLIB_IRQCHIP
#ifdef CY7_GPIO_IRQ_DEBUG
	cy7_gpio->poll_enabled = false;

	ret = device_create_file(cy7_dev->config_dev, &dev_attr_irq_enable);
	if (ret)
		SQ_ERROR(DRIVER_TAG, "Error create attrubute irq_enable\n");

#endif // CY7_GPIO_IRQ_DEBUG

	/* Create polling thread */
	INIT_DELAYED_WORK(&cy7_gpio->poll_thread, cy7_gpio_irq_poll_work_func);
	mod_delayed_work(system_wq, &cy7_gpio->poll_thread,
			 msecs_to_jiffies(CY7_GPIO_IRQ_POLL_TIMEOUT_MS));
#endif // CONFIG_GPIOLIB_IRQCHIP

	SQ_INFO(DRIVER_TAG, "cy7_gpio_init: %p\n", gpio_chip);

	return 0;
err:
	gpiochip_remove(&cy7_dev->u.gpio.gpio);
	kfree(cy7_dev->u.gpio.gpio_buf);
	return ret;
}

void cy7_gpio_remove(struct cy7_device *cy7_dev)
{
	struct cy7_gpio_device *cy7_gpio = &cy7_dev->u.gpio;

	device_remove_file(cy7_dev->config_dev, &dev_attr_gpio_state);

#ifdef CONFIG_GPIOLIB_IRQCHIP
#ifdef CY7_GPIO_IRQ_DEBUG
	device_remove_file(cy7_dev->config_dev, &dev_attr_irq_enable);
#endif // CY7_GPIO_IRQ_DEBUG
	cancel_delayed_work_sync(&cy7_gpio->poll_thread);
#endif // CONFIG_GPIOLIB_IRQCHIP
	gpiochip_remove(&cy7_dev->u.gpio.gpio);
	kfree(cy7_dev->u.gpio.gpio_buf);
}

bool is_gpio_avalible_for_led(struct cy7_device *cy7_dev, int idx)
{
	int ret = false;

	switch (cy7_dev->gpios_type[idx]) {
	case CY7_GPIO_TYPE_TRISTATE:
	case CY7_GPIO_TYPE_DRIVE_0:
	case CY7_GPIO_TYPE_DRIVE_1:
	case CY7_GPIO_TYPE_INPUT:
		return true;
	default:
		return false;
	}

	return ret;
}

bool is_gpio_avalible_for_gpio(struct cy7_device *cy7_dev, int idx)
{
	return is_gpio_avalible_for_led(cy7_dev, idx);
}
