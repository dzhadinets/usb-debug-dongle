/* SPDX-License-Identifier: GPL-2.0 */
/**
 *
 * @brief usb-debug-whistle linux kernel driver gpio
 *        management routines.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_GPIO_H
#define _CY7_GPIO_H

#include "cy7_device.h"

int cy7_gpio_probe(struct cy7_device *cy7_dev);

void cy7_gpio_remove(struct cy7_device *cy7_dev);

static const __maybe_unused char *cy7_gpio_state_to_str(int val)
{
	switch (val) {
	case CY7_GPIO_TYPE_DRIVE_0:
		return "DRIVE_0";
	case CY7_GPIO_TYPE_DRIVE_1:
		return "DRIVE_1";
	case CY7_GPIO_TYPE_INPUT:
		return "INPUT";
	case CY7_GPIO_TYPE_SCB0:
		return "SCB0";
	case CY7_GPIO_TYPE_SCB1:
		return "SCB1";
	case CY7_GPIO_TYPE_SCB0_RxLED:
		return "SCB0_RxLED";
	case CY7_GPIO_TYPE_SCB0_TxLED:
		return "SCB0_TxLED";
	case CY7_GPIO_TYPE_SCB0_RxTxLED:
		return "SCB0_RxTxLED";
	case CY7_GPIO_TYPE_SCB0_UART_RS485:
		return "SCB0_UART_RS485";
	case CY7_GPIO_TYPE_SCB1_RxLED:
		return "SCB1_RxLED";
	case CY7_GPIO_TYPE_SCB1_TxLED:
		return "SCB1_TxLED";
	case CY7_GPIO_TYPE_SCB1_RxTxLED:
		return "SCB1_RxTxLED";
	case CY7_GPIO_TYPE_SCB1_UART_RS485:
		return "SCB1_UART_RS485";
	default:
		return "TRISTATE";
	};
	return "UNUSED";
};

bool is_gpio_avalible_for_led(struct cy7_device *cy7_dev, int idx);
bool is_gpio_avalible_for_gpio(struct cy7_device *cy7_dev, int idx);
int cy7_gpio_update_states(struct cy7_device *cy7_dev);

#endif /* _CY7_GPIO_H */
