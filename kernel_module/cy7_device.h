/* SPDX-License-Identifier: GPL-2.0 */
/**
 *
 * @brief usb-debug-whistle linux kernel driver main types
 *        definitions.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_DEVICE_H
#define _CY7_DEVICE_H

#include <linux/i2c.h>
#include <linux/usb.h>
#include <linux/spi/spi.h>
#include <linux/gpio/driver.h>
#include "cypressdefs.h"
#include "cy7_config.h"

/* #define SQ_DEBUG */
#define DRIVER_TAG "cy7_driver"
#include "sq_log.h"

/* CY7C65215A max Serial Comunication blocks */
#define CY7C65215_SCB_DEV_MAX 2

/* CY7C65215A wMaxPacketSize for BULK ep */
#define CY7C65215_USB_MAX_BULK_SIZE 64

/* CY7C65215A wMaxPacketSize for INT ep */
#define CY7C65215_USB_MAX_INTR_SIZE 64

#define CY7_NUM_GPIO 19
#define CY7_PER_GPIO_BUF_SIZE 64

#define MAX_OPTION_LEN 256

struct cy7_device_config;

struct cy7_spi_device {
	struct spi_master *master;
	struct spi_device *slave;
	struct cy7_spi_config config;
	struct spi_board_info board_info;
	uint32_t status;
};

struct cy7_i2c_device {
	struct i2c_adapter i2c_dev;
	uint8_t status[3];
};

/* Device configuration blob */

#define CY7_CONFIG_BLOB_SIZE 512

struct cy7_config_blob {
	uint8_t buf[CY7_CONFIG_BLOB_SIZE];
};

static inline struct cy7_config_blob *cy7_config_blob_alloc(void)
{
	return kzalloc(sizeof(struct cy7_config_blob), GFP_KERNEL);
}

static inline void cy7_config_blob_free(struct cy7_config_blob *blob)
{
	kfree(blob);
}

enum _CY7_GPIO_TYPE {
	CY7_GPIO_TYPE_TRISTATE = 0,
	CY7_GPIO_TYPE_DRIVE_0,
	CY7_GPIO_TYPE_DRIVE_1,
	CY7_GPIO_TYPE_INPUT,
//	CY7_GPIO_TYPE_OUTPUT,
	CY7_GPIO_TYPE_SCB0,
	CY7_GPIO_TYPE_SCB1,
	CY7_GPIO_TYPE_SCB0_RxLED,
	CY7_GPIO_TYPE_SCB0_TxLED,
	CY7_GPIO_TYPE_SCB0_RxTxLED,
	CY7_GPIO_TYPE_SCB0_UART_RS485,
	CY7_GPIO_TYPE_SCB1_RxLED,
	CY7_GPIO_TYPE_SCB1_TxLED,
	CY7_GPIO_TYPE_SCB1_RxTxLED,
	CY7_GPIO_TYPE_SCB1_UART_RS485,
	CY7_GPIO_TYPE_WAKEUP_POWER,
	CY7_GPIO_TYPE_BCD0,
	CY7_GPIO_TYPE_BCD1,
	CY7_GPIO_TYPE_BUS_DETECT,
	CY7_GPIO_TYPE_DRIVE,
	CY7_GPIO_TYPE_CAPSENCE,
	CY7_GPIO_TYPE_WATER_SHIELD,
	CY7_GPIO_TYPE_COMMON_LED,
	CY7_GPIO_TYPE_CAPSENCE_BTN0,
	CY7_GPIO_TYPE_CAPSENCE_BTN1,
	CY7_GPIO_TYPE_CAPSENCE_BTN2,
	CY7_GPIO_TYPE_CAPSENCE_BTN3,
	CY7_GPIO_TYPE_CAPSENCE_BTN4,
	CY7_GPIO_TYPE_CAPSENCE_BTN5,
	CY7_GPIO_TYPE_CAPSENCE_BTN6,
	CY7_GPIO_TYPE_CAPSENCE_BTN7,
	CY7_GPIO_TYPE_CAPSENCE_LED_BTN0,
	CY7_GPIO_TYPE_CAPSENCE_LED_BTN1,
	CY7_GPIO_TYPE_CAPSENCE_LED_BTN2,
	CY7_GPIO_TYPE_CAPSENCE_LED_BTN3,
	CY7_GPIO_TYPE_CAPSENCE_LED_BTN4,
	CY7_GPIO_TYPE_CAPSENCE_LED_BTN5,
	CY7_GPIO_TYPE_CAPSENCE_LED_BTN6,
	CY7_GPIO_TYPE_CAPSENCE_LED_BTN7,
	CY7_GPIO_TYPE_CAPSENCE_OUT_LINE0,
	CY7_GPIO_TYPE_CAPSENCE_OUT_LINE1,
	CY7_GPIO_TYPE_CAPSENCE_OUT_LINE2,
	CY7_GPIO_TYPE_CAPSENCE_OUT_LINE3,
};
/*
 * struct cy7_gpio_device is a special device to hold gpio functionality.
 * We need this because there are GPIO pins which are not part of any SCB.
 * Becaues of that gpio code is managed independantly from SCB-specific
 * devices.
 */
struct cy7_gpio_device {
	/*
	 * gpiolib structure that implements gpio functionality.
	 */
	struct gpio_chip gpio;

	/*
	 * The flag that indicates gpio is fully initialized
	 */
	bool gpio_active;

	/*
	 * Buffer for gpio-related usb control packets. We need this allocated
	 * to pass this check:
	 *  \usb_submit_urb
	 *    \usb_hcd_map_urb_for_dma pass
	 *      \object_is_on_stack(urb->transfer_buffer)
	 *
	 * TODO: Fo now we keep it simple, but might need to allocate it
	 * a more complex way in case if any alignment issues arise.
	 */
	uint8_t *gpio_buf;

	struct irq_chip irq_chip;

	uint32_t irq_enabled;
	uint32_t irq_stat;
	uint32_t irq_trig_raise;
	uint32_t irq_trig_fall;
//#define CY7_GPIO_IRQ_DEBUG
#ifdef CY7_GPIO_IRQ_DEBUG
	bool poll_enabled;
#endif
	struct delayed_work	poll_thread;
};

#define CY7_SCB_BIND_SPI_ADDR_MIN 0
#define CY7_SCB_BIND_SPI_ADDR_MAX 4
#define CY7_SCB_BIND_I2C_ADDR_MIN 0x08 /* from i2c tools */
#define CY7_SCB_BIND_I2C_ADDR_MAX 0x77 /* from i2c tools */

#define CY7_SCB_BIND_SPI_DEVICE_MAX (CY7_SCB_BIND_SPI_ADDR_MAX + 1)
#define CY7_SCB_BIND_I2C_DEVICE_MAX (CY7_SCB_BIND_I2C_ADDR_MAX + 1)

struct bind_info {
	unsigned char addr;
	char *bind_name;
	void *dev; /* struct i2c_client* or struct spi_device* */
};

struct cy7_device;
struct cy7_scb_device {
	/* parent device*/
	struct cy7_device *cy7_dev;
	/* sysfs device */
	struct kobject scb_kobj;
	/* scb index */
	int idx;
	struct bind_info *bind;
};

static inline struct cy7_scb_device *kobj_to_cy7_scb(struct kobject *kobj)
{
	return container_of(kobj, struct cy7_scb_device, scb_kobj);
}

struct cy7_device {
	/* USB device description */
	struct usb_device *usb_dev;
	struct usb_interface *usb_if;

	/* usb endpoint bulk in */
	struct usb_endpoint_descriptor *ep_in;
	/* usb endpoint bulk out */
	struct usb_endpoint_descriptor *ep_out;
	/* usb endpoint interrupt in */
	struct usb_endpoint_descriptor *ep_intr;

	/* usb input buffer */
	uint8_t in_buf[CY7C65215_USB_MAX_BULK_SIZE];
	/* usb output buffer */
	uint8_t out_buf[CY7C65215_USB_MAX_BULK_SIZE];
	/* usb interrupt buffer */
	uint8_t intr_buf[CY7C65215_USB_MAX_INTR_SIZE];

	/* device type: I2C, SPI, etc. */
	int devtype;

	/* protocol-specific part */
	union {
		struct cy7_spi_device spi;
		struct cy7_i2c_device i2c;
		struct cy7_gpio_device gpio;
	} u;

	enum _CY7_GPIO_TYPE gpios_type[CY7_NUM_GPIO];

	/* Serial control block, common to all */
	uint8_t scb;

	struct cy7_scb_device scb_dev[CY7C65215_SCB_DEV_MAX];

	struct device *config_dev;
	struct cy7_device_config *config_blob;

	struct mutex lock;
};

/*
 * cy7_device has two kinds of type deductions. One is usb-interface
 * class/subclass related and one is logical functionality of each interface.
 * They are a bit different because we use vendor interface subclass 5(MFG) for
 * GPIO pin control. By doing that the code is simpler.
 * Why?
 * GPIO pins are not related to any of the SCBs as SPI or I2C do.
 * They are one level above SCB architecturaly. 'struct cy7_device' is per-SCB
 * structure, thus to manage GPIOs we would need one more structure that is
 * per-USB and related them to each other in a tree-like relationship.
 * This complicates the code and is not strictly necessary if we can use MFG
 * interface (class 0xff, subclass 5), which is otherwize unused, for GPIO
 * purposes.
 */

#define CY7_TYPE_NONE 0
#define CY7_TYPE_I2C  1
#define CY7_TYPE_SPI  2
#define CY7_TYPE_CONTROL 3
#define CY7_TYPE_MFG  4
#define CY7_TYPE_INVAL 0xff

#define CY7_USB_VENDOR_SUBCLASS_SPI 0x02
#define CY7_USB_VENDOR_SUBCLASS_I2C 0x03
#define CY7_USB_VENDOR_SUBCLASS_CONTROL 0x05
#define CY7_USB_VENDOR_SUBCLASS_MFG 0xFF

/* usb-interface class/subclass related types */
static inline bool is_cy7_spi_interface(struct usb_host_interface *i)
{
	return i->desc.bInterfaceSubClass == CY7_USB_VENDOR_SUBCLASS_SPI;
}

static inline bool is_cy7_i2c_interface(struct usb_host_interface *i)
{
	return i->desc.bInterfaceSubClass == CY7_USB_VENDOR_SUBCLASS_I2C;
}

static inline bool is_cy7_control_interface(struct usb_host_interface *i)
{
	return i->desc.bInterfaceSubClass == CY7_USB_VENDOR_SUBCLASS_CONTROL;
}

static inline bool is_cy7_mfg_interface(struct usb_host_interface *i)
{
	return i->desc.bInterfaceSubClass == CY7_USB_VENDOR_SUBCLASS_MFG;
}

/* logical device types */
static inline bool cy7_device_is_spi(struct cy7_device *cy7_dev)
{
	return cy7_dev->devtype == CY7_TYPE_SPI;
}

static inline bool cy7_device_is_i2c(struct cy7_device *cy7_dev)
{
	return cy7_dev->devtype == CY7_TYPE_I2C;
}

static inline bool cy7_device_is_control(struct cy7_device *cy7_dev)
{
	return cy7_dev->devtype == CY7_TYPE_CONTROL;
}

static inline bool cy7_device_is_mfg(struct cy7_device *cy7_dev)
{
	return cy7_dev->devtype == CY7_TYPE_MFG;
}

static inline int cy7_get_type_from_interface(struct usb_host_interface *i)
{
	if (i->desc.bInterfaceClass == USB_CLASS_VENDOR_SPEC) {
		if (is_cy7_i2c_interface(i))
			return CY7_TYPE_I2C;

		if (is_cy7_spi_interface(i))
			return CY7_TYPE_SPI;

	if (is_cy7_control_interface(i))
		return CY7_TYPE_CONTROL;

	if (is_cy7_mfg_interface(i))
		return CY7_TYPE_MFG;
	}

	return CY7_TYPE_INVAL;
}

int cy7_device_reset(struct cy7_device *cy7_dev);

int cy7_read_configuration(struct cy7_device *cy7_dev);
int cy7_write_configuration(struct cy7_device *cy7_dev,
			    struct cy7_device_config *cfg, bool validate);

int cy7_scb_update_config(struct cy7_device *cy7_dev);
#endif /* _CY7_DEVICE_H */
