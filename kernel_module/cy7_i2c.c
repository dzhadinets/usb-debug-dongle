// SPDX-License-Identifier: GPL-2.0
/**
 *
 * @brief usb-debug-whistle linux kernel driver I2C SCB
 *        implementation.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#include <linux/usb.h>
#include "cypressdefs.h"
#include "cy7_i2c.h"

#define CY7_GEN_STOP (1 << 0)
#define CY7_GEN_NACK (1 << 1)

//static uint speed = CY7C65215_I2C_STANDARD_SPEED;      // module parameter speed, default standard speed

static int cy7_i2c_send_read_request(struct cy7_device *cy7_dev,
				     uint16_t slave_addr, uint16_t len)
{
	return usb_control_msg(cy7_dev->usb_dev,
			       usb_sndctrlpipe(cy7_dev->usb_dev, 0),
			       CY_I2C_READ_CMD,
			       USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT,
			       (cy7_dev->scb << 15) | (slave_addr << 8) |
				       CY7_GEN_STOP | CY7_GEN_NACK,
			       len, NULL, 0, 2000);
}

static int cy7_i2c_send_write_request(struct cy7_device *cy7_dev,
				      uint16_t slave_addr, uint16_t len,
				      uint8_t stop)
{
	return usb_control_msg(cy7_dev->usb_dev,
			       usb_sndctrlpipe(cy7_dev->usb_dev, 0),
			       CY_I2C_WRITE_CMD,
			       USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT,
			       (cy7_dev->scb << 15) | (slave_addr << 8) | stop,
			       len, NULL, 0, 2000);
}

static int cy7_i2c_reset(struct cy7_device *cy7_dev, uint8_t rw)
{
	return usb_control_msg(cy7_dev->usb_dev,
			       usb_sndctrlpipe(cy7_dev->usb_dev, 0),
			       CY_I2C_RESET_CMD,
			       USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_OUT,
			       rw | (cy7_dev->scb << 15), 0, NULL, 0, 2000);
}

static int cy7_i2c_read_status(struct cy7_device *cy7_dev, uint8_t rw)
{
	return usb_control_msg(cy7_dev->usb_dev,
			       usb_rcvctrlpipe(cy7_dev->usb_dev, 0),
			       CY_I2C_GET_STATUS_CMD,
			       USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_DIR_IN,
			       rw | (cy7_dev->scb << 15), 0,
			       cy7_dev->u.i2c.status,
			       sizeof(cy7_dev->u.i2c.status), 2000);
}

static void cy7_i2c_recover(struct cy7_device *cy7_dev, uint8_t rw)
{
	int ret = cy7_i2c_read_status(cy7_dev, rw);

	if (ret == sizeof(cy7_dev->u.i2c.status)) {
		if (cy7_dev->u.i2c.status[0] & 0x7F) {
			SQ_DBG(DRIVER_TAG,
				"adapter reported an error, trying to recover");
			SQ_DBG(DRIVER_TAG,
				 "status=[0x%02x, 0x%02x, 0x%02x]",
				 cy7_dev->u.i2c.status[0],
				 cy7_dev->u.i2c.status[1],
				 cy7_dev->u.i2c.status[2]);

			cy7_i2c_reset(cy7_dev, rw);

			ret = cy7_i2c_read_status(cy7_dev, rw);

			if ((ret == sizeof(cy7_dev->u.i2c.status)) &&
			    ((cy7_dev->u.i2c.status[0] & 0x7F) == 0))
				SQ_DBG(DRIVER_TAG,
					 "Recover successful");
			else
				SQ_ERROR(DRIVER_TAG, "Recover failed");
		}
	} else {
		SQ_ERROR(DRIVER_TAG,
			"failed to read adapter status: error=%d", ret);
	}
}

static int cy7_i2c_wait_status(struct cy7_device *cy7_dev)
{
	int ret, actual;

	ret = usb_interrupt_msg(cy7_dev->usb_dev,
				usb_rcvintpipe(cy7_dev->usb_dev,
					usb_endpoint_num(cy7_dev->ep_intr)),
				cy7_dev->u.i2c.status,
				sizeof(cy7_dev->u.i2c.status),
				&actual,
				2000);

	if ((ret == 0) && (actual == sizeof(cy7_dev->u.i2c.status)))
		return 0;

	return -EBADMSG;
}

static int cy7_i2c_read(struct cy7_device *cy7_dev, struct i2c_msg *msgs)
{
	int ret, actual = 0;

	/* workaround: read at least 1 byte */
	int len = msgs->len ? msgs->len : 1;

	/* prepare to bulk transfer */
	ret = cy7_i2c_send_read_request(cy7_dev, msgs->addr, len);

	if (ret < 0) {
		SQ_ERROR(DRIVER_TAG,
			"CONTROL transaction failed. error=%d", ret);
		return ret;
	}

	memcpy(cy7_dev->out_buf, msgs->buf, msgs->len);

	ret = usb_bulk_msg(cy7_dev->usb_dev,
			      usb_rcvbulkpipe(cy7_dev->usb_dev,
					      usb_endpoint_num(cy7_dev->ep_in)),
			      cy7_dev->in_buf, len, &actual, 2000);

	if (ret < 0) {
		SQ_ERROR(DRIVER_TAG, "BULK transaction failed. error=%d",
			ret);

		if (cy7_i2c_read_status(cy7_dev, CY_I2C_MODE_READ) ==
		    sizeof(cy7_dev->u.i2c.status))
			SQ_ERROR(DRIVER_TAG, "Reported status: %x",
				cy7_dev->u.i2c.status[0]);

		return ret;
	}

	ret = cy7_i2c_wait_status(cy7_dev);

	if (ret < 0) {
		SQ_ERROR(DRIVER_TAG, "INT transaction failed. error=%d",
			ret);
		return ret;
	}

	if (cy7_dev->u.i2c.status[0] & CY_I2C_ERROR_BIT) {
		cy7_i2c_recover(cy7_dev, CY_I2C_MODE_READ);
		return -EIO;
	}

	return msgs->len ? actual : 0;
}

static int cy7_i2c_write(struct cy7_device *cy7_dev, struct i2c_msg *msgs,
			 uint8_t stop)
{
	int ret, actual = 0;

	if (msgs->len == 0) {
		/*
		 * workaround for the IC: it is not possible to read or write
		 * zero length data (like only START ADDR RW STOP)
		 * so here is a workaround with replacing WRITE operation with
		 * READ (less risks)
		 */
		SQ_DBG(DRIVER_TAG, "Workaround");
		return cy7_i2c_read(cy7_dev, msgs);
	}

	/* prepare to bulk transfer */
	ret = cy7_i2c_send_write_request(cy7_dev, msgs->addr, msgs->len,
					    stop);

	if (ret < 0) {
		SQ_ERROR(DRIVER_TAG,
			"CONTROL transaction failed. error=%d", ret);

		return ret;
	}

	memcpy(cy7_dev->out_buf, msgs->buf, msgs->len);

	ret = usb_bulk_msg(cy7_dev->usb_dev,
			   usb_sndbulkpipe(cy7_dev->usb_dev,
				usb_endpoint_num(cy7_dev->ep_out)),
			   cy7_dev->out_buf, msgs->len, &actual, 2000);

	if (ret < 0) {
		SQ_ERROR(DRIVER_TAG, "BULK transaction failed. error=%d",
			ret);

		if (cy7_i2c_read_status(cy7_dev, CY_I2C_MODE_WRITE) ==
		    sizeof(cy7_dev->u.i2c.status))
			SQ_ERROR(DRIVER_TAG, "Reported status: %x",
				cy7_dev->u.i2c.status[0]);

		return ret;
	}

	ret = cy7_i2c_wait_status(cy7_dev);

	if (ret < 0) {
		SQ_ERROR(DRIVER_TAG, "INT transaction failed. error=%d",
			ret);

		return ret;
	}

	if (cy7_dev->u.i2c.status[0] & CY_I2C_ERROR_BIT) {
		SQ_ERROR(DRIVER_TAG, "write failed with status: %x",
			cy7_dev->u.i2c.status[0]);

		cy7_i2c_recover(cy7_dev, CY_I2C_MODE_WRITE);

		return -EIO;
	}

	return actual;
}

static int cy7_i2c_transfer_msg(struct cy7_device *cy7_dev,
				struct i2c_adapter *adpt, struct i2c_msg *msg,
				bool gen_stop)
{
	int ret = 0;
	char *buf = msg->buf;

	SQ_DBG(DRIVER_TAG,
		 "addr=0x%04x, len=0x%04x, flags=0x%04x", msg->addr, msg->len,
		 msg->flags);

	/* msg size should not be larger than endpoint max transfer size */
	if (msg->len > CY7C65215_USB_MAX_BULK_SIZE) {
		SQ_ERROR(DRIVER_TAG,
			"size of data is too large for existing USB endpoints");
		return -EIO;
	}

	if (msg->flags & I2C_M_TEN) {
		SQ_ERROR(DRIVER_TAG, "10 bit i2c address not supported");
		return -EINVAL;
	}

	if (msg->flags & I2C_M_RD) {
		/* is read op */
		ret = cy7_i2c_read(cy7_dev, msg);

		if (ret < 0)
			goto out;

		/* check if we need to prepend a length byte */
		if (msg->flags & I2C_M_RECV_LEN)
			*buf++ = ret;

		memcpy(buf, cy7_dev->in_buf, msg->len);
	} else {
		/* is write op */
		ret = cy7_i2c_write(cy7_dev, msg, gen_stop ? CY7_GEN_STOP : 0);
	}

out:
	return ret;
}

static int cy7_i2c_transfer(struct i2c_adapter *adpt, struct i2c_msg *msgs,
			    int num)
{
	struct cy7_device *cy7_dev;
	int ret;
	int i;
	bool gen_stop;

	if (!adpt)
		return -EIO;

	if (!msgs)
		return -EIO;

	if (num <= 0)
		return -EIO;

	cy7_dev = (struct cy7_device *)adpt->algo_data;

	if (!cy7_dev)
		return -EIO;

	mutex_lock(&cy7_dev->lock);

	for (i = 0; i < num; i++) {
		gen_stop = i == num - 1;
		ret = cy7_i2c_transfer_msg(cy7_dev, adpt, &msgs[i], gen_stop);
		if (ret)
			break;
	}

	mutex_unlock(&cy7_dev->lock);

	if (ret < 0)
		return ret;

	return num;
}

static u32 cy7_i2c_func(struct i2c_adapter *dev)
{
	return I2C_FUNC_I2C | I2C_FUNC_SMBUS_EMUL;
}

static const struct i2c_algorithm cy7_i2c_algorithm = {
	.master_xfer = cy7_i2c_transfer,
	.functionality = cy7_i2c_func,
};

int cy7_i2c_probe(struct cy7_device *cy7_dev)
{
	int ret;

	if (!cy7_dev)
		return -EINVAL;

	/* setup i2c adapter description */
	cy7_dev->u.i2c.i2c_dev.owner = THIS_MODULE;
	cy7_dev->u.i2c.i2c_dev.class = 0;
	cy7_dev->u.i2c.i2c_dev.algo = &cy7_i2c_algorithm;
	cy7_dev->u.i2c.i2c_dev.algo_data = cy7_dev;
	cy7_dev->u.i2c.i2c_dev.nr = -1;
	snprintf(cy7_dev->u.i2c.i2c_dev.name,
		 sizeof(cy7_dev->u.i2c.i2c_dev.name),
		 "i2c-CY7C65215-usb at bus %03d device %03d",
		 cy7_dev->usb_dev->bus->busnum, cy7_dev->usb_dev->devnum);

	cy7_dev->u.i2c.i2c_dev.dev.parent = &cy7_dev->usb_if->dev;

	/* finally attach to i2c layer */
	ret = i2c_add_adapter(&cy7_dev->u.i2c.i2c_dev);
	if (ret < 0)
		return ret;

	SQ_INFO(DRIVER_TAG, "New i2c bus /dev/i2c-%d added",
		 cy7_dev->u.i2c.i2c_dev.nr);

	mutex_init(&cy7_dev->lock);

	return 0;
}

void cy7_i2c_remove(struct cy7_device *cy7_dev)
{
	if (!cy7_dev)
		return;

	if (cy7_dev->u.i2c.i2c_dev.nr >= 0)
		i2c_del_adapter(&cy7_dev->u.i2c.i2c_dev);

	mutex_destroy(&cy7_dev->lock);
}
