/* SPDX-License-Identifier: GPL-2.0 */
/**
 *
 * @brief usb-debug-whistle linux kernel driver main types
 *        definitions.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef CY7_CONFIG_DEFAULT_H
#define CY7_CONFIG_DEFAULT_H

#include <linux/string.h>
#include "cy7_config.h"

/*
 * cy7_set_defaults_uart - sets scb config to UART mode
 * with default settings, 115200 8n1 6pin
 */
static void cy7_set_defaults_uart(struct cy7_scb_config *c)
{
	memset(c, 0, sizeof(*c));
	c->mode = CY7_SCB_MODE_UART;
	c->protocol = CY7_SCB_PROTO_CDC;
	c->tx_led = 0xff;
	c->rx_led = 0xff;
	c->rx_tx_led = 0xff;
	c->u.uart.uart_baud_rate = 115200;
	c->u.uart.uart_type = CY7_UART_TYPE_PIN_2;
	c->u.uart.uart_data_width = CY7_UART_DATA_WIDTH_8;
	c->u.uart.uart_stop_bits = CY7_UART_STOP_BITS_1;
	c->u.uart.uart_protocol = CY7_UART_MODE_STANDARD;
	c->u.uart.uart_parity = CY7_UART_PARITY_NONE;
	c->u.uart.uart_bit_position = CY7_UART_BIT_POS_LSB;
	c->u.uart.uart_tx_retry_on_nack = 0;
	c->u.uart.uart_rx_invert_polarity = 0;
	c->u.uart.uart_drop_on_err = 0;
	c->u.uart.uart_flow_ctrl = CY7_UART_FLOW_CONTROL_DISABLE;
	c->u.uart.uart_loopback = 0;
	c->u.uart.uart_line_active_state = CY7_UART_LINE_ACTIVE_NONE;
}

/*
 * cy7_set_defaults_spi - sets scb config to SPI mode
 * with default settings.
 */
static void cy7_set_defaults_spi(struct cy7_scb_config *c)
{
	memset(c, 0, sizeof(*c));
	c->mode = CY7_SCB_MODE_SPI;
	c->protocol = CY7_SCB_PROTO_VENDOR;
	c->tx_led = 0xff;
	c->rx_led = 0xff;
	c->rx_tx_led = 0xff;
	c->u.spi.spi_freq = CY7_SPI_FREQ_DEF;
	c->u.spi.spi_data_width = CY7_SPI_DATA_WIDTH_DEF;
	c->u.spi.spi_bit_position = CY7_SPI_BIT_POS_MSB;
	c->u.spi.spi_protocol = CY7_SPI_PROTO_MOTOROLA;
	c->u.spi.spi_transfer_mode = CY7_SPI_XFER_MODE_TX_RX;
	c->u.spi.spi_is_master = CY7_SPI_MODE_MASTER;
	c->u.spi.spi_ssn_toggle_mode = CY7_SPI_SSN_TOGGLE_CONTIGNOUS;
	c->u.spi.spi_select_precede = 0;
	c->u.spi.spi_cpha = 0;
	c->u.spi.spi_cpol = 0;
	c->u.spi.spi_loopback = 0;
}

/*
 * cy7_set_defaults_i2c - sets scb config to I2C mode
 * with default settings - master mode, etc
 */
static void cy7_set_defaults_i2c(struct cy7_scb_config *c)
{
	memset(c, 0, sizeof(*c));
	c->mode = CY7_SCB_MODE_I2C;
	c->protocol = CY7_SCB_PROTO_VENDOR;
	c->tx_led = 0xff;
	c->rx_led = 0xff;
	c->rx_tx_led = 0xff;
	c->u.i2c.i2c_freq = CY7_I2C_FREQ_DEF;
	c->u.i2c.i2c_slave_address = CY7_I2C_SLAVE_ADDRESS_DEF;
	c->u.i2c.i2c_bit_position = CY7_I2C_BIT_POS_MSB;
	c->u.i2c.i2c_is_master = CY7_I2C_MODE_MASTER;
	c->u.i2c.i2c_signore = 0;
	c->u.i2c.i2c_clock_stretch = CY7_I2C_STRETCH_NO;
	c->u.i2c.i2c_loopback = 0;
}

/*
 * cy7_set_defaults_jtag - sets scb config to JTAG mode
 * with default settings.
 */
static void cy7_set_defaults_jtag(struct cy7_scb_config *c)
{
	memset(c, 0, sizeof(*c));
	c->mode = CY7_SCB_MODE_JTAG;
	c->protocol = CY7_SCB_PROTO_VENDOR;
	c->tx_led = 0xff;
	c->rx_led = 0xff;
	c->rx_tx_led = 0xff;
	c->u.jtag.jtag_freq = CY7_JTAG_FREQ_DEF;
}

/*
 * cy7_set_defaults_disable - sets scb config to DISABLED mode
 */
static void cy7_set_defaults_disable(struct cy7_scb_config *c)
{
	memset(c, 0, sizeof(*c));
	c->mode = CY7_SCB_MODE_DISABLE;
	c->protocol = CY7_SCB_PROTO_DISABLED;
	c->tx_led = 0xff;
	c->rx_led = 0xff;
	c->rx_tx_led = 0xff;
}

#endif /* _CY7_CONFIG_DEFAULT_H */
