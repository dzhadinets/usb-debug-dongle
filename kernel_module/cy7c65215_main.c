// SPDX-License-Identifier: GPL-2.0
/**
 *
 * @brief usb-debug-whistle linux kernel module entry point.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */

#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(3, 10, 0)
#error The driver requires at least kernel version 3.10
#else

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/types.h>
#include <linux/slab.h>
#include "cypressdefs.h"
#include "cy7_scb.h"
#include "cy7_device.h"
#include "cy7_spi.h"
#include "cy7_i2c.h"
#include "cy7_gpio.h"

static const struct usb_device_id cy7_usb_table[] = {
	{ USB_DEVICE(0x04b4, 0x0005) },
	{ USB_DEVICE(0x04b4, 0x0007) },
	{ USB_DEVICE(0x04b4, 0x0009) },
	{ USB_DEVICE(0x04b4, 0x000a) },
	{ USB_DEVICE(0x04b4, 0x00fa) },
	{}
};

MODULE_DEVICE_TABLE(usb, cy7_usb_table);

struct class *cy7_config_dev_class;

static void cy7_usb_free_device(struct cy7_device *cy7_dev)
{
	if (IS_ERR_OR_NULL(cy7_dev))
		return;

	if (cy7_device_is_i2c(cy7_dev)) {
		cy7_i2c_remove(cy7_dev);
	} else if (cy7_device_is_spi(cy7_dev)) {
		cy7_spi_remove(cy7_dev);
	} else if (cy7_device_is_control(cy7_dev)) {
		cy7_scb_dev_remove(cy7_dev);
	} else if (cy7_device_is_mfg(cy7_dev)) {
		cy7_scb_dev_remove(cy7_dev);
	} else {
		SQ_WARNING(DRIVER_TAG, "freeing unexpected device");
	}

	usb_set_intfdata(cy7_dev->usb_if, NULL);
	usb_put_dev(cy7_dev->usb_dev);

	cy7_config_blob_free((struct cy7_config_blob *) cy7_dev->config_blob);
	kfree(cy7_dev);
}

static int cy7_usb_probe(struct usb_interface *usb_if,
			 const struct usb_device_id *usb_id)
{
	struct usb_device *usb_dev = usb_get_dev(interface_to_usbdev(usb_if));
	struct usb_endpoint_descriptor *epd;
	struct cy7_device *cy7_dev;
	struct usb_host_interface *settings;
	int i;
	int error;
	int ret = 0;
	int devtype = CY7_TYPE_INVAL;

	SQ_INFO(DRIVER_TAG, "New USB device(%d:%d) intf subClass 0x%02X connecting",
			usb_dev->bus->busnum,
			usb_dev->devnum,
			usb_if->cur_altsetting->desc.bInterfaceSubClass);

	settings = usb_if->cur_altsetting;

	devtype = cy7_get_type_from_interface(settings);
	if (devtype == CY7_TYPE_INVAL)
		return -EPROTONOSUPPORT;

	/* create and initialize a new device data structure */
	cy7_dev = kzalloc(sizeof(struct cy7_device), GFP_KERNEL);
	if (!cy7_dev) {
		SQ_ERROR(DRIVER_TAG, "could not allocate device memory");
		usb_put_dev(usb_dev);
		return -ENOMEM;
	}

	/* save USB device data */
	cy7_dev->usb_dev = usb_dev;
	cy7_dev->usb_if = usb_if;

	/* save the pointer to the new cy7_device in USB interface data */
	usb_set_intfdata(usb_if, cy7_dev);

	/* find endpoints */
	SQ_INFO(DRIVER_TAG, "bNumEndpoints=%d",
			settings->desc.bNumEndpoints);

	SQ_INFO(DRIVER_TAG, "reading config_blob");
	ret = cy7_read_configuration(cy7_dev);
	if (ret != 0) {
		SQ_ERROR(DRIVER_TAG, "Failed to read config at start: %d", ret);
		goto err;
	}

	for (i = 0; i < settings->desc.bNumEndpoints; i++) {
		epd = &settings->endpoint[i].desc;

		SQ_INFO(DRIVER_TAG,
			"  endpoint=%d type=%d dir=%d addr=%0x", i,
			usb_endpoint_type(epd), usb_endpoint_dir_in(epd),
			usb_endpoint_num(epd));

		if (usb_endpoint_is_bulk_in(epd))
			cy7_dev->ep_in = epd;
		else if (usb_endpoint_is_bulk_out(epd))
			cy7_dev->ep_out = epd;
		else if (usb_endpoint_xfer_int(epd))
			cy7_dev->ep_intr = epd;
	}

	/* select correct SCB */
	cy7_dev->scb = settings->desc.bInterfaceNumber ? 1 : 0;
	cy7_dev->devtype = devtype;

	if (is_cy7_i2c_interface(settings)) {
		error = cy7_i2c_probe(cy7_dev);
	} else if (is_cy7_spi_interface(settings)) {
		error = cy7_spi_probe(cy7_dev);
	} else if (is_cy7_control_interface(settings)) {
		error = cy7_scb_dev_create(cy7_dev);
	} else if (is_cy7_mfg_interface(settings)) {
		error = cy7_scb_dev_create(cy7_dev);
	} else {
		SQ_ERROR(DRIVER_TAG, "Failed to create unexpected device");
		error = -ENODEV;
	}

	if (error < 0)
		goto err;


	SQ_INFO(DRIVER_TAG, "connected\n");

	return 0;
err:
	cy7_usb_free_device(cy7_dev);
	return error;
}

static void cy7_usb_disconnect(struct usb_interface *usb_if)
{
	struct cy7_device *cy7_dev = usb_get_intfdata(usb_if);

	if (cy7_dev == NULL)
		return;

	SQ_INFO(DRIVER_TAG,
			"New USB device(%d:%d) intf subClass 0x%02X disconnecting",
			cy7_dev->usb_dev->bus->busnum,
			cy7_dev->usb_dev->devnum,
			usb_if->cur_altsetting->desc.bInterfaceSubClass);

	cy7_usb_free_device(cy7_dev);

	SQ_INFO(DRIVER_TAG, "disconnected\n");
}

static struct usb_driver cy7_usb_driver = {
	.name = "CY7C65215-usb",
	.id_table = cy7_usb_table,
	.probe = cy7_usb_probe,
	.disconnect = cy7_usb_disconnect
};

static int __init cy7c65215_init(void)
{
	cy7_config_dev_class = class_create(THIS_MODULE, "cy7_config");
	if (IS_ERR(cy7_config_dev_class)) {
		cy7_config_dev_class = NULL;
		return PTR_ERR(cy7_config_dev_class);
	}

	return usb_register(&cy7_usb_driver);
}

static void __exit cy7c65215_exit(void)
{
	usb_deregister(&cy7_usb_driver);
	class_destroy(cy7_config_dev_class);
}

module_init(cy7c65215_init);
module_exit(cy7c65215_exit);

MODULE_ALIAS("CY7C65215");
MODULE_AUTHOR("Softeq Development");
MODULE_DESCRIPTION("CY7C65215-usb driver v1.0.0");
MODULE_LICENSE("GPL");

#endif /* LINUX_VERSION_CODE < KERNEL_VERSION(3,10,0) */
