// SPDX-License-Identifier: GPL-2.0
/**
 *
 * @brief usb-debug-whistle linux kernel driver SCB config
 *        implementation.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/device.h>
#include <linux/module.h>
#include <linux/parser.h>

#include <linux/i2c.h>
#include <linux/spi/spi.h>

#include "cy7_device.h"

#define TOKEN_BIND_CONFIG 1
static const match_table_t bind_tokens = {
	{ TOKEN_BIND_CONFIG, "%d:%s" },
	{ 0, NULL },
};

static ssize_t cy7_slave_bind_show(struct kobject *kobj, struct kobj_attribute *attr,
								   char *buf)
{
	int len = 0;
	static const char *help_txt =
			"* How to use Bind/Unbind slave device for I2C and SPI master\n"
			"* Bind >>echo address:alias > "
			"/sys/class/cy7_config/whistle_device-1.2/scb0/slave_alias/slave_bind\n"
			"* Unbind >>echo address > "
			"/sys/class/cy7_config/whistle_device-1.2/scb0/slave_alias/slave_bind\n"
			"* Example I2C bind >>echo 0x18:lis3de > "
			"/sys/class/cy7_config/whistle_device-1.2/scb0/slave_alias/slave_bind\n"
			"* Example I2C unbind >>echo 0x18: > "
			"/sys/class/cy7_config/whistle_device-1.2/scb0/slave_alias/slave_bind\n"
			"* Example SPI bind >>echo 0:spidev > "
			"/sys/class/cy7_config/whistle_device-1.2/scb0/slave_alias/slave_bind\n"
			"* Example SPI unbind >>echo 0: > "
			"/sys/class/cy7_config/whistle_device-1.2/scb0/slave_alias/slave_bind\n";

	len += scnprintf(buf + len, PAGE_SIZE - len, "%s", help_txt);

	return len;
}

static int bind_device(struct cy7_scb_device *cy7_scb, struct bind_info *bind)
{
	int ret = 0;
	struct cy7_device *cy7_dev = cy7_scb->cy7_dev;
	struct cy7_device_config *cy7_config = cy7_dev->config_blob;
	struct cy7_scb_config *scb_config = &cy7_config->scb[cy7_scb->idx];
	struct usb_device *usb_dev = cy7_dev->usb_dev;
	struct usb_host_config *config = usb_dev->config;
	struct usb_interface *interface = NULL;
	struct cy7_device *cy7_dev_remote = NULL;

	interface = config->interface[cy7_scb->idx];
	if (!interface)
		return -ENODEV;

	cy7_dev_remote = usb_get_intfdata(interface);
	if (!cy7_dev_remote)
		return -ENODEV;

	if (bind->bind_name)
		SQ_INFO(DRIVER_TAG, "Bind 0x%02X device %s\n", bind->addr, bind->bind_name);
	else
		SQ_INFO(DRIVER_TAG, "UnBind 0x%02X device\n", bind->addr);

	if (scb_config->mode == CY7_SCB_MODE_SPI) {
		if (bind->dev) {
			spi_unregister_device(bind->dev);
			bind->dev = NULL;
		}

		if (bind->bind_name) {
			struct spi_board_info info;

			memset(&info, 0, sizeof(struct spi_board_info));

			if ((bind->addr < CY7_SCB_BIND_SPI_ADDR_MIN) || (bind->addr > CY7_SCB_BIND_SPI_ADDR_MAX)) {
				SQ_ERROR(DRIVER_TAG, "Wrong SPI chipselect %d. Must be from %d to %d\n",
						 bind->addr, CY7_SCB_BIND_SPI_ADDR_MIN, CY7_SCB_BIND_SPI_ADDR_MAX);
				return -EINVAL;
			}

			strncpy(info.modalias, bind->bind_name, sizeof(info.modalias));
			info.max_speed_hz = CY7_SPI_FREQ_DEF;
			info.chip_select = bind->addr;
			info.bus_num = cy7_dev_remote->u.spi.master->bus_num;
			info.mode = SPI_MODE_0;
			info.controller_data = &cy7_dev_remote->u.spi;

			bind->dev = spi_new_device(cy7_dev_remote->u.spi.master, &info);
			if (IS_ERR_OR_NULL(bind->dev)) {
				SQ_ERROR(DRIVER_TAG, "Failed to create SPI slave device\n");
				ret = PTR_ERR(bind->dev);
				bind->dev = NULL;
			}
		}
	} else if (scb_config->mode == CY7_SCB_MODE_I2C) {
		if (bind->dev) {
			i2c_unregister_device(bind->dev);
			bind->dev = NULL;
		}

		if (bind->bind_name) {
			struct i2c_board_info info;

			memset(&info, 0, sizeof(struct i2c_board_info));

			if ((bind->addr < CY7_SCB_BIND_I2C_ADDR_MIN) || (bind->addr > CY7_SCB_BIND_I2C_ADDR_MAX)) {
				SQ_ERROR(DRIVER_TAG, "Wrong I2C address %d. Must be from %d to %d\n",
						 bind->addr, CY7_SCB_BIND_I2C_ADDR_MIN, CY7_SCB_BIND_I2C_ADDR_MAX);
				return -EINVAL;
			}

			info.addr = bind->addr;
			strscpy(info.type, bind->bind_name, I2C_NAME_SIZE);

			bind->dev = i2c_new_client_device(&cy7_dev_remote->u.i2c.i2c_dev, &info);
			if (IS_ERR_OR_NULL(bind->dev)) {
				SQ_ERROR(DRIVER_TAG, "Failed to create I2C slave device\n");
				ret = PTR_ERR(bind->dev);
				bind->dev = NULL;
			}
		}
	}
	return ret;
}

static ssize_t cy7_slave_bind_store(struct kobject *kobj, struct kobj_attribute *attr,
									const char *buf, size_t len)
{
	int ret = -EINVAL;
	struct cy7_scb_device *cy7_scb = kobj_to_cy7_scb(kobj);
	substring_t args[MAX_OPT_ARGS];
	char *options, *tmp_options = NULL;
	char *p;
	int optlen = 0;

	if (!buf || !len) {
		SQ_ERROR(DRIVER_TAG, "Error string format\n");
		return -ENOBUFS;
	}

	optlen = strnlen(buf, len);
	if (optlen <= 0) {
		SQ_ERROR(DRIVER_TAG, "Error string length\n");
		return -ENOBUFS;
	}

	tmp_options = kmemdup_nul(buf, optlen,
				   GFP_KERNEL);
	if (!tmp_options)
		return -ENOMEM;

	options = tmp_options;

	while ((p = strsep(&options, ",")) != NULL) {
		int token;

		if (!*p)
			continue;

		token = match_token(p, bind_tokens, args);

		switch (token) {
		case TOKEN_BIND_CONFIG: {
			int dev_addr = -1;
			char *s;

			ret = match_int(&args[0], &dev_addr);
			if (ret)
				goto error;

			s = match_strdup(&args[1]);

			if (strlen(s) > strlen("\n")) {
				s[strlen(s)-1] = 0; /* del "\n" */
				cy7_scb->bind[dev_addr].addr = dev_addr;
				cy7_scb->bind[dev_addr].bind_name = s;
			} else {
				cy7_scb->bind[dev_addr].addr = 0;
				kfree(cy7_scb->bind[dev_addr].bind_name);
				cy7_scb->bind[dev_addr].bind_name = NULL;
			}

			ret = bind_device(cy7_scb, &cy7_scb->bind[dev_addr]);
			if (!ret)
				ret = len;

			break;
		}
		default:
			SQ_ERROR(DRIVER_TAG, "FAIL\n");
			break;
		}
	}

error:
	kfree(tmp_options);
	return ret;
}

static ssize_t cy7_slave_info_show(struct kobject *kobj, struct kobj_attribute *attr,
								   char *buf)
{
	struct cy7_scb_device *cy7_scb = kobj_to_cy7_scb(kobj);
	struct cy7_device *cy7_dev = cy7_scb->cy7_dev;
	struct cy7_device_config *cy7_config = cy7_dev->config_blob;
	struct cy7_scb_config *scb_config = &cy7_config->scb[cy7_scb->idx];
	int len = 0;
	int idx;

	int max_dev = 0;

	if (scb_config->mode == CY7_SCB_MODE_SPI) {
		max_dev = CY7_SCB_BIND_SPI_DEVICE_MAX;
	} else if (scb_config->mode == CY7_SCB_MODE_I2C) {
		max_dev = CY7_SCB_BIND_I2C_DEVICE_MAX;
	}

	len += scnprintf(buf + len, PAGE_SIZE - len, "addr : name\n");
	for (idx = 0; idx < max_dev; idx++) {
		if (!cy7_scb->bind[idx].bind_name)
			continue;

		len += scnprintf(
			buf + len, PAGE_SIZE - len, "0x%02X   %s\n", cy7_scb->bind[idx].addr,
			cy7_scb->bind[idx].bind_name);
	}

	return len;
}

static struct kobj_attribute cy7_scb_slave_bind_attr =
	__ATTR(slave_bind, 0660,
		   cy7_slave_bind_show,
		   cy7_slave_bind_store);

static struct kobj_attribute cy7_scb_slave_bind_info_attr =
	__ATTR(slave_bind_info, 0440,
		   cy7_slave_info_show,
		   NULL);

static struct attribute *cy7_scb_slave_dev_attrs[] = {
	&cy7_scb_slave_bind_attr.attr,
	&cy7_scb_slave_bind_info_attr.attr,
	NULL,
};

static const struct attribute_group cy7_scb_slave_dev_group = {
	.name = "slave_alias",
	.attrs = cy7_scb_slave_dev_attrs,
};

int cy7_scb_slavedev_probe(struct cy7_scb_device *cy7_scb)
{
	int ret = 0;
	struct cy7_device *cy7_dev = cy7_scb->cy7_dev;
	struct cy7_device_config *cy7_config = cy7_dev->config_blob;
	struct cy7_scb_config *scb_config = &cy7_config->scb[cy7_scb->idx];

	ret = sysfs_create_group(&cy7_scb->scb_kobj,
			   &cy7_scb_slave_dev_group);

	if (ret)
		return ret;

	if (scb_config->mode == CY7_SCB_MODE_SPI) {
		cy7_scb->bind = devm_kzalloc(cy7_dev->config_dev,
				sizeof(*cy7_scb->bind)*CY7_SCB_BIND_SPI_DEVICE_MAX, GFP_KERNEL);

	} else if (scb_config->mode == CY7_SCB_MODE_I2C) {
		cy7_scb->bind = devm_kzalloc(cy7_dev->config_dev,
				sizeof(*cy7_scb->bind)*CY7_SCB_BIND_I2C_DEVICE_MAX, GFP_KERNEL);
	}

	if (((scb_config->mode == CY7_SCB_MODE_SPI)
			|| (scb_config->mode == CY7_SCB_MODE_I2C))
			&& !cy7_scb->bind)
		return -ENOMEM;

	return ret;
}

int cy7_scb_slavedev_remove(struct cy7_scb_device *cy7_scb)
{
	struct cy7_device *cy7_dev = cy7_scb->cy7_dev;
	struct cy7_device_config *cy7_config = cy7_dev->config_blob;
	struct cy7_scb_config *scb_config = &cy7_config->scb[cy7_scb->idx];
	int max_dev = 0;
	int idx = 0;

	if (scb_config->mode == CY7_SCB_MODE_SPI) {
		max_dev = CY7_SCB_BIND_SPI_DEVICE_MAX;
	} else if (scb_config->mode == CY7_SCB_MODE_I2C) {
		max_dev = CY7_SCB_BIND_I2C_DEVICE_MAX;
	}

	for (idx = 0; idx < max_dev; idx++) {
		if (cy7_scb->bind[idx].dev)
			bind_device(cy7_scb, &cy7_scb->bind[idx]); /* unbind */

		kfree(cy7_scb->bind[idx].bind_name);
		cy7_scb->bind[idx].bind_name = NULL;
	}

	sysfs_remove_group(&cy7_scb->scb_kobj,
			   &cy7_scb_slave_dev_group);
	return 0;
}
