#include <common/CyUSB.h>

using namespace Cy;

CyUsbDwordField UsbDeviceConfig::getChecksum() const
{
    CyUsbDwordField sum = 0;
    int end = sizeof(CyUsbscDeviceConfig) / sizeof(CyUsbDwordField);
    const CyUsbDwordField *data = reinterpret_cast<const CyUsbDwordField *>(this);
    for (int i = 3; i < end; ++i)
    {
        sum += data[i];
    }
    return sum;
}

CyUsbWordField UsbDeviceConfig::getDefaultPid() const
{
    // valid for 2215 only
    if (this->scb[0].value.protocol == CY_SCB_PROTOCOL::CDC)
    {
        if (this->scb[1].value.protocol == CY_SCB_PROTOCOL::CDC)
        {
            return CYPRESS_PID_BOTH_CDC;
        }
        return CYPRESS_PID_SCB0_CDC;
    }
    return this->scb[1].value.protocol != CY_SCB_PROTOCOL::CDC ? CYPRESS_PID_SCB1_NOCDC : CYPRESS_PID_SCB1_CDC;
}
