#ifndef GPIOSTATE_H
#define GPIOSTATE_H

#include <QtCore>

#include "common/CyUSB.h"

#define GPIO_COUNT 19

namespace ScbNum {
    enum ScbNum { SCB0 = 0, SCB1 = 1 } ;
}

class GpioState
{
public:
    enum State
    {
        Free,
        Drive_0,
        Drive_1,
        Input,
        SCB0,
        SCB1,
        SCB0_RxLED,
        SCB0_TxLED,
        SCB0_RxTxLED,
        SCB0_UART_RS485,
        SCB1_RxLED,
        SCB1_TxLED,
        SCB1_RxTxLED,
        SCB1_UART_RS485,
        WAKEUP_POWER,
        BCD0,
        BCD1,
        BUS_DETECT,
        DRIVE,
        CAPSENCE,
        WATER_SHIELD,
        COMMON_LED,
        CAPSENCE_BTN0,
        CAPSENCE_BTN1,
        CAPSENCE_BTN2,
        CAPSENCE_BTN3,
        CAPSENCE_BTN4,
        CAPSENCE_BTN5,
        CAPSENCE_BTN6,
        CAPSENCE_BTN7,
        CAPSENCE_LED_BTN0,
        CAPSENCE_LED_BTN1,
        CAPSENCE_LED_BTN2,
        CAPSENCE_LED_BTN3,
        CAPSENCE_LED_BTN4,
        CAPSENCE_LED_BTN5,
        CAPSENCE_LED_BTN6,
        CAPSENCE_LED_BTN7,
        CAPSENCE_OUT_LINE0,
        CAPSENCE_OUT_LINE1,
        CAPSENCE_OUT_LINE2,
        CAPSENCE_OUT_LINE3,
    };

    static State SCB_UART_RS485[2];
    static State SCB_RxLED[2];
    static State SCB_TxLED[2];
    static State SCB_RxTxLED[2];
    static State CAPSENCE_BTN[CAPSENSE_BUTTONS_COUNT];
    static State CAPSENCE_LED_BTN[CAPSENSE_BUTTONS_COUNT];
    static State CAPSENCE_OUT_LINE[CAPSENSE_OUTLINES_COUNT];

    State operator[](Cy::GPIO gpio) const
    {
        return gpio == Cy::GPIO::None ? Free : gpios[static_cast<uint>(gpio)];
    }

    bool isGPIOFree(Cy::GPIO gpio, GpioState::State state = {}) const
    {
        return (*this)[gpio] == Free || (*this)[gpio] == state;
    }

    std::function<bool(Cy::GPIO)> freeFilter(GpioState::State includeState, bool includeNone = true) const;

    void unuse(Cy::GPIO gpio)
    {
        use(gpio, Free);
    }
    void unuse(ScbNum::ScbNum scbNum, Cy::CY_SCB_MODE mode, Cy::CY_UART_TYPE uartType = {});

    void use(Cy::GPIO gpio, State user)
    {
        if (gpio != Cy::GPIO::None)
        {
            gpios[static_cast<uint>(gpio)] = user;
        }
        // else just ignore, because many users "use" None
    }
    QSet<GpioState::State> use(ScbNum::ScbNum scbNum, Cy::CY_SCB_MODE mode, State newState,
                               Cy::CY_UART_TYPE uartType = {});

    void changeGPIO(Cy::GPIO &gpio, Cy::GPIO newGpioValue, State newState);

    QVector<Cy::GPIO> getUnusedGPIOs();
    State &operator[](Cy::GPIO gpio)
    {
        return gpios[static_cast<uint>(gpio)];
    }

private:


    QSet<GpioState::State> walk(ScbNum::ScbNum scbNum, Cy::CY_SCB_MODE mode, Cy::CY_UART_TYPE uartType, State newState);

    State gpios[GPIO_COUNT] = {Free};
};

#endif // GPIOSTATE_H
