#ifndef CYUSB_H
#define CYUSB_H

#include <cstdint>
#include <type_traits>
#include "common/padded.hh"

#pragma pack(push, 1)

#define NUMBER_OF_GPIOS 19
#define CAPSENSE_BUTTONS_COUNT 8
#define CAPSENSE_OUTLINES_COUNT 4

#define CYPRESS_VID 0x04b4
#define CYPRESS_PID_DEFAULT 0x5
#define CYPRESS_PID_BOTH_CDC CYPRESS_PID_DEFAULT
#define CYPRESS_PID_SCB0_CDC 0x7
#define CYPRESS_PID_SCB1_CDC 0x9
#define CYPRESS_PID_SCB1_NOCDC 0xA

#define DEBOUNTCE_COUNT 5

namespace Cy {

    template <class T, int size>
    using Padded = softeq::common::Padded<T, size>;

    // Simple types
    typedef uint16_t CyUsbWordField;    // 2 bytes
    typedef uint32_t CyUsbDwordField;   // 4 bytes
    typedef uint32_t CyUsbVersionField; // 4 bytes
    typedef uint8_t CyUsbByteField;     // 1 bytes

    enum class CyUsbBoolField : CyUsbByteField
    {
        FALSE,
        TRUE
    };

    enum class CyUsbSsnToggleField : CyUsbByteField
    {
        Frame,
        Continuous
    };

    enum class CyUsbLowHighField : CyUsbByteField
    {
        Low,
        High
    };

    inline CyUsbBoolField toBoolField(bool value)
    {
        return value ? CyUsbBoolField::TRUE : CyUsbBoolField::FALSE;
    }

    inline CyUsbLowHighField toLowHighField(bool value)
    {
        // true is high, false is low
        return value ? CyUsbLowHighField::High : CyUsbLowHighField::Low;
    }

    // Enums

    enum class CY_UART_TYPE : CyUsbByteField
    {
        PIN_2,
        PIN_4,
        PIN_6
        //UART_NUM_PIN_CFG,
    };

    enum class CY_SPI_TYPE : CyUsbByteField
    {
        Slave,
        Master
    };

    enum class CY_I2C_TYPE : CyUsbByteField
    {
        Slave,
        Master
    };


    enum class CY_UART_DATA_WIDTH : CyUsbByteField
    {
        BIT_7 = 7,
        BIT_8 = 8,
    };

    enum class CY_UART_STOP_BITS : CyUsbByteField
    {
        STOP_BIT_1 = 1,
        STOP_BIT_2 = 2,
    };

    enum class CY_UART_MODE : CyUsbByteField
    {
        CY_US_UART_MODE_STANDARD,
        CY_US_UART_MODE_SMART_CARD,
        CY_US_UART_MODE_IRDA,
        CY_US_UART_NUM_MODES,
    };

    enum class CY_UART_PARITY_MODE : CyUsbByteField
    {
        None,
        Odd,
        Even,
        Mark,
        Space,
        //CY_US_UART_NUM_PARITY,
    };

    enum class CY_BIT_POS : CyUsbByteField
    {
        CY_UART_LSB_OUT,
        CY_UART_MSB_OUT,
    };

    enum class UartLineActiveState : CyUsbByteField
    {
        NONE = 0,
        INVERT_RTS = 1,
        INVERT_CTS = 2,
        INVERT_DTR = 4,
        INVERT_DSR = 8,
        INVERT_DCD = 16, // 0x00000010
        INVERT_RI = 32, // 0x00000020
    };

    enum class CY_SPI_DATA_WIDTH : CyUsbByteField
    {
        DATA_WIDTH_4 = 4,
        DATA_WIDTH_5 = 5,
        DATA_WIDTH_6 = 6,
        DATA_WIDTH_7 = 7,
        DATA_WIDTH_8 = 8,
        DATA_WIDTH_9 = 9,
        DATA_WIDTH_10 = 10, // 0x0000000A
        DATA_WIDTH_11 = 11, // 0x0000000B
        DATA_WIDTH_12 = 12, // 0x0000000C
        DATA_WIDTH_13 = 13, // 0x0000000D
        DATA_WIDTH_14 = 14, // 0x0000000E
        DATA_WIDTH_15 = 15, // 0x0000000F
        DATA_WIDTH_16 = 16, // 0x00000010
    };

    enum class CY_SPI_PROTOCOL : CyUsbByteField
    {
        Motorola,
        TI,
        NS, // orogonally CY_US_SPI_MODE_NI,
        //CY_US_SPI_NUM_MODES,
    };

    enum class SPI_RXTXMODES : CyUsbByteField
    {
        CY_US_SPI_XFER_MODE_TX_RX,
        CY_US_SPI_XFER_MODE_TX_ONLY,
        CY_US_SPI_XFER_MODE_RX_ONLY,
        CY_US_SPI_NUM_XFER_MODES,
    };

    enum class CY_SCB_MODE : CyUsbByteField
    {
        UART = 1,
        SPI,
        I2C,
        //JTAG,
    };

    enum class CY_SCB_PROTOCOL : CyUsbByteField
    {
        DISABLED,
        CDC,
        PHDC,
        Vendor,
        VCP,
    };

    enum class CapsenseSensitivity : CyUsbByteField
    {
        SENSITIVITY_0_1_PF = 1,
        SENSITIVITY_0_2_PF = 2,
        SENSITIVITY_0_3_PF = 3,
        SENSITIVITY_0_4_PF = 4,
    };

    enum class CapsenseLedOutput : CyUsbByteField
    {
        NO_LED,
        COMMON_LED,
        INDIVIDUAL_LED,
    };

    enum class CY_DEVICE_POWER_MODE : CyUsbByteField
    {
        BUS_POWERED,
        SELF_POWERED,
    };

    enum RemoteWakeupSource : CyUsbByteField
    {
        WAKEUP_SOURCE_NONE = 0,
        WAKEUP_SOURCE_GPIO = 1,
        WAKEUP_SOURCE_I2C_SLAVE = 2,
        WAKEUP_SOURCE_SPI_SLAVE = 4,
        WAKEUP_SOURCE_CAPSENSE = 8,
    };

    enum UsbPolarity : CyUsbByteField
    {
        POLARITY_NONE = 0,
        POLARITY_INVERT_SUSPEND = 1,
        POLARITY_INVERT_REMOTE_WAKEUP = 2,
        POLARITY_INVERT_BOTH = 3
    };

    enum CyUsbBcdDrive : CyUsbByteField
    {
        DRIVE_BCD_NONE = 0,
        DRIVE_BCD0_HIGH = 1,
        DRIVE_BCD1_HIGH = 16, // 0x00000010
    };

    enum class GPIO : CyUsbByteField
    {
        GPIO_00,
        GPIO_01,
        GPIO_02,
        GPIO_03,
        GPIO_04,
        GPIO_05,
        GPIO_06,
        GPIO_07,
        GPIO_08,
        GPIO_09,
        GPIO_10,
        GPIO_11,
        GPIO_12,
        GPIO_13,
        GPIO_14,
        GPIO_15,
        GPIO_16,
        GPIO_17,
        GPIO_18,
        None = 0xff
    };

    enum GPIOList : CyUsbDwordField
    {
        GPIO_MASK_NONE = 0,
        GPIO_MASK_00 = 1,
        GPIO_MASK_01 = 2,
        GPIO_MASK_02 = 4,
        GPIO_MASK_03 = 8,
        GPIO_MASK_04 = 16, // 0x00000010
        GPIO_MASK_05 = 32, // 0x00000020
        GPIO_MASK_06 = 64, // 0x00000040
        GPIO_MASK_07 = 128, // 0x00000080
        GPIO_MASK_08 = 256, // 0x00000100
        GPIO_MASK_09 = 512, // 0x00000200
        GPIO_MASK_10 = 1024, // 0x00000400
        GPIO_MASK_11 = 2048, // 0x00000800
        GPIO_MASK_12 = 4096, // 0x00001000
        GPIO_MASK_13 = 8192, // 0x00002000
        GPIO_MASK_14 = 16384, // 0x00004000
        GPIO_MASK_15 = 32768, // 0x00008000
        GPIO_MASK_16 = 65536, // 0x00010000
        GPIO_MASK_17 = 131072, // 0x00020000
        GPIO_MASK_18 = 262144, // 0x00040000
    };

    enum class CyGpioLogicType : CyUsbByteField
    {
        CMOS,
        LVTTL,
    };

    enum class CyGpioDriveType : CyUsbByteField
    {
        Fast,
        Slow,
    };
    enum class CyGpioState : CyUsbByteField
    {
        Tristate,
        Drive_0,
        Drive_1,
        Input
    };

// Structs

// CyUsbScbDisableConfig, CyUsbUartConfig, CyUsbSpiConfig, CyUsbI2cConfig, CyUsbJtagConfigs should be 16-byte padded
struct CyUsbScbDisableConfig
{
    // empty
};

struct CyUsbUartConfig
{
    //    this.AddNewField((CyUsbField) new CyUsbDwordField("Baud Rate"));
    CyUsbDwordField baudRate;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_UART_TYPE>("UART Type"));
    CY_UART_TYPE uartType;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_UART_DATA_WIDTH>(nameof (DataWidth)));
    CY_UART_DATA_WIDTH dataWidth;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_UART_STOP_BITS>("Stop Bits"));
    CY_UART_STOP_BITS stopBits;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_UART_MODE>("UART Protocol"));
    CY_UART_MODE uartProtocol;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_UART_PARITY_MODE>(nameof (Parity)));
    CY_UART_PARITY_MODE parity;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_BIT_POS>("Bit Position"));
    CY_BIT_POS bitPosition;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("Tx Retry on NACK"));
    CyUsbBoolField txRetryOnNack;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("Rx Invert Polarity"));
    CyUsbBoolField rxInvertPolarity;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("Drop on Errors"));
    CyUsbBoolField dropOnErrors;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("Flow Control"));
    CyUsbBoolField flowControl;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField(nameof (Loopback)));
    CyUsbBoolField loopback;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<UartLineActiveState>("Line Active State"));
    UartLineActiveState lineActiveState;
};

struct CyUsbSpiConfig
{
    //    this.AddNewField((CyUsbField) new CyUsbDwordField(nameof (Frequency)));
    CyUsbDwordField frequency;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_SPI_DATA_WIDTH>("Data Width"));
    CY_SPI_DATA_WIDTH dataWidth;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_SPI_PROTOCOL>(nameof (Protocol)));
    CY_SPI_PROTOCOL protocol;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<SPI_RXTXMODES>("SPI Transfer Mode"));
    SPI_RXTXMODES spiTransferMode;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_BIT_POS>("Bit Position"));
    CY_BIT_POS bitPosition;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("SPI Master"));
    CY_SPI_TYPE spiMaster;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("Continuous Mode"));
    CyUsbSsnToggleField ssnToggleMode;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("Select Precede"));
    CyUsbBoolField selectPrecede;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField(nameof (CPHA)));
    CyUsbLowHighField cpha;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField(nameof (CPOL)));
    CyUsbLowHighField cpol;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField(nameof (Loopback)));
    CyUsbBoolField loopback;
};

struct CyUsbI2cConfig
{
    //    this.AddNewField((CyUsbField) new CyUsbDwordField(nameof (Frequency)));
    CyUsbDwordField frequncy;
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Slave Address"));
    CyUsbByteField slaveAddress;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("Bit Position"));
    CyUsbBoolField bitPosition;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("I2C Master"));
    CyUsbBoolField i2cMaster;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField(nameof (SIGNORE)));
    CyUsbBoolField signore;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("Clock Stretch"));
    CyUsbBoolField clockStretch;
    //    this.AddNewField((CyUsbField) new CyUsbBoolField(nameof (ISLOOPBACK)));
    CyUsbBoolField isLoopback;
};

struct CyUsbScbActivityConfig
{
    //      this.AddNewField((CyUsbField) new CyUsbByteField("Tx LED"));
    GPIO txLed;
    //      this.AddNewField((CyUsbField) new CyUsbByteField("Rx LED"));
    GPIO rxLed;
    //      this.AddNewField((CyUsbField) new CyUsbByteField("Rx and Tx LED"));
    GPIO rxAndTxLed;
};

struct CyUsbJtagConfig // probably unused
{
    //    this.AddNewField((CyUsbField) new CyUsbDwordField(nameof (Frequency)));
    CyUsbDwordField frequency;
};

struct CyUsbScbConfig
{
    //      this.AddNewField((CyUsbField) new CyUsbEnumField<CY_SCB_MODE>(this.MODE));
    CY_SCB_MODE mode;
    //      this.AddNewField((CyUsbField) new CyUsbEnumField<CY_SCB_PROTOCOL>(this.PROTOCOL));
    CY_SCB_PROTOCOL protocol;
    //      this.AddNewField((CyUsbField) new CyUsbWordField(this.DUMMY_DATA_ALIGN_BYTES));
    CyUsbWordField dummyDataAlignBytes;
    //      this.AddNewField((CyUsbField) new CyUsbScbActivityConfig(this.LED_ACTIVITY));
    Padded<CyUsbScbActivityConfig, 4> ledActivity;
    //      this.AddNewField(this.MODE_CONFIGURATION, (CyUsbField) modeConfig, true);
    union ModeConfiguration
    {
        CyUsbScbDisableConfig disable;
        CyUsbUartConfig uart;
        CyUsbSpiConfig spi;
        CyUsbI2cConfig i2c;
        CyUsbJtagConfig jtag;
    };
    Padded<ModeConfiguration, 16> modeConfiguration;
};

struct CyCapsenseButton
{
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CapsenseSensitivity>(nameof (Sensitivity)));
    CapsenseSensitivity sensitivity;
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Sense GPIO"));
    GPIO senseGpio;
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Output LED GPIO"));
    GPIO outputLedGpio;
};

struct CyUsbCapsenseConfig
{
    //    this.AddNewField((CyUsbField) new CyUsbBoolField(nameof (Enable)));
    CyUsbBoolField enable;
    //    this.AddNewField((CyUsbField) new CyUsbByteField(nameof (Debounce)));
    CyUsbByteField debounce;
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Scan Period"));
    CyUsbByteField scanPeriod;
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Water Shield IO"));
    GPIO waterShieldIO;
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Button Count"));
    CyUsbByteField buttonCount;
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Encoded GPIO 0"));
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Encoded GPIO 1"));
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Encoded GPIO 2"));
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Encoded GPIO 3"));
    GPIO encodedGpio[4];
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CapsenseLedOutput>("LED Output"));
    CapsenseLedOutput ledOutput;
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Common LED IO"));
    GPIO commonLedIo;
    //    this.m_buttons = new List<CyCapsenseButton>();
    //    for (int index = 0; index < 8; ++index)
    //    {
    //      CyCapsenseButton cyCapsenseButton = new CyCapsenseButton(string.Format("BUTTON_{0}", (object) index), (byte)
    //      index); this.AddNewField((CyUsbField) cyCapsenseButton); this.m_buttons.Add(cyCapsenseButton);
    //    }
    Padded<CyCapsenseButton, 4> buttons[8];
};

struct CyUsbBcdConfig
{
    //    this.AddNewField((CyUsbField) new CyUsbByteField("BCD 0"));
    //    this.AddNewField((CyUsbField) new CyUsbByteField("BCD 1"));
    GPIO bcd[2];
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Bus Detect"));
    GPIO busDetect;
};

struct CyUsbLangIdDescField
{
    //    this.AddNewField((CyUsbField) new CyUsbByteField(nameof (Length)));
    CyUsbByteField length;
    //    this.AddNewField((CyUsbField) new CyUsbByteField(nameof (Index)));
    CyUsbByteField index;
    //    this.AddNewField((CyUsbField) new CyUsbWordField("Lang ID"));
    CyUsbWordField langId;
};

template <int maxLength>
struct CyUsbUnicodeStrDescField
{
    //    this.AddNewField((CyUsbField) new CyUsbByteField(nameof (Length)));
    CyUsbByteField length;
    //    this.AddNewField((CyUsbField) new CyUsbByteField(nameof (Index)));
    CyUsbByteField index;
    //    this.AddNewField((CyUsbField) new CyUsbUnicodeStrField("String", sizeBytes - 2U, varLength));
    char16_t string[maxLength];
};

struct CyUsbConfig
{
    //    this.AddNewField((CyUsbField) new CyUsbByteField("Suspend"));
    CyUsbByteField suspend;
    //    this.AddNewField((CyUsbField) new CyUsbWordField("Vendor ID"));
    CyUsbWordField vendorId;
    //    this.AddNewField((CyUsbField) new CyUsbWordField("Product ID"));
    CyUsbWordField productId;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CY_DEVICE_POWER_MODE>("Power Mode"));
    CY_DEVICE_POWER_MODE powerMode;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<RemoteWakeupSource>("Remote Wakeup"));
    CyUsbByteField remoteWakeup; // RemoteWakeupSource
                                 //    this.AddNewField((CyUsbField) new CyUsbByteField("Current Draw"));
    CyUsbByteField currentDraw;
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<UsbPolarity>("Line Polarity"));
    CyUsbByteField linePolarity; // UsbPolarity bitmask
    //    this.AddNewField((CyUsbField) new CyUsbDwordField("String Descriptor Address 0"));
    //    this.AddNewField((CyUsbField) new CyUsbDwordField("String Descriptor Address 1"));
    //    this.AddNewField((CyUsbField) new CyUsbDwordField("String Descriptor Address 2"));
    //    this.AddNewField((CyUsbField) new CyUsbDwordField("String Descriptor Address 3"));
    CyUsbDwordField stringDescriptorAddress[4];
    //    this.AddNewField((CyUsbField) new CyUsbLangIdDescField("String Descriptor 0", 66U));
    Padded<CyUsbLangIdDescField, 66> stringDescriptor0; // 66 bytes, mostly 0s
    //    this.AddNewField((CyUsbField) new CyUsbUnicodeStrDescField("String Descriptor 1", (byte) 3, 66U, false));
    //    this.AddNewField((CyUsbField) new CyUsbUnicodeStrDescField("String Descriptor 2", (byte) 3, 66U, false));
    //    this.AddNewField((CyUsbField) new CyUsbUnicodeStrDescField("String Descriptor 3", (byte) 3, 66U, false));
    CyUsbUnicodeStrDescField<32> stringDescriptor[3]; // not 66, because 2 bytes go to length and index fields
};

struct CyUsbBcdDriveConfig
{
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CyUsbBcdDrive>("Unconfigured SDP"));
    CyUsbByteField unconfiguredSdp; // CyUsbBcdDrive enum mask
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CyUsbBcdDrive>("Configured SDP"));
    CyUsbByteField configuredSdp; // CyUsbBcdDrive enum mask
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CyUsbBcdDrive>("DCP Charging"));
    CyUsbByteField dcpCharging; // CyUsbBcdDrive enum mask
    //    this.AddNewField((CyUsbField) new CyUsbEnumField<CyUsbBcdDrive>("Suspend or Fail"));
    CyUsbByteField suspendOrFail; // CyUsbBcdDrive enum mask
};

struct CyUsbUartRS485Config
{
    //    this.AddNewField((CyUsbField) new CyUsbBoolField("RS485 Enable"));
    CyUsbBoolField rs485enable;
    //    this.AddNewField((CyUsbField) new CyUsbByteField("RS485 GPIO"));
    GPIO rs485gpio;
};

struct CyUsbscDeviceConfig
{
    //      this.AddNewField((CyUsbField) new CyUsbAsciiStrField(nameof (Signature), "CYUS", 4U, false));
    char signature[4]; // "CYUS"
                       //      this.AddNewField((CyUsbField) new CyUsbVersionField("Firmware Version"));
    CyUsbVersionField firmwareVersion;
    //      this.AddNewField(( CyUsbField) new CyUsbDwordField(nameof (Checksum)));
    CyUsbDwordField checksum;
    //      this.AddNewField((CyUsbField) new CyUsbDwordField("Firmware Entry"));
    CyUsbDwordField firmwareEntry;
    //      this.AddNewField((CyUsbField) new CyUsbDwordField("Firmware Image Start"));
    CyUsbDwordField firmwareImageStart;
    //      this.AddNewField((CyUsbField) new CyUsbDwordField("Firmware Image Size"));
    CyUsbDwordField firmwareImageSize;
    //      this.AddNewField((CyUsbField) new CyUsbDwordField("Firmware Image Checksum"));
    CyUsbDwordField firmwareImageChecksum;
    //      this.AddNewField((CyUsbField) new CyUsbScbConfig(string.Format("SCB {0}", (object) 0U), 0U, 24U));
    //      this.AddNewField((CyUsbField) new CyUsbScbConfig(string.Format("SCB {0}", (object) 1U), 1U, 24U));
    Padded<CyUsbScbConfig, 24> scb[2];
    //      this.AddNewField((CyUsbField) new CyUsbCapsenseConfig("CapSense Configuration", 64U));
    Padded<CyUsbCapsenseConfig, 64> capSenseConfiguration;
    //      this.AddNewField((CyUsbField) new CyUsbBcdConfig(this.BCD_CONFIG));
    Padded<CyUsbBcdConfig, 4> bcdConfig;
    //      this.AddNewField((CyUsbField) new CyUsbBoolField("Bypass USB Regulator"));
    CyUsbBoolField bypassUSBRegulator;
    //      this.AddNewField((CyUsbField) new CyUsbBoolField("Bypass System Regulator"));
    CyUsbBoolField bypassSystemRegulator;
    //      this.AddNewField((CyUsbField) new CyUsbBoolField("Mfg Interface"));
    CyUsbBoolField mfgInterface;
    //      this.AddNewField((CyUsbField) new CyUsbConfig("Device USB Configuration"));
    Padded<CyUsbConfig, 289> usbConfig;
    //      this.AddNewField((CyUsbField) new CyUsbDWordEnumField<GPIOList>("Drive 0"));
    CyUsbDwordField drive0; // GPIOList mask
                            //      this.AddNewField((CyUsbField) new CyUsbDWordEnumField<GPIOList>("Drive 1"));
    CyUsbDwordField drive1; // GPIOList mask
                            //      this.AddNewField((CyUsbField) new CyUsbDWordEnumField<GPIOList>("Input"));
    CyUsbDwordField input;  // GPIOList mask
                            //      this.AddNewField((CyUsbField) new CyUsbBcdDriveConfig("BCD Drive Config"));
    CyUsbBcdDriveConfig bcdDriveConfig;
    //      this.AddNewField((CyUsbField) new CyUsbBoolField("SCB SCB Config"));
    CyUsbBoolField scbScbConfig;
    //      this.AddNewField((CyUsbField) new CyUsbBoolField("Power GPIO Enable"));
    CyUsbBoolField powerGpioEnable;
    //      this.AddNewField((CyUsbField) new CyUsbByteField("Power GPIO"));
    GPIO powerGpio;
    //      this.AddNewField((CyUsbField) new CyUsbEnumField<CyGpioLogicType>("GPIO Logic Type"));
    CyGpioLogicType gpioLogicType;
    //      this.AddNewField((CyUsbField) new CyUsbEnumField<CyGpioDriveType>("GPIO Drive Type"));
    CyGpioDriveType gpioDriveType;
    //      this.AddNewField((CyUsbField) new CyUsbByteField("Reserved UART Pullup Disable"));
    CyUsbByteField reservedUartPullupDisable;
    //      this.AddNewField((CyUsbField) new CyUsbBoolField("SCB0 Suspend UART Control"));
    //      this.AddNewField((CyUsbField) new CyUsbBoolField("SCB1 Suspend UART Control"));
    CyUsbBoolField scbSuspendUartControl[2];
    //      this.AddNewField((CyUsbField) new CyUsbUartRS485Config(string.Format("SCB {0} UART RS485", (object) 0U),
    //      0U)); this.AddNewField((CyUsbField) new CyUsbUartRS485Config(string.Format("SCB {0} UART RS485", (object)
    //      1U), 1U));
    CyUsbUartRS485Config scbUartRs485[2];
};

struct UsbDeviceConfig : public CyUsbscDeviceConfig
{
    CyUsbDwordField getChecksum() const;
    CyUsbWordField getDefaultPid() const;
};
} // namespace Cy

#pragma pack(pop)

#endif // CYUSB_H
