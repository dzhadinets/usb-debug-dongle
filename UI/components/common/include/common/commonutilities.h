#ifndef COMMONUTILITIES_H
#define COMMONUTILITIES_H

#include <QtCore>
#include <QComboBox>

template <class Enum>
void setCurrentItem(QComboBox *comboBox, Enum item)
{
    int data = comboBox->findData(static_cast<int>(item));
    if (data != -1)
    {
        comboBox->setCurrentIndex(data);
    }
    else
    {
        qDebug("Cound not find data %d in a combo box", static_cast<int>(item));
    }
}

template <class Enum>
uint fillComboBoxWithEnum( QComboBox *comboBox, Enum selectedItem, std::map<Enum, std::string>mapParams, std::function<bool (Enum)> filter = [](Enum) { return true; })
{
    const QSignalBlocker blocker(comboBox);
    uint itemsAdded = 0;
    comboBox->clear();
    for(const auto &[key, value] : mapParams)
    {
        if (filter(key))
        {
            comboBox->addItem(QString::fromStdString(value), static_cast<int>(key));
            ++itemsAdded;
        }
    }
    setCurrentItem(comboBox, selectedItem);
    return itemsAdded;
}

template <class UIntType1, class UIntType2>
void setBit(UIntType1 &value, UIntType2 bit, bool set)
{
    if (set)
    {
        value |= bit;
    }
    else
    {
        value &= ~bit;
    }
}

#endif // COMMONUTILITIES_H
