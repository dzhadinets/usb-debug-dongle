#ifndef ENUMSREGISTRATION_H
#define ENUMSREGISTRATION_H
#include <QObject>
#include "common/CyUSB.h"


namespace Cy {
const std::map<CyUsbBoolField, std::string> usbBoolField = {
    {CyUsbBoolField::FALSE, "FALSE"},
    {CyUsbBoolField::TRUE, "TRUE"}
};

const std::map<CyUsbSsnToggleField, std::string> usbSsnToggleField = {
    {CyUsbSsnToggleField::Frame, "Frame"},
    {CyUsbSsnToggleField::Continuous, "Continuous"}
};

const std::map<CyUsbLowHighField, std::string> usbLowHighField = {
    {CyUsbLowHighField::High, "High"},
    {CyUsbLowHighField::Low, "Low"}
};

const std::map<CY_UART_TYPE, std::string> uartType = {
    {CY_UART_TYPE::PIN_2, "PIN_2"},
    {CY_UART_TYPE::PIN_4, "PIN_4"},
    {CY_UART_TYPE::PIN_6, "PIN_6"},
};

const std::map<CY_SPI_TYPE, std::string> spiType = {
    {CY_SPI_TYPE::Slave, "Slave"},
    {CY_SPI_TYPE::Master, "Master"}
};

const std::map<CY_UART_DATA_WIDTH, std::string> uartDataWidth = {
    {CY_UART_DATA_WIDTH::BIT_7, "BIT_7"},
    {CY_UART_DATA_WIDTH::BIT_8, "BIT_8"}
};

const std::map<CY_UART_STOP_BITS, std::string> uartStopBits = {
    {CY_UART_STOP_BITS::STOP_BIT_1, "STOP_BIT_1"},
    {CY_UART_STOP_BITS::STOP_BIT_2, "STOP_BIT_2"}
};

const std::map<CY_UART_MODE, std::string> uartMode = {
    {CY_UART_MODE::CY_US_UART_MODE_IRDA, "CY_US_UART_MODE_IRDA"},
    {CY_UART_MODE::CY_US_UART_MODE_SMART_CARD, "CY_US_UART_MODE_SMART_CARD"},
    {CY_UART_MODE::CY_US_UART_MODE_STANDARD, "CY_US_UART_MODE_STANDARD"},
    {CY_UART_MODE::CY_US_UART_NUM_MODES, "CY_US_UART_NUM_MODES"}
};

const std::map<CY_UART_PARITY_MODE, std::string> uartParityMode = {
    {CY_UART_PARITY_MODE::Even, "Even"},
    {CY_UART_PARITY_MODE::Mark, "Mark"},
    {CY_UART_PARITY_MODE::None, "None"},
    {CY_UART_PARITY_MODE::Odd, "Odd"},
    {CY_UART_PARITY_MODE::Space, "Space"}
};

const std::map<CY_BIT_POS, std::string> bitPos = {
    {CY_BIT_POS::CY_UART_LSB_OUT, "CY_UART_LSB_OUT"},
    {CY_BIT_POS::CY_UART_MSB_OUT, "CY_UART_MSB_OUT"}
};

const std::map<UartLineActiveState, std::string> uartLineActiveState = {
    {UartLineActiveState::INVERT_CTS, "INVERT_CTS"},
    {UartLineActiveState::INVERT_DCD, "INVERT_DCD"},
    {UartLineActiveState::INVERT_DSR, "INVERT_DSR"},
    {UartLineActiveState::INVERT_DTR, "INVERT_DTR"},
    {UartLineActiveState::INVERT_RI, "INVERT_RI"},
    {UartLineActiveState::INVERT_RTS, "INVERT_RTS"},
    {UartLineActiveState::NONE, "NONE"},
};

const std::map<CY_SPI_DATA_WIDTH, std::string> spiDataWidth = {
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_4, "DATA_WIDTH_4"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_5, "DATA_WIDTH_5"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_6, "DATA_WIDTH_6"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_7, "DATA_WIDTH_7"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_8, "DATA_WIDTH_8"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_9, "DATA_WIDTH_9"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_10, "DATA_WIDTH_10"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_11, "DATA_WIDTH_11"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_12, "DATA_WIDTH_12"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_13, "DATA_WIDTH_13"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_14, "DATA_WIDTH_14"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_15, "DATA_WIDTH_15"},
    {CY_SPI_DATA_WIDTH::DATA_WIDTH_16, "DATA_WIDTH_16"}
};

const std::map<CY_SPI_PROTOCOL, std::string> spiProtocol = {
   {CY_SPI_PROTOCOL::Motorola, "Motorola"},
   {CY_SPI_PROTOCOL::NS, "NS"},
   {CY_SPI_PROTOCOL::TI, "TI"}
};

const std::map<SPI_RXTXMODES, std::string> spiRxtxModels = {
    {SPI_RXTXMODES::CY_US_SPI_NUM_XFER_MODES, "CY_US_SPI_NUM_XFER_MODES"},
    {SPI_RXTXMODES::CY_US_SPI_XFER_MODE_RX_ONLY, "CY_US_SPI_XFER_MODE_RX_ONLY"},
    {SPI_RXTXMODES::CY_US_SPI_XFER_MODE_TX_ONLY, "CY_US_SPI_XFER_MODE_TX_ONLY"},
    {SPI_RXTXMODES::CY_US_SPI_XFER_MODE_TX_RX, "CY_US_SPI_XFER_MODE_TX_RX"}
};

const std::map<CY_SCB_MODE, std::string> scbMode = {
    {CY_SCB_MODE::UART, "UART"},
    {CY_SCB_MODE::SPI, "SPI"},
    {CY_SCB_MODE::I2C, "I2C"}


};

const std::map<CY_SCB_PROTOCOL, std::string> scbProtocol = {
    {CY_SCB_PROTOCOL::CDC, "CDC"},
    {CY_SCB_PROTOCOL::DISABLED, "DISABLED"},
    {CY_SCB_PROTOCOL::PHDC, "PHDC"},
    {CY_SCB_PROTOCOL::VCP, "VCP"},
    {CY_SCB_PROTOCOL::Vendor, "Vendor"}
};

const std::map<CapsenseSensitivity, std::string> capsenseSenstvt = {
    {CapsenseSensitivity::SENSITIVITY_0_1_PF, "SENSITIVITY_0_1_PF"},
    {CapsenseSensitivity::SENSITIVITY_0_2_PF, "SENSITIVITY_0_2_PF"},
    {CapsenseSensitivity::SENSITIVITY_0_3_PF, "SENSITIVITY_0_3_PF"},
    {CapsenseSensitivity::SENSITIVITY_0_4_PF, "SENSITIVITY_0_4_PF"}
};

const std::map<CapsenseLedOutput, std::string> capsenseLedOutput = {
    {CapsenseLedOutput::COMMON_LED, "COMMON_LED"},
    {CapsenseLedOutput::INDIVIDUAL_LED, "INDIVIDUAL_LED"},
    {CapsenseLedOutput::NO_LED, "NO_LED"}
};

const std::map<CY_DEVICE_POWER_MODE, std::string> devicePowerMode = {
    {CY_DEVICE_POWER_MODE::BUS_POWERED, "BUS_POWERED"},
    {CY_DEVICE_POWER_MODE::SELF_POWERED, "SELF_POWERED"}
};

const std::map<RemoteWakeupSource , std::string> remoteWakeUpSource = {
    {RemoteWakeupSource::WAKEUP_SOURCE_CAPSENSE, "WAKEUP_SOURCE_CAPSENSE"},
    {RemoteWakeupSource::WAKEUP_SOURCE_GPIO, "WAKEUP_SOURCE_GPIO"},
    {RemoteWakeupSource::WAKEUP_SOURCE_I2C_SLAVE, "WAKEUP_SOURCE_I2C_SLAVE"},
    {RemoteWakeupSource::WAKEUP_SOURCE_NONE, "WAKEUP_SOURCE_NONE"},
    {RemoteWakeupSource::WAKEUP_SOURCE_SPI_SLAVE, "WAKEUP_SOURCE_SPI_SLAVE"}
};

const std::map<UsbPolarity , std::string> usbPolarity = {
    {UsbPolarity::POLARITY_INVERT_BOTH, "POLARITY_INVERT_BOTH"},
    {UsbPolarity::POLARITY_INVERT_REMOTE_WAKEUP, "POLARITY_INVERT_REMOTE_WAKEUP"},
    {UsbPolarity::POLARITY_INVERT_SUSPEND, "POLARITY_INVERT_SUSPEND"},
    {UsbPolarity::POLARITY_NONE, "POLARITY_NONE"}
};

const std::map<CyUsbBcdDrive , std::string> usbBcdDrive = {
    {CyUsbBcdDrive::DRIVE_BCD0_HIGH, "DRIVE_BCD0_HIGH"},
    {CyUsbBcdDrive::DRIVE_BCD1_HIGH, "DRIVE_BCD1_HIGH"},
    {CyUsbBcdDrive::DRIVE_BCD_NONE, "DRIVE_BCD_NONE"}
};

const std::map<GPIO , std::string> gpio = {
    {GPIO::GPIO_00, "GPIO_00"},
    {GPIO::GPIO_01, "GPIO_01"},
    {GPIO::GPIO_02, "GPIO_02"},
    {GPIO::GPIO_03, "GPIO_03"},
    {GPIO::GPIO_04, "GPIO_04"},
    {GPIO::GPIO_05, "GPIO_05"},
    {GPIO::GPIO_06, "GPIO_06"},
    {GPIO::GPIO_07, "GPIO_07"},
    {GPIO::GPIO_08, "GPIO_08"},
    {GPIO::GPIO_09, "GPIO_09"},
    {GPIO::GPIO_10, "GPIO_10"},
    {GPIO::GPIO_11, "GPIO_11"},
    {GPIO::GPIO_12, "GPIO_12"},
    {GPIO::GPIO_13, "GPIO_13"},
    {GPIO::GPIO_14, "GPIO_14"},
    {GPIO::GPIO_15, "GPIO_15"},
    {GPIO::GPIO_16, "GPIO_16"},
    {GPIO::GPIO_17, "GPIO_17"},
    {GPIO::GPIO_18, "GPIO_18"},
    {GPIO::None, "None"}
};

const std::map<GPIOList , std::string> gpioList = {
    {GPIOList::GPIO_MASK_00, "GPIO_MASK_00"},
    {GPIOList::GPIO_MASK_01, "GPIO_MASK_01"},
    {GPIOList::GPIO_MASK_02, "GPIO_MASK_02"},
    {GPIOList::GPIO_MASK_03, "GPIO_MASK_03"},
    {GPIOList::GPIO_MASK_04, "GPIO_MASK_04"},
    {GPIOList::GPIO_MASK_05, "GPIO_MASK_05"},
    {GPIOList::GPIO_MASK_06, "GPIO_MASK_06"},
    {GPIOList::GPIO_MASK_07, "GPIO_MASK_07"},
    {GPIOList::GPIO_MASK_08, "GPIO_MASK_08"},
    {GPIOList::GPIO_MASK_09, "GPIO_MASK_09"},
    {GPIOList::GPIO_MASK_10, "GPIO_MASK_10"},
    {GPIOList::GPIO_MASK_11, "GPIO_MASK_11"},
    {GPIOList::GPIO_MASK_12, "GPIO_MASK_12"},
    {GPIOList::GPIO_MASK_13, "GPIO_MASK_13"},
    {GPIOList::GPIO_MASK_14, "GPIO_MASK_14"},
    {GPIOList::GPIO_MASK_15, "GPIO_MASK_15"},
    {GPIOList::GPIO_MASK_16, "GPIO_MASK_16"},
    {GPIOList::GPIO_MASK_17, "GPIO_MASK_17"},
    {GPIOList::GPIO_MASK_18, "GPIO_MASK_18"},
    {GPIOList::GPIO_MASK_NONE, "GPIO_MASK_NONE"}
};

const std::map<CyGpioLogicType , std::string> gpioLogicType = {
    {CyGpioLogicType::CMOS, "CMOS"},
    {CyGpioLogicType::LVTTL, "LVTTL"}
};

const std::map<CyGpioDriveType , std::string> gpioDriveType = {
    {CyGpioDriveType::Fast, "Fast"},
    {CyGpioDriveType::Slow, "Slow"}
};
const std::map<CyGpioState , std::string> gpioState = {
    {CyGpioState::Tristate, "Tristate"},
    {CyGpioState::Drive_0, "Drive_0"},
    {CyGpioState::Drive_1, "Drive_1"},
    {CyGpioState::Input, "Input"}
};

};

#endif // ENUMSREGISTRATION_H
