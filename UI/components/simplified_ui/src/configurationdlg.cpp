#include "simplified_ui/configurationdlg.h"
#include "ui_configurationdlg.h"
#include "simplified_ui/scbwidget.h"
#include "ui_scbwidget.h"

#include <QBoxLayout>
#include <common/CyUSB.h>
#include <common/enumsregistration.h>
#include <common/commonutilities.h>
#include <common/gpiostate.h>
#include <QPushButton>

using namespace Cy;
using namespace ScbNum;

ConfigurationDlg::ConfigurationDlg(const AppStateSimplified &state, QWidget *parent)
    : QDialog(parent),
      _state(state),
      ui(new Ui::ConfigurationDlg)
{
    ui->setupUi(this);
    setFixedSize(size());
    SCBWidget* scb0 = new SCBWidget();
    SCBWidget *scb1 = new SCBWidget();
    scb0->getUI()->groupBox->setTitle("SCB0");
    scb1->getUI()->groupBox->setTitle("SCB1");
    _widgetScb.append(scb0);
    _widgetScb.append(scb1);
    layoutWidget();
    for(int i = 0; i < _widgetScb.size(); ++i)
    {
        _scbNum = static_cast<ScbNum>(i);
        updateConfigurationWithData();
        connect(_widgetScb[i], &SCBWidget::modeChanged, this, &ConfigurationDlg::onModeChanged);
        connect(_widgetScb[i], &SCBWidget::typeChanged, this, &ConfigurationDlg::onTypeChanged);
        connect(_widgetScb[i], &SCBWidget::spiMasterChanged, this, &ConfigurationDlg::onSpiMasterChanged);
        connect(_widgetScb[i], &SCBWidget::i2cMasterChanged, this, &ConfigurationDlg::oni2CMasterChanged);
        connect(_widgetScb[i]->getUI()->pbDefaultConfiguration, &QPushButton::clicked, this, &ConfigurationDlg::onDefaultConfiguration);

    }

}
ConfigurationDlg::~ConfigurationDlg()
{
    delete ui;
}

const AppStateSimplified &ConfigurationDlg::dataReceived()
{
    return _state;
}

void ConfigurationDlg::layoutWidget()
{
    if(!_widgetScb.isEmpty())
    {
        QVBoxLayout *layout = new QVBoxLayout(this);
        for(int i = 0; i < _widgetScb.size(); ++i)
        {
            layout->addWidget(_widgetScb[i]);
        }
        QHBoxLayout *horizontal = new QHBoxLayout(this);
        QPushButton *pbOk = new QPushButton("Ok");
        QPushButton *pbCancel = new QPushButton("Cancel");
        horizontal->addStretch();
        horizontal->addWidget(pbOk);
        horizontal->addWidget(pbCancel);
        layout->addLayout(horizontal);
        connect(pbOk, &QPushButton::clicked, this, &ConfigurationDlg::onClickedOk);
        connect(pbCancel, &QPushButton::clicked, this, &ConfigurationDlg::reject);
    }
    else
    {
        qDebug() << "Error! No widget for view. Add widgets!";
    }
}

void ConfigurationDlg::updateConfigurationWithData()
{
    Cy::UsbDeviceConfig config = _state.config;
    const auto mode = config.scb[_scbNum]->mode;
    const auto SCB_UI = _widgetScb[_scbNum]->getUI();
    fillComboBoxWithEnum(SCB_UI->cbSCBMode, mode, scbMode);
    switch (mode)
    {
    case CY_SCB_MODE::UART:
        SCB_UI->lbProtocol ->setText(QString::fromStdString(scbProtocol.at(CY_SCB_PROTOCOL::CDC)));
        SCB_UI->stackedWidgetSCB->setCurrentWidget(SCB_UI->pageSCBUart);
        updateDateScbUart();
        break;
    case CY_SCB_MODE::SPI:
        SCB_UI->lbProtocol->setText(QString::fromStdString(scbProtocol.at(CY_SCB_PROTOCOL::Vendor)));
        SCB_UI->stackedWidgetSCB->setCurrentWidget(SCB_UI->pageSCBSpi);
        updateDateScbSpi();
        break;
    case CY_SCB_MODE::I2C:
        SCB_UI->lbProtocol->setText(QString::fromStdString(scbProtocol.at(CY_SCB_PROTOCOL::Vendor)));
        SCB_UI->stackedWidgetSCB->setCurrentWidget(SCB_UI->pageSCBI2C);
        updateDateScbI2c();
        break;
    }
}

void ConfigurationDlg::updateDateScbUart()
{
    const auto &config = _state.config;
    const auto &gpioState = _state.gpioState;
    const CyUsbUartConfig &uartConfig = config.scb[_scbNum]->modeConfiguration->uart;
    const auto SCB_UI = _widgetScb[_scbNum]->getUI();
    constexpr uint supportedBaudRates[] = {100,   300,   600,   1200,   2400,   4800,   9600,   14400,   19200,
                                                  38400, 56000, 57600, 115200, 230400, 460800, 921600, 1000000, 3000000};
    SCB_UI->cbUartBaudRate->clear();
    for (auto baudRate : supportedBaudRates)
    {
        SCB_UI->cbUartBaudRate->addItem(QString::number(baudRate), baudRate);
    }
    setCurrentItem(SCB_UI->cbUartBaudRate, uartConfig.baudRate);
    fillComboBoxWithEnum<CY_UART_TYPE>(SCB_UI->cbUartType, uartConfig.uartType , uartType);
    fillComboBoxWithEnum<CY_UART_DATA_WIDTH>(SCB_UI->cbUartDataWidth, uartConfig.dataWidth, uartDataWidth);
    fillComboBoxWithEnum<CY_UART_STOP_BITS>(SCB_UI->cbUartStopBits, uartConfig.stopBits, uartStopBits);
    fillComboBoxWithEnum<CY_UART_PARITY_MODE>(SCB_UI->cbUartParity, uartConfig.parity, uartParityMode);
}

void ConfigurationDlg::updateDateScbSpi()
{
    const auto &config = _state.config;
    const CyUsbSpiConfig &spiConfig = config.scb[_scbNum]->modeConfiguration->spi;
    const auto SCB_UI = _widgetScb[_scbNum]->getUI();
    constexpr uint supportedSPIFrequency[] = {10000};
    SCB_UI->sbSpiFrequency->clear();
    for (auto frequency : supportedSPIFrequency)
    {
        SCB_UI->sbSpiFrequency->addItem(QString::number(frequency), frequency);
    }
    setCurrentItem(SCB_UI->sbSpiFrequency, spiConfig.frequency);
    fillComboBoxWithEnum<CY_SPI_DATA_WIDTH>(SCB_UI->cbSpiDataWidth, spiConfig.dataWidth, spiDataWidth);
    fillComboBoxWithEnum<CY_SPI_TYPE>(SCB_UI->cbSpiMaster, spiConfig.spiMaster, spiType);
    fillComboBoxWithEnum<CY_SPI_PROTOCOL>(SCB_UI->cbSpiProtocol, spiConfig.protocol, spiProtocol);
    fillComboBoxWithEnum<CyUsbLowHighField>(SCB_UI->cbSpiCphaMode, spiConfig.cpha, usbLowHighField);
    fillComboBoxWithEnum<CyUsbLowHighField>(SCB_UI->cbSpiCpolMode, spiConfig.cpol, usbLowHighField);
    fillComboBoxWithEnum<CyUsbSsnToggleField>(SCB_UI->cbSpiSsnToggleMode, spiConfig.ssnToggleMode, usbSsnToggleField);
    fillComboBoxWithEnum<CY_BIT_POS>(SCB_UI->cbSpiBitOrder, spiConfig.bitPosition, bitPos);
    SCB_UI->chBoxuseAsWakeupSource->setEnabled(spiConfig.spiMaster == CY_SPI_TYPE::Slave);
    spiConfigDlg_setControlsEnabled(spiConfig.protocol);
    SCB_UI->chBoxuseAsWakeupSource->setChecked(_state.config.usbConfig->remoteWakeup & WAKEUP_SOURCE_SPI_SLAVE);
    SCB_UI->chBoxenableSelectPrecede->setChecked(spiConfig.selectPrecede == CyUsbBoolField::TRUE);
}

void ConfigurationDlg::updateDateScbI2c()
{
    const auto &config = _state.config;
    const CyUsbI2cConfig &i2cConfig = config.scb[_scbNum]->modeConfiguration->i2c;
    const auto SCB_UI = _widgetScb[_scbNum]->getUI();
    constexpr uint supportedFrequencies[] = {1000, 10000, 100000, 200000, 400000};
    SCB_UI->cbI2CFrequency->clear();
    for (auto item : supportedFrequencies)
    {
        SCB_UI->cbI2CFrequency->addItem(QString::number(item));
    }
    SCB_UI->cbI2CFrequency->setCurrentText(QString::number(i2cConfig.frequncy));
    setCurrentItem(SCB_UI->cbI2CMaster, i2cConfig.bitPosition);
    fillComboBoxWithEnum<CyUsbBoolField>(SCB_UI->cbI2CMaster, i2cConfig.i2cMaster, usbBoolField);
    SCB_UI->chBoxI2CUseAsWakeupSrc->setChecked(config.usbConfig->remoteWakeup & WAKEUP_SOURCE_I2C_SLAVE);
    SCB_UI->chBoxI2CEnableClockStretching->setChecked(i2cConfig.clockStretch == CyUsbBoolField::TRUE);
    SCB_UI->leSlaveAddress->setText(QString::number(i2cConfig.slaveAddress));
    i2cConfigDlg_setControlsEnabled(i2cConfig.i2cMaster);
}

void ConfigurationDlg::onModeChanged(int index)
{
    SCBWidget *widget = qobject_cast<SCBWidget*>(sender());
    if(widget  == _widgetScb[0])
    {
        _scbNum = ScbNum::SCB0;
    }
    if(widget == _widgetScb[1])
    {
        _scbNum = ScbNum::SCB1;
    }
    const auto SCB_UI = widget->getUI();
    int mode =index + 1;
    switch (mode)
    {
    case static_cast<int>(CY_SCB_MODE::UART):
        SCB_UI->lbProtocol->setText(QString::fromStdString(scbProtocol.at(CY_SCB_PROTOCOL::CDC)));
        SCB_UI->stackedWidgetSCB->setCurrentWidget(SCB_UI->pageSCBUart);
        updateDateScbUart();
        _state.freeGpios();
        break;
    case static_cast<int>(CY_SCB_MODE::SPI):
        SCB_UI->lbProtocol->setText(QString::fromStdString(scbProtocol.at(CY_SCB_PROTOCOL::Vendor)));
        SCB_UI->stackedWidgetSCB->setCurrentWidget(SCB_UI->pageSCBSpi);
        updateDateScbSpi();
        _state.freeGpios();
        break;
    case static_cast<int>(CY_SCB_MODE::I2C):
        SCB_UI->lbProtocol->setText(QString::fromStdString(scbProtocol.at(CY_SCB_PROTOCOL::Vendor)));
        SCB_UI->stackedWidgetSCB->setCurrentWidget(SCB_UI->pageSCBI2C);
        updateDateScbI2c();
        _state.freeGpios();
        break;
    }
}

void ConfigurationDlg::onTypeChanged(int index)
{
    SCBWidget *widget = qobject_cast<SCBWidget*>(sender());

    if(widget  == _widgetScb[0])
    {
        _scbNum = ScbNum::SCB0;
    }

    if(widget == _widgetScb[1])
    {
        _scbNum = ScbNum::SCB1;
    }

    const auto SCB_UI = widget->getUI();

    _state.updateUsedScbGPIOs(_scbNum, CY_SCB_MODE::UART,
                                 static_cast<CY_UART_TYPE>(SCB_UI->cbUartType->currentData().toUInt()));
}

void ConfigurationDlg::onSpiMasterChanged(int index)
{
    SCBWidget *widget = qobject_cast<SCBWidget*>(sender());
    if(widget  == _widgetScb[0])
    {
        _scbNum = ScbNum::SCB0;
    }
    if(widget == _widgetScb[1])
    {
        _scbNum = ScbNum::SCB1;
    }

    const auto SCB_UI = widget->getUI();
    bool slave = static_cast<CyUsbBoolField>(SCB_UI->cbSpiMaster->currentData().toUInt()) == CyUsbBoolField::FALSE;
    SCB_UI->chBoxuseAsWakeupSource->setEnabled(slave);
    if (!slave)
    {
        SCB_UI->chBoxuseAsWakeupSource->setChecked(false);
    }
}

void ConfigurationDlg::onSpiProtocolChanged(int index)
{
    SCBWidget *widget = qobject_cast<SCBWidget*>(sender());
    if(widget  == _widgetScb[0])
    {
        _scbNum = ScbNum::SCB0;
    }
    if(widget == _widgetScb[1])
    {
        _scbNum = ScbNum::SCB1;
    }
    const auto SCB_UI = widget->getUI();
    CY_SPI_PROTOCOL protocol = static_cast<CY_SPI_PROTOCOL>(SCB_UI->cbSpiProtocol->currentData().toUInt());
    spiConfigDlg_setControlsEnabled(protocol);
}

void ConfigurationDlg::oni2CMasterChanged(int index)
{
    SCBWidget *widget = qobject_cast<SCBWidget*>(sender());
    if(widget  == _widgetScb[0])
    {
        _scbNum = ScbNum::SCB0;
    }
    if(widget == _widgetScb[1])
    {
        _scbNum = ScbNum::SCB1;
    }
    _state.freeGpios();
    const auto SCB_UI = widget->getUI();
    auto master = static_cast<CyUsbBoolField>(SCB_UI->cbI2CMaster->currentData().toUInt());
    i2cConfigDlg_setControlsEnabled(master);
}

void ConfigurationDlg::onClickedOk()
{
    for(int i = 0; i < _widgetScb.size(); ++i)
    {
        _scbNum = static_cast<ScbNum>(i);
        auto index = static_cast<Cy::CY_SCB_MODE>( _widgetScb[_scbNum]->getUI()->cbSCBMode->currentIndex() + 1);
        switch (index)
        {
        case CY_SCB_MODE::UART:
            _state.config.scb[_scbNum]->mode = CY_SCB_MODE::UART;
            _state.config.scb[_scbNum]->protocol = CY_SCB_PROTOCOL::CDC;
            readUiUartData();
            break;

        case CY_SCB_MODE::SPI:
            _state.config.scb[_scbNum]->mode = CY_SCB_MODE::SPI;
            _state.config.scb[_scbNum]->protocol = CY_SCB_PROTOCOL::Vendor;
            readUiSpiData();
            break;

        case CY_SCB_MODE::I2C:
            _state.config.scb[_scbNum]->mode = CY_SCB_MODE::I2C;
            _state.config.scb[_scbNum]->protocol = CY_SCB_PROTOCOL::Vendor;
            readUiI2cData();
            break;
        }
    }
    accept();
}

void ConfigurationDlg::onDefaultConfiguration()
{
    auto &config = _state.config;
    CyUsbScbConfig &currentScb = config.scb[_scbNum];
    CyUsbScbConfig::ModeConfiguration &modeConfig = currentScb.modeConfiguration;
    CyUsbUartConfig &uartConfig = modeConfig.uart;
    uartConfig.baudRate = 10000;
    uartConfig.uartType = CY_UART_TYPE::PIN_2;
    uartConfig.dataWidth = CY_UART_DATA_WIDTH::BIT_7;
    uartConfig.stopBits =  CY_UART_STOP_BITS::STOP_BIT_1;
    uartConfig.parity = CY_UART_PARITY_MODE::None;
    updateConfigurationWithData();
}

void ConfigurationDlg::readUiUartData()
{
    auto &config = _state.config;
    CyUsbScbConfig &currentScb = config.scb[_scbNum];
    CyUsbScbConfig::ModeConfiguration &modeConfig = currentScb.modeConfiguration;
    CyUsbUartConfig &uartConfig = modeConfig.uart;
    const auto SCB_UI = _widgetScb[_scbNum]->getUI();
    uartConfig.baudRate = SCB_UI->cbUartBaudRate->currentData().toUInt();
    uartConfig.uartType = static_cast<CY_UART_TYPE>(SCB_UI->cbUartType->currentData().toUInt());
    uartConfig.dataWidth = static_cast<CY_UART_DATA_WIDTH>(SCB_UI->cbUartDataWidth->currentData().toUInt());
    uartConfig.stopBits = static_cast<CY_UART_STOP_BITS>(SCB_UI->cbUartStopBits->currentData().toUInt());
    uartConfig.parity = static_cast<CY_UART_PARITY_MODE>(SCB_UI->cbUartParity->currentData().toUInt());
}

void ConfigurationDlg::readUiSpiData()
{
    CyUsbScbConfig &scb = _state.config.scb[_scbNum];
    CyUsbScbConfig::ModeConfiguration &modeConfig = scb.modeConfiguration;
    CyUsbSpiConfig &spiConfig = modeConfig.spi;
    const auto SCB_UI = _widgetScb[_scbNum]->getUI();
    //spiConfig.frequency = SCB_UI->->value();
    spiConfig.dataWidth = static_cast<CY_SPI_DATA_WIDTH>(SCB_UI->cbSpiDataWidth->currentData().toUInt());
    spiConfig.spiMaster = static_cast<CY_SPI_TYPE>(SCB_UI->cbSpiMaster->currentData().toUInt());
    spiConfig.protocol = static_cast<CY_SPI_PROTOCOL>(SCB_UI->cbSpiProtocol->currentData().toUInt());
    spiConfig.cpha = static_cast<CyUsbLowHighField>(SCB_UI->cbSpiCphaMode->currentData().toUInt());
    spiConfig.cpol = static_cast<CyUsbLowHighField>(SCB_UI->cbSpiCpolMode->currentData().toUInt());
    spiConfig.ssnToggleMode = static_cast<CyUsbSsnToggleField>(SCB_UI->cbSpiSsnToggleMode->currentData().toUInt());
    spiConfig.bitPosition = static_cast<CY_BIT_POS>(SCB_UI->cbSpiBitOrder->currentData().toUInt());
    setBit(_state.config.usbConfig->remoteWakeup, WAKEUP_SOURCE_SPI_SLAVE, SCB_UI->chBoxuseAsWakeupSource->isChecked());
    spiConfig.selectPrecede = toBoolField(SCB_UI->chBoxenableSelectPrecede->isChecked());
}

void ConfigurationDlg::readUiI2cData()
{
    const auto SCB_UI = _widgetScb[_scbNum]->getUI();
    CyUsbI2cConfig &i2c = _state.config.scb[_scbNum]->modeConfiguration->i2c;
    i2c.frequncy = SCB_UI->cbI2CFrequency->currentText().toUInt();
    i2c.i2cMaster = toBoolField(SCB_UI->cbI2CMaster->currentData().toUInt());
    setBit(_state.config.usbConfig->remoteWakeup, WAKEUP_SOURCE_I2C_SLAVE, SCB_UI->chBoxI2CUseAsWakeupSrc->isChecked());
    i2c.clockStretch = toBoolField(SCB_UI->chBoxI2CEnableClockStretching->isChecked());
    i2c.slaveAddress = SCB_UI->leSlaveAddress->text().toUInt();
}

void ConfigurationDlg::spiConfigDlg_setControlsEnabled(CY_SPI_PROTOCOL protocol)
{
    const auto SCB_UI = _widgetScb[_scbNum]->getUI();
    switch (protocol)
    {
    case CY_SPI_PROTOCOL::Motorola:
        SCB_UI->cbSpiCphaMode->setEnabled(true);
        SCB_UI->cbSpiCpolMode->setEnabled(true);
        SCB_UI->cbSpiSsnToggleMode->setEnabled(true);
        SCB_UI->chBoxenableSelectPrecede->setEnabled(false);
        break;

    case CY_SPI_PROTOCOL::TI:
        SCB_UI->cbSpiCphaMode->setEnabled(false);
        SCB_UI->cbSpiCpolMode->setEnabled(false);
        SCB_UI->cbSpiSsnToggleMode->setEnabled(false);
        SCB_UI->chBoxenableSelectPrecede->setEnabled(true);
        break;

    case CY_SPI_PROTOCOL::NS:
        SCB_UI->cbSpiCphaMode->setEnabled(false);
        SCB_UI->cbSpiCpolMode->setEnabled(false);
        SCB_UI->cbSpiSsnToggleMode->setEnabled(true);
        SCB_UI->chBoxenableSelectPrecede->setEnabled(false);
        break;
    }
}

void ConfigurationDlg::i2cConfigDlg_setControlsEnabled(CyUsbBoolField master)
{
    const auto SCB_UI = _widgetScb[_scbNum]->getUI();
    switch (master)
    {
    case CyUsbBoolField::FALSE:
        SCB_UI->chBoxI2CEnableClockStretching->setEnabled(true);
        SCB_UI->leSlaveAddress->setEnabled(true);
        SCB_UI->chBoxI2CUseAsWakeupSrc->setEnabled(true);
        break;

    case CyUsbBoolField::TRUE:
        SCB_UI->chBoxI2CEnableClockStretching->setEnabled(false);
        SCB_UI->leSlaveAddress->setEnabled(false);
        SCB_UI->chBoxI2CUseAsWakeupSrc->setEnabled(false);
        break;
    }
}

void SCBWidget::on_cbSCBMode_currentIndexChanged(int index)
{
    emit modeChanged(index);
}

void SCBWidget::on_cbUartType_currentIndexChanged(int index)
{
    emit typeChanged(index);
}

void SCBWidget::on_chBoxSCBUartEnableRS485_stateChanged(int arg1)
{
    emit uartEnableRS485Changed(arg1);
}

void SCBWidget::on_cbUartGpioRS_currentIndexChanged(int index)
{
    emit uartGpioRSChanged(index);
}

void SCBWidget::on_cbSpiMaster_currentIndexChanged(int index)
{
    emit spiMasterChanged(index);
}

void SCBWidget::on_cbSpiProtocol_currentIndexChanged(int index)
{
    emit spiProtocolChanged(index);
}

void SCBWidget::on_cbI2CMaster_currentIndexChanged(int index)
{
    emit i2cMasterChanged(index);
}




