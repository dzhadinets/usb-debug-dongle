#include "simplified_ui/appstatesimplified.h"
#include <common/commonutilities.h>

using namespace Cy;

void AppStateSimplified::initGPIOs()
{
    using namespace ScbNum;

    // Power
    if (config.powerGpioEnable == CyUsbBoolField::TRUE)
    {
        gpioState.use(config.powerGpio, GpioState::WAKEUP_POWER);
    }

    // SCBx
    for (ScbNum scbNum : {SCB0, SCB1})
    {
        const CyUsbScbConfig &scb = config.scb[scbNum];
        if(scbNum == SCB0)
        {
            gpioState.use(scbNum, scb.mode, GpioState::SCB0, scb.modeConfiguration.value.uart.uartType);
        }

        if(scbNum == SCB1)
        {
            gpioState.use(scbNum, scb.mode, GpioState::SCB1, scb.modeConfiguration.value.uart.uartType);
        }

        gpioState.use(scb.ledActivity->rxLed, GpioState::SCB0_RxLED);
        gpioState.use(scb.ledActivity->txLed, GpioState::SCB0_TxLED);
        gpioState.use(scb.ledActivity->rxAndTxLed, GpioState::SCB0_RxTxLED);
    }

    for (uint i = 0, gpioFlag = 1; i < NUMBER_OF_GPIOS; ++i, gpioFlag <<= 1)
    {
        int stateGpio = gpioState[static_cast<GPIO>(i)];
        switch (stateGpio)
        {
        case GpioState::SCB0:
            break;
        case GpioState::SCB1:
            break;
        case GpioState::SCB0_RxLED:
            break;
        case GpioState::SCB0_TxLED:
            break;
        case GpioState::WAKEUP_POWER:
            break;
        default:
            if (gpioFlag & config.drive0)
            {
                gpioState.use(static_cast<GPIO>(i), GpioState::Drive_0);
            }
            else if (gpioFlag & config.drive1)
            {
                gpioState.use(static_cast<GPIO>(i), GpioState::Drive_1);
            }
            else if (gpioFlag & config.input)
            {
                gpioState.use(static_cast<GPIO>(i), GpioState::Input);
            }
        }
    }
}

void AppStateSimplified::freeGpios()
{
    for(int i = 0; i < GPIO_COUNT; ++i)
    {
        GPIO gpio = static_cast<GPIO>(i);
        gpioState.unuse(gpio);
    }
}

void AppStateSimplified::updateUsedScbGPIOs(ScbNum scbNum, CY_SCB_MODE newScbMode, CY_UART_TYPE uartType)
{
    GpioState::State newState{};
    switch (scbNum)
    {
    case ScbNum::SCB0:
        newState = GpioState::State::SCB0;
        break;

    case ScbNum::SCB1:
        newState = GpioState::State::SCB1;
        break;
    }

    CyUsbScbConfig &scb = config.scb[scbNum];
    gpioState.unuse(scbNum, scb.mode, scb.modeConfiguration->uart.uartType);
    auto toDisable = gpioState.use(scbNum, newScbMode, newState, uartType);

    scb.mode = newScbMode;
    if (newScbMode == CY_SCB_MODE::UART)
    {
        scb.modeConfiguration->uart.uartType = uartType;
    }

    if (toDisable.size() > 0)
    {
        gpioState.use(scbNum, newScbMode, newState, uartType);
    }
}
