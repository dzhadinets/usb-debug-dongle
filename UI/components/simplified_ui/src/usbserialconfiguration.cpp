#include <common/CyUSB.h>
#include <common/enumsregistration.h>

#include "../include/simplified_ui/configurationdlg.h"
#include "./ui_usbserialconfiguration.h"
#include "simplified_ui/usbserialconfiguration.h"

#define GPIO_COUNT 19

using namespace Cy;

UsbSerialConfiguration::UsbSerialConfiguration(QWidget *parent)
    : QMainWindow(parent)
    , _ui(new Ui::UsbSerialConfiguration)
{
    _ui->setupUi(this);
    setFixedSize(size());
    _ui->lbImage->setPixmap(QPixmap("/home/aivashko/Project/usb-debug-whistle/subcomponents/UI/components/simplified_ui/res/images/image.png"));
    defaultDataPins();
    fillTableData();
    fillMapGpioPins();
    metka = new QPushButton(_ui->lbImage);
    metka->setVisible(false);
    _ui->pb_Configuration->setEnabled(false);
    _ui->pb_ReadConfig->setEnabled(false);
    _ui->pb_WriteConfig->setEnabled(false);
    _ui->cbListDevices->addItem("No Devices");
    _ui->tableConnection2->horizontalHeader()->setDefaultSectionSize(119);
    _ui->tableConnection3->horizontalHeader()->setDefaultSectionSize(119);
    _ui->tableConnection4->horizontalHeader()->setDefaultSectionSize(119);
}

UsbSerialConfiguration::~UsbSerialConfiguration()
{
    delete _ui;
}

void UsbSerialConfiguration::on_pb_ReadConfig_clicked()
{
    int index = _ui->cbListDevices->currentIndex();
    _device = cy7_device_open(&_devs[index]); // check for errors?
    cy7_device_read_config(_device, _buf, sizeof(_buf));
    Padded<UsbDeviceConfig, 512> tmpConfig;
    memcpy(&tmpConfig, _buf, 512);
    _state.config = tmpConfig.value;
    _state.initGPIOs();
    updateDataPins();
    fillTableData();
    _ui->pb_Configuration->setEnabled(true);
    _ui->pb_WriteConfig->setEnabled(true);
    _ui->statusbar->showMessage(tr("Read Settings"));
}

void UsbSerialConfiguration::on_pb_Configuration_clicked()
{
    ConfigurationDlg *configDlg = new ConfigurationDlg(_state, this);
    if (configDlg->exec() == QDialog::Accepted)
    {
        _state = configDlg->dataReceived();
        _state.initGPIOs();
        updateDataPins();
        fillTableData();
    }
    delete configDlg;
}

void UsbSerialConfiguration::on_pb_WriteConfig_clicked()
{
    readUIDate();
    updateGPioState();
    writeSettings();
    _ui->statusbar->showMessage(tr("Write Settings"));
}

void UsbSerialConfiguration::fillTableData()
{
    for(int n = 0; n < _con.size(); ++n)
    {
        const int tableCon2 = static_cast<int>(TABLE::TableCon2);
        const int tableCon3 = static_cast<int>(TABLE::TableCon3);
        const int tableCon4 = static_cast<int>(TABLE::TableCon4);

        switch (n)
        {
        case tableCon2:
            for(int i = 0; i < _con[n].size(); ++i)
            {
                QTableWidgetItem *name = new QTableWidgetItem;
                QTableWidgetItem *type = new QTableWidgetItem;
                QTableWidgetItem *valuePin = new QTableWidgetItem;
                name->setText(_con[n][i].name);
                type->setText(_con[n][i].type);
                valuePin->setText(_con[n][i].value);
                _ui->tableConnection2->setItem(i, 0, name);
                _ui->tableConnection2->setItem(i, 1, type);
                _ui->tableConnection2->setItem(i, 2, valuePin);
                QComboBox *box = new QComboBox();
                for(const auto &[key, value] : gpioState)
                {
                    box->addItem(QString::fromStdString(value), static_cast<int>(key));
                    if(valuePin->text() == QString::fromStdString(value))
                    {
                        box->setCurrentIndex(static_cast<int>(key));
                    }
                }
                if((type->text() == "GPIO"))
                {
                    _ui->tableConnection2->setCellWidget(i, 2, box);
                }
                else
                {
                    _ui->tableConnection2->removeCellWidget(i, 2);
                }
            }
            break;
        case tableCon3:
            for(int i = 0; i < _con[n].size(); ++i)
            {
                QTableWidgetItem *name = new QTableWidgetItem;
                QTableWidgetItem *type = new QTableWidgetItem;
                QTableWidgetItem *valuePin = new QTableWidgetItem;
                name->setText(_con[n][i].name);
                type->setText(_con[n][i].type);
                valuePin->setText(_con[n][i].value);
                name->setText(_con[n][i].name);
                type->setText(_con[n][i].type);
                valuePin->setText(_con[n][i].value);
                _ui->tableConnection3->setItem(i, 0, name);
                _ui->tableConnection3->setItem(i, 1, type);
                _ui->tableConnection3->setItem(i, 2, valuePin);
                QComboBox *box = new QComboBox();
                for(const auto &[key, value] : gpioState)
                {
                    box->addItem(QString::fromStdString(value), static_cast<int>(key));
                    if(valuePin->text() == QString::fromStdString(value))
                    {
                        box->setCurrentIndex(static_cast<int>(key));
                    }
                }
                if((type->text() == "GPIO"))
                {
                    _ui->tableConnection3->setCellWidget(i, 2, box);
                }
                else
                {
                    _ui->tableConnection3->removeCellWidget(i, 2);
                }
            }
            break;

        case tableCon4:
            for(int i = 0; i < _con[n].size(); ++i)
            {
                QTableWidgetItem *name = new QTableWidgetItem;
                QTableWidgetItem *type = new QTableWidgetItem;
                QTableWidgetItem *valuePin = new QTableWidgetItem;
                name->setText(_con[n][i].name);
                type->setText(_con[n][i].type);
                valuePin->setText(_con[n][i].value);
                _ui->tableConnection4->setItem(i, 0, name);
                _ui->tableConnection4->setItem(i, 1, type);
                _ui->tableConnection4->setItem(i, 2, valuePin);

                QComboBox *box = new QComboBox();
                for(const auto &[key, value] : gpioState)
                {
                    box->addItem(QString::fromStdString(value), static_cast<int>(key));
                    if(valuePin->text() == QString::fromStdString(value))
                    {
                        box->setCurrentIndex(static_cast<int>(key));
                    }
                }

                if((type->text() == "GPIO"))
                {
                    _ui->tableConnection4->setCellWidget(i, 2, box);
                }
                else
                {
                    _ui->tableConnection4->removeCellWidget(i, 2);
                }
            }
            break;
        default:
            break;
        }
    }
}

void UsbSerialConfiguration::defaultDataPins()
{
    Pin pin1{"Pin1", "Power", "VDDD"};
    Pin pin2{"Pin2", "-", "-"};
    Pin pin3{"Pin3", "-", "-"};
    Pin pin4{"Pin4", "Power", "GND"};
    Pin pin5{"Pin5", "-", "-"};
    Pin pin6{"Pin6", "-", "-"};
    Pin pin7{"Pin7", "-", "-"};
    Pin pin8{"Pin8", "-", "-"};
    Pin pin9{"Pin9", "-", "-"};
    Pin pin10{"Pin10", "-", "-"};
    Pin pin11{"Pin11", "Output", "-"};
    Pin pin12{"Pin12", "Input", "-"};
    Pin pin13{"Pin13", "GPIO", "Drive_0"};
    Pin pin14{"Pin14", "USBIO", "-"};
    Pin pin15{"Pin15", "USBIO", "-"};
    Pin pin16{"Pin16", "Power", "VCCD"};
    Pin pin17{"Pin17", "Power", "GND"};
    Pin pin18{"Pin18", "nXRES", "-"};
    Pin pin19{"Pin19", "Power", "VBUS"};
    Pin pin20{"Pin20", "Power", "GND"};
    Pin pin21{"Pin21", "-", "-"};
    Pin pin22{"Pin22", "-", "-"};
    Pin pin23{"Pin23", "Power", "VDDD"};
    Pin pin24{"Pin24", "Power", "GND"};
    Pin pin25{"Pin25", "GPIO_LED", "TXLED#"};
    Pin pin26{"Pin26", "GPIO_LED", "RXLED#"};
    Pin pin27{"Pin27", "-", "-"};
    Pin pin28{"Pin28", "-", "-"};
    Pin pin29{"Pin29", "-", "-"};
    Pin pin30{"Pin30", "-", "-"};
    Pin pin31{"Pin31", "-", "-"};
    Pin pin32{"Pin32", "-", "-"};
    _con.push_back({pin7, pin8, pin5, pin6, pin30, pin2, pin28, pin29, pin11, pin17});
    _con.push_back({pin19, pin24, pin12, pin13, pin10, pin9, pin3, pin32, pin31, pin4});
    _con.push_back({pin23, pin16, pin1, pin14, pin18, pin21, pin25, pin22, pin27, pin26});
}

void UsbSerialConfiguration::updateDataPins()
{
    for(int i = 0; i < static_cast<int>(GPIO::None); ++i)
    {
        switch (i) 
        {
        case static_cast<int >(GPIO::GPIO_00):
            if(_state.gpioState[GPIO::GPIO_00] == _state.gpioState.SCB0_TxLED)
            {
                _con[2][6].type = "GPIO_LED";
                _con[2][6].value = "TX_LED#";
            }
            break;
        case static_cast<int>(GPIO::GPIO_01):
            if(_state.gpioState[GPIO::GPIO_01] == _state.gpioState.SCB0_RxLED)
            {
                _con[2][9].type = "GPIO_LED";
                _con[2][9].value = "RX_LED#";
            }
            break;
        case static_cast<int>(GPIO::GPIO_02):
            if(_state.gpioState[GPIO::GPIO_02] == _state.gpioState.SCB0)
            {
                const auto scb0 = _state.config.scb[0];
                if(scb0->mode==CY_SCB_MODE::UART)
                {
                    _con[2][8].type = "SCB0/UART";
                    _con[2][8].value = "DSR#";
                }

                if(scb0->mode==CY_SCB_MODE::SPI)
                {
                    _con[2][8].type = "SCB0/SPI";
                    const auto spi = scb0->modeConfiguration->spi;
                    if(spi.spiMaster == CY_SPI_TYPE::Master)
                    {
                        _con[2][8].value = "SSEL_OUT";
                    }
                    if(spi.spiMaster == CY_SPI_TYPE::Slave)
                    {
                        _con[2][8].value = "SSEL_IN";
                    }
                }
            }
            if(_state.gpioState[GPIO::GPIO_02] == _state.gpioState.Drive_0)
            {
                _con[2][8].type = "GPIO";
                _con[2][8].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_02] == _state.gpioState.Drive_1)
            {
                _con[2][8].type = "GPIO";
                _con[2][8].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_02] == _state.gpioState.Input)
            {
                _con[2][8].type = "GPIO";
                _con[2][8].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_02] == _state.gpioState.Free)
            {
                _con[2][8].type = "GPIO";
                _con[2][8].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_03):
            if(_state.gpioState[GPIO::GPIO_03] == _state.gpioState.SCB0)
            {
                const auto scb0 = _state.config.scb[0];
                if(scb0->mode==CY_SCB_MODE::UART)
                {
                    _con[0][6].type = "SCB0/UART";
                    _con[0][6].value = "RTS#";
                }
                if(scb0->mode==CY_SCB_MODE::SPI)
                {
                    _con[0][6].type = "SCB0/SPI";
                    const auto spi = scb0->modeConfiguration->spi;
                    if(spi.spiMaster == CY_SPI_TYPE::Master)
                    {
                        _con[0][6].value = "MISO_IN";
                    }
                    if(spi.spiMaster == CY_SPI_TYPE::Slave)
                    {
                        _con[0][6].value = "MISO_OUT";
                    }
                }
                if(scb0->mode==CY_SCB_MODE::I2C)
                {
                    _con[0][6].type = "SCB0/I2C";
                    const auto i2c = scb0->modeConfiguration->i2c;
                    if(i2c.i2cMaster ==CyUsbBoolField::TRUE)
                    {
                        _con[0][6].value = " SCL_OUT";
                    }
                    if(i2c.i2cMaster ==CyUsbBoolField::FALSE)
                    {
                        _con[0][6].value = " SCL_IN";
                    }
                }
            }
            if(_state.gpioState[GPIO::GPIO_03] == _state.gpioState.Drive_0)
            {
                _con[0][6].type = "GPIO";
                _con[0][6].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_03] == _state.gpioState.Drive_1)
            {
                _con[0][6].type = "GPIO";
                _con[0][6].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_03] == _state.gpioState.Input)
            {
                _con[0][6].type = "GPIO";
                _con[0][6].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_03] == _state.gpioState.Free)
            {
                _con[0][6].type = "GPIO";
                _con[0][6].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_04):
            if(_state.gpioState[GPIO::GPIO_04] == _state.gpioState.SCB0)
            {
                const auto scb0 = _state.config.scb[0];
                if(scb0->mode==CY_SCB_MODE::UART)
                {
                    _con[0][7].type = "SCB0/UART";
                    _con[0][7].value = "CTS#";
                }
                if(scb0->mode==CY_SCB_MODE::SPI)
                {
                    _con[0][7].type = "SCB0/SPI";
                    const auto spi = scb0->modeConfiguration->spi;
                    if(spi.spiMaster == CY_SPI_TYPE::Master)
                    {
                        _con[0][7].value = "MOSI_OUT";
                    }
                    if(spi.spiMaster == CY_SPI_TYPE::Slave)
                    {
                        _con[0][7].value = "MOSI_IN";
                    }
                }
                if(scb0->mode==CY_SCB_MODE::I2C)
                {
                    _con[0][7].type = "SCB0/I2C";
                    _con[0][7].value = "SDA";
                }
            }
            if(_state.gpioState[GPIO::GPIO_04] == _state.gpioState.Drive_0)
            {
                _con[0][7].type = "GPIO";
                _con[0][7].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_04] == _state.gpioState.Drive_1)
            {
                _con[0][7].type = "GPIO";
                _con[0][7].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_04] == _state.gpioState.Input)
            {
                _con[0][7].type = "GPIO";
                _con[0][7].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_04] == _state.gpioState.Free)
            {
                _con[0][7].type = "GPIO";
                _con[0][7].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_05):
            if(_state.gpioState[GPIO::GPIO_05] == _state.gpioState.SCB0)
            {
                const auto scb0 = _state.config.scb[0];
                if(scb0->mode==CY_SCB_MODE::UART)
                {
                    _con[0][4].type = "SCB0/UART";
                    _con[0][4].value = "TxD#";
                }
                if(scb0->mode==CY_SCB_MODE::SPI)
                {
                    _con[0][4].type = "SCB0/SPI";
                    const auto spi = scb0->modeConfiguration->spi;
                    if(spi.spiMaster == CY_SPI_TYPE::Master)
                    {
                        _con[0][4].value = "SCLK_OUT";
                    }
                    if(spi.spiMaster == CY_SPI_TYPE::Slave)
                    {
                        _con[0][4].value = "SCLK_IN";
                    }
                }
            }
            if(_state.gpioState[GPIO::GPIO_05] == _state.gpioState.Drive_0)
            {
                _con[0][4].type = "GPIO";
                _con[0][4].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_05] == _state.gpioState.Drive_1)
            {
                _con[0][4].type = "GPIO";
                _con[0][4].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_05] == _state.gpioState.Input)
            {
                _con[0][4].type = "GPIO";
                _con[0][4].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_05] == _state.gpioState.Free)
            {
                _con[0][4].type = "GPIO";
                _con[0][4].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_06):
            if(_state.gpioState[GPIO::GPIO_06] == _state.gpioState.WAKEUP_POWER)
            {
                _con[1][8].type = "GPIO_POWER";
                _con[1][8].value = "POWER#";
            }
            break;
        case static_cast<int>(GPIO::GPIO_07):
            if(_state.gpioState[GPIO::GPIO_07] == _state.gpioState.Drive_0)
            {
                _con[1][7].type = "GPIO";
                _con[1][7].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_07] == _state.gpioState.Drive_1)
            {
                _con[1][7].type = "GPIO";
                _con[1][7].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_07] == _state.gpioState.Input)
            {
                _con[1][7].type = "GPIO";
                _con[1][7].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_07] == _state.gpioState.Free)
            {
                _con[1][7].type = "GPIO";
                _con[1][7].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_08):
            if(_state.gpioState[GPIO::GPIO_08] == _state.gpioState.SCB0)
            {
                _con[0][5].type = "SCB0/UART";
                _con[0][5].value = "RxD#";
            }
            if(_state.gpioState[GPIO::GPIO_08] == _state.gpioState.Drive_0)
            {
                _con[0][5].type = "GPIO";
                _con[0][5].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_08] == _state.gpioState.Drive_1)
            {
                _con[0][5].type = "GPIO";
                _con[0][5].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_08] == _state.gpioState.Input)
            {
                _con[0][5].type = "GPIO";
                _con[0][5].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_08] == _state.gpioState.Free)
            {
                _con[0][5].type = "GPIO";
                _con[0][5].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_09):
            if(_state.gpioState[GPIO::GPIO_09] == _state.gpioState.SCB0)
            {
                _con[1][6].type = "SCB0/UART";
                _con[1][6].value = "DTR#";
            }
            if(_state.gpioState[GPIO::GPIO_09] == _state.gpioState.Drive_0)
            {
                _con[1][6].type = "GPIO";
                _con[1][6].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_09] == _state.gpioState.Drive_1)
            {
                _con[1][6].type = "GPIO";
                _con[1][6].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_09] == _state.gpioState.Input)
            {
                _con[1][6].type = "GPIO";
                _con[1][6].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_09] == _state.gpioState.Free)
            {
                _con[1][6].type = "GPIO";
                _con[1][6].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_10):
            if(_state.gpioState[GPIO::GPIO_10] == _state.gpioState.SCB1)
            {
                const auto scb1 = _state.config.scb[1];
                if(scb1->mode==CY_SCB_MODE::UART)
                {
                    _con[0][2].type = "SCB1/UART";
                    _con[0][2].value = "RxD#";
                }
                if(scb1->mode==CY_SCB_MODE::SPI)
                {
                    _con[0][2].type = "SCB1/SPI";
                    const auto spi = scb1->modeConfiguration->spi;
                    if(spi.spiMaster == CY_SPI_TYPE::Master)
                    {
                        _con[0][2].value = "MISO_IN";
                    }
                    if(spi.spiMaster == CY_SPI_TYPE::Slave)
                    {
                        _con[0][2].value = "MISO_OUT";
                    }
                }
                if(scb1->mode==CY_SCB_MODE::I2C)
                {
                    _con[0][2].type = "SCB1/I2C";
                    const auto i2c = scb1->modeConfiguration->i2c;
                    if(i2c.i2cMaster ==CyUsbBoolField::TRUE)
                    {
                        _con[0][2].value = "SCL_OUT";
                    }
                    if(i2c.i2cMaster ==CyUsbBoolField::FALSE)
                    {
                        _con[0][2].value = "SCL_IN";
                    }
                }
            }
            if(_state.gpioState[GPIO::GPIO_10] == _state.gpioState.Drive_0)
            {
                _con[0][2].type = "GPIO";
                _con[0][2].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_10] == _state.gpioState.Drive_1)
            {
                _con[0][2].type = "GPIO";
                _con[0][2].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_10] == _state.gpioState.Input)
            {
                _con[0][2].type = "GPIO";
                _con[0][2].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_10] == _state.gpioState.Free)
            {
                _con[0][2].type = "GPIO";
                _con[0][2].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_11):
            if(_state.gpioState[GPIO::GPIO_11] == _state.gpioState.SCB1)
            {
                const auto scb1 = _state.config.scb[1];
                if(scb1->mode==CY_SCB_MODE::UART)
                {
                    _con[0][3].type = "SCB1/UART";
                    _con[0][3].value = "TxD#";
                }
                if(scb1->mode==CY_SCB_MODE::SPI)
                {
                    _con[0][3].type = "SCB1/SPI";
                    const auto spi = scb1->modeConfiguration->spi;
                    if(spi.spiMaster == CY_SPI_TYPE::Master)
                    {
                        _con[0][3].value = "MOSI_OUT";
                    }
                    if(spi.spiMaster == CY_SPI_TYPE::Slave)
                    {
                        _con[0][3].value = " MOSI_IN";
                    }
                }
                if(scb1->mode==CY_SCB_MODE::I2C)
                {
                    _con[0][3].type = "SCB1/I2C";
                    _con[0][3].value = "SDA";
                }
            }
            if(_state.gpioState[GPIO::GPIO_11] == _state.gpioState.Drive_0)
            {
                _con[0][3].type = "GPIO";
                _con[0][3].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_11] == _state.gpioState.Drive_1)
            {
                _con[0][3].type = "GPIO";
                _con[0][3].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_11] == _state.gpioState.Input)
            {
                _con[0][3].type = "GPIO";
                _con[0][3].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_11] == _state.gpioState.Free)
            {
                _con[0][3].type = "GPIO";
                _con[0][3].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_12):
            if(_state.gpioState[GPIO::GPIO_12] == _state.gpioState.SCB1)
            {
                const auto scb1 = _state.config.scb[1];
                if(scb1->mode==CY_SCB_MODE::UART)
                {
                    _con[0][0].type = "SCB1/UART";
                    _con[0][0].value = "RTS#";
                }
                if(scb1->mode==CY_SCB_MODE::SPI)
                {
                    _con[0][0].type = "SCB1/SPI";
                    const auto spi = scb1->modeConfiguration->spi;
                    if(spi.spiMaster == CY_SPI_TYPE::Master)
                    {
                        _con[0][0].value = "SSEL_OUT";
                    }
                    if(spi.spiMaster == CY_SPI_TYPE::Slave)
                    {
                        _con[0][0].value = "SSEL_IN";
                    }
                }
            }
            if(_state.gpioState[GPIO::GPIO_12] == _state.gpioState.Drive_0)
            {
                _con[0][0].type = "GPIO";
                _con[0][0].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_12] == _state.gpioState.Drive_1)
            {
                _con[0][0].type = "GPIO";
                _con[0][0].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_12] == _state.gpioState.Input)
            {
                _con[0][0].type = "GPIO";
                _con[0][0].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_12] == _state.gpioState.Free)
            {
                _con[0][0].type = "GPIO";
                _con[0][0].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_13):
            if(_state.gpioState[GPIO::GPIO_13] == _state.gpioState.SCB1)
            {
                const auto scb1 = _state.config.scb[1];
                if(scb1->mode==CY_SCB_MODE::UART)
                {
                    _con[0][1].type = "SCB1/UART";
                    _con[0][1].value = "CTS#";
                }
                if(scb1->mode==CY_SCB_MODE::SPI)
                {
                    _con[0][1].type = "SCB1/SPI";
                    const auto spi = scb1->modeConfiguration->spi;
                    if(spi.spiMaster == CY_SPI_TYPE::Master)
                    {
                        _con[0][1].value = " SCLK_OUT";
                    }
                    if(spi.spiMaster == CY_SPI_TYPE::Slave)
                    {
                        _con[0][1].value = " SCLK_IN";
                    }
                }
            }
            if(_state.gpioState[GPIO::GPIO_13] == _state.gpioState.Drive_0)
            {
                _con[0][1].type = "GPIO";
                _con[0][1].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_13] == _state.gpioState.Drive_1)
            {
                _con[0][1].type = "GPIO";
                _con[0][1].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_13] == _state.gpioState.Input)
            {
                _con[0][1].type = "GPIO";
                _con[0][1].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_13] == _state.gpioState.Free)
            {
                _con[0][1].type = "GPIO";
                _con[0][1].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_14):
            if(_state.gpioState[GPIO::GPIO_14] == _state.gpioState.SCB1)
            {
                const auto scb1 = _state.config.scb[1];
                if(scb1->mode==CY_SCB_MODE::UART)
                {
                    _con[1][5].type = "SCB1/UART";
                    _con[1][5].value = "DSR#";
                }
            }
            if(_state.gpioState[GPIO::GPIO_14] == _state.gpioState.Drive_0)
            {
                _con[1][5].type = "GPIO";
                _con[1][5].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_14] == _state.gpioState.Drive_1)
            {
                _con[1][5].type = "GPIO";
                _con[1][5].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_14] == _state.gpioState.Input)
            {
                _con[1][5].type = "GPIO";
                _con[1][5].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_14] == _state.gpioState.Free)
            {
                _con[1][5].type = "GPIO";
                _con[1][5].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_15):
            if(_state.gpioState[GPIO::GPIO_15] == _state.gpioState.SCB1)
            {
                const auto scb1 = _state.config.scb[1];
                if(scb1->mode==CY_SCB_MODE::UART)
                {
                    _con[1][4].type = "SCB1/UART";
                    _con[1][4].value = "DTR#";
                }
            }
            if(_state.gpioState[GPIO::GPIO_15] == _state.gpioState.Drive_0)
            {
                _con[1][4].type = "GPIO";
                _con[1][4].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_15] == _state.gpioState.Drive_1)
            {
                _con[1][4].type = "GPIO";
                _con[1][4].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_15] == _state.gpioState.Input)
            {
                _con[1][4].type = "GPIO";
                _con[1][4].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_15] == _state.gpioState.Free)
            {
                _con[1][4].type = "GPIO";
                _con[1][4].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_16):
            if(_state.gpioState[GPIO::GPIO_16] == _state.gpioState.Drive_0)
            {
                _con[1][3].type = "GPIO";
                _con[1][3].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_16] == _state.gpioState.Drive_1)
            {
                _con[1][3].type = "GPIO";
                _con[1][3].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_16] == _state.gpioState.Input)
            {
                _con[1][3].type = "GPIO";
                _con[1][3].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_16] == _state.gpioState.Free)
            {
                _con[1][3].type = "GPIO";
                _con[1][3].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_17):
            if(_state.gpioState[GPIO::GPIO_17] == _state.gpioState.Drive_0)
            {
                _con[2][5].type = "GPIO";
                _con[2][5].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_17] == _state.gpioState.Drive_1)
            {
                _con[2][5].type = "GPIO";
                _con[2][5].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_17] == _state.gpioState.Input)
            {
                _con[2][5].type = "GPIO";
                _con[2][5].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_17] == _state.gpioState.Free)
            {
                _con[2][5].type = "GPIO";
                _con[2][5].value = "Tristate";
            }
            break;
        case static_cast<int>(GPIO::GPIO_18):
            if(_state.gpioState[GPIO::GPIO_18] == _state.gpioState.Drive_0)
            {
                _con[2][7].type = "GPIO";
                _con[2][7].value = "Drive_0";
            }
            if(_state.gpioState[GPIO::GPIO_18] == _state.gpioState.Drive_1)
            {
                _con[2][7].type = "GPIO";
                _con[2][7].value = "Drive_1";
            }
            if(_state.gpioState[GPIO::GPIO_18] == _state.gpioState.Input)
            {
                _con[2][7].type = "GPIO";
                _con[2][7].value = "Input";
            }
            if(_state.gpioState[GPIO::GPIO_18] == _state.gpioState.Free)
            {
                _con[2][7].type = "GPIO";
                _con[2][7].value = "Tristate";
            }
            break;
        }
    }
}

void UsbSerialConfiguration::readUIDate()
{
    auto &gpioState = _state.gpioState;
    for (int n = 0; n < _con.size(); ++n)
    {
        for (int i = 0; i < _con[n].size(); ++i)
        {
            QComboBox *combo = nullptr;
            if(_con[n][i].type == "GPIO")
            {
                const int tableCon2 = static_cast<int>(TABLE::TableCon2);
                const int tableCon3 = static_cast<int>(TABLE::TableCon3);
                const int tableCon4 = static_cast<int>(TABLE::TableCon4);

                switch (n)
                {
                case tableCon2:
                    combo = static_cast<QComboBox *>(_ui->tableConnection2->cellWidget(i, 2));
                    break;
                case tableCon3:
                    combo = static_cast<QComboBox *>(_ui->tableConnection3->cellWidget(i, 2));
                    break;
                case tableCon4:
                    combo = static_cast<QComboBox *>(_ui->tableConnection4->cellWidget(i, 2));
                    break;
                default:
                    break;
                }
                auto selectedDrive = static_cast<CyGpioState>(combo->currentData().toUInt());

                switch (selectedDrive)
                {
                case CyGpioState::Tristate:
                    gpioState.unuse(mapGpioPins.key(_con[n][i].name));
                    break;
                case CyGpioState::Drive_0:
                    gpioState.use(mapGpioPins.key(_con[n][i].name), GpioState::Drive_0);
                    break;
                case CyGpioState::Drive_1:
                    gpioState.use(mapGpioPins.key(_con[n][i].name), GpioState::Drive_1);
                    break;
                case CyGpioState::Input:
                    gpioState.use(mapGpioPins.key(_con[n][i].name), GpioState::Input);
                    break;
                }
            }
        }
    }
}

void UsbSerialConfiguration::updateGPioState()
{
    for(int i = 0; i < GPIO_COUNT; ++i)
    {
        uint gpioFlag = 1 << i;
        if(_state.gpioState[static_cast<GPIO>(i)] == _state.gpioState.Drive_0)
        {
            _state.config.drive0 |= gpioFlag;
        }
        if(_state.gpioState[static_cast<GPIO>(i)] == _state.gpioState.Drive_1)
        {
            _state.config.drive1 |= gpioFlag;
        }
        if(_state.gpioState[static_cast<GPIO>(i)] == _state.gpioState.Input)
        {
            _state.config.input |= gpioFlag;
        }
    }
}

void UsbSerialConfiguration::writeSettings()
{
    Padded<CyUsbscDeviceConfig, 512> tmpConfig;
    memset(&tmpConfig, 0, sizeof (tmpConfig));
    tmpConfig.value = _state.config;
    tmpConfig->checksum = _state.config.getChecksum();
    memcpy(_buf, &tmpConfig, 512);
    cy7_device_write_config(_device, _buf, sizeof(_buf), false);
}

void UsbSerialConfiguration::fillMapGpioPins()
{
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_00), "Pin25");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_01), "Pin26");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_02), "Pin27");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_03), "Pin28");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_04), "Pin29");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_05), "Pin30");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_06), "Pin31");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_07), "Pin32");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_08), "Pin2");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_09), "Pin3");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_10), "Pin5");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_11), "Pin6");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_12), "Pin7");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_13), "Pin8");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_14), "Pin9");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_15), "Pin10");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_16), "Pin13");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_17), "Pin21");
    mapGpioPins.insert(static_cast<GPIO>(GPIO::GPIO_18), "Pin22");
}

void UsbSerialConfiguration::on_tableConnection2_itemPressed(QTableWidgetItem *item)
{
    metka->setVisible(true);
    metka->setStyleSheet("QPushButton{background-color:green}");

    switch (item->row())
    {
    case static_cast<int>(PIN::Pin1):
    {
        metka->setGeometry(790, 135, 15, 15);
        break;

    }
    case static_cast<int>(PIN::Pin2):
    {
        metka->setGeometry(850, 135, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin3):
    {
        metka->setGeometry(790, 190, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin4):
    {
        metka->setGeometry(850, 190, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin5):
    {
        metka->setGeometry(790, 245, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin6):
    {
        metka->setGeometry(850, 245, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin7):
    {
        metka->setGeometry(790, 300, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin8):
    {
        metka->setGeometry(850, 300, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin9):
    {
        metka->setGeometry(790, 355, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin10):
    {
        metka->setGeometry(850, 355, 15, 15);
        break;
    }
    }
}

void UsbSerialConfiguration::on_tableConnection3_itemPressed(QTableWidgetItem *item)
{
    metka->setVisible(true);
    metka->setStyleSheet("QPushButton{background-color:green}");
    switch (item->row())
    {
    case static_cast<int>(PIN::Pin1):
    {
        metka->setGeometry(235, 140, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin2):
    {
        metka->setGeometry(235, 85, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin3):
    {
        metka->setGeometry(290, 140, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin4):
    {
        metka->setGeometry(290, 85, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin5):
    {
        metka->setGeometry(345, 140, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin6):
    {
        metka->setGeometry(345, 85, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin7):
    {
        metka->setGeometry(400, 140, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin8):
    {
        metka->setGeometry(400, 85, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin9):
    {
        metka->setGeometry(455, 140, 20, 20);
        break;
    }
    case static_cast<int>(PIN::Pin10):
    {
        metka->setGeometry(455, 85, 15, 15);
        break;
    }
    }
}

void UsbSerialConfiguration::on_tableConnection4_itemPressed(QTableWidgetItem *item)
{
    metka->setVisible(true);
    metka->setStyleSheet("QPushButton{background-color:green}");
    switch (item->row())
    {
    case static_cast<int>(PIN::Pin1):
    {
        metka->setGeometry(490, 460, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin2):
    {
        metka->setGeometry(490, 405, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin3):
    {
        metka->setGeometry(545, 460, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin4):
    {
        metka->setGeometry(545, 405, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin5):
    {
        metka->setGeometry(600, 460, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin6):
    {
        metka->setGeometry(600, 405, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin7):
    {
        metka->setGeometry(655, 460, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin8):
    {
        metka->setGeometry(655, 405, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin9):
    {
        metka->setGeometry(710, 460, 15, 15);
        break;
    }
    case static_cast<int>(PIN::Pin10):
    {
        metka->setGeometry(710, 405, 15, 15);
        break;
    }
    }
}

void UsbSerialConfiguration::on_actionFind_Devices_triggered()
{
    int ret = 0;
    cy7_lib_init();
    ret = cy7_scan_devices(_devs, sizeof(_devs)/sizeof(_devs[0]));
    if(ret)
    {
        _ui->pb_ReadConfig->setEnabled(true);
        _ui->cbListDevices->clear();
        _ui->statusbar->showMessage(tr("Find devices"));
        for(int i = 0; i < ret; ++i)
        {
            _ui->cbListDevices->addItem(QString::number(( _devs[i].product_id)));
        }
    }
    else
    {
        _ui->pb_Configuration->setEnabled(false);
        _ui->pb_ReadConfig->setEnabled(false);
        _ui->pb_WriteConfig->setEnabled(false);
        _ui->cbListDevices->clear();
        _ui->cbListDevices->addItem("No Devices");
        _ui->statusbar->showMessage(tr("No devices found"));
        defaultDataPins();
        fillTableData();
    }
}

void UsbSerialConfiguration::on_cbListDevices_currentIndexChanged(int index)
{
    _currentindex = index;
}





