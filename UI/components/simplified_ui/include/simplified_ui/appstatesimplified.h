#ifndef APPSTATESIMPLIFIED_H
#define APPSTATESIMPLIFIED_H

#include <common/gpiostate.h>
#include <common/CyUSB.h>

struct AppStateSimplified
{
    using ScbNum = ScbNum::ScbNum;

    Cy::UsbDeviceConfig config;
    GpioState gpioState;
    void updateUsedScbGPIOs(ScbNum scbNum, Cy::CY_SCB_MODE newScbMode, Cy::CY_UART_TYPE uartType = {});
    void initGPIOs();
    void freeGpios();
};


#endif // APPSTATESIMPLIFIED_H
