#ifndef SCBWIDGET_H
#define SCBWIDGET_H

#include <QWidget>

namespace Ui {
class SCBWidget;
}

class SCBWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SCBWidget(QWidget *parent = nullptr );
    ~SCBWidget();
    Ui::SCBWidget* getUI() {return ui;};

public slots:
    void on_cbSCBMode_currentIndexChanged(int index);
    void on_cbUartType_currentIndexChanged(int index);
    void on_chBoxSCBUartEnableRS485_stateChanged(int arg1);
    void on_cbUartGpioRS_currentIndexChanged(int index);
    void on_cbSpiMaster_currentIndexChanged(int index);
    void on_cbSpiProtocol_currentIndexChanged(int index);
    void on_cbI2CMaster_currentIndexChanged(int index);
signals:
    void modeChanged(int index);
    void typeChanged(int index);
    void uartEnableRS485Changed(int arg1);
    void uartGpioRSChanged(int index);
    void spiMasterChanged(int index);
    void spiProtocolChanged(int index);
    void i2cMasterChanged(int index);

private:
    Ui::SCBWidget *ui;
};

#endif // SCBWIDGET_H
