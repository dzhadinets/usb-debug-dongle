#ifndef USBSERIALCONFIGURATION_H
#define USBSERIALCONFIGURATION_H

#include <array>
#include <common/CyUSB.h>
#include <QComboBox>
#include <QDialog>
#include <QMainWindow>
#include <QMap>
#include <QTableWidgetItem>

extern "C" {
#include <cy7_flasher/cy7_flasher.h>
#include <string.h>
#include <stdio.h>
}

#include "appstatesimplified.h"

namespace Ui {
class UsbSerialConfiguration;
}

class UsbSerialConfiguration : public QMainWindow
{
    Q_OBJECT
public:
    explicit UsbSerialConfiguration(QWidget *parent = nullptr);
    ~UsbSerialConfiguration();

private slots:
    void on_tableConnection2_itemPressed(QTableWidgetItem *item);
    void on_tableConnection3_itemPressed(QTableWidgetItem *item);
    void on_tableConnection4_itemPressed(QTableWidgetItem *item);
    void on_actionFind_Devices_triggered();
    void on_cbListDevices_currentIndexChanged(int index);
    void on_pb_Configuration_clicked();
    void on_pb_WriteConfig_clicked();
    void on_pb_ReadConfig_clicked();

private:
    void fillTableData();
    void defaultDataPins();
    void updateDataPins();
    void readUIDate();
    void updateGPioState();
    void writeSettings();
    void fillMapGpioPins();

    cy7_usb_dev_spec_t _devs[32];
    cy7_device_t *_device;
    AppStateSimplified _state;
    Ui::UsbSerialConfiguration *_ui;
    char _buf[512];

    struct Pin
    {
       mutable QString name;
       mutable QString type;
       mutable QString value;
        void setType(QString sType){type = sType;};
    };

    QPushButton *metka;
    int _currentindex;
    enum class PIN
    {
        Pin1,
        Pin2,
        Pin3,
        Pin4,
        Pin5,
        Pin6,
        Pin7,
        Pin8,
        Pin9,
        Pin10
    };

    enum class TABLE
    {
        TableCon2,
        TableCon3,
        TableCon4
    };

    QMap<Cy::GPIO, QString> mapGpioPins;
    std::vector<std::array<Pin, 10>> _con;
};
#endif // USBSERIALCONFIGURATION_H
