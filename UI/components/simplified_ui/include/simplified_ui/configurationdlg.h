#ifndef CONFIGURATIONDLG_H
#define CONFIGURATIONDLG_H

#include <QDialog>
#include <common/CyUSB.h>

#include "simplified_ui/scbwidget.h"
#include "simplified_ui/appstatesimplified.h"

namespace Ui {
class ConfigurationDlg;
}

class ConfigurationDlg : public QDialog
{
    Q_OBJECT
    using ScbNum = ScbNum::ScbNum;
public:
    explicit ConfigurationDlg(const AppStateSimplified &state, QWidget *parent = nullptr);
    ~ConfigurationDlg();
    const AppStateSimplified &dataReceived();

private:
    ScbNum _scbNum;
    const std::map<ScbNum, std::string> _enumSCB = {
        {ScbNum::SCB0, "SCB0"},
        {ScbNum::SCB1, "SCB1"}
    };
    QVector<SCBWidget*> _widgetScb;
    Ui::ConfigurationDlg *ui;
    AppStateSimplified _state;
    void updateConfigurationWithData();
    void updateDateScbUart();
    void updateDateScbSpi();
    void updateDateScbI2c();
    void readUiUartData();
    void readUiSpiData();
    void readUiI2cData();
    void layoutWidget();
    void spiConfigDlg_setControlsEnabled(Cy::CY_SPI_PROTOCOL protocol);
    void i2cConfigDlg_setControlsEnabled(Cy::CyUsbBoolField master);

private slots:
    void onModeChanged(int index);
    void onTypeChanged(int index);
    void onSpiMasterChanged(int index);
    void onSpiProtocolChanged(int index);
    void oni2CMasterChanged(int index);
    void onClickedOk();
    void onDefaultConfiguration();



};
#endif // CONFIGURATIONDLG_H
