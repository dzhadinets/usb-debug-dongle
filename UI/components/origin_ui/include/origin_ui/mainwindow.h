#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "appstate.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_useCypressIDs_stateChanged(int arg1);

    void on_useSerialNumberStr_stateChanged(int arg1);

    void on_scb0Mode_currentIndexChanged(int index);

    void on_scb1Mode_currentIndexChanged(int index);

    void on_configureScb0Mode_pressed();

    void on_remoteWakeupConfigBtn_pressed();

    void on_configScb0LedBtn_pressed();

    void on_configScb1LedBtn_pressed();

    void on_configureScb1Mode_pressed();

    void on_enableScb1_stateChanged(int arg1);

    void on_configureCapsense_pressed();

    void on_bcdConfig_pressed();

    void on_unusedGpioConfig_pressed();

    void on_scb0Protocol_currentIndexChanged(int index);

    void on_scb1Protocol_currentIndexChanged(int index);

    void on_actionLoad_from_file_triggered();

    void on_actionSave_to_file_triggered();

    void on_actionLoad_from_device_triggered();

    void on_actionSave_to_device_triggered();

private:
    using ScbNum = ScbNum::ScbNum;
    void defaultInit();
    void updateMainWindowWithData();
    void readMainWindowData();
    void loadFile(QString fileName);
    void saveFile(QString filename);
    void showScbModeConfigDlg(ScbNum scbNum);
    void showScbLedConfigDlg(ScbNum scbNum);
   // static const QVector<Cy::CY_SCB_MODE> allowedScbModes;
   // static const QMap<Cy::CY_SCB_MODE, QVector<Cy::CY_SCB_PROTOCOL>> allowedScbProtocols;

    AppState state;
    Cy::UsbDeviceConfig &config = state.config;
    GpioState &gpioState = state.gpioState;

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
