#ifndef SPICONFIGDLG_H
#define SPICONFIGDLG_H

#include <QDialog>

#include "appstate.h"

namespace Ui {
class SpiConfigDlg;
}

class SpiConfigDlg : public QDialog
{
    Q_OBJECT;

    using ScbNum = ScbNum::ScbNum;

public:
    explicit SpiConfigDlg(AppState &state, ScbNum scbNum, QWidget *parent = nullptr);
    ~SpiConfigDlg();

private slots:
    void on_spiMode_currentIndexChanged(int index);

    void on_protocol_currentIndexChanged(int index);

private:
    void updateUiWithData();
    void readUiData();
    void spiConfigDlg_setControlsEnabled(Cy::CY_SPI_PROTOCOL protocol);

    AppState &state;
    ScbNum scbNum;
    Ui::SpiConfigDlg *ui;
};

#endif // SPICONFIGDLG_H
