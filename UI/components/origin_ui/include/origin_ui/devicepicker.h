#ifndef DEVICEPICKER_H
#define DEVICEPICKER_H

#include <QDialog>

#include <cyusbcom.h>

namespace Ui {
class DevicePicker;
}

class DevicePicker : public QDialog
{
    Q_OBJECT

public:
    explicit DevicePicker(CyDevice::ConfigBlob &config, bool writeConfig, QWidget *parent = nullptr);
    ~DevicePicker();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::DevicePicker *ui;
    CyDeviceManager _devmgr;
    CyDevice::ConfigBlob &_config;
    bool _writeConfig;
};

#endif // DEVICEPICKER_H
