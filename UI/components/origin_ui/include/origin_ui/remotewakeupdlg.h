#ifndef REMOTEWAKEUPDLG_H
#define REMOTEWAKEUPDLG_H

#include <QDialog>

#include "appstate.h"

namespace Ui {
class RemoteWakeupDlg;
}

class RemoteWakeupDlg : public QDialog
{
    Q_OBJECT;

public:
    explicit RemoteWakeupDlg(AppState &state, QWidget *parent = nullptr);
    ~RemoteWakeupDlg();

private slots:
    void on_powerNumber_currentIndexChanged(int index);

private:
    void updateUiWithData();
    void readUiData();

    AppState &state;
    Ui::RemoteWakeupDlg *ui;
};

#endif // REMOTEWAKEUPDLG_H
