#ifndef UARTCONFIGUREDLG_H
#define UARTCONFIGUREDLG_H

#include <QDialog>

#include "appstate.h"

namespace Ui {
class UartConfigureDlg;
}

class UartConfigureDlg : public QDialog
{
    Q_OBJECT;

    using ScbNum = ScbNum::ScbNum;

public:
    explicit UartConfigureDlg(AppState &state, ScbNum scbNum, QWidget *parent = nullptr);
    ~UartConfigureDlg();

private slots:
    void on_uartType_currentIndexChanged(int index);

    void on_enableRs485_stateChanged(int arg1);

    void on_uartRs485_currentIndexChanged(int index);

private:
    void updateUiWithData();
    void readUiData();

    AppState &state;
    ScbNum scbNum;
    Ui::UartConfigureDlg *ui;
};

#endif // UARTCONFIGUREDLG_H
