#ifndef APPSTATE_H
#define APPSTATE_H

#include <common/gpiostate.h>
#include <common/CyUSB.h>

struct AppState
{
    using ScbNum = ScbNum::ScbNum;

    Cy::UsbDeviceConfig config;
    GpioState gpioState;

    void disableCapsenseConfig();
    void disableBcdConfig();
    void disableGPOIUsers(const QSet<GpioState::State> &toDisable);
    void disableGpioDrives();

    void updateUsedScbGPIOs(ScbNum scbNum, Cy::CY_SCB_MODE newScbMode, Cy::CY_UART_TYPE uartType = {});
    void enableScb1(bool enable);

    void initGPIOs();
};

#endif // APPSTATE_H
