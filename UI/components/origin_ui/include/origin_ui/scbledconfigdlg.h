#ifndef SCBLEDCONFIGDLG_H
#define SCBLEDCONFIGDLG_H

#include <QDialog>

#include "appstate.h"

namespace Ui {
class ScbLedConfigDlg;
}

class ScbLedConfigDlg : public QDialog
{
    Q_OBJECT

    using ScbNum = ScbNum::ScbNum;

public:
    explicit ScbLedConfigDlg(AppState &state, ScbNum scbNum, QWidget *parent = nullptr);
    ~ScbLedConfigDlg();

private slots:
    void on_rxLed_currentIndexChanged(int index);

    void on_txLed_currentIndexChanged(int index);

    void on_rxTxLed_currentIndexChanged(int index);

private:
    void updateUiWithData();

    AppState &state;
    ScbNum scbNum;
    Ui::ScbLedConfigDlg *ui;
};

#endif // SCBLEDCONFIGDLG_H
