#ifndef CAPSENSECONFIGDLG_H
#define CAPSENSECONFIGDLG_H

#include <QDialog>

#include "appstate.h"

namespace Ui {
class CapsenseConfigDlg;
}

class CapsenseConfigDlg;

class ButtonHandler : public QObject
{
    Q_OBJECT;

public:
    void setFields(CapsenseConfigDlg *window, uint buttonIndex)
    {
        this->window = window;
        this->buttonIndex = buttonIndex;
    }
public slots:
    void senseChanged(int);
    void ledChanged(int);
    void sensitivityChanged(int);

private:
    CapsenseConfigDlg *window;
    uint buttonIndex;
};

class CapsenseConfigDlg : public QDialog
{
    Q_OBJECT;

public:
    explicit CapsenseConfigDlg(AppState &state, QWidget *parent = nullptr);
    ~CapsenseConfigDlg();

private slots:
    void on_enable_stateChanged(int arg1);

    void on_waterShield_currentIndexChanged(int index);

    void on_ledCommon_toggled(bool checked);

    void on_ledNone_toggled(bool checked);

    void on_ledIndividual_toggled(bool checked);

    void on_commonLed_currentIndexChanged(int index);

    void on_buttonsNeeded_currentIndexChanged(int index);

    void on_outLine0_currentIndexChanged(int index);

    void on_outLine1_currentIndexChanged(int index);

    void on_outLine2_currentIndexChanged(int index);

    void on_outLine3_currentIndexChanged(int index);

private:
    void updateUiWithData();
    void readUiData();
    void enableButtons(uint buttonsCount, bool disableLedColumn);
    void capSenseChangeGPIO(Cy::GPIO &gpio, Cy::GPIO newGpio, GpioState::State newState);

    bool dialogUpdating = false;
    ButtonHandler btnHandlers[CAPSENSE_BUTTONS_COUNT];
    // ButtonHandler are logically a part of this class
    friend class ButtonHandler;

    AppState &state;
    Ui::CapsenseConfigDlg *ui;
};

#endif // CAPSENSECONFIGDLG_H
