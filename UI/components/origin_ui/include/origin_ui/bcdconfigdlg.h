#ifndef BCDCONFIGDLG_H
#define BCDCONFIGDLG_H

#include <QDialog>
#include "appstate.h"

namespace Ui {
class BcdConfigDlg;
}

class BcdConfigDlg : public QDialog
{
    Q_OBJECT;

public:
    explicit BcdConfigDlg(AppState &state, QWidget *parent = nullptr);
    ~BcdConfigDlg();

private slots:

    void on_enableBcd_stateChanged(int arg1);

    void on_bcd0_currentIndexChanged(int index);

    void on_bcd1_currentIndexChanged(int index);

    void on_busDetect_currentIndexChanged(int index);

private:
    void updateUiWithData();
    void readUiData();
    void bcdChangeGPIO(Cy::GPIO &gpio, Cy::GPIO newGpioValue, GpioState::State newState);
    void disableBcdConfig();

    AppState &state;
    Ui::BcdConfigDlg *ui;
};

#endif // BCDCONFIGDLG_H
