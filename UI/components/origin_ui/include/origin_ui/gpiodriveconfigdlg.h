#ifndef GPIODRIVECONFIGDLG_H
#define GPIODRIVECONFIGDLG_H

#include <QDialog>

#include "appstate.h"

namespace Ui {
class GpioDriveConfigDlg;
}

class GpioDriveConfigDlg : public QDialog
{
    Q_OBJECT;

public:
    explicit GpioDriveConfigDlg(AppState &state, QWidget *parent = nullptr);
    ~GpioDriveConfigDlg();

private:
    enum DriveState
    {
        TRISTATE,
        DRIVE_0,
        DRIVE_1,
        INPUT
    };
    void updateUiWithData();
    void readUiData();

    AppState &state;
    Ui::GpioDriveConfigDlg *ui;
};

#endif // GPIODRIVECONFIGDLG_H
