#ifndef I2CCONFIGDLG_H
#define I2CCONFIGDLG_H

#include <QDialog>

#include <common/CyUSB.h>
#include "appstate.h"

namespace Ui {
class I2CConfigDlg;
}

class I2CConfigDlg : public QDialog
{
    Q_OBJECT;

    using ScbNum = ScbNum::ScbNum;

public:
    explicit I2CConfigDlg(AppState &state, ScbNum scbNum, QWidget *parent = nullptr);
    ~I2CConfigDlg();

private slots:
    void on_master_currentIndexChanged(int index);

private:
    void updateUiWithData();
    void readUiData();
    void i2cConfigDlg_setControlsEnabled(Cy::CyUsbBoolField master);

    ScbNum scbNum;
    AppState &state;
    Ui::I2CConfigDlg *ui;
};

#endif // I2CCONFIGDLG_H
