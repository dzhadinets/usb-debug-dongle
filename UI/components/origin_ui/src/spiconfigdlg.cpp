#include "origin_ui/spiconfigdlg.h"
#include "ui_spiconfigdlg.h"

#include <common/commonutilities.h>
#include <common/enumsregistration.h>

using namespace Cy;

SpiConfigDlg::SpiConfigDlg(AppState &state, ScbNum scbNum, QWidget *parent)
    : QDialog(parent)
    , state(state)
    , scbNum(scbNum)
    , ui(new Ui::SpiConfigDlg)
{
    ui->setupUi(this);
    setFixedSize(size());
    updateUiWithData();
}

SpiConfigDlg::~SpiConfigDlg()
{
    readUiData();
    delete ui;
}

void SpiConfigDlg::updateUiWithData()
{
    const CyUsbScbConfig &scb = state.config.scb[scbNum];
    const CyUsbScbConfig::ModeConfiguration &modeConfig = scb.modeConfiguration;
    const CyUsbSpiConfig &spiConfig = modeConfig.spi;

    ui->frequency->setValue(spiConfig.frequency);
    fillComboBoxWithEnum<CY_SPI_DATA_WIDTH>(ui->dataWidth, spiConfig.dataWidth, spiDataWidth);
    fillComboBoxWithEnum<CY_SPI_TYPE>(ui->spiMode, spiConfig.spiMaster, spiType);
    fillComboBoxWithEnum<CY_SPI_PROTOCOL>(ui->protocol, spiConfig.protocol, spiProtocol);
    fillComboBoxWithEnum<CyUsbLowHighField>(ui->cphaMode, spiConfig.cpha, usbLowHighField);
    fillComboBoxWithEnum<CyUsbLowHighField>(ui->cpolMode, spiConfig.cpol, usbLowHighField);
    fillComboBoxWithEnum<CyUsbSsnToggleField>(ui->ssnToggleMode, spiConfig.ssnToggleMode, usbSsnToggleField);
    fillComboBoxWithEnum<CY_BIT_POS>(ui->bitOrder, spiConfig.bitPosition, bitPos);

    ui->useAsWakeupSource->setEnabled(spiConfig.spiMaster == CY_SPI_TYPE::Slave);
    spiConfigDlg_setControlsEnabled(spiConfig.protocol);

    ui->useAsWakeupSource->setChecked(state.config.usbConfig->remoteWakeup & WAKEUP_SOURCE_SPI_SLAVE);
    ui->enableSelectPrecede->setChecked(spiConfig.selectPrecede == CyUsbBoolField::TRUE);
}

void SpiConfigDlg::readUiData()
{
    CyUsbScbConfig &scb = state.config.scb[scbNum];
    CyUsbScbConfig::ModeConfiguration &modeConfig = scb.modeConfiguration;
    CyUsbSpiConfig &spiConfig = modeConfig.spi;

    spiConfig.frequency = ui->frequency->value();

    spiConfig.dataWidth = static_cast<CY_SPI_DATA_WIDTH>(ui->dataWidth->currentData().toUInt());
    spiConfig.spiMaster = static_cast<CY_SPI_TYPE>(ui->spiMode->currentData().toUInt());
    spiConfig.protocol = static_cast<CY_SPI_PROTOCOL>(ui->protocol->currentData().toUInt());
    spiConfig.cpha = static_cast<CyUsbLowHighField>(ui->cphaMode->currentData().toUInt());
    spiConfig.cpol = static_cast<CyUsbLowHighField>(ui->cpolMode->currentData().toUInt());
    spiConfig.ssnToggleMode = static_cast<CyUsbSsnToggleField>(ui->ssnToggleMode->currentData().toUInt());
    spiConfig.bitPosition = static_cast<CY_BIT_POS>(ui->bitOrder->currentData().toUInt());

    setBit(state.config.usbConfig->remoteWakeup, WAKEUP_SOURCE_SPI_SLAVE, ui->useAsWakeupSource->isChecked());

    spiConfig.selectPrecede = toBoolField(ui->enableSelectPrecede->isChecked());
}

void SpiConfigDlg::spiConfigDlg_setControlsEnabled(CY_SPI_PROTOCOL protocol)
{
    switch (protocol)
    {
    case CY_SPI_PROTOCOL::Motorola:
        ui->cphaMode->setEnabled(true);
        ui->cpolMode->setEnabled(true);
        ui->ssnToggleMode->setEnabled(true);
        ui->enableSelectPrecede->setEnabled(false);
        break;

    case CY_SPI_PROTOCOL::TI:
        ui->cphaMode->setEnabled(false);
        ui->cpolMode->setEnabled(false);
        ui->ssnToggleMode->setEnabled(false);
        ui->enableSelectPrecede->setEnabled(true);
        break;

    case CY_SPI_PROTOCOL::NS:
        ui->cphaMode->setEnabled(false);
        ui->cpolMode->setEnabled(false);
        ui->ssnToggleMode->setEnabled(true);
        ui->enableSelectPrecede->setEnabled(false);
        break;
    }
}

void SpiConfigDlg::on_spiMode_currentIndexChanged(int)
{
    bool slave = static_cast<CyUsbBoolField>(ui->spiMode->currentData().toUInt()) == CyUsbBoolField::FALSE;
    ui->useAsWakeupSource->setEnabled(slave);
    if (!slave)
    {
        ui->useAsWakeupSource->setChecked(false);
    }
}

void SpiConfigDlg::on_protocol_currentIndexChanged(int)
{
    CY_SPI_PROTOCOL protocol = static_cast<CY_SPI_PROTOCOL>(ui->protocol->currentData().toUInt());
    spiConfigDlg_setControlsEnabled(protocol);
}
