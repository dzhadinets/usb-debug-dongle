#include "origin_ui/devicepicker.h"
#include "ui_devicepicker.h"

DevicePicker::DevicePicker(CyDevice::ConfigBlob &config, bool writeConfig, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::DevicePicker)
    , _config(config)
    , _writeConfig(writeConfig)
{
    ui->setupUi(this);

    ui->deviceList->clear();
    const auto &devs = _devmgr.devices();
    ui->deviceList->setEnabled(devs.size() > 0);
    for (size_t i = 0; i < devs.size(); ++i)
    {
        ui->deviceList->addItem(_devmgr.friendlyName(devs[i]), i);
    }
}

DevicePicker::~DevicePicker()
{
    delete ui;
}

void DevicePicker::on_buttonBox_accepted()
{
    if (ui->deviceList->isEnabled())
    {
        size_t selectedIndex = ui->deviceList->currentData().value<size_t>();
        try
        {
            CyDevice dev(_devmgr.devices().at(selectedIndex));
            if (_writeConfig)
            {
                dev.writeConfig(_config);
            }
            else
            {
                _config = dev.readConfig();
            }
        }
        catch (std::exception &ex)
        {
            qDebug(ex.what());
        }
    }
}
