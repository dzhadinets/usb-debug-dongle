#include <QComboBox>

#include "origin_ui/gpiodriveconfigdlg.h"
#include "ui_gpiodriveconfigdlg.h"

#include <common/CyUSB.h>
#include <common/enumsregistration.h>

using namespace Cy;

GpioDriveConfigDlg::GpioDriveConfigDlg(AppState &state, QWidget *parent)
    : QDialog(parent)
    , state(state)
    , ui(new Ui::GpioDriveConfigDlg)
{
    ui->setupUi(this);
    setFixedSize(size());
    updateUiWithData();
}

GpioDriveConfigDlg::~GpioDriveConfigDlg()
{
    readUiData();
    delete ui;
}

void GpioDriveConfigDlg::updateUiWithData()
{
    QVector<GPIO> unusedGpios = state.gpioState.getUnusedGPIOs();
    auto table = ui->table;
    table->setHorizontalHeaderLabels({"Unused GPIOs", "Select Drive Mode"});
    table->setRowCount(unusedGpios.size());

    const char *statesNames[] = {"Tristate", "Drive 0", "Drive 1", "Input"};

    for (int row = 0; row < unusedGpios.size(); ++row)
    {
        auto value = static_cast<int>(unusedGpios[row]);
        auto key = gpio.at(unusedGpios[row]);
        uint gpioFlag = 1 << value;

        QComboBox *combo = new QComboBox(this);

        for (int i = TRISTATE; i <= INPUT; ++i)
        {
            combo->addItem(statesNames[i], i);
        }

        if (gpioFlag & state.config.drive0)
        {
            combo->setCurrentIndex(DRIVE_0);
        }
        else if (gpioFlag & state.config.drive1)
        {
            combo->setCurrentIndex(DRIVE_1);
        }
        else if (gpioFlag & state.config.input)
        {
            combo->setCurrentIndex(INPUT);
        }
        else
        {
            combo->setCurrentIndex(TRISTATE);
        }

        table->setItem(row, 0, new QTableWidgetItem(QString::fromStdString(key)));
        table->setCellWidget(row, 1, combo);
    }
    table->resizeColumnsToContents();
}

void GpioDriveConfigDlg::readUiData()
{
    auto &config = state.config;
    auto &gpioState = state.gpioState;

    auto unusedGpios = state.gpioState.getUnusedGPIOs();
    auto table = ui->table;

    config.drive0 = config.drive1 = config.input = 0;

    for (int i = 0; i < unusedGpios.size(); ++i)
    {
        QComboBox *combo = static_cast<QComboBox *>(table->cellWidget(i, 1));
        auto selectedDrive = static_cast<DriveState>(combo->currentData().toUInt());
        uint gpioFlag = 1 << static_cast<uint>(unusedGpios[i]);

        switch (selectedDrive)
        {
        case TRISTATE:
            gpioState.unuse(unusedGpios[i]);
            break;

        case DRIVE_0:
            gpioState.use(unusedGpios[i], GpioState::DRIVE);
            config.drive0 |= gpioFlag;
            break;

        case DRIVE_1:
            gpioState.use(unusedGpios[i], GpioState::DRIVE);
            config.drive1 |= gpioFlag;
            break;

        case INPUT:
            gpioState.use(unusedGpios[i], GpioState::DRIVE);
            config.input |= gpioFlag;
            break;
        }
    }
}
