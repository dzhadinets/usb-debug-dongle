#include "gpiostate.h"
#include "enumsregistration.h"

#include <QtCore>

using namespace Cy;

GpioState::State GpioState::SCB_UART_RS485[] = {GpioState::SCB0_UART_RS485, GpioState::SCB1_UART_RS485};

GpioState::State GpioState::SCB_RxLED[] = {GpioState::SCB0_RxLED, GpioState::SCB1_RxLED};

GpioState::State GpioState::SCB_TxLED[] = {GpioState::SCB0_TxLED, GpioState::SCB1_TxLED};

GpioState::State GpioState::SCB_RxTxLED[] = {GpioState::SCB0_RxTxLED, GpioState::SCB1_RxTxLED};

GpioState::State GpioState::CAPSENCE_BTN[] = {
    GpioState::CAPSENCE_BTN0, GpioState::CAPSENCE_BTN1, GpioState::CAPSENCE_BTN2, GpioState::CAPSENCE_BTN3,
    GpioState::CAPSENCE_BTN4, GpioState::CAPSENCE_BTN5, GpioState::CAPSENCE_BTN6, GpioState::CAPSENCE_BTN7};

GpioState::State GpioState::CAPSENCE_LED_BTN[] = {GpioState::CAPSENCE_LED_BTN0, GpioState::CAPSENCE_LED_BTN1,
                                                  GpioState::CAPSENCE_LED_BTN2, GpioState::CAPSENCE_LED_BTN3,
                                                  GpioState::CAPSENCE_LED_BTN4, GpioState::CAPSENCE_LED_BTN5,
                                                  GpioState::CAPSENCE_LED_BTN6, GpioState::CAPSENCE_LED_BTN7};

GpioState::State GpioState::CAPSENCE_OUT_LINE[] = {GpioState::CAPSENCE_OUT_LINE0, GpioState::CAPSENCE_OUT_LINE1,
                                                   GpioState::CAPSENCE_OUT_LINE2, GpioState::CAPSENCE_OUT_LINE3};

QSet<GpioState::State> GpioState::walk(ScbNum::ScbNum scbNum, CY_SCB_MODE mode, CY_UART_TYPE uartType, State newState)
{
    QSet<GpioState::State> toKick;

    auto reset = [this, newState, &toKick](GPIO gpio) {
        // if we change GPIO's state, we should kick those who used it before
        GpioState &currentState = *this;
        if (currentState[gpio] != GpioState::Free && currentState[gpio] != newState)
        {
            toKick.insert(currentState[gpio]);
        }
        currentState[gpio] = newState;
    };

    // each SCB0 and SCB1 use it's own pins depending on mode
    // and UART type (if mode == UART)
    // for details see CY7C65215A docs (https://www.cypress.com/file/129956/download)
    switch (scbNum)
    {
    // SCB0
    case ScbNum::SCB0:
        switch (mode)
        {
        case CY_SCB_MODE::UART:
            switch (uartType)
            {
            case CY_UART_TYPE::PIN_6:
                reset(GPIO::GPIO_02);
                reset(GPIO::GPIO_09);
                // fall through

            case CY_UART_TYPE::PIN_4:
                reset(GPIO::GPIO_03);
                reset(GPIO::GPIO_04);
                // fall through

            case CY_UART_TYPE::PIN_2:
                reset(GPIO::GPIO_05);
                reset(GPIO::GPIO_08);
                // fall through
            }

            break; // mode == CY_SCB_MODE::UART

        // case CY_SCB_MODE::JTAG: // probably unused
        case CY_SCB_MODE::SPI:
            reset(GPIO::GPIO_02);
            reset(GPIO::GPIO_05);
            // fall through

        case CY_SCB_MODE::I2C:
            reset(GPIO::GPIO_03);
            reset(GPIO::GPIO_04);
            break;
        }

        break; // case scbNum == ScbNum::SCB0

    // SCB1
    case ScbNum::SCB1:
        switch (mode)
        {
        case CY_SCB_MODE::UART:
            switch (uartType)
            {
            case CY_UART_TYPE::PIN_6:
                reset(GPIO::GPIO_15);
                reset(GPIO::GPIO_14);
                // fall through

            case CY_UART_TYPE::PIN_4:
                reset(GPIO::GPIO_13);
                reset(GPIO::GPIO_12);
                // fall through

            case CY_UART_TYPE::PIN_2:
                reset(GPIO::GPIO_11);
                reset(GPIO::GPIO_10);
                // fall through
            }
            break; // mode == CY_SCB_MODE::UART

        // case CY_SCB_MODE::JTAG: // probably unused
        case CY_SCB_MODE::SPI:
            reset(GPIO::GPIO_13);
            reset(GPIO::GPIO_12);
            // fall through

        case CY_SCB_MODE::I2C:
            reset(GPIO::GPIO_11);
            reset(GPIO::GPIO_10);
            break;
        }

        break; // case scbNum == ScbNum::SCB1
    }

    return toKick;
}

std::function<bool(Cy::GPIO)> GpioState::freeFilter(GpioState::State includeState, bool includeNone) const
{
    return [this, includeState, includeNone](Cy::GPIO gpio) {
        if (!includeNone && gpio == Cy::GPIO::None)
        {
            return false;
        }
        return includeState == (*this)[gpio] || isGPIOFree(gpio);
    };
}

void GpioState::unuse(ScbNum::ScbNum scbNum, CY_SCB_MODE mode, CY_UART_TYPE uartType)
{
    walk(scbNum, mode, uartType, GpioState::Free);
}

QSet<GpioState::State> GpioState::use(ScbNum::ScbNum scbNum, Cy::CY_SCB_MODE mode, State newState,
                                      Cy::CY_UART_TYPE uartType)
{
    return walk(scbNum, mode, uartType, newState);
}

void GpioState::changeGPIO(Cy::GPIO &gpio, Cy::GPIO newGpioValue, State newState)
{
    unuse(gpio);
    gpio = newGpioValue;
    use(gpio, newState);
}

QVector<GPIO> GpioState::getUnusedGPIOs()
{
    auto filter = freeFilter(GpioState::DRIVE, false);
    QVector<GPIO> unusedGpios;
    for (const auto &[key, value] : gpio)
    {
        if (filter(key))
        {
            unusedGpios.push_back(key);
        }
    }

    return unusedGpios;
}
