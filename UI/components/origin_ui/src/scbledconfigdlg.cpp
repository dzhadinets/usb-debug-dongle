#include "origin_ui/scbledconfigdlg.h"
#include "ui_scbledconfigdlg.h"

#include <common/commonutilities.h>
#include <common/CyUSB.h>
#include <common/enumsregistration.h>

using namespace Cy;

ScbLedConfigDlg::ScbLedConfigDlg(AppState &state, ScbNum scbNum, QWidget *parent)
    : QDialog(parent)
    , state(state)
    , scbNum(scbNum)
    , ui(new Ui::ScbLedConfigDlg)
{
    ui->setupUi(this);
    setFixedSize(size());
    updateUiWithData();
}

ScbLedConfigDlg::~ScbLedConfigDlg()
{
    delete ui;
}

void ScbLedConfigDlg::updateUiWithData()
{
    auto &scb = state.config.scb[scbNum];
    auto &gpioState = state.gpioState;

    fillComboBoxWithEnum(ui->rxLed, scb->ledActivity->rxLed, gpio, gpioState.freeFilter(GpioState::SCB_RxLED[scbNum]));
    fillComboBoxWithEnum(ui->txLed, scb->ledActivity->txLed, gpio, gpioState.freeFilter(GpioState::SCB_TxLED[scbNum]));
    fillComboBoxWithEnum(ui->rxTxLed, scb->ledActivity->rxAndTxLed, gpio,
                         gpioState.freeFilter(GpioState::SCB_RxTxLED[scbNum]));
}

void ScbLedConfigDlg::on_rxLed_currentIndexChanged(int)
{
    state.gpioState.changeGPIO(state.config.scb[scbNum]->ledActivity->rxLed,
                               static_cast<GPIO>(ui->rxLed->currentData().toUInt()), GpioState::SCB_RxLED[scbNum]);
    updateUiWithData();
}

void ScbLedConfigDlg::on_txLed_currentIndexChanged(int)
{
    state.gpioState.changeGPIO(state.config.scb[scbNum]->ledActivity->txLed,
                               static_cast<GPIO>(ui->txLed->currentData().toUInt()), GpioState::SCB_TxLED[scbNum]);
    updateUiWithData();
}

void ScbLedConfigDlg::on_rxTxLed_currentIndexChanged(int)
{
    state.gpioState.changeGPIO(state.config.scb[scbNum]->ledActivity->rxAndTxLed,
                               static_cast<GPIO>(ui->rxTxLed->currentData().toUInt()), GpioState::SCB_RxTxLED[scbNum]);
    updateUiWithData();
}
