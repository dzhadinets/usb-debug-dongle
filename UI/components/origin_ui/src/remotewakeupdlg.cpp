#include "origin_ui/remotewakeupdlg.h"
#include "ui_remotewakeupdlg.h"

#include <common/commonutilities.h>
#include <common/CyUSB.h>
#include <common/enumsregistration.h>

using namespace Cy;

RemoteWakeupDlg::RemoteWakeupDlg(AppState &state, QWidget *parent)
    : QDialog(parent)
    , state(state)
    , ui(new Ui::RemoteWakeupDlg)
{
    ui->setupUi(this);
    setFixedSize(size());
    updateUiWithData();
}

RemoteWakeupDlg::~RemoteWakeupDlg()
{
    readUiData();
    delete ui;
}

void RemoteWakeupDlg::updateUiWithData()
{
    const CyUsbConfig &usbConfig = state.config.usbConfig;

    ui->enableRemoteWakeup->setChecked(usbConfig.remoteWakeup & WAKEUP_SOURCE_GPIO);
    ui->invertRemoteWakeup->setChecked(usbConfig.linePolarity & POLARITY_INVERT_REMOTE_WAKEUP);

    ui->enableSuspend->setChecked(usbConfig.suspend != 0xff);
    ui->invertSuspend->setChecked(usbConfig.linePolarity & POLARITY_INVERT_SUSPEND);

    fillComboBoxWithEnum<GPIO>(ui->powerNumber, state.config.powerGpio, gpio,
                               state.gpioState.freeFilter(GpioState::WAKEUP_POWER));
}

void RemoteWakeupDlg::readUiData()
{
    CyUsbConfig &usbConfig = state.config.usbConfig;

    setBit(usbConfig.remoteWakeup, WAKEUP_SOURCE_GPIO, ui->enableRemoteWakeup->isChecked());

    uint polarity = 0;
    if (ui->invertRemoteWakeup->isChecked())
    {
        polarity |= POLARITY_INVERT_REMOTE_WAKEUP;
    }
    if (ui->invertSuspend->isChecked())
    {
        polarity |= POLARITY_INVERT_SUSPEND;
    }
    usbConfig.linePolarity = polarity;

    usbConfig.suspend = ui->enableSuspend->isChecked() ? 0x13 : 0xff;

    state.config.powerGpio = static_cast<GPIO>(ui->powerNumber->currentData().toUInt());
}

void RemoteWakeupDlg::on_powerNumber_currentIndexChanged(int)
{
    state.gpioState.changeGPIO(state.config.powerGpio, static_cast<GPIO>(ui->powerNumber->currentData().toUInt()),
                               GpioState::WAKEUP_POWER);
}
