#include "origin_ui/bcdconfigdlg.h"
#include "ui_bcdconfigdlg.h"

#include <common/gpiostate.h>
#include <common/commonutilities.h>
#include <common/enumsregistration.h>

using namespace Cy;

BcdConfigDlg::BcdConfigDlg(AppState &state, QWidget *parent)
    : QDialog(parent)
    , state(state)
    , ui(new Ui::BcdConfigDlg)
{
    ui->setupUi(this);
    setFixedSize(size());
    updateUiWithData();
}

BcdConfigDlg::~BcdConfigDlg()
{
    // if the data is not consistent (i.e. not all GPIOs are filled)
    // we cannot save such configuration
    CyUsbBcdConfig &bcd = state.config.bcdConfig;
    bool consistent = bcd.bcd[0] != GPIO::None && bcd.bcd[1] != GPIO::None && bcd.busDetect != GPIO::None;
    if (consistent)
    {
        readUiData();
    }
    else
    {
        state.disableBcdConfig();
    }

    delete ui;
}

void BcdConfigDlg::updateUiWithData()
{
    const CyUsbBcdConfig &bcd = state.config.bcdConfig;
    auto &gpioState = state.gpioState;

    bool enabled = bcd.bcd[0] != GPIO::None || bcd.bcd[1] != GPIO::None || bcd.busDetect != GPIO::None;

    ui->enableBcd->setChecked(enabled);
    ui->bcdConfigBox->setEnabled(enabled);

    fillComboBoxWithEnum(ui->bcd0, bcd.bcd[0], gpio, gpioState.freeFilter(GpioState::BCD0));
    fillComboBoxWithEnum(ui->bcd1, bcd.bcd[1], gpio, gpioState.freeFilter(GpioState::BCD1));
    fillComboBoxWithEnum(ui->busDetect, bcd.busDetect, gpio, gpioState.freeFilter(GpioState::BUS_DETECT));

    const CyUsbBcdDriveConfig &bcdDrive = state.config.bcdDriveConfig;

    fillComboBoxWithEnum(ui->unconfigSdp0, toLowHighField(bcdDrive.unconfiguredSdp & DRIVE_BCD0_HIGH), usbLowHighField);
    fillComboBoxWithEnum(ui->unconfigSdp1, toLowHighField(bcdDrive.unconfiguredSdp & DRIVE_BCD1_HIGH), usbLowHighField);

    fillComboBoxWithEnum(ui->configSdp0, toLowHighField(bcdDrive.configuredSdp & DRIVE_BCD0_HIGH), usbLowHighField);
    fillComboBoxWithEnum(ui->configSdp1, toLowHighField(bcdDrive.configuredSdp & DRIVE_BCD1_HIGH), usbLowHighField);

    fillComboBoxWithEnum(ui->cdpDcpCharging0, toLowHighField(bcdDrive.dcpCharging & DRIVE_BCD0_HIGH), usbLowHighField);
    fillComboBoxWithEnum(ui->cdpDcpCharging1, toLowHighField(bcdDrive.dcpCharging & DRIVE_BCD1_HIGH), usbLowHighField);

    fillComboBoxWithEnum(ui->usbSuspend0, toLowHighField(bcdDrive.suspendOrFail & DRIVE_BCD0_HIGH), usbLowHighField);
    fillComboBoxWithEnum(ui->usbSuspend1, toLowHighField(bcdDrive.suspendOrFail & DRIVE_BCD1_HIGH), usbLowHighField);
}

void BcdConfigDlg::readUiData()
{
    CyUsbBcdDriveConfig &bcdDrive = state.config.bcdDriveConfig;

    auto isHighSelected = [](QComboBox *box) {
        return static_cast<CyUsbLowHighField>(box->currentData().toUInt()) == CyUsbLowHighField::High;
    };

    setBit(bcdDrive.unconfiguredSdp, DRIVE_BCD0_HIGH, isHighSelected(ui->unconfigSdp0));
    setBit(bcdDrive.unconfiguredSdp, DRIVE_BCD1_HIGH, isHighSelected(ui->unconfigSdp1));

    setBit(bcdDrive.configuredSdp, DRIVE_BCD0_HIGH, isHighSelected(ui->configSdp0));
    setBit(bcdDrive.configuredSdp, DRIVE_BCD1_HIGH, isHighSelected(ui->configSdp1));

    setBit(bcdDrive.dcpCharging, DRIVE_BCD0_HIGH, isHighSelected(ui->cdpDcpCharging0));
    setBit(bcdDrive.dcpCharging, DRIVE_BCD1_HIGH, isHighSelected(ui->cdpDcpCharging1));

    setBit(bcdDrive.suspendOrFail, DRIVE_BCD0_HIGH, isHighSelected(ui->usbSuspend0));
    setBit(bcdDrive.suspendOrFail, DRIVE_BCD1_HIGH, isHighSelected(ui->usbSuspend1));
}

void BcdConfigDlg::on_enableBcd_stateChanged(int checked)
{
    bool enabled = checked == Qt::Checked;
    ui->bcdConfigBox->setEnabled(enabled);
    if (!enabled)
    {
        state.disableBcdConfig();
    }
}

void BcdConfigDlg::bcdChangeGPIO(GPIO &gpio, GPIO newGpioValue, GpioState::State newState)
{
    readUiData();
    state.gpioState.changeGPIO(gpio, newGpioValue, newState);
    updateUiWithData();
}

void BcdConfigDlg::on_bcd0_currentIndexChanged(int)
{
    bcdChangeGPIO(state.config.bcdConfig->bcd[0], static_cast<GPIO>(ui->bcd0->currentData().toUInt()), GpioState::BCD0);
}

void BcdConfigDlg::on_bcd1_currentIndexChanged(int)
{
    bcdChangeGPIO(state.config.bcdConfig->bcd[1], static_cast<GPIO>(ui->bcd1->currentData().toUInt()), GpioState::BCD1);
}

void BcdConfigDlg::on_busDetect_currentIndexChanged(int)
{
    bcdChangeGPIO(state.config.bcdConfig->busDetect, static_cast<GPIO>(ui->busDetect->currentData().toUInt()),
                  GpioState::BUS_DETECT);
}
