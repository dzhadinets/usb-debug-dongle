#include "origin_ui/appstate.h"
#include "common/commonutilities.h"

using namespace Cy;

void AppState::disableCapsenseConfig()
{
    CyUsbCapsenseConfig &capsenseCfg = config.capSenseConfiguration;

    config.capSenseConfiguration->enable = CyUsbBoolField::FALSE;

    gpioState.changeGPIO(capsenseCfg.waterShieldIO, GPIO::None, GpioState::Free);
    gpioState.changeGPIO(capsenseCfg.commonLedIo, GPIO::None, GpioState::Free);

    for (uint i = 0; i < capsenseCfg.buttonCount; ++i)
    {
        gpioState.changeGPIO(capsenseCfg.buttons[i]->senseGpio, GPIO::None, GpioState::Free);
        gpioState.changeGPIO(capsenseCfg.buttons[i]->outputLedGpio, GPIO::None, GpioState::Free);
    }

    for (uint i = 0, j = 1; j <= capsenseCfg.buttonCount; ++i, j <<= 1)
    {
        gpioState.changeGPIO(capsenseCfg.encodedGpio[i], GPIO::None, GpioState::Free);
    }

    capsenseCfg.buttonCount = 0;
    capsenseCfg.ledOutput = CapsenseLedOutput::NO_LED;
    setBit(config.usbConfig->remoteWakeup, RemoteWakeupSource::WAKEUP_SOURCE_CAPSENSE, false);
    capsenseCfg.debounce = 1;
    capsenseCfg.scanPeriod = 1;
}

void AppState::disableBcdConfig()
{
    CyUsbBcdConfig &bcd = config.bcdConfig;
    gpioState.unuse(bcd.bcd[0]);
    gpioState.unuse(bcd.bcd[1]);
    gpioState.unuse(bcd.busDetect);
    bcd.bcd[0] = bcd.bcd[1] = bcd.busDetect = GPIO::None;
}

void AppState::disableGpioDrives()
{
    uint gpioToDisableFlags = config.drive0 | config.drive1 | config.input;

    for (uint i = 0, gpioFlag = 1; i < NUMBER_OF_GPIOS; ++i, gpioFlag <<= 1)
    {
        if (gpioFlag & gpioToDisableFlags)
        {
            GPIO gpio = static_cast<GPIO>(i);
            gpioState.unuse(gpio);
        }
    }

    config.drive0 = config.drive1 = config.input = 0;
}

void AppState::disableGPOIUsers(const QSet<GpioState::State> &toDisable)
{
    for (GpioState::State kick : toDisable)
    {
        switch (kick)
        {
        case GpioState::CAPSENCE:
        case GpioState::WATER_SHIELD:
        case GpioState::COMMON_LED:
        case GpioState::CAPSENCE_BTN0:
        case GpioState::CAPSENCE_BTN1:
        case GpioState::CAPSENCE_BTN2:
        case GpioState::CAPSENCE_BTN3:
        case GpioState::CAPSENCE_BTN4:
        case GpioState::CAPSENCE_BTN5:
        case GpioState::CAPSENCE_BTN6:
        case GpioState::CAPSENCE_BTN7:
        case GpioState::CAPSENCE_LED_BTN0:
        case GpioState::CAPSENCE_LED_BTN1:
        case GpioState::CAPSENCE_LED_BTN2:
        case GpioState::CAPSENCE_LED_BTN3:
        case GpioState::CAPSENCE_LED_BTN4:
        case GpioState::CAPSENCE_LED_BTN5:
        case GpioState::CAPSENCE_LED_BTN6:
        case GpioState::CAPSENCE_LED_BTN7:
        case GpioState::CAPSENCE_OUT_LINE0:
        case GpioState::CAPSENCE_OUT_LINE1:
        case GpioState::CAPSENCE_OUT_LINE2:
        case GpioState::CAPSENCE_OUT_LINE3:
            disableCapsenseConfig();
            break;

        case GpioState::DRIVE:
            disableGpioDrives();
            break;

        case GpioState::SCB0_RxLED:
            config.scb[0]->ledActivity->rxLed = GPIO::None;
            break;

        case GpioState::SCB0_TxLED:
            config.scb[0]->ledActivity->txLed = GPIO::None;
            break;

        case GpioState::SCB0_RxTxLED:
            config.scb[0]->ledActivity->rxAndTxLed = GPIO::None;
            break;

        case GpioState::SCB0_UART_RS485:
            config.scbUartRs485[0].rs485gpio = GPIO::None;
            config.scbUartRs485[0].rs485enable = CyUsbBoolField::FALSE;
            break;

        case GpioState::SCB1_RxLED:
            config.scb[1]->ledActivity->rxLed = GPIO::None;
            break;

        case GpioState::SCB1_TxLED:
            config.scb[1]->ledActivity->txLed = GPIO::None;
            break;

        case GpioState::SCB1_RxTxLED:
            config.scb[1]->ledActivity->rxAndTxLed = GPIO::None;
            break;

        case GpioState::SCB1_UART_RS485:
            config.scbUartRs485[1].rs485gpio = GPIO::None;
            config.scbUartRs485[1].rs485enable = CyUsbBoolField::FALSE;
            break;

        case GpioState::WAKEUP_POWER:
            config.powerGpio = GPIO::None;
            break;

        case GpioState::BCD0:
        case GpioState::BCD1:
        case GpioState::BUS_DETECT:
            disableBcdConfig();
            break;

        case GpioState::Free:
        case GpioState::SCB0:
        case GpioState::SCB1:
            // these option are not possible since
            // SCB0 and SCB1 use different pins
            break;
        }
    }
}

void AppState::updateUsedScbGPIOs(ScbNum scbNum, CY_SCB_MODE newScbMode, CY_UART_TYPE uartType)
{
    GpioState::State newState{};
    switch (scbNum)
    {
    case ScbNum::SCB0:
        newState = GpioState::State::SCB0;
        break;

    case ScbNum::SCB1:
        newState = GpioState::State::SCB1;
        break;
    }

    CyUsbScbConfig &scb = config.scb[scbNum];
    gpioState.unuse(scbNum, scb.mode, scb.modeConfiguration->uart.uartType);
    auto toDisable = gpioState.use(scbNum, newScbMode, newState, uartType);

    scb.mode = newScbMode;
    if (newScbMode == CY_SCB_MODE::UART)
    {
        scb.modeConfiguration->uart.uartType = uartType;
    }

    if (toDisable.size() > 0)
    {
        disableGPOIUsers(toDisable);
        gpioState.use(scbNum, newScbMode, newState, uartType);
    }
}

void AppState::enableScb1(bool enable)
{
    using namespace ScbNum;

    CyUsbScbConfig &scb1 = config.scb[1];
    if (enable)
    {
        auto toKick = gpioState.use(SCB1, scb1.mode, GpioState::SCB1, scb1.modeConfiguration->uart.uartType);
        if (toKick.size() > 0)
        {
            disableGPOIUsers(toKick);
            gpioState.use(SCB1, scb1.mode, GpioState::SCB1, scb1.modeConfiguration->uart.uartType);
        }
        gpioState.use(scb1.ledActivity->rxLed, GpioState::SCB1_RxLED);
        gpioState.use(scb1.ledActivity->txLed, GpioState::SCB1_TxLED);
        gpioState.use(scb1.ledActivity->rxAndTxLed, GpioState::SCB1_RxTxLED);

        if (config.scbUartRs485[1].rs485enable == CyUsbBoolField::TRUE)
        {
            gpioState.use(config.scbUartRs485[1].rs485gpio, GpioState::SCB1_UART_RS485);
        }
    }
    else
    {
        gpioState.unuse(SCB1, scb1.mode, scb1.modeConfiguration->uart.uartType);
        gpioState.unuse(scb1.ledActivity->rxLed);
        gpioState.unuse(scb1.ledActivity->txLed);
        gpioState.unuse(scb1.ledActivity->rxAndTxLed);

        if (config.scbUartRs485[1].rs485enable == CyUsbBoolField::TRUE)
        {
            gpioState.unuse(config.scbUartRs485[1].rs485gpio);
        }

        scb1.ledActivity->rxLed = scb1.ledActivity->txLed = scb1.ledActivity->rxAndTxLed = GPIO::None;
        config.scbUartRs485[1].rs485gpio = GPIO::GPIO_00; // cannot be None
    }
}

void AppState::initGPIOs()
{
    using namespace ScbNum;

    // Power
    if (config.powerGpioEnable == CyUsbBoolField::TRUE)
    {
        gpioState.use(config.powerGpio, GpioState::WAKEUP_POWER);
    }

    // SCBx
    for (ScbNum scbNum : {SCB0, SCB1})
    {
        const CyUsbScbConfig &scb = config.scb[scbNum];
        if(scbNum == SCB0)
        {
            gpioState.use(scbNum, scb.mode, GpioState::SCB0, scb.modeConfiguration.value.uart.uartType);
        }
        if(scbNum == SCB1)
        {
            gpioState.use(scbNum, scb.mode, GpioState::SCB1, scb.modeConfiguration.value.uart.uartType);
        }


        gpioState.use(scb.ledActivity->rxLed, GpioState::SCB0_RxLED);
        gpioState.use(scb.ledActivity->txLed, GpioState::SCB0_TxLED);
        gpioState.use(scb.ledActivity->rxAndTxLed, GpioState::SCB0_RxTxLED);
    }

    // CapSence
    if (config.capSenseConfiguration->enable == CyUsbBoolField::TRUE)
    {
        const CyUsbCapsenseConfig &capsenseCfg = config.capSenseConfiguration;

        gpioState.use(GPIO::GPIO_00, GpioState::CAPSENCE);
        gpioState.use(capsenseCfg.waterShieldIO, GpioState::WATER_SHIELD);
        gpioState.use(capsenseCfg.commonLedIo, GpioState::COMMON_LED);

        for (uint i = 0; i < capsenseCfg.buttonCount; ++i)
        {
            gpioState.use(capsenseCfg.buttons[i]->senseGpio, GpioState::CAPSENCE_BTN[i]);
            gpioState.use(capsenseCfg.buttons[i]->outputLedGpio, GpioState::CAPSENCE_LED_BTN[i]);
        }

        for (uint i = 0, j = 1; j <= capsenseCfg.buttonCount; ++i, j <<= 1)
        {
            gpioState.use(capsenseCfg.encodedGpio[i], GpioState::CAPSENCE_OUT_LINE[i]);
        }
    }

    // BCD
    gpioState.use(config.bcdConfig->bcd[0], GpioState::BCD0);
    gpioState.use(config.bcdConfig->bcd[1], GpioState::BCD1);
    gpioState.use(config.bcdConfig->busDetect, GpioState::BUS_DETECT);

    // Drive
    uint gpioToDisableFlags = config.drive0 | config.drive1 | config.input;
    for (uint i = 0, gpioFlag = 1; i < NUMBER_OF_GPIOS; ++i, gpioFlag <<= 1)
    {
        if (gpioFlag & gpioToDisableFlags)
        {
            gpioState.use(static_cast<GPIO>(i), GpioState::DRIVE);
        }
    }
}
