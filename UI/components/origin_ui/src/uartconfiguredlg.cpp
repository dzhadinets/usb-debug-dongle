#include "origin_ui/uartconfiguredlg.h"
#include "ui_uartconfiguredlg.h"

#include <common/commonutilities.h>
#include <common/enumsregistration.h>


using namespace Cy;

UartConfigureDlg::UartConfigureDlg(AppState &state, ScbNum scbNum, QWidget *parent)
    : QDialog(parent)
    , state(state)
    , scbNum(scbNum)
    , ui(new Ui::UartConfigureDlg)
{
    ui->setupUi(this);
    setFixedSize(size());
    updateUiWithData();
}

UartConfigureDlg::~UartConfigureDlg()
{
    readUiData();
    delete ui;
}

void UartConfigureDlg::updateUiWithData()
{
    const auto &config = state.config;
    const auto &gpioState = state.gpioState;

    const CyUsbScbConfig &scb = config.scb[scbNum];
    const CyUsbScbConfig::ModeConfiguration &modeConfig = scb.modeConfiguration;
    const CyUsbUartConfig &uartConfig = modeConfig.uart;

    static const uint supportedBaudRates[] = {100,   300,   600,   1200,   2400,   4800,   9600,   14400,   19200,
                                              38400, 56000, 57600, 115200, 230400, 460800, 921600, 1000000, 3000000};
    for (auto baudRate : supportedBaudRates)
    {
        ui->uartBaudrate->addItem(QString::number(baudRate), baudRate);
    }
    setCurrentItem(ui->uartBaudrate, uartConfig.baudRate);

    fillComboBoxWithEnum<CY_UART_TYPE>(ui->uartType, uartConfig.uartType, uartType);
    fillComboBoxWithEnum<CY_UART_DATA_WIDTH>(ui->uartDataWidth, uartConfig.dataWidth, uartDataWidth);
    fillComboBoxWithEnum<CY_UART_STOP_BITS>(ui->uartStopBits, uartConfig.stopBits, uartStopBits);
    fillComboBoxWithEnum<CY_UART_PARITY_MODE>(ui->uartParity, uartConfig.parity, uartParityMode);

    ui->dropPacketsOnError->setChecked(uartConfig.dropOnErrors == CyUsbBoolField::TRUE);

    ui->disableCts->setChecked(config.scbSuspendUartControl[scbNum] == CyUsbBoolField::FALSE);
    bool gpiosAvailable = fillComboBoxWithEnum(ui->uartRs485, config.scbUartRs485[scbNum].rs485gpio, gpio,
                                               gpioState.freeFilter(GpioState::SCB_UART_RS485[scbNum], false)) > 0;
    ui->enableRs485->setEnabled(gpiosAvailable);
    ui->enableRs485->setChecked(config.scbUartRs485[scbNum].rs485enable == CyUsbBoolField::TRUE);
}

void UartConfigureDlg::readUiData()
{
    auto &config = state.config;
    CyUsbScbConfig &scb = config.scb[scbNum];
    CyUsbScbConfig::ModeConfiguration &modeConfig = scb.modeConfiguration;
    CyUsbUartConfig &uartConfig = modeConfig.uart;

    uartConfig.baudRate = ui->uartBaudrate->currentText().toUInt();
    uartConfig.uartType = static_cast<CY_UART_TYPE>(ui->uartType->currentData().toUInt());
    uartConfig.dataWidth = static_cast<CY_UART_DATA_WIDTH>(ui->uartDataWidth->currentData().toUInt());
    uartConfig.stopBits = static_cast<CY_UART_STOP_BITS>(ui->uartStopBits->currentData().toUInt());
    uartConfig.parity = static_cast<CY_UART_PARITY_MODE>(ui->uartParity->currentData().toUInt());
    uartConfig.dropOnErrors = toBoolField(ui->dropPacketsOnError->isChecked());

    config.scbSuspendUartControl[scbNum] = toBoolField(!ui->disableCts->isChecked());
    bool rs485enable = ui->enableRs485->isChecked();
    config.scbUartRs485[scbNum].rs485enable = toBoolField(rs485enable);
    config.scbUartRs485[scbNum].rs485gpio =
        rs485enable ? static_cast<GPIO>(ui->uartRs485->currentData().toUInt()) : GPIO::GPIO_00;
}

void UartConfigureDlg::on_uartType_currentIndexChanged(int)
{
    auto &config = state.config;
    auto &gpioState = state.gpioState;

    state.updateUsedScbGPIOs(scbNum, CY_SCB_MODE::UART,
                             static_cast<CY_UART_TYPE>(ui->uartType->currentData().toUInt()));
    bool gpiosAvailable = fillComboBoxWithEnum<GPIO>(ui->uartRs485, config.scbUartRs485[scbNum].rs485gpio, gpio,
                                                     gpioState.freeFilter(GpioState::SCB_UART_RS485[scbNum], false));
    ui->enableRs485->setEnabled(gpiosAvailable);

    if (config.scbUartRs485[scbNum].rs485gpio == GPIO::None)
    {
        config.scbUartRs485[scbNum].rs485enable = CyUsbBoolField::FALSE;
        ui->enableRs485->setChecked(false);
    }
    else
    {
        setCurrentItem(ui->uartRs485, config.scbUartRs485[scbNum].rs485gpio);
    }
}

void UartConfigureDlg::on_enableRs485_stateChanged(int arg1)
{
    auto &config = state.config;
    auto &gpioState = state.gpioState;

    if (arg1 == Qt::Checked)
    {
        config.scbUartRs485->rs485gpio = static_cast<GPIO>(ui->uartRs485->currentData().toUInt());
        gpioState.use(config.scbUartRs485->rs485gpio, GpioState::SCB_UART_RS485[scbNum]);
    }
    else
    {
        gpioState.unuse(config.scbUartRs485->rs485gpio);
        config.scbUartRs485->rs485gpio = GPIO::GPIO_00; // None is not allowed
    }
    ui->uartRs485->setEnabled(arg1 == Qt::Checked);
}

void UartConfigureDlg::on_uartRs485_currentIndexChanged(int)
{
    state.config.scbUartRs485[scbNum].rs485enable = CyUsbBoolField::TRUE;
    state.gpioState.changeGPIO(state.config.scbUartRs485[scbNum].rs485gpio,
                               static_cast<GPIO>(ui->uartRs485->currentData().toUInt()),
                               GpioState::SCB_UART_RS485[scbNum]);
}
