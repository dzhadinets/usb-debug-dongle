#include "origin_ui/i2cconfigdlg.h"
#include "ui_i2cconfigdlg.h"

#include <common/commonutilities.h>
#include <common/enumsregistration.h>

using namespace Cy;

I2CConfigDlg::I2CConfigDlg(AppState &state, ScbNum scbNum, QWidget *parent)
    : QDialog(parent)
    , scbNum(scbNum)
    , state(state)
    , ui(new Ui::I2CConfigDlg)
{
    ui->setupUi(this);
    setFixedSize(size());
    updateUiWithData();
}

I2CConfigDlg::~I2CConfigDlg()
{
    readUiData();
    delete ui;
}

void I2CConfigDlg::updateUiWithData()
{
    const CyUsbI2cConfig &i2c = state.config.scb[scbNum]->modeConfiguration->i2c;

    static const uint supportedFrequencies[] = {1000, 10000, 100000, 200000, 400000};
    for (auto frequency : supportedFrequencies)
    {
        ui->frequency->addItem(QString::number(frequency), frequency);
    }
    ui->frequency->setCurrentText(QString::number(i2c.frequncy));

    fillComboBoxWithEnum<CyUsbBoolField>(ui->master, i2c.i2cMaster, usbBoolField);
    ui->useAsWakeupSrc->setChecked(state.config.usbConfig->remoteWakeup & WAKEUP_SOURCE_I2C_SLAVE);
    ui->enableClockStretching->setChecked(i2c.clockStretch == CyUsbBoolField::TRUE);

    ui->slaveAddress->setText(QString::number(i2c.slaveAddress));
    i2cConfigDlg_setControlsEnabled(i2c.i2cMaster);
}

void I2CConfigDlg::readUiData()
{
    CyUsbI2cConfig &i2c = state.config.scb[scbNum]->modeConfiguration->i2c;

    i2c.frequncy = ui->frequency->currentText().toUInt();
    i2c.i2cMaster = toBoolField(ui->master->currentData().toUInt());
    setBit(state.config.usbConfig->remoteWakeup, WAKEUP_SOURCE_I2C_SLAVE, ui->useAsWakeupSrc->isChecked());
    i2c.clockStretch = toBoolField(ui->enableClockStretching->isChecked());
    i2c.slaveAddress = ui->slaveAddress->text().toUInt();
}

void I2CConfigDlg::i2cConfigDlg_setControlsEnabled(CyUsbBoolField master)
{
    switch (master)
    {
    case CyUsbBoolField::FALSE: // slave
        ui->enableClockStretching->setEnabled(true);
        ui->slaveAddress->setEnabled(true);
        ui->useAsWakeupSrc->setEnabled(true);
        break;

    case CyUsbBoolField::TRUE: // master
        ui->enableClockStretching->setEnabled(false);
        ui->slaveAddress->setEnabled(false);
        ui->useAsWakeupSrc->setEnabled(false);
        break;
    }
}

void I2CConfigDlg::on_master_currentIndexChanged(int)
{
    auto master = static_cast<CyUsbBoolField>(ui->master->currentData().toUInt());
    i2cConfigDlg_setControlsEnabled(master);
}
