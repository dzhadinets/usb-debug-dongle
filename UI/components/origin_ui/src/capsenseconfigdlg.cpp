#include "origin_ui/capsenseconfigdlg.h"
#include "ui_capsenseconfigdlg.h"

#include <common/commonutilities.h>
#include <common/enumsregistration.h>

using namespace Cy;

void ButtonHandler::senseChanged(int)
{
    QComboBox *buttons[] = {window->ui->btnInput0, window->ui->btnInput1, window->ui->btnInput2, window->ui->btnInput3,
                            window->ui->btnInput4, window->ui->btnInput5, window->ui->btnInput6, window->ui->btnInput7};
    if (window)
    {
        window->capSenseChangeGPIO(window->state.config.capSenseConfiguration->buttons[buttonIndex]->senseGpio,
                                   static_cast<GPIO>(buttons[buttonIndex]->currentData().toUInt()),
                                   GpioState::CAPSENCE_BTN[buttonIndex]);
    }
    else
    {
        qDebug("windows == nullptr, looks like a bug");
    }
}

void ButtonHandler::ledChanged(int)
{
    QComboBox *buttons[] = {window->ui->btnLed0, window->ui->btnLed1, window->ui->btnLed2, window->ui->btnLed3,
                            window->ui->btnLed4, window->ui->btnLed5, window->ui->btnLed6, window->ui->btnLed7};
    if (window)
    {
        window->capSenseChangeGPIO(window->state.config.capSenseConfiguration->buttons[buttonIndex]->outputLedGpio,
                                   static_cast<GPIO>(buttons[buttonIndex]->currentData().toUInt()),
                                   GpioState::CAPSENCE_LED_BTN[buttonIndex]);
    }
    else
    {
        qDebug("windows == nullptr, looks like a bug");
    }
}

void ButtonHandler::sensitivityChanged(int)
{
    QComboBox *buttons[] = {window->ui->btnSensitivity0, window->ui->btnSensitivity1, window->ui->btnSensitivity2,
                            window->ui->btnSensitivity3, window->ui->btnSensitivity4, window->ui->btnSensitivity5,
                            window->ui->btnSensitivity6, window->ui->btnSensitivity7};
    if (window)
    {
        window->state.config.capSenseConfiguration->buttons[buttonIndex]->sensitivity =
            static_cast<CapsenseSensitivity>(buttons[buttonIndex]->currentData().toUInt());
    }
    else
    {
        qDebug("windows == nullptr, looks like a bug");
    }
}

CapsenseConfigDlg::CapsenseConfigDlg(AppState &state, QWidget *parent)
    : QDialog(parent)
    , state(state)
    , ui(new Ui::CapsenseConfigDlg)
{
    ui->setupUi(this);
    setFixedSize(size());

    for (uint i = 0; i < 8; ++i)
    {
        btnHandlers[i].setFields(this, i);
    }
    updateUiWithData();
}

CapsenseConfigDlg::~CapsenseConfigDlg()
{
    readUiData();
    delete ui;
}

void CapsenseConfigDlg::enableButtons(uint buttonsCount, bool disableLedColumn)
{
    CyUsbCapsenseConfig &capsenseCfg = state.config.capSenseConfiguration;
    auto &gpioState = state.gpioState;

    enum ButtonParameters
    {
        BTN_INPUT_GPIO,
        BTN_LED_GPIO,
        BTN_SENSITIVITY,
        BTN_PARAMS_COUNT
    };
    QComboBox *buttons[CAPSENSE_BUTTONS_COUNT][BTN_PARAMS_COUNT] = {
        {ui->btnInput0, ui->btnLed0, ui->btnSensitivity0}, {ui->btnInput1, ui->btnLed1, ui->btnSensitivity1},
        {ui->btnInput2, ui->btnLed2, ui->btnSensitivity2}, {ui->btnInput3, ui->btnLed3, ui->btnSensitivity3},
        {ui->btnInput4, ui->btnLed4, ui->btnSensitivity4}, {ui->btnInput5, ui->btnLed5, ui->btnSensitivity5},
        {ui->btnInput6, ui->btnLed6, ui->btnSensitivity6}, {ui->btnInput7, ui->btnLed7, ui->btnSensitivity7},
    };

    QComboBox *outLines[] = {
        ui->outLine0,
        ui->outLine1,
        ui->outLine2,
        ui->outLine3,
    };

    // correspondence between out enabled buttons and out lines
    // I am not sure how it works, but looks like line=2^btn-1
    QMap<uint, uint> btnVsOutLines = {{0, 0}, {1, 1}, {3, 2}, {7, 3}};

    for (uint i = 0; i < CAPSENSE_BUTTONS_COUNT; ++i)
    {
        // CapSense buttons
        if (i < buttonsCount) // enable these buttons
        {
            for (uint j = 0; j < BTN_PARAMS_COUNT; ++j)
            {
                buttons[i][j]->setEnabled(!disableLedColumn || j != BTN_LED_GPIO);
            }

            buttons[i][BTN_INPUT_GPIO]->disconnect(&btnHandlers[i]);
            fillComboBoxWithEnum(buttons[i][BTN_INPUT_GPIO], capsenseCfg.buttons[i].value.senseGpio, gpio,
                                 gpioState.freeFilter(GpioState::CAPSENCE_BTN[i]));
            connect(buttons[i][BTN_INPUT_GPIO], SIGNAL(currentIndexChanged(int)), &btnHandlers[i],
                    SLOT(senseChanged(int)));

            if (disableLedColumn)
            {
                const QSignalBlocker blocker(buttons[i][BTN_LED_GPIO]);
                gpioState.unuse(capsenseCfg.buttons[i]->outputLedGpio);
                capsenseCfg.buttons[i]->outputLedGpio = GPIO::None;
                buttons[i][BTN_LED_GPIO]->clear();
            }
            else
            {
                buttons[i][BTN_LED_GPIO]->disconnect(&btnHandlers[i]);
                fillComboBoxWithEnum(buttons[i][BTN_LED_GPIO], capsenseCfg.buttons[i]->outputLedGpio, gpio,
                                     gpioState.freeFilter(GpioState::CAPSENCE_LED_BTN[i]));
                connect(buttons[i][BTN_LED_GPIO], SIGNAL(currentIndexChanged(int)), &btnHandlers[i],
                        SLOT(ledChanged(int)));
            }

            fillComboBoxWithEnum(buttons[i][BTN_SENSITIVITY], capsenseCfg.buttons[i].value.sensitivity,
                                 capsenseSenstvt);
            connect(buttons[i][BTN_SENSITIVITY], SIGNAL(currentIndexChanged(int)), &btnHandlers[i],
                    SLOT(sensitivityChanged(int)));
        }
        else // i >= buttonCount, disable buttons
        {
            gpioState.unuse(capsenseCfg.buttons[i]->senseGpio);
            gpioState.unuse(capsenseCfg.buttons[i]->outputLedGpio);
            capsenseCfg.buttons[i]->senseGpio = capsenseCfg.buttons[i]->outputLedGpio = GPIO::None;

            for (uint j = 0; j < BTN_PARAMS_COUNT; ++j)
            {
                buttons[i][j]->disconnect(&btnHandlers[i]);
                buttons[i][j]->setEnabled(false);
                buttons[i][j]->clear();
            }
        }

        // Output lines
        if (btnVsOutLines.contains(i))
        {
            uint j = btnVsOutLines[i];
            auto outLine = outLines[j];

            if (i < buttonsCount) // enable
            {
                outLine->setEnabled(true);
                fillComboBoxWithEnum(outLine, capsenseCfg.encodedGpio[j], gpio,
                                     gpioState.freeFilter(GpioState::CAPSENCE_OUT_LINE[j]));
            }
            else // disable
            {
                QSignalBlocker blocker(outLine);
                gpioState.unuse(capsenseCfg.encodedGpio[j]);
                capsenseCfg.encodedGpio[j] = GPIO::None;
                outLine->clear();
                outLine->setEnabled(false);
            }
        }
    }

    if (buttonsCount < capsenseCfg.buttonCount)
    {
        fillComboBoxWithEnum(ui->waterShield, capsenseCfg.waterShieldIO, gpio,
                             gpioState.freeFilter(GpioState::WATER_SHIELD));
        fillComboBoxWithEnum(ui->commonLed, capsenseCfg.commonLedIo, gpio, gpioState.freeFilter(GpioState::COMMON_LED));
    }
    capsenseCfg.buttonCount = buttonsCount;
}

void CapsenseConfigDlg::updateUiWithData()
{
    if (dialogUpdating)
    {
        return;
    }
    dialogUpdating = true;

    const CyUsbCapsenseConfig &capsenseCfg = state.config.capSenseConfiguration;
    auto &gpioState = state.gpioState;

    ui->enable->setEnabled(gpioState.isGPIOFree(GPIO::GPIO_00, GpioState::CAPSENCE));
    ui->enable->setChecked(capsenseCfg.enable == CyUsbBoolField::TRUE);
    ui->capSenseConfigBox->setEnabled(capsenseCfg.enable == CyUsbBoolField::TRUE);

    ui->useAsWakeupSource->setChecked(state.config.usbConfig->remoteWakeup &
                                      RemoteWakeupSource::WAKEUP_SOURCE_CAPSENSE);

    ui->debounce->clear();
    for (int i = 1; i <= DEBOUNTCE_COUNT; ++i)
    {
        ui->debounce->addItem(QString::number(i), i);
    }
    setCurrentItem(ui->debounce, capsenseCfg.debounce);

    ui->scanRate->setValue(capsenseCfg.scanPeriod);
    fillComboBoxWithEnum(ui->waterShield, capsenseCfg.waterShieldIO, gpio,
                         gpioState.freeFilter(GpioState::WATER_SHIELD));

    switch (capsenseCfg.ledOutput)
    {
    case CapsenseLedOutput::NO_LED:
        ui->ledNone->setChecked(true);
        ui->commonLed->setEnabled(false);
        break;

    case CapsenseLedOutput::INDIVIDUAL_LED:
        ui->ledIndividual->setChecked(true);
        ui->commonLed->setEnabled(false);
        break;

    case CapsenseLedOutput::COMMON_LED:
        ui->ledCommon->setChecked(true);
        ui->commonLed->setEnabled(true);
        break;
    }

    fillComboBoxWithEnum(ui->commonLed, capsenseCfg.commonLedIo, gpio, gpioState.freeFilter(GpioState::COMMON_LED));

    const QSignalBlocker blocker(ui->buttonsNeeded);
    ui->buttonsNeeded->clear();
    for (int i = 0; i <= CAPSENSE_BUTTONS_COUNT; ++i)
    {
        ui->buttonsNeeded->addItem(QString::number(i), i);
    }
    setCurrentItem(ui->buttonsNeeded, capsenseCfg.buttonCount);

    enableButtons(ui->buttonsNeeded->currentData().toUInt(),
                  capsenseCfg.ledOutput != CapsenseLedOutput::INDIVIDUAL_LED);

    dialogUpdating = false;
}

void CapsenseConfigDlg::readUiData()
{
    if (dialogUpdating) // do not read UI data while we have not filled it yet
    {
        return;
    }

    CyUsbCapsenseConfig &capsenseCfg = state.config.capSenseConfiguration;

    bool enabled = ui->enable->isChecked();

    if (!enabled)
    {
        state.disableCapsenseConfig();
    }
    else
    {
        capsenseCfg.enable = CyUsbBoolField::TRUE;
        capsenseCfg.debounce = ui->debounce->currentData().toUInt();
        capsenseCfg.scanPeriod = ui->scanRate->value();

        setBit(state.config.usbConfig->remoteWakeup, RemoteWakeupSource::WAKEUP_SOURCE_CAPSENSE,
               ui->useAsWakeupSource->isChecked());

        if (ui->ledNone->isChecked())
        {
            capsenseCfg.ledOutput = CapsenseLedOutput::NO_LED;
        }
        else if (ui->ledIndividual->isChecked())
        {
            capsenseCfg.ledOutput = CapsenseLedOutput::INDIVIDUAL_LED;
        }
        else // if ui->ledCommon->isChecked()
        {
            capsenseCfg.ledOutput = CapsenseLedOutput::COMMON_LED;
        }

        capsenseCfg.buttonCount = ui->buttonsNeeded->currentData().toUInt();
    }
}

void CapsenseConfigDlg::capSenseChangeGPIO(GPIO &gpio, GPIO newGpio, GpioState::State newState)
{
    readUiData();
    state.gpioState.changeGPIO(gpio, newGpio, newState);
    updateUiWithData();
}

void CapsenseConfigDlg::on_enable_stateChanged(int arg1)
{
    bool checked = (arg1 == Qt::Checked);
    ui->capSenseConfigBox->setEnabled(checked);
    // GPIO_00 will be free at this point because the checkbox will be disabled otherwise
    state.gpioState.use(GPIO::GPIO_00, checked ? GpioState::CAPSENCE : GpioState::Free);
    state.config.capSenseConfiguration->enable = toBoolField(checked);
    updateUiWithData();
}

void CapsenseConfigDlg::on_waterShield_currentIndexChanged(int)
{
    capSenseChangeGPIO(state.config.capSenseConfiguration->waterShieldIO,
                       static_cast<GPIO>(ui->waterShield->currentData().toUInt()), GpioState::WATER_SHIELD);
}

void CapsenseConfigDlg::on_ledCommon_toggled(bool checked)
{
    auto newState = checked ? GpioState::COMMON_LED : GpioState::Free;
    state.gpioState.changeGPIO(state.config.capSenseConfiguration->commonLedIo,
                               static_cast<GPIO>(ui->commonLed->currentData().toUInt()), newState);
    ui->commonLed->setEnabled(checked);
    if (checked)
    {
        readUiData();
        state.config.capSenseConfiguration->ledOutput = CapsenseLedOutput::COMMON_LED;
        updateUiWithData();
    }
    else
    {
        state.config.capSenseConfiguration->commonLedIo = GPIO::None;
    }
}

void CapsenseConfigDlg::on_ledNone_toggled(bool checked)
{
    if (checked)
    {
        readUiData();
        state.config.capSenseConfiguration->ledOutput = CapsenseLedOutput::NO_LED;
        updateUiWithData();
    }
}

void CapsenseConfigDlg::on_ledIndividual_toggled(bool checked)
{
    if (checked)
    {
        readUiData();
        state.config.capSenseConfiguration->ledOutput = CapsenseLedOutput::INDIVIDUAL_LED;
        updateUiWithData();
    }
}

void CapsenseConfigDlg::on_commonLed_currentIndexChanged(int)
{
    capSenseChangeGPIO(state.config.capSenseConfiguration->commonLedIo,
                       static_cast<GPIO>(ui->commonLed->currentData().toUInt()), GpioState::COMMON_LED);
}

void CapsenseConfigDlg::on_buttonsNeeded_currentIndexChanged(int)
{
    auto buttonCount = ui->buttonsNeeded->currentData().toUInt();
    enableButtons(buttonCount, state.config.capSenseConfiguration->ledOutput != CapsenseLedOutput::INDIVIDUAL_LED);
}

void CapsenseConfigDlg::on_outLine0_currentIndexChanged(int)
{
    capSenseChangeGPIO(state.config.capSenseConfiguration->encodedGpio[0],
                       static_cast<GPIO>(ui->outLine0->currentData().toUInt()), GpioState::CAPSENCE_OUT_LINE0);
}

void CapsenseConfigDlg::on_outLine1_currentIndexChanged(int)
{
    capSenseChangeGPIO(state.config.capSenseConfiguration->encodedGpio[1],
                       static_cast<GPIO>(ui->outLine1->currentData().toUInt()), GpioState::CAPSENCE_OUT_LINE1);
}

void CapsenseConfigDlg::on_outLine2_currentIndexChanged(int)
{
    capSenseChangeGPIO(state.config.capSenseConfiguration->encodedGpio[2],
                       static_cast<GPIO>(ui->outLine2->currentData().toUInt()), GpioState::CAPSENCE_OUT_LINE2);
}

void CapsenseConfigDlg::on_outLine3_currentIndexChanged(int)
{
    capSenseChangeGPIO(state.config.capSenseConfiguration->encodedGpio[3],
                       static_cast<GPIO>(ui->outLine3->currentData().toUInt()), GpioState::CAPSENCE_OUT_LINE3);
}
