#include "../include/origin_ui/mainwindow.h"

#include <cstring>
#include <algorithm>

#include <QFileDialog>
#include <QMessageBox>
#include <../common/include/common/CyUSB.h>
#include <../common/include/common/commonutilities.h>

#include "ui_mainwindow.h"
#include "origin_ui/bcdconfigdlg.h"
#include "origin_ui/capsenseconfigdlg.h"
#include "origin_ui/gpiodriveconfigdlg.h"
#include "origin_ui/i2cconfigdlg.h"
#include "origin_ui/remotewakeupdlg.h"
#include "origin_ui/scbledconfigdlg.h"
#include "origin_ui/spiconfigdlg.h"
#include "origin_ui/uartconfiguredlg.h"
#include "origin_ui/devicepicker.h"
#include <common/gpiostate.h>
#include <common/enumsregistration.h>
using namespace Cy;
using namespace ScbNum;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(size());

    try
    {
        defaultInit();
    }
    catch (std::logic_error &le)
    {
        qDebug("Error opening default config: %s", le.what());
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_useCypressIDs_stateChanged(int arg1)
{
    ui->usbVidEdit->setEnabled(Qt::Unchecked == arg1);
    ui->usbPidEdit->setEnabled(Qt::Unchecked == arg1);

    if (arg1 == Qt::Checked)
    {
        ui->usbVidEdit->setText(QString::number(CYPRESS_VID, 16));
        ui->usbPidEdit->setText(QString::number(config.getDefaultPid(), 16));
    }
}

void MainWindow::on_useSerialNumberStr_stateChanged(int arg1)
{
    ui->serialNumberString->setEnabled(Qt::Checked == arg1);
}

void MainWindow::updateMainWindowWithData()
{
    // USB tab

    const CyUsbConfig &usbConfig = config.usbConfig;

    ui->useCypressIDs->setChecked(usbConfig.productId == config.getDefaultPid() && usbConfig.vendorId == CYPRESS_VID);
    ui->usbVidEdit->setText(QString::number(usbConfig.vendorId, 16));
    ui->usbPidEdit->setText(QString::number(usbConfig.productId, 16));

    fillComboBoxWithEnum(ui->powerMode, usbConfig.powerMode, devicePowerMode);
    fillComboBoxWithEnum(ui->ioLevel, config.gpioLogicType, gpioLogicType);
    fillComboBoxWithEnum(ui->ioMode, config.gpioDriveType, gpioDriveType);

    ui->bMaxPower->setValue(usbConfig.currentDraw * 2);

    QLineEdit *strings[] = {ui->manufactirerString, ui->productString, ui->serialNumberString};

    for (uint i = 0; i < 3; ++i)
    {
        strings[i]->setText(QString::fromUtf16(usbConfig.stringDescriptor[i].string,
                                               usbConfig.stringDescriptor[i].length / sizeof(char16_t)));
    }

    bool serialEnabled = config.usbConfig->stringDescriptorAddress[3] == 0xFFFFFFFF;
    ui->useSerialNumberStr->setChecked(serialEnabled);
    ui->serialNumberString->setEnabled(serialEnabled);

    ui->vbusVoltage33V->setChecked(static_cast<bool>(config.bypassUSBRegulator));
    ui->vdddVoltage2V->setChecked(static_cast<bool>(config.bypassSystemRegulator));
    ui->enableManufacturing->setChecked(static_cast<bool>(config.mfgInterface));

    // SCB0/SCB1 tabs

    const CyUsbScbConfig &scb0 = config.scb[0];

    state.initGPIOs();

    fillComboBoxWithEnum(ui->scb0Mode, scb0.mode, scbMode);

    fillComboBoxWithEnum(ui->scb0Protocol, scb0.protocol, scbProtocol);

    const CyUsbScbConfig &scb1 = config.scb[1];
    bool scb1enabled = static_cast<bool>(scb1.mode);
    ui->enableScb1->setChecked(scb1enabled);

    fillComboBoxWithEnum(ui->scb1Mode, scb1.mode, scbMode);

    fillComboBoxWithEnum(ui->scb1Protocol, scb1.protocol, scbProtocol);

    ui->scb1ConfigBox->setEnabled(scb1enabled);
}

void MainWindow::readMainWindowData()
{
    CyUsbConfig &usbConfig = config.usbConfig;

    usbConfig.vendorId = ui->usbVidEdit->text().toUInt(nullptr, 16);
    usbConfig.productId = ui->usbPidEdit->text().toUInt(nullptr, 16);

    usbConfig.powerMode = static_cast<CY_DEVICE_POWER_MODE>(ui->powerMode->currentData().toUInt());
    config.gpioLogicType = static_cast<CyGpioLogicType>(ui->ioLevel->currentData().toUInt());
    config.gpioDriveType = static_cast<CyGpioDriveType>(ui->ioMode->currentData().toUInt());

    usbConfig.currentDraw = ui->bMaxPower->value() / 2;

    std::u16string strings[] = {ui->manufactirerString->text().toStdU16String(),
                                ui->productString->text().toStdU16String(),
                                ui->serialNumberString->text().toStdU16String()};

    for (int i = 0; i < 3; ++i)
    {
        usbConfig.stringDescriptor[i].index = 3;
        //usbConfig.stringDescriptor[i].length =
            //static_cast<CyUsbByteField>(std::min(sizeof(char16_t) * strings[i].size(), 64ull));
        std::memcpy(usbConfig.stringDescriptor[i].string, strings[i].c_str(), usbConfig.stringDescriptor[i].length);
    }

    config.usbConfig->stringDescriptorAddress[3] = ui->useSerialNumberStr->isChecked() ? 0xFFFFFFFF : 0;

    config.bypassUSBRegulator = toBoolField(ui->vbusVoltage33V->isChecked());
    config.bypassSystemRegulator = toBoolField(ui->vdddVoltage2V->isChecked());
    config.mfgInterface = toBoolField(ui->enableManufacturing->isChecked());

    // SCB0/SCB1 tabs

    CyUsbScbConfig &scb0 = config.scb[0];

    scb0.protocol = static_cast<CY_SCB_PROTOCOL>(ui->scb0Protocol->currentData().toUInt());

    CyUsbScbConfig &scb1 = config.scb[1];
    scb1.mode = static_cast<CY_SCB_MODE>(ui->enableScb1->isChecked() ? ui->scb1Mode->currentData().toUInt() : 0);
    bool scb1enabled = static_cast<bool>(scb1.mode);

    if (scb1enabled)
    {
        scb1.protocol = static_cast<CY_SCB_PROTOCOL>(ui->scb1Protocol->currentData().toUInt());
    }
    else
    {
        scb1.protocol = CY_SCB_PROTOCOL::DISABLED;
    }
}

void MainWindow::on_scb0Mode_currentIndexChanged(int)
{
    CY_SCB_MODE scbMode = static_cast<CY_SCB_MODE>(ui->scb0Mode->currentData().toInt());
    fillComboBoxWithEnum(ui->scb0Protocol, config.scb[SCB0]->protocol, scbProtocol);
    state.updateUsedScbGPIOs(SCB0, scbMode, config.scb[SCB0]->modeConfiguration->uart.uartType);
}

void MainWindow::on_scb1Mode_currentIndexChanged(int)
{
    CY_SCB_MODE scbMode = static_cast<CY_SCB_MODE>(ui->scb1Mode->currentData().toInt());
    fillComboBoxWithEnum(ui->scb1Protocol, config.scb[SCB1]->protocol, scbProtocol);
    state.updateUsedScbGPIOs(SCB1, scbMode, config.scb[SCB1]->modeConfiguration->uart.uartType);
}

void MainWindow::showScbModeConfigDlg(ScbNum scbNum)
{
    QComboBox *modes[] = {ui->scb0Mode, ui->scb1Mode};
    CY_SCB_MODE scbMode = static_cast<CY_SCB_MODE>(modes[scbNum]->currentData().toInt());

    switch (scbMode)
    {
    case CY_SCB_MODE::UART:
        UartConfigureDlg(state, scbNum, this).exec();
        break;

    case CY_SCB_MODE::SPI:
        SpiConfigDlg(state, scbNum, this).exec();
        break;

    case CY_SCB_MODE::I2C:
        I2CConfigDlg(state, scbNum, this).exec();
        break;
    }
}

void MainWindow::on_configureScb0Mode_pressed()
{
    showScbModeConfigDlg(SCB0);
}

void MainWindow::on_configureScb1Mode_pressed()
{
    showScbModeConfigDlg(SCB1);
}

void MainWindow::on_remoteWakeupConfigBtn_pressed()
{
    RemoteWakeupDlg(state, this).exec();
}

void MainWindow::showScbLedConfigDlg(ScbNum scbNum)
{
    ScbLedConfigDlg(state, scbNum, this).exec();
}

void MainWindow::on_configScb0LedBtn_pressed()
{
    showScbLedConfigDlg(SCB0);
}

void MainWindow::on_configScb1LedBtn_pressed()
{
    showScbLedConfigDlg(SCB1);
}

void MainWindow::on_enableScb1_stateChanged(int arg1)
{
    bool scb1enabled = (arg1 == Qt::Checked);
    ui->scb1ConfigBox->setEnabled(scb1enabled);
    state.enableScb1(scb1enabled);
}

void MainWindow::on_configureCapsense_pressed()
{
    CapsenseConfigDlg(state, this).exec();
}
void MainWindow::on_actionLoad_from_file_triggered()
{
    QString file = QFileDialog::getOpenFileName(this);
    if (file != "")
    {
        try
        {
            loadFile(file);
        }
        catch (std::logic_error &le)
        {
            QMessageBox::warning(this, "Error", le.what());
        }
    }
}
void MainWindow::loadFile(QString fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        throw std::logic_error("Could not open file");
    }

    QDataStream readStream(&file);
    Padded<UsbDeviceConfig, 512> tmpConfig;

    if (static_cast<int>(sizeof(tmpConfig)) >
        readStream.readRawData(reinterpret_cast<char *>(&tmpConfig), sizeof(tmpConfig)))
    {
        throw std::logic_error("The file is too small");
    }

    if (std::memcmp(tmpConfig->signature, "CYUS", 4) != 0)
    {
        throw std::logic_error("The file format is incorrect");
    }

    if (tmpConfig->checksum != tmpConfig.value.getChecksum())
    {
        throw std::logic_error("Checksum is incorrect");
    }

    config = tmpConfig.value;
    updateMainWindowWithData();
}

void MainWindow::defaultInit()
{
    UsbDeviceConfig usbDeviceConfigDefault;

    usbDeviceConfigDefault.usbConfig->vendorId = 1204;
    usbDeviceConfigDefault.usbConfig->productId = 5;
    usbDeviceConfigDefault.usbConfig->powerMode = CY_DEVICE_POWER_MODE::BUS_POWERED;
    usbDeviceConfigDefault.usbConfig->currentDraw = 50;
    usbDeviceConfigDefault.usbConfig->stringDescriptor[0].length = 44;
    QString strManufacture("Cypress Semiconductor");
    std::memcpy(usbDeviceConfigDefault.usbConfig->stringDescriptor[0].string, strManufacture.toStdU16String().c_str(),
                usbDeviceConfigDefault.usbConfig->stringDescriptor[0].length);

    usbDeviceConfigDefault.usbConfig->stringDescriptor[1].length = 52;
    QString strProduct("USB-Serial (Dual Channel )");
    std::memcpy(usbDeviceConfigDefault.usbConfig->stringDescriptor[1].string, strProduct.toStdU16String().c_str(),
                usbDeviceConfigDefault.usbConfig->stringDescriptor[1].length);

    usbDeviceConfigDefault.gpioLogicType = CyGpioLogicType::CMOS;
    usbDeviceConfigDefault.gpioDriveType = CyGpioDriveType::Fast;
    usbDeviceConfigDefault.bypassUSBRegulator = CyUsbBoolField::FALSE;
    usbDeviceConfigDefault.bypassSystemRegulator = CyUsbBoolField::FALSE;
    usbDeviceConfigDefault.mfgInterface = CyUsbBoolField::TRUE;
    usbDeviceConfigDefault.usbConfig->remoteWakeup = 0xff;
    usbDeviceConfigDefault.usbConfig->suspend = 0xf1;
    // SCB0/SCB1
    usbDeviceConfigDefault.scb[0]->mode = CY_SCB_MODE::UART;
    usbDeviceConfigDefault.scb[0]->protocol = CY_SCB_PROTOCOL::CDC;
    usbDeviceConfigDefault.scb[1]->mode = CY_SCB_MODE::UART;
    usbDeviceConfigDefault.scb[1]->protocol = CY_SCB_PROTOCOL::CDC;

    config = usbDeviceConfigDefault;
    updateMainWindowWithData();
}

void MainWindow::saveFile(QString fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        throw std::logic_error("Could not open file");
    }

    QDataStream writeStream(&file);
    Padded<CyUsbscDeviceConfig, 512> tmpConfig;
    // std::memset(tmpConfig.payload, 0, 512);

    readMainWindowData();
    tmpConfig.value = config;
    tmpConfig->checksum = config.getChecksum();
    if (static_cast<int>(sizeof(tmpConfig)) >
        writeStream.writeRawData(reinterpret_cast<const char *>(&tmpConfig), sizeof(tmpConfig)))
    {
        throw std::logic_error("Could not write all bytes");
    }
}

void MainWindow::on_actionSave_to_file_triggered()
{
    QString file = QFileDialog::getSaveFileName(this);
    if (file != "")
    {
        try
        {
            saveFile(file);
        }
        catch (std::logic_error &le)
        {
            QMessageBox::warning(this, "Error", le.what());
        }
    }
}
void MainWindow::on_bcdConfig_pressed()
{
    BcdConfigDlg(state, this).exec();
}

void MainWindow::on_unusedGpioConfig_pressed()
{
    GpioDriveConfigDlg(state, this).exec();
}

void MainWindow::on_scb0Protocol_currentIndexChanged(int)
{
    config.scb[0]->protocol = static_cast<CY_SCB_PROTOCOL>(ui->scb0Protocol->currentData().toUInt());
    if (ui->useCypressIDs->isChecked())
    {
        config.usbConfig->productId = config.getDefaultPid();
    }
    updateMainWindowWithData();
}

void MainWindow::on_scb1Protocol_currentIndexChanged(int)
{
    config.scb[1]->protocol = static_cast<CY_SCB_PROTOCOL>(ui->scb1Protocol->currentData().toUInt());
    if (ui->useCypressIDs->isChecked())
    {
        config.usbConfig->productId = config.getDefaultPid();
    }
    updateMainWindowWithData();
}

void MainWindow::on_actionLoad_from_device_triggered()
{
    CyDevice::ConfigBlob cfg;
    DevicePicker picker(cfg, false);
    if (QDialog::Accepted == picker.exec())
    {
        memcpy(&config, cfg.data(), cfg.size()); // maybe verify?
        updateMainWindowWithData();
    }
}

void MainWindow::on_actionSave_to_device_triggered()
{
    Padded<CyUsbscDeviceConfig, 512> tmpConfig;
    memset(&tmpConfig, 0, sizeof(tmpConfig));

    readMainWindowData();
    tmpConfig.value = config;
    tmpConfig->checksum = config.getChecksum();

    CyDevice::ConfigBlob cfg(sizeof(tmpConfig));
    memcpy(cfg.data(), &tmpConfig, sizeof(tmpConfig));

    DevicePicker picker(cfg, true);
    picker.exec();
}
