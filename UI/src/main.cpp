#include <iostream>
#include <origin_ui/mainwindow.h>
#include <simplified_ui/usbserialconfiguration.h>

#include <QApplication>

int main(int argc, char *argv[]) // USBSerialConfiguration --form[-f] [simple|origin]
{
    QApplication a(argc, argv);

    if(argc < 2)
    {
        UsbSerialConfiguration usbConfiguration;
        usbConfiguration.show();
        return a.exec();
    }
    if(argc == 2)
    {
        std::cout << "undefined UI to run !  Please, enter next command: --form [simple/origin] ";
    }

    if(argc == 3)
    {
        if(std::string(argv[1]) == "--form")
        {
            if(std::string(argv[2]) == "simple")
            {
                UsbSerialConfiguration usbConfiguration;
                usbConfiguration.show();
                return a.exec();
            }
            else if(std::string(argv[2]) == "origin")
            {
                MainWindow w;
                w.show();
                return a.exec();
            }
            else
            {
                std::cout << "undefined UI to run !  Please, enter next command: --form [simple/origin] ";
            }
        }
        else
        {
            std::cout << " wrong command! Please, enter next command: --form [simple/origin]";
        }
    }
}
