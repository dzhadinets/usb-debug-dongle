How do I make libusb work as non-root.
When launching the application as a user example: $ ./myapp libusb_open () does not work and the LIBUSB_ERROR_ACCESS error is generated.

For libusb to work as a non-root user, you need:
1. Create file 100-cy7_flasher.rules. Such a file is located in data/
 SUBSYSTEM == "usb", ATTRS {idVendor} == "VID", ATTRS {idProduct} == "PID", MODE = "0666"
    • VID is the USB-IF-assigned Vendor ID of the device in question
    • PID is the Vendor-assigned Product ID of the device in question
    • 0666 gives universal read / write access to whatever matches this line

2. Copy it to the /etc/udev/rules.d folder:
	sudo cp -v data/100-cy7_flasher.rules /etc/udev/rules.d

3. Reload udev rules:
	sudo udevadm control --reload-rules

