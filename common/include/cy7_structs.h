/**
 * @file cy7_config.h
 *
 * @brief usb-debug-whistle linux kernel driver config blob
 *        structure and values.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CY7_DEVICE_STRUCTS_H
#define _CY7_DEVICE_STRUCTS_H

#ifdef _MSC_VER

#ifndef uint32_t
typedef unsigned __int32 uint32_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int8 uint8_t;
#endif

#define PACKED_STRUCT
#pragma pack(push, 1)

#define UNUSED

#else

#include <linux/types.h>
#include <linux/build_bug.h>

#define PACKED_STRUCT __attribute__((packed))
#define UNUSED __attribute__((unused))

#endif

#define CY7_SIGNATURE "CYUS"

#define CY7_DEFAULT_VENDOR_ID 0x04b4

static const UNUSED int cy7_scb_uart_speeds[] = {
	300,
	600,
	1200,
	1800,
	2400,
	4800,
	9600,
	19200,
	28800,
	38400,
	57600,
	76800,
	115200,
	230400,
	460800,
	576000,
	921600,
	1000000,
	3000000,
};
//static const int cy7_scb_uart_speeds_count = ARRAY_SIZE(cy7_scb_uart_speeds);

enum CY7_UART_TYPE {
	CY7_UART_TYPE_PIN_2 = 0,
	CY7_UART_TYPE_PIN_4 = 1,
	CY7_UART_TYPE_PIN_6 = 2,
	CY7_UART_TYPE_PIN_8 = 3,
	CY7_UART_TYPE_PIN_NUM = 4,
	CY7_UART_TYPE_PIN_MIN = CY7_UART_TYPE_PIN_2,
	CY7_UART_TYPE_PIN_MAX = CY7_UART_TYPE_PIN_8,
};

static const UNUSED int cy7_scb_uart_types[] = {
	CY7_UART_TYPE_PIN_2,
	CY7_UART_TYPE_PIN_4,
	CY7_UART_TYPE_PIN_6,
//    CY7_UART_TYPE_PIN_8, ///supported??
};

static const UNUSED char *cy7_scb_uart_type_to_str(int val)
{
	switch (val) {
	case CY7_UART_TYPE_PIN_2:
		return "2pin";
	case CY7_UART_TYPE_PIN_4:
		return "4pin";
	case CY7_UART_TYPE_PIN_6:
		return "6pin";
	case CY7_UART_TYPE_PIN_8:
		return "8pin";
	default:
		return "";
	}
}

enum CY7_UART_DATA_WIDTH {
	CY7_UART_DATA_WIDTH_7 = 7,
	CY7_UART_DATA_WIDTH_8 = 8,
	CY7_UART_DATA_WIDTH_NUM = 2,
	CY7_UART_DATA_WIDTH_MIN = CY7_UART_DATA_WIDTH_7,
	CY7_UART_DATA_WIDTH_MAX = CY7_UART_DATA_WIDTH_8,
};

static const UNUSED int cy7_scb_uart_data_widths[] = {
    CY7_UART_DATA_WIDTH_7,
    CY7_UART_DATA_WIDTH_8,
};

static const UNUSED char *cy7_scb_uart_data_width_to_str(int val)
{
	switch (val) {
	case CY7_UART_DATA_WIDTH_7:
		return "7bit";
	case CY7_UART_DATA_WIDTH_8:
		return "8bit";
	default:
		return "";
	}
}

enum CY7_UART_STOP_BIT {
	CY7_UART_STOP_BITS_1 = 1,
	CY7_UART_STOP_BITS_2 = 2,
	CY7_UART_STOP_BITS_NUM = 2,
	CY7_UART_STOP_BITS_MIN = CY7_UART_STOP_BITS_1,
	CY7_UART_STOP_BITS_MAX = CY7_UART_STOP_BITS_2,
};

static const UNUSED int cy7_scb_uart_stop_bits[] = {
    CY7_UART_STOP_BITS_1,
    CY7_UART_STOP_BITS_2,
};

static const UNUSED char *cy7_scb_uart_stop_bits_to_str(int val)
{
	switch (val) {
	case CY7_UART_STOP_BITS_1:
		return "1bit";
	case CY7_UART_STOP_BITS_2:
		return "2bit";
	default:
		return "";
	}
}

enum CY7_UART_PROTOCOL {
	CY7_UART_MODE_STANDARD = 0,
	CY7_UART_MODE_SMART_CARD = 1,
	CY7_UART_MODE_IRDA = 2,
	CY7_UART_MODE_NUM = 3,
	CY7_UART_MODE_MIN = CY7_UART_MODE_STANDARD,
	CY7_UART_MODE_MAX = CY7_UART_MODE_IRDA,
};

static UNUSED int cy7_scb_uart_protocols[] = {
    CY7_UART_MODE_STANDARD,
    CY7_UART_MODE_SMART_CARD,
    CY7_UART_MODE_IRDA,
};

static const UNUSED char *cy7_scb_uart_protocol_to_str(int val)
{
	switch (val) {
	case CY7_UART_MODE_STANDARD:
		return "standart";
	case CY7_UART_MODE_SMART_CARD:
		return "smartcard";
	case CY7_UART_MODE_IRDA:
		return "irda";
	default:
		return "";
	}
}

enum CY7_UART_PARITY {
	CY7_UART_PARITY_NONE  = 0,
	CY7_UART_PARITY_ODD   = 1,
	CY7_UART_PARITY_EVEN  = 2,
	CY7_UART_PARITY_MARK  = 3,
	CY7_UART_PARITY_SPACE = 4,
	CY7_UART_PARITY_NUM = 5,
	CY7_UART_PARITY_MIN = CY7_UART_PARITY_NONE,
	CY7_UART_PARITY_MAX = CY7_UART_PARITY_SPACE,
};

static UNUSED int cy7_scb_uart_paritys[] = {
	CY7_UART_PARITY_NONE,
	CY7_UART_PARITY_ODD,
	CY7_UART_PARITY_EVEN,
	CY7_UART_PARITY_MARK,
	CY7_UART_PARITY_SPACE
};

static const UNUSED char *cy7_scb_uart_parity_to_str(int val)
{
	switch (val) {
	case CY7_UART_PARITY_NONE:
		return "none";
	case CY7_UART_PARITY_ODD:
		return "odd";
	case CY7_UART_PARITY_EVEN:
		return "even";
	case CY7_UART_PARITY_MARK:
		return "mark";
	case CY7_UART_PARITY_SPACE:
		return "space";
	default:
		return "";
	}
}

enum CY7_UART_FLOW_CONTROL {
	CY7_UART_FLOW_CONTROL_DISABLE = 0,       /*Disable Flow control*/
	CY7_UART_FLOW_CONTROL_DSR,               /*Enable DSR mode of flow control*/
	CY7_UART_FLOW_CONTROL_RTS_CTS,           /*Enable RTS CTS mode of flow control*/
	CY7_UART_FLOW_CONTROL_ALL                /*Enable RTS CTS and DSR flow control */

};

static UNUSED int cy7_scb_uart_flow_ctrls[] =
{
	CY7_UART_FLOW_CONTROL_DISABLE,
	CY7_UART_FLOW_CONTROL_DSR,
	CY7_UART_FLOW_CONTROL_RTS_CTS,
	CY7_UART_FLOW_CONTROL_ALL
};

static const UNUSED char* cy7_scb_uart_flow_ctrl_to_str(int val)
{
	switch (val) {
	case CY7_UART_FLOW_CONTROL_DISABLE:
		return "disable";
	case CY7_UART_FLOW_CONTROL_DSR:
		return "dcr";
	case CY7_UART_FLOW_CONTROL_RTS_CTS:
		return "rts_cts";
	case CY7_UART_FLOW_CONTROL_ALL:
		return "all";
	default:
		return "";
	}
}

enum CY7_UART_BIT_POSITION {
	CY7_UART_BIT_POS_LSB = 0,
	CY7_UART_BIT_POS_MSB = 1,
};

static UNUSED int cy7_scb_uart_bit_positions[] =
{
	CY7_UART_BIT_POS_LSB,
	CY7_UART_BIT_POS_MSB
};

static const UNUSED char* cy7_scb_uart_bit_position_to_str(int val)
{
	switch (val) {
	case CY7_UART_BIT_POS_LSB:
		return "LSB";
	case CY7_UART_BIT_POS_MSB:
		return "MSB";
	default:
		return "";
	}
}

#define CY7_UART_LINE_ACTIVE_NONE     0
#define CY7_UART_LINE_ACTIVE_INV_RTS (1<<0)
#define CY7_UACT_LINE_ACTIVE_INV_CTS (1<<1)
#define CY7_UACT_LINE_ACTIVE_INV_DTR (1<<2)
#define CY7_UACT_LINE_ACTIVE_INV_DSR (1<<3)
#define CY7_UACT_LINE_ACTIVE_INV_DCD (1<<4)
#define CY7_UACT_LINE_ACTIVE_INV_RI  (1<<5)

/* 16 bytes */
struct cy7_scb_config_uart {
	uint32_t uart_baud_rate;
	uint8_t uart_type;
	uint8_t uart_data_width;
	uint8_t uart_stop_bits;
	uint8_t uart_protocol;
	uint8_t uart_parity;
	uint8_t uart_bit_position;
	uint8_t uart_tx_retry_on_nack;
	uint8_t uart_rx_invert_polarity;
	uint8_t uart_drop_on_err;
	uint8_t uart_flow_ctrl;
	uint8_t uart_loopback;
	uint8_t uart_line_active_state;
} PACKED_STRUCT;


enum CY7_SPI_FREQ {
	CY7_SPI_FREQ_MIN = 1000,    /* 1 KHz */
	CY7_SPI_FREQ_DEF = 1000000,  /* 1 MHz */
	CY7_SPI_FREQ_MAX = 3000000,  /* 3 MHz */
};

enum CY7_SPI_MODE {
	CY7_SPI_MODE_SLAVE = 0,
	CY7_SPI_MODE_MASTER = 1,
};

static UNUSED int cy7_scb_spi_modes[] = {
	CY7_SPI_MODE_SLAVE,
	CY7_SPI_MODE_MASTER
};

static const UNUSED char *cy7_scb_spi_mode_to_str(int val)
{
	switch (val) {
	case CY7_SPI_MODE_MASTER:
		return "master";
	case CY7_SPI_MODE_SLAVE:
		return "slave";
	default:
		return "";
	}
}

enum CY7_SPI_PROTOCOL {
	CY7_SPI_PROTO_MOTOROLA = 0,
	CY7_SPI_PROTO_TI       = 1,
	CY7_SPI_PROTO_NS       = 2,
};

static UNUSED int cy7_scb_spi_protocols[] = {
	CY7_SPI_PROTO_MOTOROLA,
	CY7_SPI_PROTO_TI,
	CY7_SPI_PROTO_NS
};

static const UNUSED char *cy7_scb_spi_protocol_to_str(int val)
{
	switch (val) {
	case CY7_SPI_PROTO_MOTOROLA:
		return "MOTOROLA";
	case CY7_SPI_PROTO_TI:
		return "TI";
	case CY7_SPI_PROTO_NS:
		return "NATIONAL";
	default:
		return "";
	}
}

enum CY7_SPI_SSN_TOGGLE {
	CY7_SPI_SSN_TOGGLE_FRAME = 0,
	CY7_SPI_SSN_TOGGLE_CONTIGNOUS = 1,
};

static UNUSED int cy7_scb_spi_ssn_toggles[] = {
	CY7_SPI_SSN_TOGGLE_FRAME,
	CY7_SPI_SSN_TOGGLE_CONTIGNOUS
};

static const UNUSED char *cy7_scb_spi_ssn_toggles_to_str(int val)
{
	switch (val) {
	case CY7_SPI_SSN_TOGGLE_FRAME:
		return "frame";
	case CY7_SPI_SSN_TOGGLE_CONTIGNOUS:
		return "contignous";
	default:
		return "";
	}
}

enum CY7_SPI_DATA_WIDTH {
	CY7_SPI_DATA_WIDTH_MIN = 4,
	CY7_SPI_DATA_WIDTH_DEF = 8,
	CY7_SPI_DATA_WIDTH_MAX = 16,
};

enum CY7_SPI_BIT_POSITION {
	CY7_SPI_BIT_POS_LSB = 0,
	CY7_SPI_BIT_POS_MSB = 1,
};

static UNUSED int cy7_scb_spi_bit_positions[] =
{
	CY7_SPI_BIT_POS_LSB,
	CY7_SPI_BIT_POS_MSB
};

static const UNUSED char* cy7_scb_spi_bit_position_to_str(int val)
{
	switch (val) {
	case CY7_SPI_BIT_POS_LSB:
		return "LSB";
	case CY7_SPI_BIT_POS_MSB:
		return "MSB";
	default:
		return "";
	}
}

enum CY7_SPI_XFER_MODE {
	CY7_SPI_XFER_MODE_TX_RX   = 0,
	CY7_SPI_XFER_MODE_TX_ONLY = 1,
	CY7_SPI_XFER_MODE_RX_ONLY = 2,
};

static UNUSED int cy7_scb_spi_xfer_mode[] =
{
	CY7_SPI_XFER_MODE_TX_RX,
	CY7_SPI_XFER_MODE_TX_ONLY,
    CY7_SPI_XFER_MODE_RX_ONLY
};

static const UNUSED char* cy7_scb_spi_xfer_mode_to_str(int val)
{
	switch (val) {
	case CY7_SPI_XFER_MODE_TX_RX:
		return "txrx";
	case CY7_SPI_XFER_MODE_TX_ONLY:
		return "tx";
	case CY7_SPI_XFER_MODE_RX_ONLY:
		return "rx";
	default:
		return "";
	}
}

/* 16 bytes */
struct cy7_scb_config_spi {
	uint32_t spi_freq;
	uint8_t spi_data_width;
	uint8_t spi_protocol;
	uint8_t spi_transfer_mode;
	uint8_t spi_bit_position;
	uint8_t spi_is_master;
	uint8_t spi_ssn_toggle_mode;
	uint8_t spi_select_precede;
	uint8_t spi_cpha;
	uint8_t spi_cpol;
	uint8_t spi_loopback;
	uint8_t spi_padding[2];
} PACKED_STRUCT;

enum CY7_I2C_FREQ {
	CY7_I2C_FREQ_MIN = 1000,    /* 1 KHz */
	CY7_I2C_FREQ_DEF = 100000,  /* 100 KHz */
	CY7_I2C_FREQ_MAX = 400000,  /* 400 KHs */
};

enum CY7_I2C_SLAVE_ADDR {
	CY7_I2C_SLAVE_ADDRESS_MIN = 0x08,    /* 0..7 reserved for system trasaction */
	CY7_I2C_SLAVE_ADDRESS_DEF = 0xA5,
	CY7_I2C_SLAVE_ADDRESS_MAX = 0x7F,
};

enum CY7_I2C_MODE {
	CY7_I2C_MODE_SLAVE = 0,
	CY7_I2C_MODE_MASTER = 1,
};

static UNUSED int cy7_scb_i2c_modes[] = {
	CY7_I2C_MODE_SLAVE,
	CY7_I2C_MODE_MASTER
};

static const UNUSED char *cy7_scb_i2c_mode_to_str(int val)
{
	switch (val) {
	case CY7_I2C_MODE_MASTER:
		return "master";
	case CY7_I2C_MODE_SLAVE:
		return "slave";
	default:
		return "";
	}
}

enum CY7_I2C_STRETCH {
	CY7_I2C_STRETCH_NO = 0,
	CY7_I2C_STRETCH_YES = 1,
};

static UNUSED int cy7_scb_i2c_stretch[] = {
	CY7_I2C_STRETCH_NO,
	CY7_I2C_STRETCH_YES
};

static const UNUSED char *cy7_scb_i2c_stretch_to_str(int val)
{
	switch (val) {
	case CY7_I2C_STRETCH_YES:
		return "yes";
	case CY7_I2C_STRETCH_NO:
		return "no";
	default:
		return "";
	}
}

enum CY7_I2C_BIT_POSITION {
	CY7_I2C_BIT_POS_LSB = 0,
	CY7_I2C_BIT_POS_MSB = 1,
};

static UNUSED int cy7_scb_i2c_bit_positions[] = {
	CY7_I2C_BIT_POS_LSB,
	CY7_I2C_BIT_POS_MSB
};

static const UNUSED char* cy7_scb_i2c_bit_position_to_str(int val)
{
	switch (val) {
	case CY7_I2C_BIT_POS_LSB:
		return "LSB";
	case CY7_I2C_BIT_POS_MSB:
		return "MSB";
	default:
		return "";
	}
}

/* 16 bytes */
struct cy7_scb_config_i2c {
	uint32_t i2c_freq;
	uint8_t i2c_slave_address;
	uint8_t i2c_bit_position;
	uint8_t i2c_is_master;
	uint8_t i2c_signore;
	uint8_t i2c_clock_stretch;
	uint8_t i2c_loopback;
	uint8_t i2c_padding[6];
} PACKED_STRUCT;

enum UNUSED _cy7_scb_jtag_freq {
	CY7_JTAG_FREQ_MIN = 1000,    /* 1 KHz */
	CY7_JTAG_FREQ_DEF = 400000,  /* 100 KHz */
	CY7_JTAG_FREQ_MAX = 400000,  /* 400 KHz */
};

struct cy7_scb_config_jtag{
	uint32_t jtag_freq;
	uint8_t padding[12];
} PACKED_STRUCT;

struct cy7_scb_config_none{
	uint8_t padding[16];
};

enum sbc_mode{
	CY7_SCB_MODE_DISABLE = 0,
	CY7_SCB_MODE_UART    = 1,
	CY7_SCB_MODE_SPI     = 2,
	CY7_SCB_MODE_I2C     = 3,
	CY7_SCB_MODE_JTAG    = 4,
	CY7_SCB_MODE_NUM     = 5,
};

#define CY7_SCB_PROTO_DISABLED 0
#define CY7_SCB_PROTO_CDC      1
#define CY7_SCB_PROTO_PHDC     2
#define CY7_SCB_PROTO_VENDOR   3
#define CY7_SCB_PROTO_VCP      4

#define CY7_PRODUCT_ID_INVAL 0xffff

struct cy7_scb_config {
	uint8_t mode;
	uint8_t protocol;
	uint8_t padding0[2];
	uint8_t tx_led;
	uint8_t rx_led;
	uint8_t rx_tx_led;
	uint8_t padding1;
	union {
		struct cy7_scb_config_none none;
		struct cy7_scb_config_uart uart;
		struct cy7_scb_config_spi spi;
		struct cy7_scb_config_i2c i2c;
		struct cy7_scb_config_jtag jtag;
	} u;
} PACKED_STRUCT;

#define CY7_CAPSENSE_SENSITIVITY_0_1_PF 1
#define CY7_CAPSENSE_SENSITIVITY_0_2_PF 2
#define CY7_CAPSENSE_SENSITIVITY_0_3_PF 3
#define CY7_CAPSENSE_SENSITIVITY_0_4_PF 4

#define CY7_CAPSENSE_LED_NO 0
#define CY7_CAPSENSE_LED_COMMON 1
#define CY7_CAPSENSE_LED_INDIVIDUAL 2

struct cy7_capsense_button_config {
	uint8_t sensitivity;
	uint8_t sense_gpio;
	uint8_t output_led_gpio;
	uint8_t padding;
};

/* 64 bytes */
struct cy7_capsense_config {
	uint8_t enable;
	uint8_t debounce;
	uint8_t scan_period;
	uint8_t water_shield_io;
	uint8_t button_count;
	uint8_t encoded_gpio[4];
	uint8_t led_output;
	uint8_t common_led_gpio;
	struct cy7_capsense_button_config buttons[8];
	uint8_t padding[21];
} PACKED_STRUCT;

struct cy7_bcd_config {
	uint8_t gpio_bcd[2];
	uint8_t gpio_bus_detect;
	uint8_t padding;
} PACKED_STRUCT;

struct cy7_usb_lang_id_desc {
	uint8_t length;
	uint8_t index;
	uint16_t lang_id;
	uint8_t padding[62];
} PACKED_STRUCT;

struct cy7_usb_unicode_str_desc {
	uint8_t length;
	uint8_t index;
	uint16_t str[32];
} PACKED_STRUCT;

struct cy7_usb_config {
	uint8_t suspend;
	uint16_t vendor_id;
	uint16_t product_id;
	uint8_t power_mode;
	uint8_t remote_wakeup;
	uint8_t current_draw;
	uint8_t line_polarity;
	uint32_t string_descriptor_addr[4];
	struct cy7_usb_lang_id_desc string_desc0;
	struct cy7_usb_unicode_str_desc string_desc[3];
} PACKED_STRUCT;

struct cy7_bcd_drive_config {
	uint8_t unconfigure_sdp;
	uint8_t configure_sdp;
	uint8_t dcp_charging;
	uint8_t suspend_or_fail;
} PACKED_STRUCT;

struct cy7_uart_rs485_config {
	uint8_t rs485_enable;
	uint8_t rs485_gpio;
} PACKED_STRUCT;

#define CY7_POWER_MODE_BUS_POWERED 0
#define CY7_POWER_MODE_SELF_POWERED 1

#define CY7_WAKEUP_SRC_NONE      0
#define CY7_WAKEUP_SRC_GPIO      (1<<0)
#define CY7_WAKEUP_SRC_I2C_SLAVE (1<<1)
#define CY7_WAKEUP_SRC_SPI_SLAVE (1<<2)
#define CY7_WAKEUP_SRC_CAPSENSE  (1<<3)

#define CY7_POLARITY_NONE                 0
#define CY7_POLARITY_INVERT_SUSPEND       1
#define CY7_POLARITY_INVERT_REMOTE_WAKEUP 2
#define CY7_POLARITY_INVERT_BOTH          3

#define CY7_BCD_DRIVE_NONE      0
#define CY7_BCD_DRIVE_BCD0_HIGH 1
#define CY7_BCD_DRIVE_BCD1_HIGH 16

#define CY7_GPIO_LOGIC_TYPE_CMOS  0
#define CY7_GPIO_LOGIC_TYPE_LVTTL 1

#define CY7_GPIO_DRIVE_TYPE_FAST 0
#define CY7_GPIO_DRIVE_TYPE_SLOW 1

struct cy7_device_config {
	char signature[4];
	uint32_t firmware_ver;
	uint32_t checksum;
	uint32_t firmware_entry;
	/* +0x10 */
	uint32_t firmware_image_start;
	uint32_t firmware_image_size;
	uint32_t firmware_image_checksum;
	/* +0x1c */
	struct cy7_scb_config scb[2];
	struct cy7_capsense_config capsence;
	struct cy7_bcd_config bcd;
	uint8_t bypass_usb_reg;
	uint8_t bypass_system_reg;
	uint8_t mfg_interface;
	struct cy7_usb_config usb;
	uint32_t gpio_drive0;
	uint32_t gpio_drive1;
	uint32_t gpio_input;
	struct cy7_bcd_drive_config bcd_drive;
	uint8_t scb_config;
	uint8_t power_gpio_enable;
	uint8_t power_gpio;
	uint8_t gpio_logic_type;
	uint8_t gpio_drive_type;
	uint8_t res_uart_pullup_disable;
	uint8_t scb_suspend_uart_control[2];
	struct cy7_uart_rs485_config scb_uart_rs485[2];
	uint8_t padding[48];
} PACKED_STRUCT;


#ifdef _MSC_VER // Windows

#undef PACKED_STRUCT
#pragma pack(pop)

#else // Linux

#include <linux/version.h>
#if (LINUX_VERSION_CODE >= KERNEL_VERSION(5, 1, 0))

#define COMPILE_TIME_SIZE_CHECK(__struct, __size) \
	static_assert(sizeof(__struct) == __size, \
	"'" #__struct "' should be " #__size "bytes")

#define COMPILE_TIME_OFFSET_CHECK(__struct, __field, __off) \
	static_assert((size_t)(&((__struct *)0)->__field) == __off, \
	"'" #__struct "." #__field "' offset should be " #__off)

COMPILE_TIME_SIZE_CHECK(struct cy7_scb_config, 24);
COMPILE_TIME_SIZE_CHECK(struct cy7_capsense_config, 64);
COMPILE_TIME_SIZE_CHECK(struct cy7_usb_lang_id_desc, 66);
COMPILE_TIME_SIZE_CHECK(struct cy7_usb_unicode_str_desc, 66);
COMPILE_TIME_SIZE_CHECK(struct cy7_usb_config, 289);
COMPILE_TIME_SIZE_CHECK(struct cy7_device_config, 512);
COMPILE_TIME_OFFSET_CHECK(struct cy7_device_config, padding, 464);
#endif

#endif

#endif /* _CY7_DEVICE_CONFIG_H */
