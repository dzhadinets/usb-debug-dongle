/**
 * @file cypressdefs.h
 *
 * @brief usb-debug-whistle linux kernel driver main cypress
 *        API values and types.
 *
 * Developed by Softeq Development Corporation.
 * http://www.softeq.com
 */
#ifndef _CYPRESSDEFS_H_
#define _CYPRESSDEFS_H_

enum CY_VENDOR_CMDS {
	/*
	 * Get the version of the boot-loader.
	 * value = 0, index = 0, length = 4;
	 * data_in = 32 bit version.
	 */
	CY_GET_VERSION_CMD = 0xB0,
	/*
	 * Get the signature of the firmware
	 * It is suppose to be 'CYUS' for normal firmware
	 * and 'CYBL' for Bootloader.
	 */
	CY_GET_SIGNATURE_CMD = 0xBD,
	/*
	 * Read configuration from the devices memory.
	 * Works only in mainainance mode.
	 */
	CY_READ_CONFIG_CMD = 0xB5,
	/*
	 * Write configuration to the devices memory.
	 * Works only in mainainance mode.
	 */
	CY_WRITE_CONFIG_CMD = 0xB6,
	/*
	 * Retrieve the 16 byte UART configuration information.
	 * MS bit of value indicates the SCB index.
	 * length = 16, data_in = 16 byte configuration.
	 */
	CY_UART_GET_CONFIG_CMD = 0xC0,
	/*
	 * Update the 16 byte UART configuration information.
	 * MS bit of value indicates the SCB index.
	 * length = 16, data_out = 16 byte configuration information.
	 */
	CY_UART_SET_CONFIG_CMD,
	/*
	 * Retrieve the 16 byte SPI configuration information.
	 * MS bit of value indicates the SCB index.
	 * length = 16, data_in = 16 byte configuration.
	 */
	CY_SPI_GET_CONFIG_CMD,
	/*
	 * Update the 16 byte SPI configuration information.
	 * MS bit of value indicates the SCB index.
	 * length = 16, data_out = 16 byte configuration information.
	 */
	CY_SPI_SET_CONFIG_CMD,
	/*
	 * Retrieve the 16 byte I2C configuration information.
	 * MS bit of value indicates the SCB index.
	 * length = 16, data_in = 16 byte configuration.
	 */
	CY_I2C_GET_CONFIG_CMD,
	/*
	 * Update the 16 byte I2C configuration information.
	 * MS bit of value indicates the SCB index.
	 * length = 16, data_out = 16 byte configuration information.
	 */
	CY_I2C_SET_CONFIG_CMD = 0xC5,
	/*
	 * Perform I2C write operation.
	 * value = bit0 - start, bit1 - stop, bit3 - start on idle,
	 * bits[14:8] - slave address, bit15 - scbIndex. length = 0. The
	 * data is provided over the bulk endpoints.
	 */
	CY_I2C_WRITE_CMD,
	/*
	 * Perform I2C read operation.
	 * value = bit0 - start, bit1 - stop, bit2 - Nak last byte,
	 * bit3 - start on idle, bits[14:8] - slave address, bit15 - scbIndex,
	 * length = 0.  The data is provided over the bulk endpoints.
	 */
	CY_I2C_READ_CMD,
	/*
	 * Retrieve the I2C bus status.
	 * value = bit0 - 0: TX 1: RX, bit15 - scbIndex, length = 3,
	 * data_in = byte0: bit0 - flag, bit1 - bus_state, bit2 - SDA state,
	 * bit3 - TX underflow, bit4 - arbitration error, bit5 - NAK
	 * bit6 - bus error,
	 * byte[2:1] Data count remaining.
	 */
	CY_I2C_GET_STATUS_CMD,
	/*
	 * The command cleans up the I2C state machine and frees the bus.
	 * value = bit0 - 0: TX path, 1: RX path; bit15 - scbIndex,
	 * length = 0.
	 */
	CY_I2C_RESET_CMD,
	/*
	 * The command starts a read / write operation at SPI.
	 * value = bit 0 - RX enable, bit 1 - TX enable, bit 15 - scbIndex;
	 * index = length of transfer.
	 */
	CY_SPI_READ_WRITE_CMD = 0xCA,
	/*
	 * The command resets the SPI pipes and allows it to receive new
	 * request.
	 * value = bit 15 - scbIndex
	 */
	CY_SPI_RESET_CMD,
	/*
	 * The command returns the current transfer status. The count will match
	 * the TX pipe status at SPI end. For completion of read, read all data
	 * at the USB end signifies the end of transfer.
	 * value = bit 15 - scbIndex
	 */
	CY_SPI_GET_STATUS_CMD,
	/* Enable JTAG module */
	CY_JTAG_ENABLE_CMD = 0xD0,
	/* Disable JTAG module */
	CY_JTAG_DISABLE_CMD,
	/* JTAG read vendor command */
	CY_JTAG_READ_CMD,
	/* JTAG write vendor command */
	CY_JTAG_WRITE_CMD,
	/* Get the GPIO configuration */
	CY_GPIO_GET_CONFIG_CMD = 0xD8,
	/* Set the GPIO configuration */
	CY_GPIO_SET_CONFIG_CMD,
	/* Get GPIO value */
	CY_GPIO_GET_VALUE_CMD,
	/* Set the GPIO value */
	CY_GPIO_SET_VALUE_CMD,
	/*
	 * Program user flash area. The total space available is 512 bytes.
	 * This can be accessed by the user from USB. The flash area
	 * address offset is from 0x0000 to 0x00200 and can be written to
	 * page wise (128 byte).
	 */
	CY_PROG_USER_FLASH_CMD = 0xE0,
	/*
	 * Read user flash area. The total space available is 512 bytes.
	 * This can be accessed by the user from USB. The flash area
	 * address offset is from 0x0000 to 0x00200 and can be written to
	 * page wise (128 byte).
	 */
	CY_READ_USER_FLASH_CMD,
	/* Set / unset maintainance mode on a device */
	CY_MAINTAINANCE_MODE_CMD = 0xE2,
	/* Performs a device reset from firmware*/
	CY_DEVICE_RESET_CMD = 0xE3,
};

#define CY_MAINTAINANCE_MODE_WVALUE       0xa6bc
#define CY_MAINTAINANCE_MODE_SET_WINDEX   0xb1b0
#define CY_MAINTAINANCE_MODE_UNSET_WINDEX 0xb9b0

#define CY_DEVICE_RESET_WVALUE            0xA6B6
#define CY_DEVICE_RESET_WINDEX            0xADBA
#define CY_I2C_ERROR_BIT (1)
#define CY_I2C_ARBITRATION_ERROR_BIT (1 << 1)
#define CY_I2C_NAK_ERROR_BIT (1 << 2)
#define CY_I2C_BUS_ERROR_BIT (1 << 3)
#define CY_I2C_STOP_BIT_ERROR (1 << 4)
#define CY_I2C_BUS_BUSY_ERROR (1 << 5)

#define CY_I2C_MODE_WRITE 1
#define CY_I2C_MODE_READ 0

struct cy7_spi_config {
	uint32_t frequency;
	uint8_t dataWidth;
	uint8_t mode;
	uint8_t xferMode;
	uint8_t isMsbFirst;
	uint8_t isMaster;
	uint8_t isContinuous;
	uint8_t isSelectPrecede;
	uint8_t cpha;
	uint8_t cpol;
	uint8_t isLoopback;
	uint8_t reserved[2];
} __packed;

#define CY_SPI_READ_BIT (1)
#define CY_SPI_WRITE_BIT (1 << 1)

#endif /* _CYPRESSDEFS_H_ */
